+++
date = "2014-09-17T18:00:00-18:00"
title = "Quais os erros do islamismo?"
categories = ["Apologetica","Religioes Nao Cristas","Islamismo"]
path = "Islamismo"
featured_image = "posts/images/quais-os-erros-do-islamismo.jpg"
+++

Os islamismo é famoso pela revelação feita por Allah à Maomé através do Anjo Gabriel. Este movimento religioso pretende restaurar o cristianismo e o judaísmo às suas origens e destruir as invenções sobre o profeta Jesus e Moisés.

<!--more-->

Esta mentalidade leva a que seu próprio Alcorão reconheça a autoridade divina do Evangelho oferencendo ele como regra de fé:

>E depois deles (profetas [do Antigo Testamento]), enviamos Jesus, filho de Maria, corroborando a Tora que o precedeu; e lhe concedemos o Evangelho, que encerra orientação e luz, corroborante do que foi revelado na Tora e exortação para os tementes. Que os adeptos do Evangelho julguem segundo o que Deus nele revelou, porque aqueles que não julgarem conforme o que Deus revelou serão depravados. <cite>Alcorão 5:46</cite>

Porém, na prática ocorre outra coisa, imitando a mentalidade da Sola Scriptura, segundo a qual somente é válido como revelação aquilo que está explicitamente escrito e descartado toda e qualquer interpretação, mesmo que seja coerente.

Ora, como não existia uma interpretação do Evangelho na Igreja de Cristo cuja síntese estivesse de acordo com a opinião de Maomé, então foi necessário acusar os cristãos de interpretarem mal as próprias Escrituras.

Ademais, seus sectários, com o tempo, percebendo uma contradição entre a afirmação de que o Evangelho provinha de Deus ao mesmo tempo que atestava doutrinas cristãs, surgiu a invenção de que o Evangelho foi adulterado por gente mentirosa, de modo que somente Maomé sabia disso mais de 600 anos depois devido a uma revelação divina.

Portanto, faz-se necessário perceber, primeiro, que este argumento contradiz o próprio Maomé, para que então seja possível julgar sua doutrina original.

O argumento de adulteração do Evangelho nunca foi citado ou postulado por nenhum pensador islâmico sério durante 400 anos (entre o ano 600 e o ano 1000).

Com efeito, foi Ibn-Khazem, que morreu em 1064 em Córdoba, que foi o primeiro a incorporar este argumento ao islamismo.

Assim, este argumento não tem fundamento em Maomé, nem no Alcorão. Portanto, trata-se de uma invenção de Ibn-Khazem.

Alguns poderia pensar que este argumento tem algum fundamento.

Porém, deve-se notar que só é possível demonstrar que algo foi adulterado quando se tem acesso aos originais.

Ora, os próprios mulçumanos dizem que os manuscritos originais das Escrituras se perderam, restando apenas cópias adulteradas.

Assim, a acusação não fundamenta-se na realidade, mas na manifesta invenção dos sectários do Alcorão.

Resolvida esta questão, deve-se considerar qual o verdadeiro erro de Maomé.

Encontramos no profeta de Allah um brado de ódio contra a doutrina da Santíssima Trindade.

Com efeito, o profeta diz que os cristãos inventaram esta doutrina alegando que se fundamentaram numa falsa interpretação das Sagradas Escrituras, que sempre afirmaram que Deus é Uno:

>São blasfemos aqueles que dizem: Deus é um da Trindade!, portanto não existe divindade alguma além do Deus Único. Se não desistirem de tudo quanto afirmam, um doloroso castigo açoitará os incrédulos entre eles. <cite>Alcorão 5:73</cite>

Ora, com a negação da Trindade, segue-se, como consequência necessária, a rejeição da divindade de Cristo:

>São blasfemos aqueles que dizem: Deus é o Messias, filho de Maria, ainda quando o mesmo Messias disse: Ó israelitas, adorai a Deus, Que é meu Senhor e vosso. A quem atribuir parceiros a Deus, ser-lhe-á vedada a entrada no Paraíso e sua morada será o fogo infernal! Os iníquos jamais terão socorredores. <cite>Alcorão 5:72</cite>

Por estas palavras, pode-se perceber que Maomé não entendeu o que ensina a Igreja e também não entendeu em que consistia a Trindade, coisa que qualquer criança da catequese, se bem ensinada, sabe.

Com efeito, a doutrina da Trindade nunca afirmou que Deus é uma das três Pessoas, mas sim que o Deus único consiste nas Três Pessoas sem separação e sem confusão formando um único Deus, assim como nossa pessoa forma um so ser humano junto com nossa inteligência e nossa vontade.

Ora, as Pessoas em Deus não multiplicam a divindade, mas significam relações que existem na sua própria intimidade.

Assim, é manifesto que Maomé não conseguiu entender absolutamente nada do que os cristãos diziam sobre o tema.

Ora, que Maomé cometesse um erro tão básico é possível, mas que o "Deus Onisciente" que lhe fez a revelação tivesse se enganado sobre o conteúdo do Credo Apostólico e dos escritos cristãos é, por si só, uma demonstração de uma revelação que não poderia proceder de Deus.

Ademais, o evangelista Lucas narra muitos fatos ocultos da infância de Cristo que somente seus pais vivenciaram, demonstrando que seu Evangelho recebeu informações da própria Virgem Maria.

Assim, conclui-se que Maria Santíssima ainda era vida enquanto os Apóstolos se empenhavam em pregar a doutrina da Santíssima Trindade.

Deve-se notar que encontramos no Alcorão a seguinte doutrina:

>O Messias, filho de Maria, não é mais do que um mensageiro, do nível dos mensageiros que o precederam; e sua mãe era sinceríssima. Ambos se sustentavam de alimentos terrenos, como todos. Observa como lhes elucidamos os versículos e observa como se desviam. <cite>Alcorão 5:75</cite>

Ora, segundo Maomé, a Mãe de Cristo é "sinceríssima", isto é, dotada de um testemunho indiscutível. Se ela viu os Apóstolos pregando a Trindade e isto era uma heresia, então, porque ela não falou nada? Por que ela aceitou ser chamada de "Mãe do Senhor" por Isabel?

É absurdo que Maria tenha observado a heresia propagar-se sem se opôr e que não restasse um grupo de sectários que desmentisse os Apóstolos.

Ademais, deve-se considerar o modo como Maomé recebeu a revelação do Alcorão.

Conta-se que Maomé recebeu um anúncio do Anjo Gabriel.

Ora, o fato de receber a visão de uma anjo não demonstra qualquer autoridade divina sobre tal revelação, pois os demônios também são anjos e podem aparecer aos homens.

Portanto, o fato da revelação proceder deum anjo apenas demonstra que poderia ser um demônio travestido de anjo de luz, como adverte as Sagradas Escrituras:

>Mas, ainda que alguém - nós ou um anjo baixado do céu - vos anunciasse um evangelho diferente do que vos temos anunciado, que ele seja anátema. Repito aqui o que acabamos de dizer: se alguém pregar doutrina diferente da que recebestes, seja ele excomungado! <cite>Epístola aos Gálatas 1, 8s</cite>

Ademais, deve-se notar que o Alcorão foi sendo mudado por seu autor conforme a conveniência.

Com efeito, foram substituidos mandamentos anteriores por novos até que se passou de uma seita espiritual para uma seita que tinha a violência como bandeira.

Esta mudança tem o nome de "ab-rogação", coisa que o próprio autor do Alcorão admite que fez e defende:

>Quando Nós substituímos uma revelação por outra – e Alá sabe melhor o que Ele revela (em estágios) – eles dizem, “você não passa de um forjador”: mas a maioria deles não entende. <cite>Alcorão 16:101</cite>

De fato, o islamismo entende que Deus não tem obrigação de ser coerente consigo mesmo. Assim, poderia decretar que a prostituição, que antes era imoral, agora seria moral e que todos deveriam praticá-la:

>Alá apaga ou confirma o que Lhe agrada; com Ele está a mãe do Livro. <cite>Alcorão 13:39</cite>

Deve-se notar que mesmo quando as leis mais novas ab-rogam as antigas, as duas podem permanecer no Alcorão, embora uma delas não tenha mais valor.

Isto faz a maioria das pessoas não percebam o que Alcorão realmente diz.

Com efeito, segundo a ordem cronológica do Alcorão, o famoso "verso da espada" ab-rogou os versos pacíficos anteriores e não há outro verso posterior que ab-rogue este. Assim, este verso é válido para qualquer mulçumano que queira seguir o Alcorão coerentemente:

>Então, quando os meses sagrados (1º, 7º, 11º e 12º meses do calendário islâmico) passarem, então mate os Mushrikun [infiéis] onde quer que você os encontre e capture-os e cerque-os e prepare para eles para todas as emboscadas. Mas se eles se arrependerem e executarem As-Salat (Iqamat-as-Salat [as orações rituais islâmicas]), e darem Zakat [esmolas], então deixe o seu caminho livre. Em verdade, Allah é Indulgente, Misericordiosíssimo. <cite>Alcorão 9:5</cite>

Portanto, não há incoerência nenhuma que membros do Estado Islâmico façam tais violências em nome de Alá, pois estão sendo coerentes com uma interpretação fiel do Alcorão segundo os próprios princípios estabelecidos pelo profeta.

Assim, fica manifesto a quantidade de incoerências e contradições existentes no livro sagrado do Islamismo, além de uma legitimação de uma violência injusta.

Que o Espírito Santo ilumine os infiéis muçulmanos para que se convertam e espalhem sobre a terra aquela paz espiritual que somente Cristo, Deus Verdadeiro de Deus Verdadeiro, pode nos dar.