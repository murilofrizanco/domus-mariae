+++
date = "2014-11-06T18:00:00-18:00"
title = "Jesus pertencia à descendência de Davi?"
categories = ["Apologetica","Religioes Nao Cristas"]
path = "Judaísmo"
featured_image = "posts/images/jesus-pertencia-a-descendencia-de-davi.jpg"
+++

As Sagradas Escrituras não fazem uma descrição da árvore genealógica de Maria, mas apenas da genealogia de José. Isso leva alguns a questionarem se Jesus é realmente descendente de Davi.

<!--more-->

Com efeito, constatamos a descendência de Jesus em dois dos Evangelhos:

>Livro da origem de Jesus Cristo, filho de Davi, filho de Abraão: Abraão gerou Isaac, Isaac gerou Jacó, Jacó gerou Judá e seus irmãos, (...) Jessé gerou o rei Davi. Davi gerou Salomão, daquela que foi mulher de Urias, (...) Matã gerou Jacó, Jacó gerou José, o esposo de Maria, da qual nasceu Jesus chamado Cristo. <cite>Evangelho segundo São Mateus 1, 1-16</cite>

>Ao iniciar o ministério, Jesus tinha mais ou menos trinta anos e era, conforme se supunha, filho de José, filho de Eli, filho de Matat, (...) filho de Natã, filho de Davi, filho de Jessé, (...) filho de Adão, filho de Deus. <cite>Evangelho segundo São Lucas 3, 23-38</cite>

Estas duas passagens apresentam uma dificuldade. Mateus afirma que José foi gerado por Jacó, enquanto que Lucas afirma que José é filho de Eli, aparentando uma contradição.

Porém, deve-se notar que isto decorre das leis judaicas do Antigo Testamento.

A lei ordenava que quando uma mulher perdia seu marido e não possuía descendentes, o irmão deveria unir-se à cunhada para permitir que a descendência daquela família continuasse a existir (cf. Dt 25, 5-10).

Baseado neste conhecimento, Beda explica as diferenças na genealogia de Cristo:

>Jacó, tomando por mandato da lei a mulher de seu irmão Eli, morto sem deixar filhos, gerou a José, filho seu segundo a natureza, mas, segundo a lei, filho de Eli. <cite>São Beda</cite>

Assim, Jacó era pai de José segundo a carne, enquanto Eli era pai segundo a lei:

>Quando São Mateus diz: "Abraão gerou Isaac, Isaac gerou Jacó" e continuando com esta palavra gerou até que diz no último: "Jacó gerou José", claramente expressa que fala daquela paternidade e daquela origem pela qual José foi gerado, não adotado. E mesmo quando São Lucas disse que São José descendia de Eli, nem ainda assim devemos nos perturbar com a frase, porque pode muito bem dizer-se que o que adota um filho o gera, não segundo a carne, senão segundo a caridade. <cite>Santo Agostinho, das questões do Novo e Velho Testamento 2, 3</cite>

Assim, é manifesto que José é um descendente da linhagem de Davi.

Se é um filho adotado é tido como descendente pelas Escrituras, isto significa que Cristo é realmente descendente de Davi segundo a Lei, cumprindo a professia, pois não poderia ser o Messias se não descendesse de Davi.

Porém, deve-se notar que isto não significa que a Virgem não fosse da mesma linhagem.

>Como se descreve a genealogia de José ao invés da de Maria (sendo que Maria havia gerado Jesus por obra do Espírito Santo e São José não tem parte na geração do Senhor)? Poderíamos duvidar sobre isto se as Sagradas Escrituras não nos ensinassem a preferência que sempre dá à genealogia do marido, e especialmente aqui em que a genealogia de José e de Maria vem a ser uma só, porque sendo José um varão justo, tomou certamente mulher de sua própria tribo e de sua própria pátria. E assim no tempo do célebre recenseamento, subiu São José, da casa e da família de Davi, para recensear-se com sua esposa Maria. Esta que descende da mesma família e da mesma pátria vem recensear-se e dá a entender de uma maneira clara que pertence a mesma tribo e mesma família, de quem descende seu esposo. <cite>Santo Ambrósio, Sobre Lucas, 3</cite>

Assim, duas são as razões pela quais a descendência descrita como de José também pertence à Maria Santíssima.

Primeiro. O recensiamento era feito na terra natal. Ora, Maria recenseou-se no mesmo local que José, o que só poderia ocorrer se ela tivesse uma origem comum com seu esposo. Portanto, ela pertence à mesma tribo e pátria.

Segundo. Os judeus mais justos sempre tinham o costume de tomar por esposas mulheres de sua própria família e povo. Ora, José seguiu bom costume e tomou Maria por esposa. Portanto, ela pertence à mesma tribo e pátria.

Assim, Maria é verdadeiramente descendente de Davi segundo a carne, assim como José.

Portanto, Jesus é descendente do Rei Davi tanto segundo a carne, por parte de Maria, como segundo a Lei, por parte de José.

Por isso, é legítimo o louvor prestado por Zacarias:

>Zacarias, seu pai, repleto do Espírito Santo, profetizou: Bendito seja o Senhor Deus de Israel, porque visitou e redimiu o seu povo, e suscitou-nos uma força de salvação na casa de Davi, seu servo, como prometera desde tempos remotos pela boca de seus santos profetas. <cite>Evangelho segundo São Lucas 1, 67-70</cite>

Que com um estudo profundo das Escrituras possamos reconhecer o erro e passar à submissão à Nosso Senhor Jesus Cristo, Filho de Deus e Senhor do Universo, para que possamos alcançar as delícias e mistérios reservados para os que O amam.