+++
date = "2015-05-11T18:00:00-18:00"
title = "Os sinais da verdadeira religião"
categories = ["Apologetica","Religioes Nao Cristas"]
path = "Paganismo"
featured_image = "posts/images/os-sinais-da-verdadeira-religiao.jpg"
+++

Parece impossível discernir qual religião é falsa e qual é a verdadeira, nos levando a concluir que o debate religioso deve ser descartado de modo que devamos aceitar todas como iguais.

<!--more-->

Porém, deve-se notar que a verdadeira religião possui um conjunto de características únicas que, quando reunidos em uma só, permitem comprovar sua veracidade.

Para tanto, recorremos novamente a síntese feita pelo Doutor Angélico, autor de um trabalho sistemático e preciso sobre este ponto, concluindo que sao oito sinais que identificam a verdadeira religião.

Primeor. Deve ser revelada diretamente por Deus.

>a) Os segredos da sabedoria divina, ela mesma - que conhece tudo perfeitamente - dignou-se revelar aos homens, mostrando-lhes sua presença, a verdade de sua doutrina, e inspirando-os, com testemunhos condizentes. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, como o conhecimento divino excede a capacidade humana. Portanto, faz-se necessário que o próprio Deus revele pessoalmente seus mistérios aos homens.

Com efeito, não poderia haver maior garantia do que o próprio Deus fazer-se homem, nascer de uma Virgem e ensinar seus mistérior diretamente a nós. Ademais, para que uma doutrina tão elevada não escapasse do entendimento das pessoas mais rudes, Ele mesmo as praticou dando exemplo para nós.

Embora a revelação pudesse ter sido dada através de visões ou intuições, deve-se notar que poderiam ser contaminadas pela ignorância ou erro daquele que as ensinasse ou pela incapacidade humana de praticá-las com a devida perfeição.

Por isso, só pode haver uma segurança total na revelação feita diretamente pelo Deus Encarnado e por Ele mesmo praticada.

Segundo. Os milagres operados.

>b) Para confirmar as verdades que excedem o conhecimento natural, realizou ações visíveis que superam a capacidade de toda a natureza, como sejam a cura de doenças, ressurreição dos mortos e maravilhosas mudanças nos corpos celestes. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Com efeito, alguns dos que viram Deus ensinar pessoalmente a doutrina e vivê-la como um ser humano pensaram tratar-se apenas de mais um homem sábio, um filósofo, um profeta ou alguém muito bondoso.

Para que os homens não fossem induzidos neste erro, demonstrou sua divindade fazendo prodígios que somente Deus seria capaz. Para tanto, curou doenças, devolveu a vida aos mortos, desceu aos infernos e ressuscitou, perdoou os pecados, operou mudanças nos corpos celestes, silenciou tempestades, submeteu demônios, andou sobre as águas, multiplicou pães e suportou os maiores sofrimentos e injúrias amando seus algozes.

Ora, somente um ser divino poderia realizar tais coisas. Por isso, trata-se de uma atestação divina de uma determinada religião.

Terceiro. A posse de uma sabedoria sobrenatural por pessoas ignorantes.

>c) Mais maravilhoso ainda é, inspirando as mentes humanas, ter feito que homens ignorantes e rudes, enriquecidos pelos dons do Espírito Santo, adquirissem instantaneamente tão elevada sabedoria e eloquência. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, para que os homens não pudessem pensar que os milagres fossem meras ilusões, operou milagres na própria inteligência humana.

Assim, dotou homens rudes e ignorantes de uma sabedoria repentina sobre a natureza e sobre a vida espiritual sem que proviesse de um prolongado estudo, o que foi manifesto no caso dos Apóstolos que trabalhavam em profissões rudes.

Por isso, a verdadeira religião manifesta-se pela própria iluminação interior provocada pelo próprio Deus, não por recursos retóricos, argumentos aparentemente convincentes ou artifícios engenhosos para manipular as mentes.

Quarto. A conversão dos sábios.

>d) Depois de termos considerados tais fatos, acrescente-se agora, para a confirmação da eficácia dos mesmos, que uma enorme multidão de homens, não só rudes como também os sábios, acorreu para a fé cristã. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, se uma religião é verdadeira ela deve ser capaz de manifestar que sua doutrina é certa até mesmo diante daqueles que a odeiam.

Com efeito, foi o que aconteceu com o Apóstolo Paulo, violento perseguidor da Igreja e judeu convicto que, por um milagre, percebeu que aquilo que odiava era a própria Verdade.

Ainda, homens sapientíssimos perceberam que tudo aquilo que encontraram de verdadeiro e mais profundo em seus estudos estava contido no Evangelho, levando-o a conversão. Com efeito, a maioria dos Santos Padres surpreende as pessoas por sua tão grande erudição.

Ademais, muitos são os que odeiam a doutrina revelada por saberem ser verdadeira. É o caso das seitas satânicas que reconhecem as verdades da fé, mas as odeiam.

Além disso, homens de todas as classes se converteram à Verdade que se revelava. Desde filósofos até pescadores; judeus e gentios; libertinos e virtuosos; casados e solteiro; homens e mulheres; reis e escravos.

Sem dúvida isto é um sinal distintivo da verdadeira religião, embora não o único, pois as falsas religiões tendem a atrair simpatizantes de determinadas ideias, classes ou costumes e excluir os demais, enquanto que a verdadeira deve necessariamente satisfazer e santificar todos os homens independente de sua condição.

Quinto. A causa da conversão.

>e) Assim o fizeram, não premidos pela violência das armas, nem pela promessa do prazer, mas também - o que é maravilhoso - sofrendo a perseguição dos tiranos. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, as pessoas não podem se converter a verdadeira religião por causa de interesses temporais.

Com efeito, muitas são as falsas religiões que atraem sectários por causa de interesses políticos, por causa da concupiscência, por causa do medo.

Porém, somente a verdadeira é capaz de atrair os homens de modo que busquem seguí-la por razões espirituais mesmo debaixo das perseguições mais tirânicas.

Por isso, a verdadeira religião converte os homens por amor à Verdade e a busca de uma profunda vida espiritual.

Assim, os fiéis sempre permaneceram firmes diante de fogueiras, leões, cruzes, apedrejamentos, decapitações, flechas, espancamentos, expulsões, difamações, inimizades, desterros, perda de cargos e até mesmo de seus próprios bens com toda liberdade e padecendo com alegria.

Sexto. O desprezo do mundo.

>f) Além disso, na fé cristã, são expostas as virtudes que excedem todo o intelecto humano, os prazeres são reprimidos e se ensina o desprezo das coisas do mundo. Ora, tendo os espíritos humanos concordado com tudo isso é ainda maior milagre e claro efeito da inspiração divina. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, os homens naturalmente desejam e buscam seu próprio bem. Como somos criaturas dotadas de um corpo, temos uma inclinação natural a buscar as coisas do mundo como alimentação, saúde, segurança, prazeres, entretenimento e assim por diante.

Com efeito, muitas das falsas religiões apenas alimentam aquilo que o homem já possuía de natural.

Contudo, a verdadeira o ensina a desprezar todos estes bens e preferir perder a própria vida do que entregar-se a busca dos prazeres ou a concentrar-se nas coisas do mundo.

Ora, não é possível que o homem violente sua natureza de tal modo que abra mão de tudo o que lhe é próprio e ainda assim encontre felicidade.

Porém, o que aconteceu com todos aqueles que seguiram a verdadeira religião conseguiram justamente isto: a felicidade, mesmo desprezando esta vida.

Sétimo. As profecias.

>g) Essas coisas não aconteceram de improviso ou por acaso, mas por disposição divina, porque ficou evidenciado que elas se realizaram mais tarde, porquanto Deus as havia predito pelos oráculos de muitos profetas, cujos livros são venerados por todos nós como portadores do testemunho da nossa fé. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, as falsas religiões oferecem muitos sortilégios e previsões que demonstram-se falsas.

Porém, a verdadeira religião possui uma coerência completa com todos os verdadeiros profetas da humanidade.

Ademais, deve-se notar que a própria Criação anuncia os mistérios de Deus. Portanto, a verdadeira deverá ser aquela que também seja coerente com toda a ordem do Universo.

De fato, somente uma religião pode e tem o privilégio de ser coerente com estas coisas em sua totalidade.

Oitavo. A persistência destes sinais.

>h) Tão maravilhosa conversão do mundo para a fé cristã é de tal modo certíssimo indício dos sinais havidos no passado, que eles não precisaram ser reiterados no futuro, visto que os seus efeitos os evidenciavam. Seria realmente o maior dos sinais miraculosos se o mundo tivesse sido induzido, sem aqueles maravilhosos sinais, por homens rudes e vulgares, a crer em verdades tão elevadas, a realizar coisas tão difíceis e a desprezar bens tão valiosos. Mas ainda: em nossos dias Deus, por meio de seus santos, não cessa de operar milagres para a confirmação da fé. <cite>Santo Tomás de Aquino, Suma contra os Gentios, Livro I, VI, 2</cite>

Ora, se estes sinais existissem apenas no passado seria sinal de que a verdadeira religião teria se extinguido.

Por isso, a verdadeira religião permanece até hoje ostentando todos estes sinais para que todos os povos em todos os tempos tenham condições de distinguir a verdade da falsidade.

Por todas estas razões, a única religião dotada destes sinais e limpa de qualquer sinal de falsidade é aquela conservada e propagada pela Igreja instituída por Nosso Senhor Jesus Cristo que é Una, Santa, Católica e Apostólica.