+++
date = "2015-09-09T18:00:00-18:00"
title = "Testemunho do Ex-Satanista Zachary King"
categories = ["Apologetica","Testemunhos"]
tags = ["Satanismo"]
featured_image = "posts/images/testemunho-do-ex-satanista-zachary-king.jpg"
+++

Garoto normal de um bairro americano, criado em um lar evangélico batista, Zachary começou a praticar magia aos 10 anos, juntou-se a uma seita satânica aos 13 e, com 15, já havia quebrado todos os Dez Mandamentos.

<!--more-->

Dos seus anos de juventude até a idade adulta, ele trabalhou o seu caminho até se tornar "sumo sacerdote" da seita e praticou ativamente a agenda satânica, incluindo rituais de aborto.

**Zac, você tem uma longa história para contar. Poderia nos dar uma ideia geral de como você foi cair no satanismo?**

Tudo começou com uma forte curiosidade, em que eu me perguntava se a magia era algo real. Isso veio depois que assisti a filmes sobre feiticeiros e bruxos, na década de 1970, quando eu cresci. Nós tínhamos um jogo na escola chamado Bloody Mary, ou I Hate You, Bloody Mary, no qual você ia ao banheiro e recitava essas frases um certo número de vezes com as luzes apagadas. Todas as vezes que o meu grupo fazia isso, sempre víamos um rosto demoníaco no espelho. Não fazíamos ideia do que era aquilo que estávamos encarando, apenas que, de repente, aparecia aquela coisa assustadora no espelho e todos corriam para fora do banheiro, morrendo de medo... exceto eu. Eu sempre achava aquilo bem legal. Então, na mesma época em que eu fazia isso, também jogava torneios de Dungeons and Dragons todo fim de semana, e eu sempre era o mago ou o feiticeiro do jogo. Eventualmente, eu me perguntava se podia fazer magia de verdade e tentei um par de feitiços para ganhar dinheiro. Ambos funcionaram, mas, como poderia ter sido apenas uma coincidência, eu tentei fazer uma terceira vez e, na terceira vez em que fiz isso, joguei o feitiço em frente ao demônio do banheiro e achei que pudesse aumentar um pouco o valor do lance para ver o que acontecia. Consegui 1.000 dólares no dia seguinte. A partir de então, fiquei convencido de que magia era real.

Quando eu tinha cerca de 12, um amigo me apresentou a um grupo que jogava Dungeons and Dragons e que também acreditava que magia era real. Aquele grupo acabou se revelando uma seita satânica. Muitas pessoas me perguntam: "Você não correu e se escondeu a essa altura?" Eu lembro a elas que cresci nos anos 70, quando seitas satânicas na TV eram realmente assustadoras, mas... eu adorava fliperama, video games, ficção científica, como Jornada nas Estrelas e Guerra nas Estrelas, e aqueles rapazes tinham quase todos os filmes de ficção científica e de fantasia que eu sempre quis assistir. Eles tinham fliperama, uma piscina, uma grande churrasqueira, e era como um clube de meninos e meninas, tudo muito divertido. Deixem-me colocar deste modo: eles sabiam como recrutar, sabiam tudo o que uma criança queria fazer. Então, foi assim que eu me envolvi com isso.

Aquele foi o meu primeiro grupo. Fiquei lá dentro até os meus 18 anos, quando me juntei à Igreja Mundial de Satanás, que é um grupo muito maior, internacional. A posição que eu atingi é chamada de high wizard (uma espécie de "sumo sacerdote"). Em uma seita satânica maior, eles são as pessoas que fazem a magia pelo grupo. Poderia haver somente um ou até dez deles, mas o número geral variava de 2 a 5, e o nosso trabalho era viajar ao redor do mundo fazendo quaisquer feitiços que as pessoas quisessem que fizéssemos. Quando eu digo "pessoas", falo de estrelas do rock, astros de filmes, personalidades políticas, pessoas ricas... Não há limites para quem quer um feitiço e para o valor que eles estão dispostos a pagar.

##### Escolhidos a dedo por Satanás

**Então, você era um sumo sacerdote no satanismo... Bem rapidamente, como aconteceu de você se tornar um high wizard?**

Dizem que high wizards são escolhidos a dedo por Satanás. Eu não sei qual o critério. Tinha feito magia desde a idade dos 10 e me tornei um deles quando tinha cerca de 21. Eu estava na Igreja Mundial de Satanás por uns 3 anos. Já tinha visto um high wizard quando era criança, mas ainda não sabia do que se tratava. A aparência é muito peculiar. Uma cartola, uma varinha ou um bastão, o rosto pintado como um cadáver e um velho smoking da sorte.

(...)

Satanás escolhe você e, para um culto grande como esse, há um chefe executivo e um quadro de diretores. Então, o chefe manda uma carta para você, você se encontra com ele e com o quadro de diretores, e eles dizem que você foi escolhido. Dão a você um livro que diz quais são os seus deveres de trabalho como sumo sacerdote e você decide se quer fazer aquilo ou não – embora eu nunca tivesse conhecido ninguém a recusar aquele convite.

**Então, você foi chamado diante de um grande conselho de comuns, eles ofereceram a posição e você se tornou um high wizard naquele instante?**

Isso, e eu trabalhei nisso por volta de 10 a 12 anos.

##### O primeiro ritual de aborto

**Qual o papel que tem o aborto nos rituais satânicos, e quando foi a primeira vez que você se envolveu com aborto dentro do satanismo?**

Logo depois que completei 14 anos, os membros da seita vieram a mim e me disseram que eu me envolveria em um aborto em cerca de 9 meses. Houve uma orgia com todos os homens entre 12 e 15 anos e uma mulher com mais de 18, e o propósito dela era ficar grávida, para ter o aborto em 9 meses. Quando me contaram isso, eu disse bem alto: "Legal", mas não fazia ideia do que era um aborto. Em minha família, acho que ouvi meus pais sussurrarem uma vez a palavra aborto, enquanto falavam de outra pessoa. Então, eu pensava que aquela era uma palavra suja, porque eles cochicharam, e eu não tinha ouvido aquela palavra em nenhum outro lugar. Quando perguntei aos membros da seita o que era um aborto, eu disse que não sabia o que deveria fazer. Eles explicaram que haveria um bebê na barriga e que eu iria matá-lo; que haveria um médico aborteiro lá para me ajudar e uma enfermeira, porque seria um procedimento médico completo. Minha primeira pergunta foi: "Isso é legal?" A resposta foi: "Sim, desde que aconteça no ventre. Enquanto o bebê estiver dentro da mulher, você pode matá-lo."

Foi dessa forma que isso foi explicado para nós. Também foi explicado que eu estaria "matando um bebê". Eles não disseram que nós iríamos matar um feto ou algumas células em um corpo. Nada disso. Era um bebê.

Agora, eu não acho que eu teria concordado em matar uma criança fora do corpo da mulher, mas, sabendo que eu poderia matar o quanto quisesse se alguém estivesse dentro do corpo... No satanismo, matar alguém ou a morte de algo é a forma mais efetiva de ter o seu feitiço realizado. Quando se quer conseguir a aprovação de Satanás, para que ele lhe dê algo que você deseja, matar alguém é a melhor forma de conseguir. Matar algo é a oferta final para Satanás e, se você pode matar uma criança não nascida, essa é a sua meta final.

**Conte-me sobre o primeiro aborto que você fez dentro de um ritual satânico.**

O primeiro que fiz foi cerca de 3 meses antes de completar 15 anos. Aconteceu em uma casa de campo. Surpreendentemente, o lugar era mais higienizado que muitas das clínicas nas quais eu tinha feito abortos. Havia um médico e uma enfermeira, uma mulher nos estribos pronta para dar à luz, rodeada de 13 líderes da nossa seita, os quais eram todos sumo sacerdotes e sacerdotisas. Eu estava dentro do círculo com a mulher e o médico. Todos os membros adultos de minha seita estavam lá. Havia várias mulheres se ajoelhando no chão, balançando para frente e para trás cantando repetidamente: "Nosso corpo e nós mesmas". Do lado estavam vários membros masculinos de nossa seita, todos cantando e orando. O ritual começou às 23h45min, e o feitiço começou à meia-noite, que é a hora das bruxas, e a morte da criança aconteceu às 3h, que é a chamada hora do demônio.

Tudo o que eu tinha que fazer era colocar o bisturi. Eu não necessariamente precisava praticar o assassinato... o importante era que eu sujasse minhas mãos de sangue. Então, eu tive que sujar minhas mãos com o sangue de alguém, ou o da mulher ou o do bebê, e, depois, o médico terminou o procedimento. Naquele em particular – provavelmente um dos abortos mais hediondos que eu fiz –, o médico pegou, arrancou e jogou a criança no chão, onde essas mulheres estavam se remexendo. Elas pareciam estar possuídas, e quando o médico jogou-lhes o bebê, elas canibalizaram a criança.

**Santo Deus! De quantos abortos rituais você participou?**

Antes de me tornar um high wizard, eu fiz cinco. Depois, fiz 141 ou mais.

##### Todos os bebês são oferecidos a Satanás no final do dia

**Você chegou a fazer algum ritual satânico nas instalações de clínicas de aborto?**

Cheguei sim. Eu estimo ter feito cerca de 20 abortos rituais dentro dessas instalações, embora nunca tenha chegado a contar. Mas eu estive em muitas delas. Cerca de dois anos atrás, entrei em uma para fazer uma pesquisa para um novo CD em que estava trabalhando, e ela era bem limpa e as pessoas bem legais. Mas todas às que fui, fazendo aborto nelas, eram terrivelmente anti-higiênicas. Pareciam-se mais com uma casa de horrores, com sangue em todo o lugar, incluindo algumas salas com sangue no teto.

**Como você foi convidado para fazer abortos satânicos nesses lugares? Alguém o chamou? Como isso aconteceu?**

Como sumo sacerdote, você é o membro mais ativo da seita satânica, então a maioria das pessoas liga para alguém que elas conhecem na seita (...) [e] você é convidado a participar. A Igreja Mundial de Satanás não é a única organização que faz sacrifícios satânicos nessas instalações. Há outras organizações de bruxaria, como as wiccanas, que estão realmente engajadas em praticar abortos dentro dessas clínicas. Às vezes, você é chamado para o aborto ritual por um diretor da clínica ou algum administrador, isso quando o próprio médico não é um satanista. Outras vezes, eles fazem uma cerimônia no final do dia.

Na verdade, todos os dias, à noite, grupos satânicos fazem como que uma Missa Negra, geralmente por volta da meia-noite, e acontece um culto estendido com duração de mais ou menos 2 ou 3 horas, no qual eles oferecem todos os bebês que foram mortos naquele dia para Satanás. Não importa o motivo pelo qual as mulheres procuraram o aborto, todos os bebês são oferecidos a Satanás no final do dia.

**O que geralmente acontece durante esses abortos rituais?**

Há crianças que vêm para esses eventos, mas elas geralmente não ficam na sala onde o aborto acontece, ficam em uma sala separada. Elas têm competições para ver qual delas fica acordada até às 3h da madrugada, e a criança que consegue ficar até tarde ganha um prêmio. Os homens que não fazem parte dos 13 cabeças do grupo ficam fazendo feitiços e cantando. Eles também jogam feitiços, seja para se protegerem de qualquer um que possa estar rezando contra eles, como um cristão, seja por quem quer que tenhamos no bolso para proteger – assim, se nós pagamos um xerife, um policial ou algo do tipo, ninguém nos investigará naquela ocasião. Há mulheres cantando e dançando. Os 13 membros ficam em volta da mulher prestes a ter o aborto e conduzem a bruxaria. Uma vez, quem conduziu o feitiço foi o prefeito da cidade. Ele nos procurou porque queria passar uma lei para a sua cidade, e tinha tentado duas ou três vezes, sem sucesso. Ele foi membro da seita por algum tempo. Havia tentado todos os caminhos legais para passar a lei, mas, como não funcionou, ele conseguiu alguém para fazer um aborto com a nossa seita, durante a noite e em um lugar em que pudéssemos fazer o procedimento e a bruxaria ao mesmo tempo. Geralmente, em uma seita de cidade pequena, que era o caso, todos apareciam para o evento. Em um lugar maior, como quando eu era membro da Igreja Mundial de Satanás, você chama o sumo sacerdote, as pessoas que querem fazer o feitiço, um médico e uma enfermeira. Várias vezes, nessas clínicas de aborto profissionalizadas, há um monte dessas pessoas, porque muitos que trabalham nesses lugares são bruxas ou satanistas. Então, você consegue um grande número de pessoas que queira participar do evento satânico.

Você diria que clínicas de aborto profissionalizadas atraem pessoas vindas do ocultismo por causa da oportunidade de realizar rituais de aborto?

Eu diria que sim, essa é uma afirmação absolutamente verdadeira. (...) Assim como os rapazes católicos aderem ao sacerdócio porque são atraídos pela santidade e pelo serviço a Deus, uma clínica de aborto atrai os satanistas para o ministério satânico.

##### O poder da oração

**Você já experimentou alguma dificuldade em concluir um aborto ritual, devido a pessoas rezando fora de uma clínica de aborto?**

Mais de uma vez, tivemos bebês que, contra todas as possibilidades, sobreviveram ao aborto. Uma vez, cheguei à clínica de aborto e havia pessoas dos dois lados da rua. De um lado, havia pessoas rezando e clamando contra o aborto; do outro, no qual eu estava, havia pessoas que eram obviamente pelo aborto, e gritando todo tipo de obscenidades às pessoas do outro lado da rua. Quando nós entramos e olhamos para o lado de lá, vimos todas as pessoas do outro lado de joelhos. Naquele dia, o aborto que tínhamos agendado para um ritual não deu certo. Eu acho que isso aconteceu comigo umas três vezes, e todas as três... Engraçado, mas nunca tinha parado para pensar que todos os três abortos foram frustrados pelo que só pode ser atribuído às orações que estavam acontecendo fora da clínica.

**Qual conselho você tem para as pessoas que rezam em frente às clínicas de aborto, especialmente se elas suspeitam que há algum tipo de ato ocultista acontecendo dentro dela?**

Antes de tudo, não pare! Não há nada acontecendo nessa clínica de aborto que possa machucar você. Com certeza, haverá demônios em volta, mas você deve pensar em Satanás como um cachorro amarrado a uma coleira; se você não chegar perto da coleira, ele não o pode morder. Esteja em estado de graça quando você for. Leve uma garrafa de água benta consigo. Não a jogue nas pessoas que estão lá para se opor porque você pode ir parar na justiça. Você sabe, essas pessoas irão processar você por causa das coisas mais bobas. Mas, com certeza, aspirja-se quando chegar lá e quando sair. Aspirja todos os membros da sua família. Se puder receber a Sagrada Comunhão antes de chegar lá, seria o ideal. Se for à Missa nesse dia, permaneça alguns minutos depois da celebração para pedir a Nosso Senhor que mande a Sua Mãe com você. Leve um Rosário consigo e combata o demônio até a morte com ele. Há coisas de que o diabo tem medo, mas, acima de tudo, ele teme um católico bem formado, um católico que entende a sua fé e que sabe o que é uma guerra espiritual. Ele não quer lutar contra alguém que está com toda a sua armadura a postos.

##### A conversão

Em janeiro de 2008, enquanto trabalhava numa joalheria, Zachary teve um encontro com a bem-aventurada Mãe de Deus, encontro que mudou a sua vida. No meio do shopping, pelo poder da Medalha Milagrosa, Zachary experimentou uma paz que supera todo o entendimento. Aquela paz era Jesus Cristo, o Príncipe da Paz. O amor de Nossa Senhora resgatou Zachary do inferno e trouxe-o para perto do Coração do seu Filho, Jesus Cristo. Ele começou a frequentar a comunidade católica de São Francisco Xavier, em Vermont, e, em maio de 2008 (o mês de Maria), entrou na Igreja Católica! Depois de 26 anos de envolvimento com o ocultismo, Zachary tornou-se um guerreiro por Jesus Cristo e quer compartilhar o seu conhecimento para a proteção do povo de Deus. O testemunho de Zachary é uma inspiração que prova quão grandes são a misericórdia e o perdão do Senhor e, acima de tudo, mostra a profundidade do seu amor para conosco. Atualmente, Zachary vive na Flórida com sua esposa e é pregador internacional, espalhando a história de seu miraculoso resgate do satanismo onde quer que ele possa. O seu site é [www.allsaintsministry.org](http://www.allsaintsministry.org).

Fonte: [Padre Paulo Ricardo](https://padrepauloricardo.org/blog/ex-satanista-eu-fazia-rituais-satanicos-dentro-de-clinicas-de-aborto)