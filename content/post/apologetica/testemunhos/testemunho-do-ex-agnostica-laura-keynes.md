+++
date = "2015-09-14T18:00:00-18:00"
title = "Testemunho da Ex-Agnóstica Laura Keynes"
categories = ["Apologetica","Testemunhos"]
tags = ["Agnosticismo"]
featured_image = "posts/images/testemunho-do-ex-agnostica-laura-keynes.jpg"
+++

Laura Keynes, tátara-tátara-tataraneta do naturalista Charles Darwin, cuja teoria da evolução é usada por muitos ateus para negar a existência de Deus, é uma jovem católica que defende sua fé através do Catholic Voices, do Reino Unido.

<!--more-->

Precisamente em uma publicação no Catholic Voices, Laura explica sua conversão.

De pai ateu e mãe conversa ao catolicismo, mas logo budista, Laura foi batizada católica. Entretanto, em sua adolescência se converteu em agnóstica, afastada "de qualquer contato com a Igreja".

"Do lado da família do meu pai, o lado Darwin-Keynes", a jovem recebeu uma influência "altamente racional, científica, secular, humanista".

"Meu pai é um neurocientista, e eu absorvi a visão de que todos os fenômenos são produto do cérebro material. Aos poucos derivei ao agnosticismo", recordou.

Foi quando realizava seus estudos de doutorado em Filosofia em Oxford (Reino Unido), e se viu rodeada pelo "Debate de Deus", depois da publicação de "Deus, um delírio", de Richard Dawkins, foi quando começou a regressar ao seio da Igreja.

"Pensei que ia mudar do agnosticismo para o ateísmo por causa dos seus argumentos, mas depois de ler a ambos os lados do debate, não podia desprezar um caso convincente a favor da fé", recordou.

"Quanto a ser boa sem Deus, tinha tentado e não tinha chegado muito longe. Em algum ponto, a vida te coloca de joelhos, e nenhum ato de vontade é suficiente nessa situação. Render-se e pedir a graça é a resposta humana lógica", assegurou.

O debate a levou a concluir que "o novo ateísmo parecia conter um germe de intolerância e desprezo pelas pessoas, que só poderia socavar as reclamações humanistas seculares ao liberalismo".

"Se o ateísmo assegura que a superioridade intelectual está reforçada pela habilidade característica do meu ancestral de explorar e analisar as inconsistências na evidência, então a mesma característica familiar me levou para uma avaliação cética do que se pode e o que não se pode conhecer absolutamente".

Laura voltou a rezar o Terço durante a prolongada doença de sua avó, quando "nessas longas horas ao lado de sua cama, lembrei o poder redentor do sofrimento de Cristo".

"Ver a morte me fez questionar sobre o espírito: o que é, de onde vem, aonde vai. Assim, neste ponto, já estava desenvolvendo um despertar espiritual, mas não tinha dado o passo de volta à Igreja Católica. Esse passo veio depois de muita reflexão e leitura".

Laura assegurou que a sua escolha livre de ser católica aconteceu "depois de muita análise e pensamento, e que não lavaram a minha cabeça para entrar, isso desconcertou tanto os meus amigos como a minha família".

"Escutei um comentário: ‘Mas ela parecia ser uma garota inteligente’. Quando as pessoas perguntam ‘uma Darwin é uma católica?’, o que estão dizendo é que quebrei as expectativas".

Laura assegura que os "ateus preferem a certeza e usam a teoria da evolução de Darwin para assegurar categoricamente que Deus não existe, manipulando Darwin em seu argumento de uma forma que até o próprio Darwin estaria incômodo".

Darwin, disse a jovem apologista, "dedicou-se a seguir a evidência aonde o levava, não a derrubar o cristianismo. A evidência não tem que concluir indevidamente no materialismo, mas, por diversas razões culturais, aqui é aonde levou: o materialismo e a cultura da morte".

"Esta é a verdadeira batalha: a cultura da vida, apoiada pelo cristianismo, frente à cultura da morte, apoiada pelo materialismo".

Laura, que recentemente visitou Roma pela primeira vez, como parte de uma peregrinação pelo Ano da Fé, assegurou que esta "certamente põe um pouco de combustível no tanque espiritual".

"Necessitarei. Há muito trabalho por diante", concluiu.

Fonte: [ACI Digital](http://www.acidigital.com/noticias/descendente-de-charles-darwin-e-firme-defensora-da-fe-catolica-14880/)