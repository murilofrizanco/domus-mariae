+++
date = "2015-09-11T18:00:00-18:00"
title = "Testemunho do Ex-LGBT Eric Hess"
categories = ["Apologetica","Testemunhos"]
tags = ["LGBT"]
featured_image = "posts/images/testemunho-do-ex-lgbt-eric-hess.jpg"
+++

Depois de uma vida desregrada, Eric Hess, ativista gay de Wisconsin, Estados Unidos, encontrou-se com um homem de fé - o Cardeal Raymond Burke - e com a misericórdia divina. Em agosto de 1998, ele decidiu abandonar o pecado do homossexualismo e viver a castidade.

<!--more-->

##### O próprio convertido conta sua história

Minha atração homossexual começou, para ser sincero, como reação ao meu pai, que era um alcoólatra violento. Ele bebia com frequência, chegava para espalhar coisas pela casa e abusar da minha mãe, além de ameaçar a mim e a meu irmão. Eu achava que ele nos odiava. Em consequência, não queria ser nenhum pouco parecido com ele. 

Na minha mágoa, comecei a procurar pelo amor do meu pai nos braços de outros homens. Aos 17 anos, um predador se aproveitou de mim sob a dinâmica professor/aluno e eu fiquei completamente confuso em relação à sexualidade humana. Com o passar dos anos, uma coisa levou a outra até que eu fui morar com um homem 20 anos mais velho que eu. 

Antes de ir mais longe, é importante entender uma das principais causasda desordem da atração por pessoas do mesmo sexo. Como um ex-membroda comunidade, eu posso dizer que os chamados direitos homossexuais – e o direito ao aborto – são um resultado imediato da mentalidade contraceptiva predita há 40 anos pelo Papa Paulo VI, na Humanae Vitae. Pessoas abusando umas das outras como objetos sexuais trouxeram à tona uma cultura de morte que tolera e defende todos os tipos de adultério e abuso infantil, incluindo o aborto. Essa mentalidade egoísta levou também às pesquisas com células-tronco embrionárias e à eutanásia. 

##### Retorno ao meu Pai 

De 1990 a 1994, eu ia à Missa de vez em quando. Em 1995, eu disse ao meu “parceiro" que não podia ir mais porque estava muito bravo com a Igreja. Eu coloquei todos os meus crucifixos e Bíblias em caixas e deixei-os no escritório do bispo de La Crosse, Wisconsin, com uma carta renunciando à fé católica. 

Para minha surpresa, o bispo Raymond Burke respondeu com uma carta amigável, expressando sua tristeza. Ele escreveu que respeitava minha decisão e notificaria a paróquia onde eu havia sido batizado. Sempre muito gentil, Burke disse que rezaria por mim e esperava ansiosamente pelo dia em que eu me reconciliaria com a Igreja.

Como um dos mais francos ativistas “gays" de Wisconsin, eu pensei: “Que arrogância!" Então eu respondi ao bispo Burke com uma carta acusando-o de preconceito. Eu disse a ele que suas cartas eram desagradáveise perguntava como ele se atrevia a escrever-me. 
Meus esforços de pará-lo foram em vão. Burke enviou-me mais uma carta, assegurando-me que não escreveria de novo – mas que se eu quisesse reconciliar-me com a Igreja, ele me acolheria de volta de braços abertos. 

De fato, o Pai, o Filho e o Espírito Santo nunca desistiram de mim. Dentro dealguns anos, eu conversei com um bom padre, que se uniu intensamente às orações do bispo Burke em agosto de 1998. 

Em 14 de agosto, festa de São Maximiliano Maria Kolbe e vigília da Assunção de nossa Bem-aventurada Mãe, a misericórdia divina penetrou a minha alma, em um restaurante chinês – de todos e entre tantos lugares. Eu mal sabia, ao entrar naquele restaurante com o meu companheiro de mais de oito anos, que o Senhor me tomaria para Si naquela mesma tarde e me levaria a outro lugar, fora de Sodoma, para o juízo de Sua misericórdia, o santo Sacramento da Penitência. 

O padre que eu tinha consultado estava lá. Assim que eu olhei do outro lado da sala para ele, uma voz interiorfalou ao meu coração. Era suave, radiantee clara em minha alma. A voz me disse: “O padre é uma imagem do que você ainda pode tornar-se, se voltar para Mim." 

No caminho para casa, euseriamente disse ao meu companheiro: “Eu preciso voltar à Igreja Católica". Mesmo em lágrimas, ele amavelmente respondeu: “Eric, eu sabia disso há muito tempo. Faça o que você precisa fazer para ser feliz. Eu sempre soubeque esse dia chegaria." 

Depois, eu liguei para o escritório do bispo Burke. A sua secretária já me conhecia bem, então eu lhe disse que queria que o bispo Burke fosse o primeiro a saber que eu estava voltando para a Igreja e me preparando para o Sacramento da Penitência. Ela me pediu para esperar. Quando voltou, anunciou que o bispo Burke queria agendar uma conversa. 

Mais tarde, eu confessei meus pecados a um devoto e humilde pastor de almas local e recebi a absolvição. Como parte essencial de meu restabelecimento, uma boa família católica deu-me proteção até que eu pudesse encontrar minha própria casa. 

Um mês depois de minha reconciliação com Deus e com a Igreja, eu fui ao escritório do bispo Burke, onde ele me abraçou. Ele perguntou se eu me lembrava dos pertences que havia deixado com ele junto com minha carta de renúncia. É claro que eu me lembrava e o bispo Burke os tinha guardado nos arquivos da diocese porque acreditava que eu retornaria. 

Por dois anos eu me pergunteise a mensagem mística significava que eu deveria me tornar padre. Finalmente, eu percebi que não era chamado ao sacerdócio. Afinal, o Vaticano determina que homens que têm uma inclinação homossexual bem estabelecida não podem ser admitidos às Ordens Sagradas ou às comunidades monásticas. Mais do que isso, o padre que eu vi no restaurante era uma imagem de que eu poderia me tornar santo e fiel através dos Sacramentos. Como todas as pessoas – solteiras, casadas e religiosas –, eu sou chamado à castidade. Para mim, é o bastante tentar e chegar ao Céu. Por isso, eu me esforço para viver fielmente minha vocação de solteiro. 

Desde a minha experiência mística, eu me alegro por Raymond Burke, agora prelado de Saint Louis, Missouri. Enquanto alguns criticam o arcebispo Burke por sua fidelidade a Deus, à Igreja e às almas, eu digo que ele é um verdadeiro pastor dos fiéis e um Atanásio dos nossos dias. Eu digo a vocês que ele continua sendo um conselheiro e uma inspiração para mim. Embora meu pai biológico tenha me rejeitado, o arcebispo Burke se tornou meu pai espiritual, representando amorosamente nosso Pai dos céus. Como as Pessoas Divinas da Santíssima Trindade, o arcebispo Burke foi e é absolutamente fiel a mim. 

##### A chave para a felicidade

Apesar da bênção do arcebispo Burke e de padres como ele, eu quero salientar que há outros que tiram as almas da vida eterna e da felicidade. 

Por exemplo, quando eu recentemente fui à Confissão, um padre me disse algo contraditório à verdade que o arcebispo Burke me ensinou. 
O padre apóstata me disse: Você é gay e a Igreja nos chama a aceitar nossa sexualidade. Eu sou um professor de ética – um estudioso. (...) Se você é atraído por pessoas do mesmo sexo, isso é natural para você. E, para você, negar isso e resistir é ir contra a lei natural. Eu acredito, como professor de ética, que você pode ter um colega de quarto homem e ser íntimo dele – sem contato genital, é claro. Mas se você escorregar, não seria um pecado mortal. 

Esse é o tipo de conselho que me convenceu a deixar a Igreja. Eu o escutava muito frequentemente de protestantes e de vários padres católicos durante os anos 1980. Eu escutei todo tipo de heresia sobre a sexualidade e Nosso Senhor. Hoje, que já estou separado da “comunidade gay", eu escuto essas heresias apenas de padres mais velhos, em seus cinquenta e sessenta anos, mas não de padres com quarenta anos ou menos. Maus bispos e maus padres sozinhos desviaram muitas pessoas em relação à atração homossexual. Não há nenhum “novo evangelho" ou estudo, e essa negligência espiritual deve parar. 

Como alguém que sofreu no estado de pecado mortal por vários anos, eu asseguro a vocês que não há nenhuma felicidade fora da ordem moral. A única resposta autêntica ao desafio da atração homossexual e do pecado é a verdade contida no Catecismo da Igreja Católica.

Fonte: [Padre Paulo Ricardo](https://padrepauloricardo.org/blog/categoria/23-testemunhos/pagina/3)