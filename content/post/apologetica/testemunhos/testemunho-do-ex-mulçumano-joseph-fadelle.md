+++
date = "2015-09-12T18:00:00-18:00"
title = "Testemunho do Ex-Mulçumano Joseph Fadelle"
categories = ["Apologetica","Testemunhos"]
tags = ["Islamismo"]
featured_image = "posts/images/testemunho-do-ex-mulçumano-joseph-fadelle.jpg"
+++

"Sua doença se chama Cristo e não tem remédio. Nunca poderá ser curado", disse o tio de Mohammed al-Sayyid al-Moussawi, como quem pronuncia uma "sentença de morte". Isto aconteceu em 22 de dezembro de 2000, quando Mohammed, hoje conhecido como Joseph Fadelle, se reuniu com seu tio e alguns dos seus irmãos após sua conversão do Islã ao Cristianismo.

<!--more-->

A história começou em 1987, quando o jovem Sayyid (título nobiliário) al-Moussawi, foi enviado ao serviço militar, onde conheceu Massoud, um camponês com mais idade que ele e tiveram que compartilhar o mesmo quarto, algo que ele rejeitou, pois o homem era cristão. Para Mohammed, os cristãos eram inferiores, impuros e compartilhar o espaço com um deles o colocaria em perigo.

Com o passar dos anos e devido ao novo companheiro de quarto, sua mentalidade com relação aos cristãos foi mudando e ajudou que ele aprofundasse nas raízes da fé católica, causando uma crise existencial em Sayyid, que mexeu com as bases da sua crença e, finalmente, o levou à conversão ao cristianismo.

Assim deixou de ser o favorito do grupo familiar, nascido em uma das famílias xiitas mais importantes do Iraque e membro da realeza do seu povo, para ser um cristão perseguido e refugiado, ameaçado de morte até hoje pela sua própria família.

"Eu venho de uma tribo, da qual meu pai era o líder, e eu deveria ser líder depois dele", declarou Fadelle, com o objetivo de explicar a diferença da sua vida anterior com a atual, afastada da sua cultura e da sua família após sua conversão.

Na religião muçulmana, a conversão ao cristianismo ou a qualquer outra religião é motivo de castigo, inclusive de morte.

A pesar das perseguições que suportou, sobreviveu a toda dificuldade e a partir desta experiência escreveu o livro "O preço a pagar", lançado em espanhol no final do mês de maio de 2015, no salão Fresno do Centro de Extensão da Pontifícia Universidade Católica do Chile, num evento organizado pela Editora LMH (Ler Mais Hoje) e cuja venda ultrapassou a 50 mil cópias em 2010.

"Estou aqui pelo grande amor que tenho a Jesus Cristo", disse no início do seu testemunho durante a conferência realizada no lançamento da quinta edição do livro que narra sua história.

A obra transformou-se num instrumento pastoral, narrando vários períodos da sua vida, desde a compreensão do Corão e da religião muçulmana até o descobrimento do cristianismo e da fé católica através da leitura da Bíblia.

Este livro e sua própria história são também uma denúncia do drama dos cristãos perseguidos no Iraque e em outros países desta região, na qual devem esconder-se, pagar impostos adicionais e sofrer varias formas de ameaças e perigos devido à sua fé.

##### "Se quiser atravessar o rio, deverá comer o Pão de Vida"

Mohammed estudou durante um tempo o Corão, a pedido do cristão como condição para entregar-lhe uma Bíblia. Este período de tempo, teve como consequência uma crise existencial profunda, devido à falta de respostas diante das suas interrogantes sobre sua religião e a figura de Maomé, que culminou com um sonho que ele não conseguiu compreender no princípio.

Neste sonho, ele via um homem que estava em pé na outra margem de um rio estreito e este homem parecia ser amável e sentia muita vontade de ir até ele, mas quando tentava atravessar o rio ficava paralisado e não conseguia passar para o outro lado. Então o homem pegava na sua mão e o ajudava e quando chegava junto dele lhe dizia: "Se quiser atravessar o rio, deverá comer o Pão de Vida".

No princípio, este fato parecia somente um sonho, mas se transformou na chave que abriu o caminho de Mohammed a uma revolução interior que permanece até hoje, quando finalmente seu amigo cristão entregou-lhe uma Bíblia. O jovem recebeu o Livro sagrado e, casualmente abriu no Evangelho de São João, no qual encontrou as mesmas palavras que, no princípio não tinham significado algum para ele: O Pão de Vida.

Depois de ser batizado, cerca de 13 longos e angustiantes anos, Mohammed al-Sayyid al-Moussawi trocou o seu nome para Joseph Fadelle.

Não somete ele se converteu ao Cristianismo, como também a sua esposa e os seus filhos, são muçulmanos conversos, um acontecimento que os coloca em perigo. Eles tiveram que fugir do Iraque à Jordânia e mais tarde à França, onde residem atualmente.

Fonte: [ACI Digital](http://www.acidigital.com/noticias/sua-doenca-se-chama-cristo-e-nao-tem-cura-uma-incrivel-historia-de-conversao-do-isla-ao-cristianismo-96905/)