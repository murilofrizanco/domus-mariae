+++
date = "2015-09-10T18:00:00-18:00"
title = "Testemunho do Ex-Mulçumano Mario Joseph"
categories = ["Apologetica","Testemunhos"]
tags = ["Islamismo"]
featured_image = "posts/images/testemunho-do-ex-mulcumano-mario-joseph.jpg"
+++

Mario Joseph era imã muçulmano, espécie de clérigo, e depois de converter-se ao cristianismo afirma que Deus o protegeu das ameaças de morte e tortura por parte de sua própria família.

<!--more-->

O ex-imã compartilhou o seu testemunho de conversão e posterior perseguição ao programa Changing Tracks, difundido pela Fundação E.U.K Mamie dirigida por uma comunidade religiosa.

Mario Joseph cresceu na Índia em uma família muçulmana. Matriculado desde pequeno em um colégio muçulmano em Kerala, estudou filosofia e teologia durante 10 anos. Converteu-se em imã antes dos 18 anos.

Mario começou a investigar sobre o cristianismo depois que uma pessoa lhe perguntou quem era Jesus. Estudando o Alcorão percebeu que o nome de Jesus é mencionado com mais frequência que o nome de Maomé. Do mesmo modo, Maria, conhecida em árabe como Mariam, era a única mulher mencionada pelo seu nome no Alcorão. No Islã, Maria é reconhecida como uma virgem perpétua que foi concebida sem pecado.

O Alcorão descreve Jesus como a "Palavra de Deus" e o "Espírito de Deus". Diz que Jesus curou os doentes, devolveu a vida aos mortos e subiu vivo ao céu. Não atribui nada disso ao Profeta Maomé.

Assim, Mario Joseph começou a ver a Deus como pai, algo que também é ensinado pelo cristianismo. "Cada vez que penso que o criador do universo é meu pai, tenho uma espécie de alegria que não posso expressar", disse na entrevista.

Com esta motivação, explicou, "decidi aceitar Jesus".

Entretanto, esta conversão provocou uma reação violenta em sua família. Quando o seu pai o encontrou em uma casa de retiro católica, bateu nele até chegar a desmaiar. Quando acordou, encontrava-se nu em um quartinho da sua casa. Seus braços e pernas estavam presos e tinha feridas e pimenta na boca.

Mario Joseph disse que o seu pai estava obedecendo à lei do Alcorão, que castiga aqueles que abandonam o Islã. Ficou sem comida e água por vários dias e seu irmão o obrigou a beber urina como castigo.

Depois de 20 dias o seu pai entrou na cela e o ameaçou com uma faca se não renunciasse a Jesus.

"Quando soube que era o meu último momento... pensei, 'Jesus morreu, mas voltou. Se eu acredito em Jesus e morro, também recuperarei a minha vida'".

Neste momento se sentiu cheio de energia, se jogou na mão do seu pai para retirar-lhe a faca e gritou o nome de Jesus.

O seu pai caiu e se cortou com a própria faca. Quando os familiares o levaram para o hospital esqueceram de fechar a porta do quarto.

O jovem saiu correndo e pegou um táxi. O motorista era cristão e lhe ajudou a conseguir comida e bebida.

"Nesse dia eu realmente entendi que meu Jesus está vivo, inclusive agora. Quando o chamei, ele me salvou".

Atualmente, Mario Joseph vive em uma casa de retiro católica na Índia, onde realiza conferências em diferentes idiomas.

Mudou seu nome para "Mario", versão masculina de Maria em italiano, e Joseph, por causa do esposo de Maria.

Mario Joseph confessou que não esperava estar vivo depois de sua conversão aos 18 anos. Há pessoas que ainda procuram matá-lo e seus pais celebraram uma cerimônia fúnebre para significar que ele estava morto. Marcaram em um túmulo a data do seu batismo como a data de sua morte

Apesar de não ter contato com os membros de sua família, Mario reza por eles. Mesmo que nunca aceitem o cristianismo, explicou Mario, "Eu sempre digo 'Jesus, leva-os para o céu'".

Fonte: [ACI Digital](http://www.acidigital.com/noticias/ex-ima-converso-ao-cristianismo-assegura-jesus-me-livrou-de-ser-morto-pelo-meu-proprio-pai-76496/)