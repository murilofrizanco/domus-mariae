+++
date = "2014-08-05T18:00:00-18:00"
title = "Existem registros históricos de Jesus?"
categories = ["Apologetica","História"]
path = "História"
featured_image = "posts/images/existem-registros-historicos-de-jesus.jpg"
+++

Muitos historiadores modernos negam a existência histórica de Jesus sob o pretexto de que não existem documentos que comprovem sua existência. Será que isso é verdade?

<!--more-->

Deve-se notar que esta afirmação procede de um preconceito contra todo e qualquer testemunho dos Apóstolos e testemunhas oculares.

Com efeito, o Novo Testamento é uma fonte primária vasta e rica à respeito dos ensinamentos, feitos e legado de Nosso Senhor Jesus Cristo, conforme podemos atestar:

>Muitos empreenderam compor uma história dos acontecimentos que se realizaram entre nós, como no-los transmitiram aqueles que foram desde o princípio testemunhas oculares e que se tornaram ministros da palavra. Também a mim me pareceu bem, depois de haver diligentemente investigado tudo desde o princípio, escrevê-los para ti segundo a ordem, excelentíssimo Teófilo, para que conheças a solidez daqueles ensinamentos que tens recebido. <cite>Evangelho segundo São Lucas 1, 1-4</cite>

Porém, estas fontes são tidas como fantasiosas por pessoas que, devido a uma discrimação professa, só querem aceitar testemunhos que não provenham de seus discípulos, sem que possuem um argumento realmente sério para assim proceder.

Portanto, faz-se necessário demonstrar fontes que demonstram sua existência sem que tenham seus discípulos como autores.

Uma destas fontes é o governador romano da Ásia por volta de 112 d.C., chamado Tácito. Este romano era historiador e em suas obras cita a entrega de Cristo a Pôncio Pilatos por causa da supertição de sua divindade:

>Para destruir o boato (que o acusava do incêndio de Roma), Nero supôs culpados e infringiu tormentos requintadíssimos àqueles cujas abominações os faziam detestar, e a quem a multidão chamava cristãos. Este nome lhes vem de Cristo, que, sob o principado de Tibério, o procurador Pôncio Pilatos entregara ao suplício. Reprimida, essa detestável superstição repontava de novo, não mais somente na Judéia, onde nascera o mal, mas ainda em Roma, para onde tudo quanto há de horroroso e de vergonhoso no mundo aflui e acha numerosa clientela. <cite>Tácito, Anais da Roma Imperial, XV, 44 trad.</cite>

Ademais, temos o testemunho do satirista Luciano de Samosata (século II) que escarnece o Cristo:

>...o homem que foi crucificado na Palestina porque introduziu uma nova seita no mundo... Além disso, o primeiro legislador dos cristãos os persuadiu de que todos eles seriam irmãos uns dos outros, após terem finalmente cometido o pecado de negar os deuses gregos, adorar o sofista crucificado e viver de acordo com as leis que ele deixou. <cite>Luciano de Samosata, O Peregrino Passageiro</cite>

Ademais, há o testemunho de Suetônio, historiador romano oficial do Imperador Adriano, que cita a existência de Cristo. Com efeito, encontramos em um de seus escritos publicados por volta do ano 120 d.C.:

>[O imperador Cláudio] expulsou os judeus de Roma, tornados sob o impulso de Chrestos [Cristo], uma causa de desordem. <cite>Suetônio, Vida dos doze Césares, n. 25</cite>

Ademais, o governador romano da Bitínia, na Ásia Menor, chamado Plínio, o Jovem, também atesta o culto dos discípulos ao Cristo como a um Deus em suas cartas datadas do ano 112 d.C.:

>Todos estes adoraram a tua imagem e as estátuas dos deuses e amaldiçoaram a Cristo, porém, afirmaram que a culpa deles, ou o erro, não passava do costume de se reunirem num dia fixo, antes do nascer do sol, para cantar um hino a Cristo como a um deus..."
Plínio, o Jovem, Carta 96

Ademais, Tertuliano, jurista da Cártago, relata em seus escritos no ano 197 d.C. uma tentativa de Tibério propôr o reconhecimento da divindade de Cristo:

>Portanto, naqueles dias em que o nome cristão começou a se tornar conhecido no mundo, Tibério, tendo ele mesmo recebido informações sobre a verdade da divindade de Cristo, trouxe a questão perante o Senado, tendo já se decidido a favor de Cristo. O Senado, por não haver dado ele próprio a aprovação, rejeitou a proposta. César manteve sua opinião, fazendo ameaças contra todos os acusadores dos cristãos. <cite>Tertuliano, Apologia, V.2</cite>

Ademais, temos o testemunho de um filósofo estóico da Síria, chamado Mara Bar-Serapião que compara Cristo com outros filósofos gregos por volta do ano 70 em uma carta ao seu filho:

>Que vantagem os atenienses obtiveram em condenar Sócrates à morte? Fome e peste lhes sobrevieram como castigo pelo crime que cometeram. Que vantagem os habitantes de Samos obtiveram ao pôr fogo em Pitágoras? Logo depois sua terra ficou coberta de areia. Que vantagem os judeus obtiveram com a execução de seu sábio Rei? Foi logo após esse acontecimento que o reino dos judeus foi aniquilado. Com justiça Deus vingou a morte desses três sábios: os atenienses morreram de fome; os habitantes de Samos foram surpreendidos pelo mar; os judeus, arruinados e expulsos de sua terra, vivem completamente dispersos. Mas Sócrates não está morto; ele sobrevive nos ensinos de Platão. Pitágoras não está morto; ele sobrevive na estátua de Hera. Nem o sábio Rei está morto; Ele sobrevive nos ensinos que deixou.

Ademais, um historiador judeu chamado Flávio Josefo escreveu em suas Antiguidades:

>Havia por esses dias um homem sábio, Jesus, se é que é licito chamá-lo de homem, pois operava maravilhas - mestre de homens que acolhiam a verdade com prazer. Atraiu a si muitos judeus como também muitos gentios. Ele era Cristo; e havendo Pilatos, por sugestão dos principais do nosso meio, o sentenciado à cruz, aqueles que antes o amavam não o abandonaram, pois apareceu-lhes vivo novamente ao terceiro dia. Isto os profetas Divinos haviam predito, bem como dez mil outros fatos maravilhosos a seu respeito; e a tribo dos cristãos, de quem tomam emprestado o nome sobrevive até hoje. <cite>Flavio Josefo, Antiguidades, VIII, III</cite>

Ademais, o próprio Talmude judaico, que contém registros feitos por rabinos, relata que:

>Na véspera da Páscoa eles penduraram Yeshu [...] ia ser apedrejado por prática de magia e por enganar Israel e fazê-lo se desviar [...] e eles o penduraram na véspera da Páscoa. <cite>Talmude Babilônico, Sanhedrim 43a</cite>

Assim, com estes testemunhos de judeus e pagão, de simpatizantes e escarnecedores, de juristas e historiadores, não é possível sustentar a suposição de que Jesus não tenha existido.