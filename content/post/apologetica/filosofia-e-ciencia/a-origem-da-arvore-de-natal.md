+++
date = "2015-12-04T18:00:00-17:00"
title = "A origem da árvore de natal"
categories = ["Apologetica","História"]
path = "História"
featured_image = "posts/images/a-origem-da-arvore-de-natal.jpg"
+++

Quando pensamos em um santo, talvez em um primeiro momento não consideramos que essa pessoa seja ousada, empunhe um machado, um martelo ou que derrube árvores como os carvalhos. Entretanto, existe um santo assim, conhecido como São Bonifácio.

<!--more-->

Este santo nasceu na Inglaterra por volta do ano 680. Bonifácio ingressou em um monastério beneditino antes de ser enviado pelo Papa para evangelizar os territórios que pertencem a atual a Alemanha. Primeiro foi como um sacerdote e depois eventualmente como bispo.

Sob a proteção do grande Charles Martel (conhecido como Carlos Magno), Bonifácio viajou por toda a Alemanha fortalecendo as regiões que já tinham abraçado o cristianismo e levou a luz de Cristo àqueles que ainda não o conheciam.
 
Aproximadamente no ano 723, Bonifácio viajou com um pequeno grupo de pessoas na região da Baixa Saxônia. Ele conhecia uma comunidade de pagãos perto de Geismar que, no meio do inverno, realizavam um sacrifício humano (onde a vítima normalmente era uma criança) a Thor, o deus do trovão, na base de um carvalho o qual consideravam sagrado e que era conhecido como "O Carvalho do Trovão".

Bonifácio, acatando o conselho de um irmão bispo, quis destruir o Carvalho do Trovão não somente a fim de salvar a vítima, mas também para mostrar àqueles pagãos que ele não seria derrubado por um raio lançado por Thor.

O Santo e seus companheiros chegaram à aldeia na véspera de Natal, bem a tempo para interromper o sacrifício. Com seu báculo de bispo na mão, Bonifácio se aproximou dos pagãos que estavam reunidos na base do Carvalho do Trovão e lhes disse: 

>"Aqui está o Carvalho do Trovão e aqui a cruz de Cristo que romperá o martelo do Thor, o deus falso".

O verdugo levantou um martelo para matar o pequeno menino que tinha sido entregue para o sacrifício. Mas, o Bispo estendeu seu báculo para impedir o golpe e milagrosamente quebrou o grande martelo de pedra e salvou a vida deste menino.

Logo, dizem que Bonifácio disse ao povo:

>Escutai filhos do bosque! O sangue não fluirá esta noite, a não ser que piedade se derrame do peito de uma mãe. Porque esta é a noite em que nasceu Cristo, o Filho do Altíssimo, o Salvador da humanidade. Ele é mais justo que Baldur, maior que Odim, o Sábio, mais gentil do que Freya, o Bom. Desde sua vinda, o sacrifício terminou. A escuridão, Thor, a quem chamaram em vão, é a morte. No profundo das sombras de Niffelheim ele se perdeu para sempre. Desta forma, a partir de agora vocês começarão a viver. Esta árvore sangrenta nunca mais escurecerá sua terra. Em nome de Deus, vou destruí-la.

Então, Bonifácio pegou um machado que estava perto dele e, segundo a tradição, quando o brandiu poderosamente ao carvalho, uma grande rajada de vento atingiu o bosque e derrubou a árvore, inclusive as suas raízes. A árvore caiu no chão, quebrou-se em quatro pedaços.

Depois deste acontecimento, o Santo construiu uma capela com a madeira do carvalho, mas esta história foi muito além das destruições da poderosa árvore.

O "Apóstolo da Alemanha" continuou pregando ao povo alemão que estava assombrado e não podia acreditar que o assassino do Carvalho de Thor não tivesse sido ferido por seu deus. Bonifácio olhou mais à frente onde jazia o carvalho e assinalou um pequeno abeto e disse:

>Esta pequena árvore, este pequeno filho do bosque, será sua árvore santa esta noite. Esta é a madeira da paz…É o sinal de uma vida sem fim, porque suas folhas são sempre verdes. Olhem como as pontas estão dirigidas para o céu. Terá que chamá-lo a árvore do Menino Jesus; reúnam-se em torno dela, não no bosque selvagem, mas em seus lares; ali haverá refúgio e não haverá ações sangrentas, mas presentes amorosos e gestos de bondade.

Desta forma, os alemães começaram uma nova tradição nessa noite, a qual foi estendida até os nossos dias. Ao trazer um abeto a seus lares, decorando-o com velas e ornamentos e ao celebrar o nascimento do Salvador, o Apóstolo da Alemanha e seu rebanho nos mostraram o que hoje conhecemos como a árvore de Natal.

Fonte: [ACI Digital](http://www.acidigital.com/noticias/thor-sao-bonifacio-e-a-origem-da-arvore-de-natal-46046/)