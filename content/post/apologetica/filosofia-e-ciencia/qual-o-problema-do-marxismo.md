+++
date = "2015-04-23T18:00:00-18:00"
title = "Qual o problema com o Marxismo?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/qual-o-problema-do-marxismo.jpg"
+++

Karl Marx não era um ativista político, mas um filósofo que propôs princípios que muitas pessoas atualmente seguem sem perceber e que gerou consequências nefastas no campo militar e cultural.

<!--more-->

Com efeito, muitos não conseguem compreender qual a relação das teorias Marxistas com certas atuações de alguns movimentos sociais que defendem os negros, os homossexuais, os animais, os índígenas, os pobres e assim por diante.

Ora, para que se entenda todo o problema do Marxismo e até onde chegam suas consequências, faz-se necessário descobrir seu fundamento.

É verdade que toda a teoria de Marx e seus sectários partem da observação da atuação humana:

>As premissas com que começamos não são arbitrárias, não são dogmas, são premissas reais, e delas só na imaginação se pode abstrair. São os indivíduos reais, a sua ação e as suas condições materiais de vida, tanto as que encontraram como as que produziram pela sua própria ação. Estas premissas são, portanto, constatáveis de um modo puramente empírico. <cite>Karl Marx, A Ideologia Alemã, I, 2</cite>

Porém, de uma observação limitada desta realidade ele extrai o fundamento de toda sua doutrina e a raiz de todos os seus erros:

>[Os homens] começam a distinguir-se dos animais assim que começam a produzir os seus meios de vida, passo este que é condicionado pela sua organização física. (...) Aquilo que eles são coincide, portanto, com a sua produção, com o que produzem e também com o como produzem. Aquilo que os indivíduos são depende, portanto, das condições materiais da sua produção. <cite>Karl Marx, A Ideologia Alemã, I, 2</cite>

Ora, o que ele afirma é que o que o homem será depende daquilo que ele produziu e de como ele produziu.

Isso é o inverso do que se percebeu na autêntica filosofia.

Com efeito, os gregos e os cristãos perceberam que o homem é capaz de atingir a realidade que o cerca com sua inteligência, sendo isto sua característica distintiva. Assim o que o homem será depende da sua inteligência.

Ademais, o marxismo entende que o pensamento é consequência da ação humana, isto é, o homem pensa do mesmo modo que age:

>Não têm história, não têm desenvolvimento, são os homens que desenvolvem a sua produção material e o seu intercâmbio material que, ao mudarem esta sua realidade, mudam também o seu pensamento e os produtos do seu pensamento. Não é a consciência que determina a vida, é a vida que determina a consciência. <cite>Karl Marx, A Ideologia Alemã, I, 4</cite>

Ora, esta afirmação contradiz a realidade.

As pessoas mudam seu modo de agir e pensar por se convencerem de um erro, como acontece com os convertidos, ou até mesmo sustentam suas convicções numa sociedade que mudou completamente quando estão certas de uma verdade, como vemos no caso dos mártires.

Assim, estas pessoas atestam por si só a falsidade da teoria de Marx.

Muito diferente é a concepção da autêntica filosofia que percebeu que, ao contrário de Marx, o homem age de acordo com o pensamento e acondiciona sua ação através dele.

Por erros como este é que somos capazes de entender o modo pelo qual é possível mudar a ideia das pessoas é, segundo Marx, mudando tudo o que existe de exterior:

>Para o materialista prático, isto é, para o comunista, trata-se de revolucionar o mundo existente, de atacar e transformar na prática as coisas que encontra no mundo. <cite>Karl Marx, A Ideologia Alemã, II, 2</cite>

Diante disto não é difícil perceber o padrão de comportamento existente nos vários movimentos marxistas como o feminismo, o ecologismo, o indigenismo, o movimento negro, o socialismo ou os ideólogos de gênero, entre outros.

Com efeito, estes movimentos trabalham para forçar as pessoas a mudar seu agir na intenção de mudar seu pensar, como vemos na estratégia da discriminação que proíbe as pessoas de criticar algumas condutas com a intenção mudar seu pensamento sobre elas.

Não obstante, deve-se reconhecer que o ambiente exterior pode influenciar fortemente pelo agir humano. Porém, isto não se faz por uma subordinação do pensamento à ação como propõe Marx, mas sim pelo fato da maioria das pessoas se guiarem pelas paixões induzidas pelas coisas exteriores, ao invés de guiarem-se, como um homem sábio, pela inteligência e pelo amor.

A prova de que a inteligência e o amor são mais fortes é que quando elas estão presentes a "práxis" não é capaz de subordiná-las.

Demonstrado erros sutis de Marx, deve-se notar ainda que:

>É impossível que naquilo que no princípio é corrompido e desordenado no fim não ocorra o mal. (...) Embora no princípio pareça tratar-se de uma pequena desordem, todavia depois torna-se grande, e o mal se torna evidente. <cite>Santo Tomás, Comentários à Política de Aristóteles, Livro V, 5</cite>

Portanto, o melhor modo de demonstrar que estes erros apontados realmente existem e são falsos, deve-se demonstrar as consequências de sua aplicação. Isto pode ser demonstrado por três exemplos.

Primeiro. Se o pensamento não constata a realidade, mas apenas segue a prática de cada um, deve-se concluir que a verdade absoluta não existe sobre nada. Portanto, o que ontem era bom, hoje pode ser mal; o que antes era real, amanhã se torna um mito. Assim, não existe mais moralidade, filosofia, ciência e nem nada que seja percebido pelo homem como permanente. Com efeito:

>Pelo que se vê, é a própria concepção da verdade que aqui está em causa e que se encontra totalmente subvertida: não existe verdade, afirma-se, a não ser na e pela práxis partidarista. <cite>Santa Sé, Instrução sobre alguns aspectos da Teologia da Libertação, VIII, 4</cite>

Ora, deste modo as próprias teorias de Marx são inválidas, pois seria verdades absolutas.

Segundo. O amor nasce da uma concepção interna sobre alguém. Ora, se o pensamento nasce da prática do trabalho, conclui-se que na verdade não existe amor, mas apenas interesses produtivos e econômicos.

Esta conclusão foi a que chegou Marx e Engles na obra "A Origem da Família, da Propriedade e do Estado" onde explica que a família originou-se por interesses econômicos do homem.

Porém, isto é falso, pois manifestamente os seres humanos se casam e agem por amor, de modo que constantemente contradigam, em maior ou menor grau, interesses econômicos e produtivos.

Terceiro. A dignidade humana é respeitada conforme é conveniente aos meios de produção. Com efeito:

>De fato, ele considera cada homem simplesmente como um elemento e uma molécula do organismo social, de tal modo que o bem do indivíduo aparece totalmente subordinado ao funcionamento do mecanismo econômico-social. <cite>São João Paulo II, Centesimus Annum, 13</cite>

Ora, isto nos leva a tratar o homem como uma engrenagem que pode e deve ser descartada quando houver uma outra que funcione melhor.

Assim, quando o nascimento de novas pessoas for um empecílio para a economia da sociedade, será lícito recorrer ao aborto ou até mesmo a formas diversas de assassinato.

Portanto, é manifesto que colocar o fim último do homem na produção de seus meios de vida é um erro nefasto que fatalmente levará a consequências que destroem a dignidade do homem, degradam a sociedade e rejeitam o valor da inteligência humana.