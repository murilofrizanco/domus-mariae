+++
date = "2015-04-06T18:00:00-18:00"
title = "A Igreja defendia o geocentrismo como um dogma de fé?"
categories = ["Apologetica","Ciência"]
path = "Ciência"
featured_image = "posts/images/a-igreja-defendia-o-geocentrismo-como-dogma-de-fe.jpg"
+++

Muitos acusam a Igreja da Idade Média de sustentar como certo e indiscutível que a Terra se encontrava no centro do Universo.

<!--more-->

Porém, deve-se notar que esta acusação não possui fundamento na realidade.

Na polêmica com Galileu Galilei, a questão não foi o dado científico, mas a má conduta, falta de provas e a insistência deste suposto cientista, que atualmente tem sua teoria heliocêntrica desmentida, em invadir o campo da teologia e da interpretação das Sagradas Escrituras, coisa que não é a intenção do presente artigo.

Para se entender a sóbria posição dos cristãos neste quesito, faz-se necessário ter uma breve noção sobre as duas visões.

O fundamento do geocentrismo era este.

Aristóteles percebeu que os elementos possuiam uma ordem de modo que a terra sempre era dotada de um movimento natural para o lugar inferior, enquanto que o fogo era dotado de um movimento natural para o lugar superior. Portanto, o fogo e a terra possuíam movimentos naturais opostos.

Ademais, os gregos haviam deduzido que o Universo possuíam um formato circular.

Ora, as únicas lugares que se opõe em um círculo são as extremidades e o centro.

Portanto, se o fogo, retirado qualquer impedimento ao seu movimento natural, tendia para a extremidade, era razoável que o elemento terrestre tenderia para o oposto da extremidade, que é o centro.

Ora, como o movimento do todo não é diferente do movimento da parte, estava demonstrado que a Terra estava no centro do Universo.

Assim, estava concluído, indiscutivelmente, que a Terra teria que estar no centro do Universo.

Ptolomeu aprimorou a teoria Aristotélica e passou a descrever os movimentos dos planetas a partir dela baseado em círculos excêntricos e epiciclos:

Deste modo, o movimento dos planetas era entendido do seguinte modo:

Ora, como a descrição de Ptolomeu era coerente com o movimento aparente dos astros, foi tomado como mais provável.

Ademais, Aristóteles excluiu que a Terra pudesse ser movida por outro corpo, pois isto constituiria um movimento violento que iria contra o que se observava em suas partes.

Ora, como o Filósofo entendia que o Universo tinha que ser eterno e que um movimento violento não pode durar eternamente, concluiu que a Terra não poderia ser movida por outra coisa.

Por sua vez, o Heliocentrismo proposto por Copérnico, que mais tarde inspirou Galileu, postulava o Sol como localizado no centro do Universo e os planetas orbitando em torno deste.

As duas visões foram derrubadas pelas observações modernas que mostraram que o Universo é muito mais complexo do que isso.

Por ocasião do questionamento do geocentrismo, os homens doutos da Igreja reagiram de forma racional e científica diante da controvérsia.

Com efeito, temos o testemunho do ilustre Cardeal São Roberto Belarmino, Geral da Companhia de Jesus e Consultor do Santo Ofício, tido como um dos maiores estudiosos da época:

>Se tivéssemos uma prova verdadeiramente conclusiva de que o Sol está no centro do Universo, de que a Terra está no terceiro Céu e de que o Sol não gira em torno da Terra, mas a Terra em torno do Sol, nesse caso deveríamos proceder com a maior circunspeção, explicando as passagens da Escritura que parecem ensinar o contrário, e admitir que não as compreendemos, em vez de declarar falsa uma opinião que se provou verdadeira. Mas, por minha conta, não acreditarei que haja tais provas até que me deixem vê-las. Dizer que as coisas se passariam do mesmo modo se o Sol se achasse, por hipótese, no centro do Universo e a Terra no terceiro Céu não constitui prova. <cite>Carta de São Roberto Belarmino ao Pe. Foscarini</cite>

Portanto, vemos que o geocentrismo não era tido como "dogma de fé", mas uma a opinião científica mais bem fundamentada para explicar o movimento aparente dos astros.

Ademais, Belarmino deixa claro que as Escrituras devem se conformar com aquilo que é real no Universo.

Além disso, o Cardeal não era um homem ao sabor de modas "científicas", mas um homem que exigia provas indiscutíveis e coerentes antes de aderir a uma opinião científica.

Alguns ainda poderiam objetar que a posição de Belarmino teria sido forjada para que a Igreja não perdesse sua credibilidade e seu prestígio quando seus mitos estavam sendo desmascarados.

Porém, um outro homem doutíssimo, Santo Tomás de Aquino, já no século XII, demonstrava que a visão geocêntrica não era indiscutível para os cristãos, mas era aceita por explicar coerentemente o que se observava sem fechar-se à possibilidade de outras hipóteses:

>Em astronomia é afirmada a razão dos círculos excêntricos e dos epiciclos pelo fato de que esta hipótese, uma vez admitida, pode explicar as aparências sensíveis referentes aos movimentos celestes. Entretanto, esta razão não prova suficientemente, porque talvez os movimentos pudessem ser explicados por outra hipótese. <cite>Santo Tomás de Aquino, Suma Teológica, q. 32, a. 1</cite>

Portanto, o geocentrismo nunca foi dogma da Igreja, nunca foi defendido irracionalmente pelos cristãos e nem nunca foi um exemplo de oposição entre fé e razão.