+++
date = "2015-11-03T18:00:00-18:00"
title = "Qual o problema com a Física de Isaac Newton?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/qual-o-problema-com-a-fisica-de-isaac-newton.jpg"
+++

Os postulados da física newtoniana foram amplamente aceitos como verdade absoluta por pelo menos três séculos. Por causa disto, toda a filosofia moderna, começando com Kant, começa a produzir erros em cima de erros partindo de postulados de Isaac Newton.

<!--more-->

Quanto a isto deve-se notar que não tratamos aqui de refutar ou demonstrar experimentos que tornassem incoerentes os postulados leis de Newton, mas demonstrar os erros filosóficos que este famoso autor cometeu e que trouxe consequências posteriores.

Ademais, deve-se notar que o sistema newtoniano não pretendeu ser um mero modelo matemático de cálculo, mas sim uma síntese filosófica que explicava o funcionamento do Universo, visando substituir o sistema Aristótelico:

>A partir destes princípios da Filosofia, que são as leis e as condições dos movimentos e das forças, e de outros sobre os quais a Filosofia parece baseada, como o espaço vazio dos corpos, pode-se demonstrar o Esquema do Sistema do Mundo, os fenômenos do céu e do mar. <cite>Princípio Matemáticos da Filosofia Natural, Livro III, Introdução e Escólio Geral</cite>

Ademais, deve-se notar que Newton foi vítima da gradual decadência intelectual de sua época que se iniciou com a fundação das Universidades introduzindo a aquisição do diploma, resultando no abandono do estudo pelo conhecimento e pela virtude para dar espaço à busca de um papel que garantia uma carreira.

Um dos exemplos claríssimos de que o próprio Newton foi uma das vítimas está em que ele não percebeu a imaterialidade da própria alma, coisa que foi e é experimentalmente perceptível a qualquer um que desenvolva uma alta capacidade de abstração como possuíam os filósofos gregos e cristãos.

Embora a imaterialidade da alma pareça desinteressar a um estudo para uma síntese física, isto na verdade é falso. Ao passar despercebido a imaterialidade da própria alma, o filósofo termina excluindo completamente de sua visão a possibilidade da existência de entes imateriais e o fato de que as realidades imateriais não estão submetidas às mesmas leis que regem os corpos.

Portanto, a consequência será necessariamente uma síntese sobre o universo inconsistente que não apenas ignora, mas até mesmo tende a negar realidades que procedem da alma, como a consciência, a ética que se baseia na consciência, o direito que se baseia na ética e a sociedade que se baseia no direito.

Ora, estas realidades são completamente reais e evidentes. Porém, um sistema como o de Newton se fecha a possibilidade de compreender ou analisar estas realidades que fazem parte da estrutura do Universo.

Outra consequência de uma capacidade de abstração reduzida foi que Newton acabou introduzindo erros e hipósteses absolutamente impossíveis, coisa que não ocorria na síntese aristotélica.

Porém, o que mais contribuiu para bagunçar o cenário intelectual e científico foi ele ter praticado um método decadente que mais tarde acabaria se popularizando.

Este método foi a aquele segundo o qual postula-se uma hipótese e enquanto aquilo não apresenta uma contradição com uma experiência você admite como verdade certa e vai desenvolvendo todo seu pensamento, sem nem sequer checar antes a sua consistência com os princípios mais abstratos e elementares da lógica.

Com efeito, diante da acusação de Leibniz de que a teoria da gravidade era absurda, Newton assim se comportou:

>Pois como os movimentos celestes seguem-se precisamente, tanto quanto eu esteja ciente, apenas de que a gravidade age de acordo com as leis descritas por mim, eu próprio concluo que todas as outras causas devam ser rejeitadas. Mas se alguém conseguir explicar a gravidade e todas as suas leis pela ação de alguma matéria sutil, eu estarei longe de objetar. <cite>Newton, Carta para Leibniz, 16 outubro 1693</cite>

Portanto, temos um sistema de explicação do Universo que se fundamenta em princípios que o próprio autor terminou sua vida sem ter certeza se eram realmente coerentes ou não respondendo a todas as objeções levantadas.

Não é de surpreender que com este tipo de mentalidade o autor postulasse que o tempo seria absoluto e o espaço também seria absoluto, ou seja, o tempo e o espaço são duas entidades por si mesmas, coisa que já havia sido postulada e derrubada pela filosofia clássica.

Ademais, Newton postulou que o espaço seria infinito, enquanto na Antiguidade já havia sido esclarecido que:

>Nenhum [dos corpos] poderá ser infinito, porque se assim fosse, os outros não poderiam existir, porque o elemento infinito preencheria tudo e os demais se converteriam nele, por causa do excesso de sua virtude. <cite>Santo Tomás de Aquino, Comentários à Física de Aristóteles, Livro XI, 32</cite>

Outro erro que ele colocou foi o princípio da inércia que afirmava que:

>Todo corpo continua em repouso ou em movimento uniforme a menos que seja obrigado a mudar este estado por meio de uma força. <cite>Princípio Matemáticos da Filosofia Natural</cite>

Ora, isto também é absurdo, pois este movimento não existe e é impossível de existir, tanto na teoria quanto na prática.

Na prática qualquer corpo no Universo está circundado de vários outros corpos agindo sobre o mesmo. Isto significa que ele nunca vai estar em movimento retilíneo uniforme, pois para isto teria que estar na ausência completa de forças atuando sobre ele, o que jamais ocorre

Na teoria, este princípio toma como possível que um corpo possa mover-se sem ser movido por outro, pois ele teria um movimento infinito sustentado por si só sem qualquer influência de outro corpo, quando já se havia percebido que:

>É impossível que sob um mesmo aspecto e numa mesma forma algo deva ser movente e movido, isto é, que ele deva mover-se a si mesmo. Então, seja o que for que seja movido, deve ser movido por outro. <cite>Suma Teológica, Ia, q. 2, a. 3</cite>

Outro erro foi tomar como regra absoluta de todos os movimentos do Universo o princípio de ação e reação, segundo o qual um corpo quando exerce uma força sobre um outro recebe uma força igual e contrária.

O fenômeno ocorre entre os corpos, mas ele não é absoluto, pois o primeiro motor (que é imaterial e imóvel), ao mover os outros, não recebe uma reação igual e contrária.

Ora, se este primeiro motor pode fazer isso, pode haver outras situações em que isto não aconteça, como por exemplo o movimento do nosso corpo produzido pela alma.

Por todas estas coisas, é que autores como Bertrand Russel perceberam que:

>Talvez o dissolvente mais poderoso da visão pré-científica [aristotélica] tenha sido a primeira lei do movimento do mundo. <cite>Bertrand Russel, O Impacto da Ciência na Sociedade, Cap. I</cite>

Devido a estas coisas, Newton se tornou relevante para a filosofia ao introduzir erros metafísicos que mais tarde foram absorvidos pela filosofia de Imannuel Kant e ter desenvolvido um método científico degradado em que a lógica e a abstração, que poderiam poupar esforços e eliminar erros, ficaram em segundo plano.

Assim, Newton terminou gerando pressupostos que mais tarde gerariam um acúmulo de erros no campo da filosofia.