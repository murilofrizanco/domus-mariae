+++
date = "2015-09-25T18:00:00-18:00"
title = "A Igreja foi a instituição mais violenta da história?"
categories = ["Apologetica","História"]
path = "História"
featured_image = "posts/images/a-igreja-foi-a-instituicao-mais-violenta-da-historia.jpg"
+++

Os inimigos da Igreja costumam invocar o argumento de que ela promoveu o maior massacre que já existiu na história da humanidade.

<!--more-->

Esta afirmação baseia-se em supostos números de mortos provenientes das cruzadas e das inquisições.

Evidentemente que sobre estes dois temas muitas coisas deveriam ser consideradas: sua motivação, o real papel da Igreja, a punição dos que cometeram abusos, os procedimentos utilizados e assim por diante.

Com efeito, podemos ver, a título de conhecimento, que os dados que se seguem não parecem fazer uma distinção entre os tribunais do Santo Ofício e o Civil, visto que em 250 anos as fontes primárias registram apenas 3 mil sentenças à morte na Inquisição Espanhola por parte das autoridades eclesiásticas.

Portanto, no momento iremos apenas avaliar os números reais que constam em uma pesquisa imparcial, mesmo considerando a falta de distinção entre tribunal civil e eclesiástico.

Todos os dados da tabela podem ser conferidos nesta [pesquisa sobre os democídios](http://www.hawaii.edu/powerkills/20TH.HTM) feitos na história da humanidade.

Uma vez que o período entre os eventos descritos são muito variados, os dados foram ordenados pela quantidade de mortes anuais, permitindo ver quais eventos mataram mais pessoas de modo mais ou menos frenético, permitindo comparar melhor a magnitude dos fatos.

A primeira coisa que chama a atenção é que chama a atenção é o fato das cruzadas e inquisições constarem no final da lista apresentando um número de mortes anuais e totais bem menor que muitos eventos históricos de menor duração.

Para se ter uma comparação com a história atual, apresentamos uma estimativa da quantidade de mortes em um ano em alguns países com diferentes quantidades de população e extensão territorial:

1. [Estados Unidos](http://www.indexmundi.com/): 1.574.100 de mortes em 2014.
2. [Espanha](http://www.indexmundi.com/): 198.858 de mortes em 2014.
3. [Israel](http://www.indexmundi.com/): 45.062 de mortes em 2014.
4. [Abortos na Espanha](http://www.acidigital.com/noticias/em-2014-a-espanha-superara-os-2-milhoes-de-abortos-praticados-desde-1985-75231/): 112.000 em 2012.

Tendo estas estatísticas em mente, podemos perceber que os eventos relacionados com as autoridades eclesiásticas são de pouquíssima magnitude quando comparados com outros e até mesmo com o número de mortes em países desenvolvidos.

Ademais, deve-se notar que o aborto legal, amplamente aceito na sociedade moderna em certas condições, matou em um ano o que a caça às bruxas matou em 300; matou em um ano o que a Inquisição Espanhola matou em 100; matou em um ano o que as cruzadas mataram em 17, lembrando que estas dizem respeito a um combate armado.

Portanto, os dados históricos desmentem que a Igreja operou um extermínio humano com as inquisições e com as cruzadas, além de demonstrar claramente que os regimes socialistas e o aborto representam problemas bem mais críticos.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;border:none;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
.tg .tg-0ord{text-align:right}
.tg .tg-s6z2{text-align:center}
.tg .tg-2od3{background-color:#FCFBE3;font-weight:bold;text-align:right}
.tg .tg-3c54{font-weight:bold;background-color:#ff6969;text-align:center}
.tg .tg-e3zv{font-weight:bold}
.tg .tg-lyaj{background-color:#FCFBE3;text-align:center}
.tg .tg-z2zr{background-color:#FCFBE3}
.tg .tg-gyqc{background-color:#FCFBE3;text-align:right}
.tg .tg-3jhu{background-color:#FCFBE3;font-weight:bold}
.tg .tg-ub8w{background-color:#FCFBE3;font-weight:bold;text-align:center}
.tg .tg-hgcj{font-weight:bold;text-align:center}
.tg .tg-34fq{font-weight:bold;text-align:right}
th.tg-sort-header::-moz-selection { background:transparent; }th.tg-sort-header::selection      { background:transparent; }th.tg-sort-header { cursor:pointer; }table th.tg-sort-header:after {  content:'';  float:right;  margin-top:7px;  border-width:0 4px 4px;  border-style:solid;  border-color:#404040 transparent;  visibility:hidden;  }table th.tg-sort-header:hover:after {  visibility:visible;  }table th.tg-sort-desc:after,table th.tg-sort-asc:after,table th.tg-sort-asc:hover:after {  visibility:visible;  opacity:0.4;  }table th.tg-sort-desc:after {  border-bottom:none;  border-width:4px 4px 0;  }@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
<div class="tg-wrap"><table id="tg-BrHBZ" class="tg">
  <tr>
    <th class="tg-3c54">Local/Causa</th>
    <th class="tg-3c54">Início</th>
    <th class="tg-3c54">Fim</th>
    <th class="tg-3c54">Período</th>
    <th class="tg-3c54">Total de mortes</th>
    <th class="tg-3c54">Mortes por ano</th>
  </tr>
  <tr>
    <td class="tg-z2zr">China (Partido Comunista Chines)</td>
    <td class="tg-z2zr">1949</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">38 anos</td>
    <td class="tg-gyqc">76.702.000,00</td>
    <td class="tg-gyqc">2.018.473,68</td>
  </tr>
  <tr>
    <td class="tg-031e">Nazismo (Nacional-Socialismo)</td>
    <td class="tg-031e">1933</td>
    <td class="tg-031e">1945</td>
    <td class="tg-s6z2">12 anos</td>
    <td class="tg-0ord">20.946.000,00</td>
    <td class="tg-0ord">1.745.500,00</td>
  </tr>
  <tr>
    <td class="tg-z2zr">União Soviética</td>
    <td class="tg-z2zr">1917</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">70 anos</td>
    <td class="tg-gyqc">61.911.000,00</td>
    <td class="tg-gyqc">884.442,86</td>
  </tr>
  <tr>
    <td class="tg-031e">Japão</td>
    <td class="tg-031e">1936</td>
    <td class="tg-031e">1945</td>
    <td class="tg-s6z2">9 anos</td>
    <td class="tg-0ord">5.964.000,00</td>
    <td class="tg-0ord">662.666,67</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Polônia</td>
    <td class="tg-z2zr">1945</td>
    <td class="tg-z2zr">1948</td>
    <td class="tg-lyaj">3 anos</td>
    <td class="tg-gyqc">1.585.000,00</td>
    <td class="tg-gyqc">528.333,33</td>
  </tr>
  <tr>
    <td class="tg-031e">Camboja</td>
    <td class="tg-031e">1975</td>
    <td class="tg-031e">1979</td>
    <td class="tg-s6z2">4 anos</td>
    <td class="tg-0ord">2.035.000,00</td>
    <td class="tg-0ord">508.750,00</td>
  </tr>
  <tr>
    <td class="tg-z2zr">China (Partido Nacionalista Chinês)</td>
    <td class="tg-z2zr">1928</td>
    <td class="tg-z2zr">1949</td>
    <td class="tg-lyaj">21 anos</td>
    <td class="tg-gyqc">10.075.000,00</td>
    <td class="tg-gyqc">479.761,90</td>
  </tr>
  <tr>
    <td class="tg-031e">Revolução Francesa</td>
    <td class="tg-031e">1793</td>
    <td class="tg-031e">1794</td>
    <td class="tg-s6z2">1 anos</td>
    <td class="tg-0ord">263.000,00</td>
    <td class="tg-0ord">263.000,00</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Turquia (Mustafa Kemal Atatürk)</td>
    <td class="tg-z2zr">1919</td>
    <td class="tg-z2zr">1923</td>
    <td class="tg-lyaj">4 anos</td>
    <td class="tg-gyqc">878.000,00</td>
    <td class="tg-gyqc">219.500,00</td>
  </tr>
  <tr>
    <td class="tg-031e">Turquia</td>
    <td class="tg-031e">1909</td>
    <td class="tg-031e">1918</td>
    <td class="tg-s6z2">9 anos</td>
    <td class="tg-0ord">1.883.000,00</td>
    <td class="tg-0ord">209.222,22</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Guerra dos Trinta Anos</td>
    <td class="tg-z2zr">1618</td>
    <td class="tg-z2zr">1648</td>
    <td class="tg-lyaj">30 anos</td>
    <td class="tg-gyqc">5.750.000,00</td>
    <td class="tg-gyqc">191.666,67</td>
  </tr>
  <tr>
    <td class="tg-031e">Mongóis</td>
    <td class="tg-031e">1301</td>
    <td class="tg-031e">1500</td>
    <td class="tg-s6z2">199 anos</td>
    <td class="tg-0ord">29.927.000,00</td>
    <td class="tg-0ord">150.386,93</td>
  </tr>
  <tr>
    <td class="tg-z2zr">China (Soviéticos de Mao)</td>
    <td class="tg-z2zr">1923</td>
    <td class="tg-z2zr">1948</td>
    <td class="tg-lyaj">25 anos</td>
    <td class="tg-gyqc">3.468.000,00</td>
    <td class="tg-gyqc">138.720,00</td>
  </tr>
  <tr>
    <td class="tg-031e">México</td>
    <td class="tg-031e">1900</td>
    <td class="tg-031e">1920</td>
    <td class="tg-s6z2">20 anos</td>
    <td class="tg-0ord">1.417.000,00</td>
    <td class="tg-0ord">70.850,00</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Russia</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-z2zr">1917</td>
    <td class="tg-lyaj">17 anos</td>
    <td class="tg-gyqc">1.065.000,00</td>
    <td class="tg-gyqc">62.647,06</td>
  </tr>
  <tr>
    <td class="tg-031e">Paquistão</td>
    <td class="tg-031e">1958</td>
    <td class="tg-031e">1987</td>
    <td class="tg-s6z2">29 anos</td>
    <td class="tg-0ord">1.503.000,00</td>
    <td class="tg-0ord">51.827,59</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Coréia do Norte</td>
    <td class="tg-z2zr">1948</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">39 anos</td>
    <td class="tg-gyqc">1.663.000,00</td>
    <td class="tg-gyqc">42.641,03</td>
  </tr>
  <tr>
    <td class="tg-031e">Escravatura dos Africanos</td>
    <td class="tg-031e">1451</td>
    <td class="tg-031e">1870</td>
    <td class="tg-s6z2">419 anos</td>
    <td class="tg-0ord">17.267.000,00</td>
    <td class="tg-0ord">41.210,02</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Vietnã</td>
    <td class="tg-z2zr">1945</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">42 anos</td>
    <td class="tg-gyqc">1.670.000,00</td>
    <td class="tg-gyqc">39.761,90</td>
  </tr>
  <tr>
    <td class="tg-031e">Escravatura dos Ameríndios</td>
    <td class="tg-031e">1501</td>
    <td class="tg-031e">1900</td>
    <td class="tg-s6z2">399 anos</td>
    <td class="tg-0ord">13.778.000,00</td>
    <td class="tg-0ord">34.531,33</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Indonesia</td>
    <td class="tg-z2zr">1965</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">22 anos</td>
    <td class="tg-gyqc">729.000,00</td>
    <td class="tg-gyqc">33.136,36</td>
  </tr>
  <tr>
    <td class="tg-031e">Colonialismo</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">1700</td>
    <td class="tg-s6z2">1700 anos</td>
    <td class="tg-0ord">50.000.000,00</td>
    <td class="tg-0ord">29.411,76</td>
  </tr>
  <tr>
    <td class="tg-z2zr">China (Chefes Militares)</td>
    <td class="tg-z2zr">1917</td>
    <td class="tg-z2zr">1949</td>
    <td class="tg-lyaj">32 anos</td>
    <td class="tg-gyqc">910.000,00</td>
    <td class="tg-gyqc">28.437,50</td>
  </tr>
  <tr>
    <td class="tg-031e">Iugoslávia (Tito)</td>
    <td class="tg-031e">1944</td>
    <td class="tg-031e">1987</td>
    <td class="tg-s6z2">43 anos</td>
    <td class="tg-0ord">1.072.000,00</td>
    <td class="tg-0ord">24.930,23</td>
  </tr>
  <tr>
    <td class="tg-z2zr">China (Pré Séc. XX)</td>
    <td class="tg-z2zr">-221</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-lyaj">2121 anos</td>
    <td class="tg-gyqc">33.519.000,00</td>
    <td class="tg-gyqc">15.803,39</td>
  </tr>
  <tr>
    <td class="tg-031e">Portugal (Ditadura)</td>
    <td class="tg-031e">1926</td>
    <td class="tg-031e">1982</td>
    <td class="tg-s6z2">56 anos</td>
    <td class="tg-0ord">741.000,00</td>
    <td class="tg-0ord">13.232,14</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Reino Unido</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-z2zr">1987</td>
    <td class="tg-lyaj">87 anos</td>
    <td class="tg-gyqc">816.000,00</td>
    <td class="tg-gyqc">9.379,31</td>
  </tr>
  <tr>
    <td class="tg-031e">Índia</td>
    <td class="tg-031e">1201</td>
    <td class="tg-031e">1900</td>
    <td class="tg-s6z2">699 anos</td>
    <td class="tg-0ord">4.511.000,00</td>
    <td class="tg-0ord">6.453,51</td>
  </tr>
  <tr>
    <td class="tg-3jhu">Cruzados</td>
    <td class="tg-3jhu">1095</td>
    <td class="tg-3jhu">1272</td>
    <td class="tg-ub8w">177 anos</td>
    <td class="tg-2od3">1.000.000,00</td>
    <td class="tg-2od3">5.649,72</td>
  </tr>
  <tr>
    <td class="tg-e3zv">Cruzada contra os Cátaros</td>
    <td class="tg-e3zv">1208</td>
    <td class="tg-e3zv">1249</td>
    <td class="tg-hgcj">41 anos</td>
    <td class="tg-34fq">200.000,00</td>
    <td class="tg-34fq">4.878,05</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Japão (Pré Séc. XX)</td>
    <td class="tg-z2zr">1570</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-lyaj">330 anos</td>
    <td class="tg-gyqc">1.007.000,00</td>
    <td class="tg-gyqc">3.051,52</td>
  </tr>
  <tr>
    <td class="tg-031e">Império Otomano</td>
    <td class="tg-031e">1101</td>
    <td class="tg-031e">1900</td>
    <td class="tg-s6z2">799 anos</td>
    <td class="tg-0ord">1.500.000,00</td>
    <td class="tg-0ord">1.877,35</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Irã (Pré Séc. XX)</td>
    <td class="tg-z2zr">401</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-lyaj">1499 anos</td>
    <td class="tg-gyqc">2.000.000,00</td>
    <td class="tg-gyqc">1.334,22</td>
  </tr>
  <tr>
    <td class="tg-e3zv">Inquisição Espanhola</td>
    <td class="tg-e3zv">1501</td>
    <td class="tg-e3zv">1800</td>
    <td class="tg-hgcj">299 anos</td>
    <td class="tg-34fq">350.000,00</td>
    <td class="tg-34fq">1.170,57</td>
  </tr>
  <tr>
    <td class="tg-z2zr">Russia (Pré Séc. XX)</td>
    <td class="tg-z2zr">901</td>
    <td class="tg-z2zr">1900</td>
    <td class="tg-lyaj">999 anos</td>
    <td class="tg-gyqc">1.000.000,00</td>
    <td class="tg-gyqc">1.001,00</td>
  </tr>
  <tr>
    <td class="tg-e3zv">Caça às bruxas</td>
    <td class="tg-e3zv">1401</td>
    <td class="tg-e3zv">1700</td>
    <td class="tg-hgcj">299 anos</td>
    <td class="tg-34fq">100.000,00</td>
    <td class="tg-34fq">334,45</td>
  </tr>
</table></div>
<script type="text/javascript" charset="utf-8">var TgTableSort=window.TgTableSort||function(n,t){"use strict";function r(n,t){for(var e=[],o=n.childNodes,i=0;i<o.length;++i){var u=o[i];if("."==t.substring(0,1)){var a=t.substring(1);f(u,a)&&e.push(u)}else u.nodeName.toLowerCase()==t&&e.push(u);var c=r(u,t);e=e.concat(c)}return e}function e(n,t){var e=[],o=r(n,"tr");return o.forEach(function(n){var o=r(n,"td");t>=0&&t<o.length&&e.push(o[t])}),e}function o(n){return n.textContent||n.innerText||""}function i(n){return n.innerHTML||""}function u(n,t){var r=e(n,t);return r.map(o)}function a(n,t){var r=e(n,t);return r.map(i)}function c(n){var t=n.className||"";return t.match(/\S+/g)||[]}function f(n,t){return-1!=c(n).indexOf(t)}function s(n,t){f(n,t)||(n.className+=" "+t)}function d(n,t){if(f(n,t)){var r=c(n),e=r.indexOf(t);r.splice(e,1),n.className=r.join(" ")}}function v(n){d(n,L),d(n,E)}function l(n,t,e){r(n,"."+E).map(v),r(n,"."+L).map(v),e==T?s(t,E):s(t,L)}function g(n){return function(t,r){var e=n*t.str.localeCompare(r.str);return 0==e&&(e=t.index-r.index),e}}function h(n){return function(t,r){var e=+t.str,o=+r.str;return e==o?t.index-r.index:n*(e-o)}}function m(n,t,r){var e=u(n,t),o=e.map(function(n,t){return{str:n,index:t}}),i=e&&-1==e.map(isNaN).indexOf(!0),a=i?h(r):g(r);return o.sort(a),o.map(function(n){return n.index})}function p(n,t,r,o){for(var i=f(o,E)?N:T,u=m(n,r,i),c=0;t>c;++c){var s=e(n,c),d=a(n,c);s.forEach(function(n,t){n.innerHTML=d[u[t]]})}l(n,o,i)}function x(n,t){var r=t.length;t.forEach(function(t,e){t.addEventListener("click",function(){p(n,r,e,t)}),s(t,"tg-sort-header")})}var T=1,N=-1,E="tg-sort-asc",L="tg-sort-desc";return function(t){var e=n.getElementById(t),o=r(e,"tr"),i=o.length>0?r(o[0],"td"):[];0==i.length&&(i=r(o[0],"th"));for(var u=1;u<o.length;++u){var a=r(o[u],"td");if(a.length!=i.length)return}x(e,i)}}(document);document.addEventListener("DOMContentLoaded",function(n){TgTableSort("tg-BrHBZ")});</script>