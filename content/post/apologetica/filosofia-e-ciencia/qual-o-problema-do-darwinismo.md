+++
date = "2014-08-03T18:00:00-18:00"
title = "Qual o problema com o Darwinismo?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/qual-o-problema-do-darwinismo.jpg"
+++

Os princípios da teoria de Charles Darwin são amplamente aceitos pela ciência moderna. Porém, será que ele faz realmente sentido?

<!--more-->

Muitos são os dados que contrastam com a Teoria de Darwin. Porém, não iremos aqui fazer uma análise das provas, mas sim dos princípios, de modo que possamos avaliar a coerência desta teoria somente com a razão natural.

Um dos pilares de Darwin é o princípio que admite a possibilidade de que estruturas e efeitos anteriormente inexistentes possam surgir da mistura de vários elementos, conforme afirma Darwin:

>Por conseguinte, pelo princípio da seleção natural com divergência de caracteres, não parece impossível que os animais e as plantas tenham podido desenvolver-se partindo dessas formas inferiores e intermediárias; ora, se admitirmos este ponto, devemos admitir também que todos os seres organizados que vivem ou que viveram na Terra podem derivar de uma só forma primordial. <cite>A Origem das Espécies, Capítulo XIV</cite>

Em outras palavras, Darwin admite a possibilidade de que novas essências possam surgir através da mistura dos elementos.

Com efeito, aquilo que se chama de "espécie" é a expressão exata e completa de uma essência imutável. Esta essência, por sua vez, é um conjunto de determinações que faz com que algo seja "isto" e não "aquilo".

Deve-se notar que um cachorro é dotado de uma essência e de acidentes. Os acidentes percebemos com os sentidos, como a cor, a textura, a figura, o tamanho, e assim por diante, de modo que estas características possam variar de cachorro para cachorro. Mas a estrutura que é comum a este cachorro e a todos os demais não é algo que percebemos com nossos sentidos, mas com nossa inteligência.

Assim, a estrutura de um cachorro não pode ser mudada sem que imediatamente deixe de ser um cachorro, tornando-o outra coisa. Os acidentes podem mudar mantendo a espécie e nisto os Darwinistas são coerentes se admitem a hipótese de que determinados fatores possam mudar as caracteristicas acidentais.

Ora, se isso é verdade, só haveriam dois modos pelo qual é possível a transição de uma espécie a outra, isto é, de uma essência à outra.

Primeiro. Tomando a essência de um cachorro como algo passível de aumento e decremento.

Isto significa a possibilidade de que um determinado cachorro com o passar do tempo pudesse ser mais ou menos cachorro do que antes.

Ora, isto é manifestamente absurdo, pois não existe essência pela metade, nem algo que seja mais ou menos cachorro que outro. Ou este ser é um cachorro completo ou não o é, não havendo possibilidade de que seja pacialmente um cachorro e parcialmente não.

Esta primeira hipótese foi afirmada por Darwin.

Segundo. Negando a existência das essências imutáveis que determinam a estrutura do que seja um cachorro.

Esta hipótese é tomada como possível por Darwin, consistindo aí seu erro.

Por isso, um grande estudioso afirmou:

>As falsas afirmações de semelhante evolucionismo pelas quais se rechaça tudo o que é absoluto, firme e imutável, vieram abrir o caminho a uma moderna pseudo-filosofia que (...) nega as essências imutáveis das coisas e não se preocupa mais senão com a 'existência' de cada uma delas. <cite>Papa Pio XII, Humanis Generis, n.6</cite>

Com efeito, admitida esta hipótese não se pode nem sequer falar em espécies, pois cada ser será único não possuindo nenhuma estrutura em comum com os demais semelhantes.

Admitido isto, a conclusão é que qualquer coisa pode causar qualquer outra coisa.

Assim, qualquer componente físico é capaz de produzir qualquer ordem, estrutura ou efeito que não estava contido naturalmente nele.

Portanto, seria coerente que novas essências pudessem surgir de uma mistura inédita de circunstâncias diversas, como temperatura, localidade, material genético e assim por diante.

Deve-se notar que esta afirmação contradiz a realidade, pois os elementos físicos não possuem em si mesmos capacidade de produzir as potências de nutrição, dos sentidos ou da inteligência, mas são apenas instrumentos utilizados por uma estrutura superior que não se encontra neles individualmente, mas sim na essência de determinado ser que é gerada naqueles elementos.

É como uma música que não está contida em cada nota em particular, mas sim numa ordem que lhes é imposta extrinsecamente.

O erro de Darwin reviveu princípios apresentados pelo filósofo persa Avicena que já haviam sido demonstrados como falso:

>Avicena ensinava, que todos os animais podem ser gerados por uma certa mistura dos elementos, sem seminação, mesmo por via da natureza. Mas isto é inadmissível, porque a natureza, procedendo nos seus efeitos por modos determinados, os seres naturalmente gerados por seminação não o podem ser sem ela. <cite>Suma Teológica, Ia, q.71, a.1</cite>

Portanto, é admissível aceitar o evolucionismo quando aplicado aos acidentes encontrados nas criaturas. Porém, é absurdo aplicar isto à essência das coisas e formular coerentemente uma teoria de como se originaram as espécies e de como uma espécie pode transitar para a outra.

Por esta razão, até hoje não se encontrou provas empíricas para a teoria darwinista no que diz respeito ao surgimento de novas essências ou da constatação onde houve esta transição, relegando estas coisas meramente à fantasia de seus estudiosos.