+++
date = "2010-11-03T18:00:00-18:00"
title = "Qual o problema com Immanuel Kant?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/qual-o-problema-com-immanuel-kant.jpg"
+++

A filosofia de Immanuel Kant é base de toda a filosofia moderna e parte de uma série de erros.

<!--more-->

Newton apresentou sua metodologia como um novo sistema filosófico para substituir o antigo, conforme [fica evidente em seus escritos](/posts/apologetica/filosofia-e-ciencia/qual-o-problema-com-a-fisica-de-isaac-newton).

Ora, percebendo o alcance que tinha as teses de Newton sob este aspecto, os pensadores iluministas fizeram vasta propaganda de suas teorias como verdades absolutas, usando seu sistema filosófico para fundamentar a disseminação do iluminismo.

Na época de Kant as teses newtonianas desfrutavam da infalibilidade conferida pela propaganda e por esta razão foram tomadas pelo novo filósofo como ponto de partida para sua filosofia. 

Com efeito, acreditava que as teses da física de Newton tinham conseguido se estruturar no mesmo nível de certeza da Geometria, isto é, eram tão certas quanto a soma do ângulos internos de um triângulo resultar sempre em 180°.

Assim, passou a procurar filosoficamente entender como era possível que a mente humana alcançasse um conhecimento tão certo.

Sua história adquire relevância em 1770, próximo da época da Revolução Francesa. Por esta época, ficou vaga uma cátedra na faculdade onde Kant era professor de filosofia segundo os autores de sua época. Por causa disto, houve a necessidade de defender uma tese para a livre docência.

Foi então que escreveu a tese "A forma e os princípios do mundo sensível e intelegível" e a defendeu oralmente.

Sua tese se fundamentava nos conceitos newtonianos de espaço e tempo, isto é, de um espaço que existe por si mesmo e de um tempo que corre independentemente de qualquer movimento.

Assim, defendeu que o espaço e o tempo newtonianos na verdade não existiam, mas eram estruturas da mente humana, ou seja, a mente humana para entender as coisas encaixava toda a realidade nos conceitos de espaço e tempo que, segundo Kant, eram anteriores a qualquer outra percepção. Depois Kant silenciou sobre o assunto meditando em todas as consequências disto tentando entender como seria o Universo e de como era possível reformular toda a filosofia na hipótese de que o espaço e o tempo fossem estruturas da mente e não da realidade. Estrutura pré-formada do espaço e tempo.

12 anos depois ele apareceu com a Crítica da Razão Pura pronta para ser publicada.

Neste livro ele trouxe a ideia de que as ciências se agrupam em três grandes categorias: as ciêcias matemáticas, as ciências físicas e as ciências metafísicas.

As ciência matemáticas são exatas e definitivas. Então na matemática temos a certeza absoluta.

Nas ciências físicas não ocorre assim, pois somente começou a ter certeza absoluta a partir de Newton, que levou estas ciências ao nível da matemática e da geometria. Inclusive por isso se surpreende como a mente humana demorou tanto tempo para perceber uma coisa tão certa e evidente.

Na metafísica, por sua vez, não se tem certeza de nada. Não é possível provar que temos uma alma, que tudo é movido por um primeiro motor, que o Universo funciona de determinado jeito e afirmações semelhantes com a mesma certeza que se tem na matemática.

Então, quero investigar porque acontece isso. Isso certamente não é problema com a matemática, com a física ou com a metafísica, mas com a estrutura da mente humana que concebe tanta certeza na matemática e na física e nenhuma na metafísica.

Para explicar isso ele inventou o seguinte:
A filosofia tradicional dizia que a inteligência humana tem três operações. A primeira é a simples apreensão e é aquela pela qual apreendemos um conceito, por exemplo, o conceito de homem, de casa, de ser, de belo, de triângulo, etc.

Esta simples apreensão não é nem verdadeira nem falsa, é apenas o que é. E é uma operação bastante limitada. Se apenas apreendessemos conceitos, o nosso conhecimento seria fragmentário e não perceberíamos a complexidade de todo o universo.

Em segundo lugar, temos a segunda operação que é o julgamento que ocorre quando a mente humana atribui um conceito a outro conceito. Quando dizemos que o homem é mortal. Que dois mais três é cinco. Que o ser é e não pode não ser. Quando dizemos que um ser não pode ser e não ser ao mesmo tempo sob um mesmo aspecto. Nesta operação que pressupõe o conhecimento do conceito de ser, porque estamos dizendo que algo é tal coisa, o que não pode ser feito sem saber o que é ser, esta operação já pode ser verdadeira ou falsa.

Além dela existe o raciocínio, que justamente assim como o julgamento combina um conceito a outro, o raciocínio passa de um julgamento a outro. E esta operação basicamente é de dois tipos: o raciocínio que se chama silogismo e indução.

No silogismo nós partimos de um julgamento tido como verdadeiro, normalmente tiramos uma conclusão de algo implícito ou contido no primeiro julgamento. COmo quando dizemos que todo homem é mortal, cSócrates é homem, portanto Sócrates é mortal.

Com isto estamos deduzindo para este Sócrates alguma coisa que já estava ao dizer que todo homem é mortal.

O raciocínio só pode ser feito com se conhecermos o conceito de ser e suas propriedades. Não é possível sem conhecer o princípio da não contradição.

O raciocínio é uma maneira de ampliarmos os julgamentos que nós temos que talvez nós não conseguíssimos compreender se apenas fizessemos julgamentos sem silogismos.

Mas existe também a indução que é um outro tipo de raciocínio que partindo de várias premissas chegamos a uma conclusão geral.

Por exemplo: quando vemos que este homem é mortal porque ele morreu, depois vemos que outro eh mortal porque morreu e que o homem vivo é mortal pq posso matá-lo. Assim, muito provavelmente não existe homem imortal. E isto é a indução.

Note que tanto o silogismo como a indução acabam num julgamento. Assim, são modos pelos quais podemos julgar a veracidade de um julgamento ou inferir um julgamento verdadeiro.

Portanto, existem três maneiras de podermos fazer um julgamento verdadeiro. 

O primeiro de uma forma direta que ocorre baseado principalmente nos primeiros princípios do ser que são poucos e auto evidentes.

Outra maneira de chegar a verdade é pelo silogismo onde se extrai um julgamento de dentro de outro mais amplo que conhecíamos. E a indução onde através de vários verificados pela experiência chega a um conhecimento que não poderia chegar de antemão.

Kant se preocupa no começo da crítica da razão pura sobre a questão de como podemos saber que os julgamentos são verdadeiros.

Os julgamentos são verdadeiros pela filosofia clássica ou porque são evidentes por si mesmo, mas neste caso são basicamente os princípios do ser ou os julgamentos onde um está contido no outro e se reduzem ao fato de que o ser é, ou então pela indução quando pela experiência se chega a conclusão de algo desconhecido.

A veracidade de um julgamento depende ou de já conhecer ele antes (a priori) por estar conhecido dentro de outro já conhecido, ou porque o conhece depois (a posteriori) pela experiência através de outros julgamentos que somente a experiência pode validar a conclusão que se tira pela repetição incessante deles.

Neste ponto Kant introduz uma doutrina nova.

Ele diz que é possível que existem julgamentos a priori que não estavam embutidos em nenhum outro conhecimento prévio sem precisar recorrer à experiência.

Com isto não quer dizer que nós nascemos sabendo. Nós não sabíamos neste tipo de julgamento que B se atribui a A. Precisamos examinar e pensar. Porém, ao fazer isso percebemos que apesar de B não estar contido em A ou em nada prévio a A, você percebe ainda assim que apesar da atribuição ser um conhecimento novo, pela natureza de B e de A você consegue enxergar que realmente B pode ser atribuido a A. Ele chama isso de conhecimento a priori, que não estava implícito dentro de outro já conhecido, totalmente extrínseco e a priori porque não precisa de experiência para demonstrar isso.

Porém, esse conhecimento não existe.

Quando você conhece os três modo de julgamento que são os primeiros princípios do ser, o que você conhece é o ser. E ai vc percebe que implícito no ser existem certas propriedades que só podem ser expressas por meio de um julgamento. Quando você percebe o ser está implícito no ser que o ser não pode ser e não ser ao mesmo tempo.

Então este conhecimento não acrescenta algo novo, mas acrescenta algo que estava implícito no que já se conhecia.

Os demais julgamento que se chega a sua veracidade deles pelo silogismo eles revelam a verdade de algo que já estava implícita dentro de algo que você já conhecia. O que se está fazendo é aprofundando os detalhes de algo que você já sabia.

E na indução este realmente se chega a um conhecimento novo, mas este se chega pela experiência.

Não existe a possibilidade de se conhecer algo novo que não estava implícita dentro de outra a não ser pela experiência a posteriori.

Na prática postula no início do lívro que existem três tipos de julgamento:

1. Analíticos a priori analítico: que é o da dedução, que já estava implícito no outro e não necessitam da experiência.
2. Sintéticos a posteriori: quando se faz uma síntese e acrescenta um novo conhecimento, mas necessita da experiência, o que obtemos geralmente pela indução.
3. Sintético a priori: uma espécie de julgamento que não estava implícito no sujeito sem recorrer a nenhuma experiência. Como que uma intuição.

Quando se lêem Kant veem um texto que não permite o leitor perceber que o terceiro tipo de conhecimento nunca foi percebido pela humanidade.

Até a época de Kant a filosofia conhecia apenas os dois primeiros.

Claro que existe um conhecimento infuso, mas este é um conhecimento sobrenatural e não sobrenatural.

Aqui ele não fala de raciocínio, mas apenas de julgamentos. Porém, como todo raciocínio termina em um julgamento, na prática ele elaborou uma teoria geral do conhecimento.

Não é possível encaixar aqui a simples apreensão que somente pode ser explicado mais adiante.

A grande descoberta dele é que os conhecimentos perenes da matemática e da física são conhecimentos sintéticos a priori e ele diz que a estrutura da mente humana prova que é assim.

Ele dá quatro exemplos de julgamentos sintéticos a priori que não existem.

Análise a priori: você pode dizer que A é B porque B já estava contido no B. Assim, conhecendo A, você implicitamente conhecia B.

Sintético a priori: você pode dizer que A é B porque você fez milhões de experiências e viu que sempre A é B. B não estava já implícito no A, mas pela experiência você viu que sim.

Sintético a posteriori: você pode dizer que A é B sem que B esteja implícito no A, ou seja, você já sabe antes que B está dentro do A. Isso não existe.

E passa a dar exemplos desse terceiro.

1) Todas as operações aritiméticas são deste terceiro tipo.

5 + 7 = 12 é uma síntese à priori. O 12 não está embutido no 5 ou no 7, mas é algo totalmente novo, então é sintético. Mas você sabe antes disso que só pode dar 12 sem fazer experiência.

12 é uma coisa, 5 + 7 é outra completamente diferente. Mas você sabe antes que o resultado é doze. Então você está fazendo o terceiro julgamento.

Isso é absurdo, porque 12 está implícito no 5 + 7. Porém, esse erro ele comete por ser nominalista.

Para ele um conceito não é uma ideia abstrata, mas é um simples nome que se atribui a um conjunto de coisas.

Se você é nominalista você vai acabar enxergando que 12 é uma coisa que não está contida dentro de 5 + 7.

Se os conceito fossem meros nomes ele teria razão. Porque se for um mero nome o 12 não está entre os objetos incluídos pelo 5 nem pelo 7. Enquanto que quando se diz homem está incluído neste grupo todos os homens.

Porém, na verdade isso é uma dedução.

2) Ele diz que o fato da linha reta ser a menor distância entre dois pontos é um julgamento sintético a priori

Quando você diz isso, linha reta é uma coisa, enquanto a menor distância entre dois pontos é outra.

Se você diz que a linha reta é a menor distância entre dois pontos e você não precisa fazer uma indução e está acrescentando uma coisa diferente a algo completaente diferente é porque você tinha duas coisas completamente diferentes e juntou as duas coisas sem qualquer experiência.

Isso é absurdo, pois ele não percebeu que neste caso um conceito está no outro.

O conceito de menor distância entre dois pontos está no conceito de linha reta porque a própria definição de linha reta é a menor distância entre dois pontos.

Não se trata de outro conhecimento, mas é o mesmo.

Como que se define uma linha reta? Não podemos dizer que uma linha reta é aquela que é reta.

O modo de definir linha reta é justamente dizer que é a menor distância entre dois pontos.

Inclusive porque existem muitas linhas que são corretamente chamadas de retas e que não são retas, mas simplesmente a menor distância entre dois pontos, mas do ponto de vista prático é uma reta.

Por exemplo, a linha reta entre RJ e BR por causa da curvatura da Terra não é uma linha reta. Se você pegar um carro ou avião e ir em linha reta até brasília você está fazendo uma curva acompanhando a curvatura da terra.

Mas quando pedimos que alguém vá do RJ à Brasília em linha reta iremos pegar uma linha reta que na verdade não é reta por causa da curvatura da terra, mas aquela é reta assim chamada porque é a menor distância entre dois pontos.

Ou seja, num espaço curvo bidimensaional que seria a casca da terra, a linha reta para que vivesse nele somente seria definida e corretamente chamada por causa da própria ideia da menor distância entre dois pontos.

Isto é novamente dedução.

3) A lei de lavoisier que diz que em qualquer transformação física a matéria não se perde nem se cria, mas se conversa e apenas se transforma.

Ele diz que isto é um juízo do terceiro tipo. Mas isto é bobagem porque isto nem sequer é uma verdade necessária.

É simplesmente o resultado de indução, até porque só se sabe isso experimentalmente.

É um juízo sintético a posteriori e que não é obrigado a ser verdadeiro.

4) Todas as leis de Newton

Todas as leis de Newton para Kant são julgamentos sintéticos a priori.

Você não precisa fazer experiência. A própria mente humana já sabe antes que aquilo é verdade absoluta.

