+++
date = "2016-04-26T18:00:00-03:00"
title = "As diferenças entre os sexos"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
+++

Com a controvérsia apresentada pela Ideologia de Gênero quanto as diferenças sexuais serem meras construções culturais, torna-se útil e salutar relembrarmos qual o fundamento das diferenças sexuais e quais são estas diferenças.

<!--more-->

Para tanto, deve-se compreender e aprofundar o que seja a "masculinidade" e o que seja a "feminilidade", o que deve ser feito expondo, inicialmente, a definição destes conceitos:

>**Masculinidade**: é uma qualidade própria segundo a qual o sujeito é ordenado à geração da prole enquanto princípio ativo.

>**Feminilidade**: é uma qualidade própria segundo a qual o sujeito é ordenado à geração da prole enquanto princípio passivo.

Por *qualidade própria* se significa que todos os seres humanos possuem necessariamente esta qualidade. Por *princípio ativo e passivo* pretende-se significar que na geração o homem causa um efeito na mulher e ela, por sua vez, padece um efeito, que é conceber.

Porém, a geração da prole não se limita somente a dar à luz, como pensam alguns:

>A natureza não visa só a geração dos filhos, mas, a criação deles e a sua educação até o estado de homem perfeito, como tal, que é o estado de homem virtuoso. <cite>Suma Teológica, Supl., q. 41, art. 1</cite>

Portanto, a união dos dois sexos não visa somente conceber um filho, mas também constituir uma educação que o torne um homem sábio, santo e virtuoso.

Assim, as diferenças entre homens e mulheres necessariamente irão muito além da mera distinção física. Do sexo de cada um procederá algumas características que influem sobre o ser humano por completo, as quais foram muito bem enumeradas por um sábio dominicano, que transcrevemos abaixo:

>**DOIS SERES DISTINTOS**

>**A) Na ordem psicológica**

>O homem possui uma natureza contraditória, feita para a luta, ambição e fracasso:

>1. O homem procura atuar, avançar, influir, sair de si mesmo. Aspira transformar. Sua psicologia é centrífuga, ativa.
>2. A mente do homem vai ao objetivo. Em seu pensamento domina o conceito. É teorético [voltado para a especulação]. O homem pensa no futuro, não lhe basta o presente.
>3. Em sua vida afetiva predomina o apreço e o desprezo. São próprios do homem os sentimentos objetivos. Se guia pela inteligência. Tenaz e batalhador. A luta é seu elemento de vida. Aspira ao êxito e, frequentemente, desemboca no fracasso. Sua missão é de trabalhador e transformador.

> Nesta ordem, a característica mais representativa da mulher é sua capacidade única de interiorização:

>1. A mulher é receptiva. Não tende a atuar e a buscar, senão a ser buscada e a que atuem sobre ela. É centrípeta [, passiva].
>2. A mulher vai ao subjetivo. Domina nela a imagem. Não gosta de abstrações. Prefere o presente.
>3. Em sua vida afetiva predomina o amor e o ódio. Os sentimentos pessoais. Se guia pelo coração. Ama profundamente a vida. Sua missão no mundo é cuidar.

>**B) Na ordem ética**

>O homem se apresenta revestido das seguintes características:

>1. É capaz do heroísmo, mas não de um heroísmo sem brilho e contínuo. Se irrita diante da dor e declara abertamente luta contra ela.
>2. O homem está dividido em si mesmo. Não lhe basta o a outra pessoa como basta à mulher. Busca mais a luta e o triunfo. A ação.
>3. O amor é somente um parte do homem, não absorve toda sua atividade como na mulher.

>As principais características éticas da mulher são:

>1. Capacidade de sofrimento. A mulher suporta por muito tempo um sofrimento contínuo. Possui grande capacidade de resistência. Sabe esperar.
>2. Relação com o outro. A mulher centra a atenção no ser humano, mais no outro, do que nas coisas. Ama mais o pessoal.
>3. Capacidade de amor e sentimento de humanidade. A mulher possui em seu coração uma pré-disposição ao amor.

>**C) Na ordem religiosa**

>É um fato que a mulher se mostra sempre mais inclinada ao religioso do que o homem:

>1. Mais vinculada às coisas, descobre em sua profundidade seu secreto poder de alusão. Percebe melhor o invisível do que o visível. É mais permeável no espírito. O homem está mais distraído, não sente essa atração secreta.
>2. A lógica férrea do homem encontra um obstáculo no mistério. A mulher, acostumada a viver entre imposições, não tem dificuldade de crer. Nem tem porque pedir razões à fé quem tampouco costuma pedi-las à vida, ao amor, ao homem.
>3. O homem mediante seu trabalho e sua técnica, crê conhecer tudo e, às vezes, pensa que não tem necessidade da fé nem da religião. A mulher, em sua vida simples e sacrificada, está mais preparada para o transcendente.

> Todavia, a postura da mulher é, talvez, mais superficial, menos consistente. O homem religioso o é com toda a alma, com todas as consequências. Sem medidas, profundamente.

>**DOIS SERES COMPLEMENTARES**

>**A) É uma necessidade de suas características diversas e parciais**

>1. Em primeiro lugar, o sexo: esta realidade impulsiona insistentemente o homem e a mulher à união. Ambos buscam esse outro ser que seja ao mesmo tempo semelhante e dessemelhante: complementar.
>2. O homem necessita da mulher: ela lhe dá consciência de si mesmo, lhe afirma em seu ser. O homem faz com que a mulher possua uma personalidade mais plena, mais robusta e equilibrada. Suscita nela as ocultas energias masculinas que latejam em toda mulher.
>3. Se complementam em diversas ordens:
    * No psicológico, a mulher dá ao varão um pouco de interiorização, de uma unidade íntima; o homem dá à mulher um pouco de agilidade e de adaptação ao mundo que a cerca.
    * Na ordem humana, o homem necessita um pouco de espera paciente, decisiva na vida do homem. Dela dependerá, às vezes, que essa vida se apague ou ressuscite. Mas o homem tem também aqui seu papel: das à vida religiosa da mulher mais profundidade, mais seriedade, mais verdade.

>**CONCLUSÃO**

>1. A coordenação entre homem e mulher alcança sua ordenação hierárquica ao culminar na íntima associação matrimonial.
>2. A mulher, enquanto desposada, no nível pessoal, permite ao homem alcançar um domínio do espírito sobre o corpo; através de sua maternidade vive o homem a sua paternidade; através de sua companhia chega o homem à maestria no trabalho.
>3. Homem e mulher, um e outro se apoiam, se aliviam, se consolam, se complementam. Se ordenam um ao outro por natureza. 
<cite>Pe. Antonio Royo Marin, A Espiritualidade dos Leigos, 2ª Seção, Cap. 1, Art. 1</cite>