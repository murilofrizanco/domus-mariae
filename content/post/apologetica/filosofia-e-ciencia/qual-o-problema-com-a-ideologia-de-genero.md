+++
date = "2015-08-15T18:00:00-18:00"
title = "Qual o problema com a Ideologia de Gênero?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/qual-o-problema-com-a-ideologia-de-genero.jpg"
+++

Atualmente vivemos um intenso debate sobre as políticas de gênero em todas as áreas da sociedade. Os defensores afirmam que ela visa apenas combater preconceitos, os criadores dizem outra coisa.

<!--more-->

Em 1995, a ONU, já submissa a esta ideologia, fez uma conferência sobre a mulher. A Plataforma de Ação da Conferência convocou os governos, sem qualquer explicação sobre o que significada isto a "incorporar a perspectiva de gênero em todo programa e em toda política, em cada instituição pública e privada".

Esta ideologia aplica as [teses de Karl Marx](/posts/apologetica/filosofia-e-ciencia/qual-o-problema-do-marxismo) no conflito entre os sexos.

Com efeito, ela entende que a diferença entre os sexos é um instrumento de opressão feminina sendo propagado e sustentado por uma instituição que se chama "família":

>O grande valor da contribuição de Engels para a revolução sexual reside na sua análise do casamento patriarcal e da família. Na submissão do feminino ao masculino, Engels, assim como também Marx, compreenderam o protótipo histórico e conceitual de todos os subseqüentes sistemas de poder, de todas as relações econômicas opressoras e o próprio fato da opressão em si mesmo. <cite>Kate Millet, Política Sexual</cite>

Ora, se o fundamento da perversa instituição familiar, princípio de todas as demais formas de opressão, é a distinção dos sexos, deve-se concluir que:

>Assim como para garantir a eliminação das classes econômicas exige-se a revolta da classe inferior (o proletariado) e a tomada dos meios de produção, assim também, para garantir a eliminação das classes sexuais, exige-se a revolta da classe inferior (as mulheres) e a tomada do controle da reprodução. Isto é, exige-se a restituição, para a mulher, da propriedade dos seus próprios corpos e o controle feminino da sexualidade humana. E assim como o objetivo final da revolução socialista não era apenas a eliminação do privilégio da classe econômica, mas a própria distinção da classe econômica, assim também o objetivo final da revolução feminina deve ser, diversamente do objetivo do primeiro movimento feminista, não apenas a eliminação do privilégio masculino, mas da própria distinção sexual. <cite>Shulamith Firestone, A Dialética do Sexo</cite>

Não obstante, estas teoricas reconhecem a existência da distinção biológica:

>A natureza não é necessariamente um valor humano. A humanidade já começou a superar a natureza; não podemos mais justificar a manutenção de um sistema discriminatório de classes sexuais fundamentadas em sua origem natural. (...) Nossa etapa final deve ser a eliminação das próprias condições da feminilidade e da infância. (...) se nós nos desfizermos da família, iremos de fato desfazer-nos das repressões que moldam a sexualidade em formas específicas. <cite>Shulamith Firestone, A Dialética do Sexo</cite>

Com esta finalidade é que o movimento feminista, somando a estas premissas a teoria descontrutivista, entendeu que o melhor modo de alcançar este objetivo era descontruir as palavras "masculino" e "feminino" e tudo quanto se segue a estes termo, como "família" e "paternidade".

Para tanto, foi criado o termo "gênero" alegando que o papel sexual pode ser assumido pelo indivíduo sem qualquer coerência com sua biologia e que houve uma conspiração universal para impôr os papéis sexuais binários visando oprimir as mulheres, os homossexuais, os hermafroditas e assim por diante:

>A distinção entre sexo e gênero serve ao argumento segundo o qual o gênero é culturalmente construído. Portanto, o gênero não seria nem o resultado causal do sexo nem seria aparentemente fixo como o sexo. Se o gênero são os significados culturais que o corpo sexuado assume, então não se pode dizer absolutamente que o gênero seja conseqüência do sexo. Além disso, mesmo que, em sua morfologia e constituição, os sexos pareçam ser binários (algo que questionaremos mais adiante), não há razão para presumir que os gêneros devam também continuar sendo dois. Quando o status construído do gênero é teorizado como radicalmente independente do sexo, o gênero se torna uma artificialidade livremente flutuante. A conseqüência é que homem e masculino podem facilmente significar tanto um corpo feminino como um corpo masculino, e mulher e feminino podem significar tanto um corpo masculino como um corpo feminino. Se o caráter imutável do sexo for contestado, talvez esta construção chamada ‘sexo’ seja tão culturalmente construída como ‘gênero’; na verdade, talvez ela já tivesse sido sempre ‘gênero’, com a conseqüência de que a distinção entre sexo e gênero termine por não ser distinção alguma. <cite>Judith Butler, O Problema do Gênero: Feminismo e Subversão da Identidade</cite>

Portanto, o grande problema da ideologia de gênero é tentar impôr na sociedade que as diferenças sexuais não produzem diferenças saudáveis nas demais dimensões do ser humano, como se o homem fosse um conjunto de partes desconexas sem qualquer harmonia entre si.

Ora, é evidente que a diferença sexual por natureza produz diferenças necessárias entre homens e mulheres.

Com efeito, embora não haja diferenças no nível espiritual, há várias diferenças entre os seres masculinos e femininos no nível animal, a saber, as físicas, como o homem ser fisicamente mais forte e a mulher apta a engravidar; e as psicológicas, como o homem ter uma inclinação maior a lógica e a mulher ao aspecto emotivo.

Evidentemente que estas diferenças irão naturalmente inclinar cada sexo a determinadas atividades pelo simples fato destas serem mais fáceis e agradáveis para cada sexo, pois aquilo que mais facilmente praticamos, é-nos mais agradável.

Daí que as mulheres possuem um maior interesse em relacionar-se socialmente, em cuidar e educar as crianças, em lidar com várias situações simultaneamente, enquanto o homem tem maior interesse em trabalhos servis, em lutar, em combater e proteger sua família e sacrificar-se e assim por diante.

Deve-se notar que a existência de ambos os sexos só tem sentido por causa desta distinção, caso contrário poderíamos exterminar todos os homens ou todas as mulheres sem causar qualquer prejuízo à sociedade humana.

Por isso, o gênero não apenas nega a distinção sexual, mas, por consequência, despreza na sociedade toda contribuição que somente as mulheres podem oferecer.

Por outro lado, a reta filosofia entende que a mulher tem um papel insubstituível na perfeição do universo, da sociedade e da natureza humana que não pode ser descartado nem suprido pelos homens.

Com efeito, sua contribuição insubstituível é justamente na obra da geração que ocorre dentro da família.

Porém, deve entender-se que não se trata de simplesmente "parir filhos" ou ser um "instrumento de procriação" dos homens como as feministas pensam. Esta obra inclui três coisas impossíveis de serem realizadas com máxima perfeição sem o auxílio da mulher: gerar um novo ser humano, sustentá-lo e educá-lo.

No entanto, deve-se notar que educar não é ensinar um trabalho como hoje se concebe ou deixar o filho na escola, mas levar aquele novo ser humano ao máximo desenvolvimento intelectual e ao cultivo da excelência das virtudes, o que somente é possível na estrutura familiar, alvo desta ideologia

Por isso, diz um grande ensaista:

>As mulheres não foram mantidas em casa para que fossem limitadas, foram mantidas em casa para que fossem amplas. O mundo do lado de fora é uma massa de limitações, um labirinto de caminhos estreitos, um hospício de monomaníacos. (...) Como pode ser uma larga carreira ensinar a Regra de Três a crianças dos outros, e uma estreita carreira ensinar à sua própria criança sobre o universo? Como pode ser amplo ensinar a mesma coisa a todo mundo, e estreito ensinar tudo a alguém? Não; a função de uma mulher é trabalhosa, mas porque é gigantesca, não porque é minúscula. <cite>G. K. Chesterton, Capítulo III do livro "O que está errado com o mundo"</cite>

Demonstrado o erro, faz-se necessário demonstrar que esta ideologia não tem nada a ver com o combate ao preconceito, coisa que é admitida por seus próprios promotores:

>A cultura gay/lésbica pode também ser vista como uma força subversiva, capaz desafiar a natureza hegemônica da ideia de família. Isso deve, contudo, ser feito de modo que as pessoas não percebam o estamos fazendo por oposição à família em si mesma. Um simples slogan no sentido de ‘esmagar a família’ pode ser visto como uma ameaça, não apenas para a classe dominante, mas também para as pessoas da classe operária. Para que a natureza subversiva da cultura gay seja usada com eficiência, temos que apresentar modos alternativos de compreender as relações humanas. <cite>Christine Riddiough, Presidente da Comissão Feminista de Socialistas Democratas da América</cite>

>A igualdade feminista radical significa não simplesmente igualdade sob a lei e nem sequer igual satisfação das necessidades básicas, mas sim que as mulheres - da mesma forma que os homens - não precisem dar à luz... A destruição da família biológica que Freud jamais vislumbrou permitirá a emergência de mulheres e homens novos, diferentes daqueles que existiram anteriormente (...) O fim da família biológica eliminará também a necessidade da repressão sexual. (...) até as categorias de homossexualidade e heterossexualidade serão abandonadas: a própria 'instituição das relações sexuais', em que o homem e a mulher desempenham um papel bem definido, desaparecerá. A humanidade poderá reverter, finalmente, a sua sexualidade polimorfamente perversa natural. <cite>Alisson Jaggar, Political Philosophies of Women's Liberation: Feminism and Philosophy, pg. 38</cite>

Assim, discutir a Ideologia de Gênero não é uma discussão sobre se devemos ou não combater a violência e a discriminação a determinados grupos, mas sim uma discussão de se o único meio de fazer isto é acolhendo ideias que pretendem destruir a instituição da família e desprezar a saudável e harmoniosa complementariedade entre os dois sexos.