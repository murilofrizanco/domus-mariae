+++
date = "2015-11-11T18:00:00-18:00"
title = "Como se origina a orientação sexual?"
categories = ["Apologetica","Filosofia"]
path = "Filosofia"
featured_image = "posts/images/como-se-origina-a-orientacao-sexual.jpg"
+++

Dale O'Leary sintetiza as três teorias sobre a origem da homossexualidade que atualmente são apresentados nos debates sobre as políticas públicas e que também são utilizadas no que diz respeito às questões de "gênero" e "orientação sexual".

<!--more-->

Consta em sua obra a seguinte síntese:

>Segundo a teoria inata, as pessoas nascem homossexuais ou heterossexuais, e não há nada que possa ser feito sobre isto. A homossexualidade é, portanto, natural e igual à heterossexualidade.

>Segundo a teoria polimórfica, os seres humanos nascem sem nenhuma orientação sexual e são capazes de ser atraídos por ambos os sexos. O sexo de cada parceiro é irrelevante. As pessoas que sustentam este ponto de vista acreditam que as categorias da homossexualidade, heterossexualidade e bissexualidade deveriam ser abandonadas, de tal maneira que os seres humanos possam reverter a uma "sexualidade natural polimorficamente pervertida".

>Segundo a teoria traumática, a homossexualidade e o lesbianismo são causados por traumas psicológicos durante a infância, pela rejeição do pai de mesmo sexo, por abusos sexuais ou por uma combinação de fatores. A orientação homossexual e lésbica é vista como semelhante à dependência às drogas ou ao alcoolismo. <cite>Dale O’Leary, A Agenda de Gênero</cite>

Ora, a primeira hipótese é completamente absurda. Isto se demonstra por duas razões.

A primeira razão é observável na experiência pessoal. Com efeito, muitas são as pessoas que sentem uma inclinação homossexual. Porém, ao desenvolver uma visão da sexualidade orientada para a formação de uma verdadeira família ao ponto deste ideal causar admiração e alegria (é o que se chama de virtude da castidade), uma consequência necessária é corrigir, com o passar do tempo, seu apetite pelo sexo oposto de modo que este desapareça e até sua lembrança provoque tristeza.

Isto se deve ao fato de que um bem mais amplo, nobre e completo atrai com maior força o apetite humano quando percebido pelo intelecto do que um bem imperfeito e mais limitado que não possui o que o outro possui. Ora, é evidente que o prazer sexual, admiração pela beleza do mesmo sexo e satisfação de certas fantasias homossexuais é nada perto da numerosa quantidade de bens presentes num matrimônio natural e virtuoso aberto à alegria da geração.

A segunda razão é segundo a própria natureza humana. Com efeito, a finalidade natural da atividade sexual é a conservação da espécie. O prazer não é unicamente característica da atividade sexual, mas de diversas outras atividades, como comer, brincar, conversar, conhecer e assim por diante.

Ora, a atividade sexual só atinge seu fim específico com a união dos dois sexos. Portanto, o coito com o mesmo sexo não procede como algo natural, mas como impedimento do efeito próprio da atividade sexual.

Ademais, não é possível que o que não acontece sempre ou na maioria dos casos proceda pela natureza. Contudo, deve-se notar que aqui o termo "natureza" é tomado no sentido segundo o qual é aquilo que determinada que um indivíduo seja um animal e não um objeto inanimado. Assim, fica evidente que é completamente impossível que a homossexualidade proceda da natureza humana, visto que acontece na minoria dos casos.

Sobre a segunda teoria, segundo a qual o homem nasce igualmente apto a todas as formas de sexualidade, deve-se dizer que aparenta ser verdadeira, mas na realidade não é.

Com efeito, ela nega que a natureza humana desempenhe o papel principal na determinação da orientação sexual do indivíduo, o que é manifestamente falso quando vemos que, mesmo diante de desenvolvimentos sexuais deficientes, as pessoas determinadamente possuem uma estrutura seja masculina, seja feminina.

Embora seja verdade que cada indivíduo possa acabar determinando sua orientação sexual, isso não significa que a natureza não induza mais fortemente nele uma orientação sexual específica conforme sua constituição física.

Sobre a terceira teoria, segundo a qual a orientação sexual proceda de uma experiência traumática, deve-se dizer que é incompleta e insuficiente, embora também aparente ser verdadeira.

Com efeito, vemos pessoas que não sofreram abusos ou experiências traumáticas caírem em atos homossexuais ou até desenvolverem atração sexual por amizades que envolvem carícias e familiaridades indevidas. Isto é algo que o próprio Santo Afonso adverte sobre o amor indevido entre os rapazes em sua obra prima sobre a castidade:

>Ainda que seja o amor a fonte de todo o bem, não deixa de ser igualmente a fonte de todo o mal. Não falo do amor impuro, que deve ser evitado em todo o caso, mas da inclinação, em si inocente, que facilmente pode degenerar em amor desordenado. O trato muito assíduo com outro, com protestos de afeição, tem por consequência tornar nocivo o amor, visto que ele prende estreitamente um coração ao outro, obscurecendo a afeição crescente cada vez mais a razão. Em pouco tempo só quererá um o que o outro quer, e então não terá mais coragem de resistir ao outro quando for convidado ao mal, e, assim, se perderão ambos. <cite>Santo Afonso citando Santa Ângela, Tratado sobre a Castidade, IV</cite>

>Alguns começaram com uma afeição aparentemente santa, continua ele, mas pouco a pouco precipitou-os o demônio num lodaçal de vícios os mais abomináveis. <cite>Santo Afonso citando São Basílio, Tratado sobre a Castidade, IV</cite>

Portanto, é completamente possível e esperado que uma pessoa que não tinha esta inclinação venha adquirir tal inclinação como consequência de um trato indevido com o mesmo sexo.

Porém, se a natureza humana inclina o homem à procriação de modo que a estrutura dos dois sexos seja completamente coerente e benéfica em muitos aspectos, como se explica que alguns indivíduos possam ter seu prazer nas práticas homossexuais ao ponto de preferí-las, se são contra a natureza e nenhuma das teorias anteriores é válida?

Quem responde satisfatoriamente a esta pergunta é o grande Doutor Angélico, colocando as devidas distinções sobre a questão:

>A natureza no homem pode ser considerada à dupla luz. ― Primeiro, enquanto o intelecto e a razão constituem, por excelência, a natureza humana, e esta é que o coloca numa determinada espécie. E a esta luz, podem-se chamar prazeres naturais aos homens aqueles que lhes convêm de conformidade com a razão; assim, é natural ao homem deleitar-se com a contemplação da verdade e com os atos virtuosos. ― Num segundo ponto de vista, a natureza no homem é aquilo que confina com a razão, isto é, que lhe é comum com os animais, e sobretudo que não obedece à razão. E a esta luz o que diz respeito à conservação do corpo, individualmente, como, a comida, a bebida, o sono e coisas semelhantes; ou, especificamente, como a atividade sexual, tudo isso é considerado como naturalmente deleitável ao homem.

>Ora, conforme esses dois pontos de vista, podem certos prazeres serem inaturais, absolutamente, mas conaturais, relativamente. Pois, pode suceder que, num determinado indivíduo, venha a corresponder-se algum dos princípios naturais da espécie, e então, o que seria contrário à natureza da espécie, pode acidentalmente ser natural a esse indivíduo; assim, é natural que a água quente aqueça. Por onde, pode dar-se que aquilo que é contra a natureza do homem, relativamente à razão ou à conservação do corpo, se torne conatural a um determinado homem por causa de alguma corrupção existente na natureza do mesmo. E esta corrupção pode provir do corpo ou da alma. Quanto ao corpo, de alguma doença ― assim, os febricitantes acham amargo o doce e reciprocamente; ou da má compleição ― assim, há quem se deleite comendo terra, carvão ou coisas semelhantes. Quanto à alma, quando alguém, por costume, se deleita em comer carne humana; no coito bestial ou com indivíduos do mesmo sexo; ou em coisas semelhantes, que não são conforme à natureza humana. <cite>Suma Teológica, Ia IIae, q. 31, a. 7</cite>

Disto resulta que o indivíduo pode, e irá, acostumar-se a operar o vício sodomítico ao ponto daquilo lhe parecer como que natural, se este costumeiramente buscar ou consentir em pensamentos, palavras e atos próprios de um homossexual.

Isto é manifesto quando vemos que a ira segue a mesma regra. Com efeito, algumas pessoas são, por causa da compleição física, mais ou menos propensas à ira. Porém, a determinação segundo a qual esta ira será aplicada para matar este ou aquele indivíduo depende do costume de considerar o alvo amigo ou inimigo e não de uma determinação natural para operar a morte de uma determinada pessoa.

Ora, com o desejo do prazer ocorre o mesmo. Todos o tem pela natureza, seja mais ou menos intensamente. Porém, a busca e a escolha dos meios de alcançar tal prazer dependem do modo como nos acostumamos a alcançá-lo com o concurso da razão.

A causa disto não é puramente corporal, segundo Santo Tomás, mas uma questão moral justamente pelo fato de proceder da razão humana que, por não apreender o bem completo da sexualidade e por consentir costumeiramente em determinadas práticas ou fantasias, toma um bem incompleto como melhor ou equivalente a um completo.

Resta ainda esclarecer um ponto: porque alguns homossexuais não conseguem sentir-se atraído pelo sexo oposto, embora tentem? Ora, pelo que foi dito fica evidente que ele poderá sim sentir essa atração. Porém, supôr que ela virá imediatamente é um engano e uma exigência que induz ao erro. O costume se forma com o tempo e com atos repetitivos da inteligência e da vontade. Portanto, isto irá acontecer no seu tempo devido, assim como o antigo também levou tempo, provavelmente anos, para se formar.

O mesmo ocorre com um fornicador na adolescência. Quando se casar, não irá corrigir imediatamente seu apetite pelo sexo fora do casamento e inclusive pode cair em vários adultérios. Somente com o passar do tempo e um sério empenho em buscar a virtude é que este costume antigo irá se corromper e dar lugar ao novo. 

Assim, o desejo sexual procede em todos pela natureza em maior ou menor grau, mas a determinação dos meios ou modos como este bem será atingido depende da ordenação da razão.