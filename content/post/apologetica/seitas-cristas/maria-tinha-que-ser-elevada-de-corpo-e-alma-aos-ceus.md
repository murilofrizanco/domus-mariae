+++
date = "2015-07-01T18:00:00-18:00"
title = "Maria tinha que ser elevada de corpo e alma aos céus?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/maria-tinha-que-ser-elevada-de-corpo-e-alma-aos-ceus.jpg"
+++

Em 1950 o Papa Pio XII proclamou solenemente como dogma a assunção de Maria, o que significa uma atestação solene e uma confirmação da fé dos fiéis naquilo que em todas as épocas sempre creram. Mas será que isto realmente tem algum fundamento ou seria mera cultura piedosa?

<!--more-->

Com efeito, embora as Escrituras se calem sobre sua morte, nem por isso ficamos impossibilitados de conhecer a verdade:

>Antes de falar do santíssimo corpo da perpétua virgem e da Assunção de sua alma sagrada, digamos primeiro que a Escritura não se refere a ela depois que o Senhor na cruz recomendou-a ao discípulo, a não ser aquilo que Lucas relata nos Atos dos apóstolos: "Todos perseveravam, unanimemente, na prece com Maria, mãe de Jesus".Que dizer então de sua morte? Que dizer de sua Assunção? Já que a Escritura se cala, deve-se pedir à razão que nos guie para a verdade. Portanto, que a verdade seja nossa autoridade, pois sem ela sequer há autoridade. Baseados no conhecimento da condição humana é que não hesitamos em dizer que ela sofreu morte temporal, mas se dizemos que ela foi alimento da podridão, dos vermes e da cinza, devemos considerar se esse estado convém à sua santidade e às prerrogativas desta casa de Deus. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Existem três razões que provam que Maria necessariamente tinha que ser elevada aos céus de corpo e alma.

Primeiro. A unidade de Cristo e de Maria na carne.

Com efeito, a carne de Cristo não veio de si mesmo e nem foi criada do nada, mas foi doada ao Salvador pelo corpo da Virgem.

isto significa que a carne do Menino-Deus não procedeu de nenhum homem, mas somente de Maria. Sendo assim, toda a carne humana que há em Cristo Jesus nada mais é que uma parte do corpo puríssimo desta Mulher que lhe foi concedido sem mistura com a carne de qualquer outro ser humano.

Ora, quando somos gerados a nossa carne é uma mistura da carne de nossos pais. Assim, não temos totalmente a mesma carne em comum com eles, pois trata-se do resultado de uma mistura onde temos uma herança genética incompleta de cada um.

Com Cristo foi diferente, pois ele recebeu todo seu corpo unicamente de sua Mãe. Portanto, a carne de Cristo, para todos os efeitos, era inteiramente a carne da própria Virgem de tal modo que o corpo do Filho era da mesma natureza da carne puríssima de sua Mãe, com a única diferença de estar unida a uma outra pessoa. Por isso, é dito:

>A carne de Jesus é a carne de Maria, que Ele elevou acima dos astros, honrando com isso toda a natureza humana, mas sobretudo a de sua mãe. Se o filho tem a natureza da mãe, é conveniente que a mãe possua a natureza do filho, não quanto à unidade da pessoa, mas quanto à unidade da natureza corporal. Se a graça pode fazer que haja unidade sem que haja comunidade de natureza, com mais razão quando há unidade na graça e no nascimento corporal. Há unidade de graça, como a dos discípulos com Cristo. Ele mesmo diz: "A fim de que eles sejam um como nós somos um", ou, em outro lugar: "Meu pai, quero que eles estejam comigo em todo lugar que eu estiver". Se Ele quer ter consigo aqueles que, reunidos pela fé, formam com Ele uma mesma pessoa, que dizer em relação à sua mãe, cujo lugar digno para estar só pode ser em presença de seu filho? Tanto quanto posso compreender, tanto quanto posso crer, a alma de Maria é honrada por seu filho com uma prerrogativa ainda superior, já que ela possui em Cristo o corpo desse filho que ela gerou com os caracteres da glória. E por que esse corpo não seria o seu, já que ela o concebeu? <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Ora, se Cristo afirma formar espiritualmente uma só pessoa com os seus discípulos apesar das diferentes naturezas que possuem, com muito mais razão devemos considerar que Ele forma, além dessa unidade pela graça, uma unidade de natureza com a Virgem Santíssima, isto é, ambos possuem totalmente a mesma natureza corporal.

Sendo assim, os privilégios de Cristo segundo a carne devem ser igualmente aplicados ao corpo da Virgem, pois pertencem à mesma natureza.

Assim, se o corpo de Cristo foi gerado com privilégios de glória, isto significa que os mesmos deveriam estar presentes no corpo da Virgem.

Portanto, era necessário que aquele santíssimo corpo virginal fosse preservado da corrupção, dos vermes e das cinzas, como foi o de seu Filho.

>Já que a natureza humana está condenada à podridão e aos vermes, e que Jesus foi poupado desse ultraje, a natureza de Maria também está imune a isso, pois foi nela que Jesus assumiu a sua natureza. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Segundo. A dignidade de Maria Santíssima.

Com efeito, devemos considerar quão precioso era o corpo da Virgem Santíssima, pois ele foi a matéria na qual o Verbo se encarnou. Além disso, acrescenta-se quão maior não era dignidade da própria pessoa da Virgem por causa de sua maternidade singular.

Quanto a dignidade de seu corpo Agostinho faz a seguinte consideração que dispensa comentário:

>O trono de Deus, o leito do esposo, a casa do Senhor e o tabernáculo de Cristo têm o direito de estar onde Ele próprio está. O Céu é mais digno que a Terra de conservar tão precioso tesouro. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Porém, deve-se considerar que muito mais digno que seu corpo é a sua própria pessoa, por sua dignidade materna.

Ora, Cristo cumpriu a Lei mais perfeitamente que todos os homens. Ademais, a Lei tinha como quarto mandamento honrar pai e mãe. 

Disto se segue que Cristo necessariamente honrou seu pai e sua mãe tributando-lhe mais honra do que qualquer criatura já recebeu de seus filhos de tal modo que deveria conceder a sua mãe mais benefícios do que aos demais santos e aos anjos.

>Não cabia à bondade do Senhor conservar a honra de sua mãe, pois Ele viera não para destruir a lei, mas para cumpri-la? Se Ele a honrou durante sua vida mais que a qualquer outra pessoa, pela graça que lhe fez de o conceber, é ato piedoso crer que a honrou também em sua morte com uma preservação particular e uma graça especial. A podridão e os vermes são a vergonha da condição humana, e se Jesus esteve isento desse opróbrio, Maria também, já que Jesus nasceu dela. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Com efeito, a podridão e os vermes são uma vergonha para o homem e Cristo, que honrou maximamente sua mãe, não poderia permitir tal injúria. Por isso, o grande tradutor das Escrituras diz:

>Aquele que disse: Honre teu pai e tua mãe e Não vim destruir a lei, mas cumpri-la, certamente honrou sua mãe acima de todas as coisas, e por isso não duvidamos que o mesmo aconteceu com a bem-aventurada Maria. (...) Essa festa [da Assunção], que acontece apenas uma vez por ano para nós, é ininterrupta nos Céus, com o próprio Salvador estando com ela durante toda a festa e colocando-a com alegria junto dele no trono. Se fosse diferente, não teria cumprido sua própria lei que diz: Honre seu pai e sua mãe. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando São Jerônimo</cite>

Alguns poderiam pensar que seria possível honrá-la acima das demais criaturas sem conceder-lhe o dom da glória da ressurreição.

Quanto a isto, deve-se notar que os anjos são mais dignos que os homens. Porém, na Saudação Angélica vemos um anjo reconhecer reverenciar um homem, isto é, Maria. Ora, só reverenciamos aqueles de condição superior à nossa. Portanto, se o arcanjo Gabriel reverenciou a Virgem é porque esta é mais digna que ele.

Ademais, Deus concedeu aos anjos o dom da glória que somente chegará plenamente aos homens na consumação dos séculos.

Assim, se Cristo devia mais honra a sua Mãe por causa de sua dignidade materna e concedeu aos anjos a bem-aventurança na glória, deve-se admitir que era necessário que concedesse tal benefício à Virgem para que não acontecesse que os anjos recebessem maior honra do que Maria Santíssima e o cumprimento do quarto mandamento fosse imperfeito.

Portanto, era necessário que Cristo elevasse sua mãe aos céus de corpo e alma.

Terceiro. A integridade sobrenatural do corpo virginal de Maria.

Quanto a isto, deve-se notar que desde o primeiro instante, por uma graça singular, seu corpo foi preservado ileso.

Com efeito, a virgindade perpétua da Santíssima Virgem significa que, mesmo após o parto, milagrosamente ela não perdeu sua virgindade, isto é, seu hímen permaneceu intacto mesmo após o nascimento de Cristo.

Ademais, Deus concedeu graças semelhante em diversas situações à pessoas de condição inferior a de sua Mãe:

>Se a divina vontade escolheu manter intactas no meio das chamas as vestes das crianças, por que não preservaria as de sua própria mãe? A misericórdia que quis manter Jonas vivo no ventre da baleia não concederia a Maria a graça da incorrupção? Daniel foi preservado apesar da grande fome dos leões, e Maria não teria sido conservada pelos tantos méritos que a dignificavam? Portanto, reconhecendo que tudo quanto dissemos ocorreu contra as leis da natureza, não podemos duvidar de que a integridade de Maria deveu-se mais à graça que à natureza. Cristo, como filho de Maria, fez com que a alegria dela decorresse da alma e do corpo de seu próprio filho, que não a submeteu ao suplício da corrupção para dar à luz íntegra, sempre incorrupta, cheia de graça, e vivendo integralmente porque gerou aquele que é a vida íntegra de todos. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Além disso, consta nas Escrituras que Cristo concedeu a ressurreição à Lázaro.

Assim, considerando que Deus deveria conservar mais o corpo de sua Mãe do que o de outras pessoas e que o fez prodigiosamente como atesta sua virgindade preservada, deve-se concluir que Deus não iria ter tanto cuidado com um corpo para que depois fosse entregue à podridão em um sepulcro.

Por isso, dizia o bispo de Hipona:

>A pena da corrupção não deve ser conhecida por aquela que não teve sua integridade corrompida quando gerou seu filho. Será sempre incorrupta aquela que foi cumulada de tantas graças, que viveu íntegra, que gerou vida em total e perfeita integridade, que deve ficar junto daquele a quem carregou em seu útero, a quem gerou, aqueceu, nutriu - Maria, mãe de Deus, nutriz e escrava de Deus. Por tudo isso não ouso pensar de outra maneira, seria presunção dizer diferentemente. <cite>Legenda Áurea, Beato Jacoppo de Varazze citando Santo Agostinho</cite>

Com as razões expostas, deve-se reconhecer que não há passagens das Escrituras que explicitamente afirmam estas coisas, mas seu conjunto o faz.

Com efeito, o Apóstolo Paulo diz sobre o Antigo Testamento:

>Ora todas estas coisas lhes aconteciam, em figura, e foram escritas para advertência de (todos) nós, para quem o fim dos séculos chegou. <cite>Vulgata Latina, Primeira Epístola aos Coríntios, 10, 11</cite>

Assim, quando olhamos o Antigo Testamento vemos com muita facilidade a assunção de Maria em várias passagens.

Com efeito, consta nas Escrituras uma exortação para que Nosso Senhor Ressuscitado entre no céu junto com a sua Arca:

>Levanta-te, Iahweh, para o teu repouso, tu e a arca da tua força. <cite>Bíblia de Jerusalém, Salmo 131, 8</cite>

Ora, Maria foi a Arca da Nova Aliança, a Arca que portou dentro de si a própria santificação, a Arca da Nova Lei.

Assim, a arca da força de Deus não pode ser outra coisa que Maria, visto que a força de Deus é o próprio Cristo.

Ademais, consta também a presença de uma dama à direita do Rei em posse de seu Reino:

>Entre as tuas amadas estão as filhas do rei; à tua direita uma dama, ornada com ouro de Ofir. <cite>Bíblia de Jerusalém, Salmo 44, 10</cite>

Ora, o Rei é uma prefiguração de Cristo. Portanto, a dama só pode ser a sua mãe.

Alguns poderiam alegar que se trata da Igreja. Porém, mesmo que assim fosse nossa interpretação seria coerente, pois Maria é imagem perfeita da Igreja.

Ademais, no Novo Testamento consta a presença da Arca nos céus e a mãe do Cristo que se encontra no céu:

>O templo de Deus que está no céu se abriu, e apareceu no templo a arca da sua aliança. Houve relâmpagos, vozes, trovões, terremotos e uma grande tempestade de granizo. Um sinal grandioso apareceu no céu: uma Mulher vestida com o sol, tendo a lua sob os pés e sobre a cabeça uma coroa de doze estrelas. <cite>Bíblia de Jerusalém, Apocalipse de São João, 11,19-12,1/cite>

Ora, a Arca presente no céu é a Virgem. Por isso, proclama um grande devoto:

>No sol há cor e esplendor estáveis; na lua só resplendor completamente incerto e mutável, pois nunca permanece no mesmo estado. Com razão, pois, Maria é apresentada vestida de sol, já que ela penetrou o profundo abismo da sabedoria divina para além de quanto pudesse crer-se. <cite>São Bernardo de Claraval, De B. Virgine, 2</cite>

Sendo assim, vemos que as Sagradas Escrituras demonstram com clareza o fato de que, seja por figura, seja em seu conjunto, a Virgem realmente foi assunta aos céus de corpo e alma de forma que isto decorra necessariamente, embora não haja um relato histórico do evento.