+++
date = "2015-06-17T18:00:00-18:00"
title = "Maria pode ser chamada de Mãe de Deus?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/maria-pode-ser-chamada-de-mae-de-deus.jpg"
+++

Todos admitem que Maria é mãe de Jesus, mas muitos negam a ela o título de Mãe de Deus. Seria realmente coerente conceder esta denominação à Virgem?

<!--more-->

Para se entender isto é necessário ter uma noção básica sobre o mistério da Encarnação do Verbo.

Deve-se considerar que Cristo é uma única pessoa que possui duas naturezas. Isto significa que quando perguntamos "o que é Jesus", a resposta é "totalmente Deus e ao mesmo tempo totalmente homem"; por outro lado, quando perguntamos "quem é Jesus", a resposta é "ele é a segunda pessoa da Santíssima Trindade". Entendido isto, pode-se proceder ao tema em questão.

Os que se opõe ao título mariano apresentam basicamente três argumentos para negar tal denominação.

Primeiro. Alguns alegam que Maria não é chamada explicitamente deste modo em nenhum lugar nas Escrituras.

Quanto a isto, deve-se responder que as Escrituras não podem ser lidas e interpretadas por versículos ou frases isolados, mas em todo seu conjunto completo sem deixar nenhum versículo de lado.

Ora, se fizermos isto concluiremos que a Virgem é Mãe de Deus, pois:

>Embora não achemos propriamente dito na Escritura, que a Santa Virgem fosse a Mãe de Deus, nela achamos porém expressamente, que Jesus Cristo é verdadeiro Deus; e que a Santa Virgem é a Mãe de Jesus Cristo. Ora, dessas palavras da Escritura se segue necessariamente, que é a Mãe de Deus. <cite>Santo Tomás de Aquino, Suma Teológica, IIIa, q. 35, a. 4</cite>

Portanto, não há um versículo que afirma esta denominação, mas sim o conjunto inteiro das Escrituras.

Todavia, alguns dividem o Verbo de Deus ao meio tentando dizer que Maria é mãe de Jesus, mas não é mãe do Verbo.

Porém, esta distinção é completamente alheia à Revelação. Com efeito, deve-se notar que o Novo Testamento não faz esta divisão entre Cristo e Deus, mas trata ambos como uma so pessoa:

>[Os judeus,] dos quais descende o Cristo, segundo a carne, que é acima de tudo, Deus bendito pelos séculos <cite>Bíblia de Jerusalém, Epístola aos Romanos, 9, 5</cite>

Ora, depreende-se desta passagem que o "Cristo, segundo a carne" é o próprio "Deus bendito".

Se as Escrituras dizem que o Deus Altíssimo é descendente dos judeus, não há qualquer incoerência dizer que o Deus Altíssimo é Filho da Virgem e ela, por sua vez, Mãe de Deus.

Ademais, acrescenta-se a isto uma outra passagem:

>Donde me vem que a mãe do meu Senhor me visite? <cite>Bíblia de Jerusalém, Evangelho segundo São Lucas 1, 43</cite>

Com efeito, Cristo, segundo a carne, não tem qualquer relação humana de senhorio para com Santa Isabel, restando apenas a relação de Criador e criatura entre os dois. Ora, ela o trata como Senhor, reconhecendo sua divindade, assim como também trata Maria de Mãe do Senhor.

Portanto, temos um testemunho claro em que Maria é chamada de Mãe de Deus, pois o senhorio dizia respeito à divindade de Cristo.

Segundo. Alguns afirmam que Maria não gerou a natureza divina e por isso não pode ser Mãe de Deus.

Quanto a isto, deve-se notar que o título "Mãe de Deus" para a religião cristã não significa que a Virgem gerou a natureza divina de Cristo, pois esta procede apenas do Pai.

Com efeito, quando se diz que Maria é Mãe de Deus, não se está afirmando que ela gerou a divindade ou que a pessoa divina do Verbo recebeu dela sua existência.

Portanto, o que se pretende dizer é que Maria, por ter gerado aquela pessoa na carne, tem uma verdadeira e real relação de maternidade com a pessoa divina do Verbo, isto é, ela é verdadeiramente Mãe da segunda pessoa da Trindade, pois Cristo e o Verbo não são duas pessoas distintas, mas a mesma e única pessoa. Pois, se o Verbo e Cristo fossem duas pessoas, então Jesus teria duas personalidades, o que é absurdo.

Ademais, lemos num escrito sapientíssimo do mártir Santo Inácio a demonstração de que se Maria não fosse Mãe de Deus, então nenhuma mulher na terra seria mãe de homem algum:

>É óbvio que na geração do homem em geral a mulher é chamada mãe; ainda que a mulher não lhe dê a alma racional, que vem de Deus, mas ela dá a substância para a formação do corpo. Mas, apesar disto, a mulher é chamada de mãe do homem inteiro, porque o que foi tomado dela está unido à alma racional. Semelhantemente, uma vez que a humanidade de Cristo foi recebida da Virgem Santíssima, tendo em conta que está unida à divindade, a Virgem Santíssima é chamada não apenas mãe do homem, mas também de Deus; embora a divindade não tenha sido recebida do mesmo modo que a alma racional não é recebida da mãe. <cite>Santo Tomás de Aquino, Comentários ao Evangelho de Mateus, Capítulo 1, Versículos 12-21</cite>

Terceiro. Alguns entendem que "Mãe de Deus" significa "Mãe da Santíssima Trindade". Com efeito, o termo "Deus" é um nome pelo qual designamos simultaneamente o Pai e o Filho e o Espírito Santo. Ora, Maria não é Mãe do Pai, nem do Espírito Santo. Portanto, não pode ser Mãe de Deus.

Embora o raciocínio esteja correto, deve-se notar que a palavra "Deus" possui um duplo significado. Ora significa a Santíssima Trindade inteira, ora significa apenas uma das três pessoas.

Do primeiro modo, Maria não é e nem poderia ser Mãe de Deus. Porém, do segundo modo, quando a palavra "Deus" significar somente o Verbo, então ela pode, sem erro, ser dita Mãe de Deus, conforme atesta o Doutor Angélico:

>O nome de Deus, embora comum às três Pessoas, contudo é suposto umas vezes só pela pessoa do Pai: e outras, só pela pessoa do Filho ou do Espírito Santo, como se estabeleceu. E assim, quando dizemos que a Santa Virgem é Mãe de Deus, o nome de Deus é suposto pela pessoa encarnada do Filho. <cite>Santo Tomás de Aquino, Suma Teológica, IIIa, q. 35, a. 4</cite>

Que a palavra "Deus" seja usada em alguns momentos para significar somente uma das três Pessoas é um uso que existe nas próprias Sagradas Escrituras:

>Ele é a Imagem do Deus invisível, o Primogênito de toda criatura <cite>Bíblia de Jerusalém, Epístola aos Colossenses, 1, 15</cite>

Ora, sem dúvida que nesta passagem a palavra Deus não significa a Trindade, mas a Pessoa do Pai, da qual o Filho é imagem.

Outro exemplo é na citação da Epístola aos Romanos (Rm 9,5), onde fala-se que Cristo é "Deus bendito", sendo que a palavra Deus diz respeito ao Filho.

Assim, por estas razões, a Igreja pôde, com a divina assistência do Espírito Santo, proclamar no Concílio de Éfeso que:

>Quem não confessar, que Deus é verdadeiramente o Emanuel e que, por isso, a Santa Virgem é a Mãe de Deus, por ter gerado corporalmente o Verbo de Deus feito carne, seja anátema <cite>Santo Tomás de Aquino, Suma Teológica, IIIa, q. 35, a. 4</cite>

Que Deus possa iluminar a todos os que se aprofundam nas Sagradas Escrituras a profundidade de seus textos e a coerência de todos seus ensinamentos.