+++
date = "2014-11-07T18:00:00-18:00"
title = "As Sagradas Escrituras afirmam que Cristo é Deus?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/as-sagradas-escrituras-afirmam-que-cristo-e-deus.jpg"
+++

Algumas seitas cristãs, como as Testemunhas de Jeová, alegam que Cristo não é Deus, pois isto não consta nas Sagradas Escrituras. Porém será mesmo que isso é verdade?

<!--more-->

Quanto a isto, deve-se notar que a maior prova da divindade de Cristo é que ele se comportava como Deus, coisa abundantemente testemunhada pelas Sagradas Escrituras.

Com efeito, demonstra ter autoridade de promulgar e revogar a Lei dada por Deus através de Moisés quando diz: "Moisés disse isso, mas eu vos digo".

Ademais, as Escrituras atestam que Cristo tem o poder de dar a vida tanto para si, como para os demais:

>Ninguém a tira de mim [a vida], mas eu a dou de mim mesmo e tenho o poder de a dar, como tenho o poder de a reassumir. Tal é a ordem que recebi de meu Pai. <cite>Evangelho segundo São João 10, 18</cite>

Ora, um mortal não pode ter este poder, pois somente Deus é Senhor da vida e da morte.

Contudo, além de afirmar que tem o poder sobre a vida e a morte, afirma: "Eu lhes dou a vida eterna" (Jo 10, 28), isto é, que pode dá-la para sempre, livrando o homem da morte.

Ademais, Cristo claramente coloca-se em pé de igualdade com o Pai quando diz: "Eu e o Pai somos um" (Jo 10, 30).

Alguns entendem que isso seria mera figura de linguagem. Todavia, deve-se notar que os judeus pegaram em pedras para atirar em Jesus dizendo que:

>Não é por causa de alguma boa obra que te queremos apedrejar, mas por uma blasfêmia, porque, sendo homem, te fazes Deus. <cite>Evangelho segundo São João 10, 33</cite>

Reação que tem a seguinte resposta de Cristo:

>... saibais e reconheçais que o Pai está em mim e eu no Pai. <cite>Evangelho segundo São João 10, 38</cite>

Ademais, Cristo afirma claramente que existiu desde toda eternidade:

>Abraão, vosso pai, exultou com o pensamento de ver o meu dia. Viu-o e ficou cheio de alegria. Os judeus lhe disseram: Não tens ainda cinquenta anos e viste Abraão!... Respondeu-lhes Jesus: Em verdade, em verdade vos digo: antes que Abraão fosse, eu sou. <cite>Evangelho segundo São João 8, 56-58</cite>

Ora, uma pessoa não poderia ter vivido desde a época de Abraão até a de Jesus, a menos que fosse eterna.

Portanto, o próprio Messias afirma sua eternidade.

Ademais, os Evangelhos atestam a indignação de Cristo diante do pedido do Apóstolo Filipe para que lhes mostrasse o Pai:

>Se me conhecêsseis, também certamente conheceríeis meu Pai; desde agora já o conheceis, pois o tendes visto. Disse-lhe Filipe: Senhor, mostra-nos o Pai e isso nos basta. Respondeu Jesus: Há tanto tempo que estou convosco e não me conheceste, Filipe! Aquele que me viu, viu também o Pai. Como, pois, dizes: Mostra-nos o Pai... Não credes que estou no Pai, e que o Pai está em mim? As palavras que vos digo não as digo de mim mesmo; mas o Pai, que permanece em mim, é que realiza as suas próprias obras. Crede-me: estou no Pai, e o Pai em mim. Crede-o ao menos por causa destas obras. <cite>Evangelho segundo São João 17, 7-11</cite>

Ademais, o Verbo Encarnado atesta que vivia com o Pai:

>Saí do Pai e vim ao mundo. Agora deixo o mundo e volto para junto do Pai. <cite>Evangelho segundo São João 16, 28</cite>

Ora, se Cristo estava no Pai, só pode significar que ele é parte de Deus.

Ademais, o Apóstolo São João afirma:

>No princípio era o Verbo, e o Verbo estava junto de Deus e o Verbo era Deus. Ele estava no princípio junto de Deus. Tudo foi feito por ele, e sem ele nada foi feito. (...) E o Verbo se fez carne e habitou entre nós, e vimos sua glória, a glória que o Filho único recebe do seu Pai, cheio de graça e de verdade. <cite>Evangelho segundo São João 1, 1-3; 14</cite>

Ora, o Evangelho diz que o Verbo era Deus. Portanto, o Verbo que se fez carne é Deus em pessoa.

Ademais, o Apóstolo São Tomá diz ao Cristo quando toca suas chagas: "Meu Senhor e meu Deus!" (Jo 20, 28).

Ademais, o Apóstolo São Paulo, como que complementando o Apóstolo João, é quem diz mais claramente que:

>Ele é a Imagem do Deus invisível, o Primogênito de toda criatura, porque nele foram criadas todas as coisas, nos céus e na terra, as visíveis e as invisíveis: Tronos, Soberanias, Principados, Autoridades, tudo foi criado por ele e para ele. Ele é antes de tudo e tudo nele subsiste. Ele é a Cabeça da Igreja, que é o seu Corpo. Ele é o Princípio, o Primogênito dos mortos, (tendo em tudo a primazia), pois nele aprouve a Deus fazer habitar toda a Plenitude e reconciliar por ele e para ele todos os seres, os da terra e os dos céus, realizando a paz pelo sangue da sua cruz. <cite>Epístola aos Colossenses 1, 15-20</cite>

Assim, é evidente que as Sagradas Escrituras testemunham claramente a divindade de Cristo e calam estas afirmações espúrias que rebaixam o Messias a um ser humano.

Dai-nos vossa Sabedoria, à Diviníssima e Santíssima Trindade, para que possamos conhecê-la, adentrar neste mistério e contemplar a beleza do Pai e do Filho e do Espírito Santo.