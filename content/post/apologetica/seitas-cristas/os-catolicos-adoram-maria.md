+++
date = "2015-09-18T18:00:00-18:00"
title = "Os católicos adoram Maria?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/os-catolicos-adoram-maria.jpg"
+++

Uma das acusações mais frequentes por parte de seitas protestantes é que os católicos cometem idolatria ao venerar Maria. Será é nisto que consiste a devoção mariana?

<!--more-->

Com efeito, já houveram grupos que realmente prestavam um culto idolátrico a Maria Santíssima, conforme testemunho um bispo do século IV:

>Outra seita que veio a público depois desta, e eu já mencionei algumas coisas sobre ela na seita precedente, na carta sobre Maria que eu escrevi à Arábia. Esta, novamente, foi levada à Arábia pela Trácia e Scítia, e sua notícia chegou até mim; é ridícula e, na opinião dos sábios, totalmente absurda. Passemos, então, a discussão e descrição dela; assim como as outras, também será considerada tola ao invés de sábia. A muito tempo, aqueles que, por uma atitude insolente diante de Maria, pareceram suspeitar destas coisas, foram semeando suspeitas danosas na mente das pessoas, assim, as pessoas que se inclinaram na outra direção são culpadas de causar maior dano. Neles a máxima dos filósofos pagãos de que "os extremos são iguais", será exemplificada. Ora, o dano feito por estas duas seitas foi igual, uma vez que uma desmerece a Santíssima Virgem, enquanto a outra, por sua vez, a glorifica em excesso. E quem, senão as mulheres, ensinaram isto? Mulheres são instáveis, inclinadas ao erro, e malvadas. <cite>Santo Epifânio de Salamina, Panarion, Contra os Coliridianos, que faziam ofertas à Maria, 1</cite>

Portanto, é verdade que já existiram seitas que fazem exatamente o que os protestantes acusam os católicos. Por isso, Epifânio descreve em detalhes as práticas desta seita:

>Como em nosso capítulo anterior sobre Quintila, Maximila e Priscila, aqui o demônio parece ter vomitado ensinamentos ridículos pela boca das mulheres. Com efeito, certas mulheres enfeitavam cadeiras ou assentos quadrados, deitavam uma roupa sobre ele, apresentavam pães e ofereciam em nome de Maria em um certo dia do ano, e todas tomavam parte na ceia - como foi parcialmente discutido em minha carta à Arábia. Agora, portanto, devo falar claramente sobre isto e, com a ajuda de Deus, oferecer as melhores refutações que eu puder, de modo que sejam  arrancadas as raízes desta seita idolátrica e com a ajuda de Deus, ser capaz de curar certas pessoas de sua loucura. <cite>Santo Epifânio de Salamina, Panarion, Contra os Coliridianos, que faziam ofertas à Maria, 1</cite>

Porém, deve-se notar que uma coisa é idolatrar, outra é venerar. A doutrina católica proíbe a primeira e propaga a segunda, conforme explica o Doutor Angélico:

>Sendo a latria devida só a Deus, não o é à criatura; não o é, no sentido em que a venerássemos em si mesma. Mas, embora as criaturas insensíveis não sejam em si mesmas dignas de veneração, a criatura racional o é. Por isso, não devemos o culto de latria a nenhuma criatura racional como tal. Sendo, porém, a bem-aventurada Virgem uma criatura racional, em si mesma, não lhe devemos a adoração de latria, mas só a veneração de dulia. De maneira mais eminente, contudo, que às outras criaturas, por ser Mãe de Deus. E por isso dizemos que lhe é devida, não qualquer dulia, mas a hiperdulia. <cite>Suma Teológica, IIIa, q. 25, art. 5</cite>

Assim, é manifesto que a Igreja jamais ensinou a idolatria à Maria, mas se empenhou em condená-la quando surgiu entre os fiéis. Contudo, isso não significa que devamos condenar a justa veneração que é devida à Virgem, motivo pelo qual a Igreja fomenta a devoção mariana entre os fiéis.

Por isso, Santo Epifânio declarava, ao mesmo tempo, uma condenação da idolatria mariana e a legitimação da veneração da Mãe de Deus:

>Que ninguém coma deste erro que foi levantado sobre Maria Santíssima. Ainda que a árvore seja "amável", não é para comer; e ainda que Maria seja toda justa, santa e digna de honra, ela não deve ser adorada. <cite>Santo Epifânio de Salamina, Panarion, Contra os Coliridianos, que faziam ofertas à Maria, 7</cite>

>Maria deve ser honrada, mas o Senhor deve ser adorado! <cite>Santo Epifânio de Salamina, Panarion, Contra os Coliridianos, que faziam ofertas à Maria, 9</cite>