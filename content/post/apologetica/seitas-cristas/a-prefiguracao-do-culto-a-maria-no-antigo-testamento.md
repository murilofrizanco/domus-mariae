+++
date = "2014-10-22T18:00:00-18:00"
title = "A prefiguração do culto à Maria no Antigo Testamento"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/a-prefiguracao-do-culto-a-maria-no-antigo-testamento.jpg"
+++

Muitas são as acusações de que os que prestam culto de dulia à Virgem Santíssima estão praticando a idolatria. Porém, será que o ato de dulia é uma adoração? As Escrituras demonstrariam que não?

<!--more-->

Em primeiro lugar, temos que entender que dulia é "prestar honra à alguém por sua excelência", enquanto que latria é prestar culto à Deus. A prática da dulia pode ser testemunhada em vários textos das Escrituras, porém aqui nos deteremos em um em específico.

É muito curioso que, entre os livros pertencentes às Sagradas Escrituras rejeitados pelo protestantismo, esteja incluído justamente o que testemunha e atesta de forma mais clara e manifesta a prestação de honras à uma mulher por causa de sua excelência e a reta adoração à Deus acompanhada no mesmo ato, sem que ambos se confundam.

Com efeito, no livro de Judite, que foi reiteradamente confirmado como canônico pela Igreja primitiva, existe uma passagem em que ela retorna à sua casa com a cabeça do rei dos assírios que assolava seu povo.

Com a posse do troféu da vitória, os judeus levantaram grande louvor:

>Todo o povo ficou extasiado e, inclinando-se, adorou a Deus, dizendo a uma só voz: "Bendito sejas, ó nosso Deus, que hoje aniquilaste os inimigos de teu povo! <cite>Livro de Judite, 13, 17</cite>

Ora, a grande excelência do feito daquela mulher despertou uma profunda disposição de adorar o único e verdadeiro Deus por causa do que havia ocorrido. Isto é exatamente a disposição católica em relação à Virgem Maria no que diz respeito à sua virtude e missão de cooperar na Encarnação do Verbo.

Para que isto se torne mais evidente, deve-se notar que o relato vai mais longe e atesta um louvor dirigido diretamente a própria Judite por causa de suas obras:

>Ozias, então, disse a Judite: "Bendita sejas, filha, pelo Deus altíssimo, mais que todas as mulheres da terra, e bendito seja o Senhor Deus, Criador do céu e da terra, que te conduziu para cortar a cabeça do chefe dos nossos inimigos. Jamais tua confiança se afastará do coração dos homens, que recordarão para sempre o poder de Deus. Faça Deus com que sejas exaltada para sempre, que te visite com seus bens, pois que não poupaste tua vida por causa da humilhação de nossa raça, mas vieste em socorro de nosso abatimento, caminhando, retamente, diante de nosso Deus." Todo o povo respondeu: "Amém! Amém! <cite>Livro de Judite, 13, 18-20</cite>

Esta passagem é uma clara prefiguração das palavras que mais tarde serão dirigidas à Maria Santíssima pelos lábios de Santa Isabel: "Bendita és tu entre todas as mulheres" (Lc 1, 42).

Além disto, as Escrituras atestam um ousado pedido de Ozias: "Faça Deus com que sejas exaltada para sempre".

Como não reconhecer a semelhança entre este louvor à Judite e os louvores levantados para a digníssima Maria, Mãe de Deus? Como não lembrar de todas aquelas piedosas honras que com coração jubiloso se tributa à tão terna auxiliadora?

Com efeito, se uma mulher como Judite mereceu receber honras deste tipo sem que o povo caísse em idolatria, muito mais a Virgem Santíssima merece tais benefícios.

Porém, deve-se notar que o louvor à Judite não se limita a palavras, mas chega até mesmo a uma ato de veneração corporal:

>... prostrou-se aos pés de Judite, saudou-a profundamente e disse: "Bendita sejas tu em todas as tendas de Judá e entre todos os povos; os que ouvirem teu nome ficarão inquietos". <cite>Livro de Judite, 14, 7</cite>

Por ventura, não é a mesma coisa que os católicos fazem para com a digníssima Mãe de Deus? Como não perceber a semelhança destas honras com aquelas prestadas ao Vaso Honorífico e à Arca da Nova Aliança?

A prostração é exatamente um dos argumentos mais utilizados para afirmar que a dulia é na verdade idolatria. Assim, se isto fosse coerente a prostração diante de Judite deveria ser uma ofensa ao Criador. Porém, o que lemos mais adiante no relato é justamente que ass honras prestadas à Judite não apenas não roubaram nada do Deus Altíssimo, mas fomentaram um verdadeira gratidão e reconhecimento que o que aquela mulher fez foi devido à Providência Divina a ponto de converter Aquior:

>Aquior, vendo tudo o que fizera o Deus de Israel, acreditou firmemente nele, circuncidou sua carne e foi acolhido, definitivamente, na casa de Israel. <cite>Livro de Judite, 14, 10</cite>

Assim, é manifesto que aquilo não foi um ato de idolatria, pois a idolatria não reconhece Deus como primeiro princípio das obras realizadas e muito menos provoca a conversão de um pecador.

Ademais, curiosamente a prática da devoção mariana é anterior a própria retirada do Livro de Judite do cânon bíblico, tanto que até mesmo os reformadores protestantes nutriam uma profunda veneração pela Virgem Santíssima.

Com efeito, lemos algumas passagens interessantes vindas dos primeiros reformadores protestantes:

>Maria não quer ser um ídolo; não é Ela que faz, é Deus que faz todas as coisas. Deve ser invocada para que Deus, por meio da vontade dela, faça aquilo que pedimos; assim devem ser invocados também todos os outros santos, dei­xando que a obra seja inteiramente de Deus. <cite>Martinho Lutero, Maria Mãe dos homens, pp.574-575</cite>

>Não podemos reconhecer as bênçãos que nos trouxe Jesus, sem reconhecer ao mesmo tempo quão imensamente Deus honrou e enriqueceu Maria, ao escolhê-la para Mãe de Deus. <cite>João Calvino, Comm. Sur l‟Harm. Evang.”,20</cite>

>Estimo grandemente a Mãe de Deus, a virgem Maria perpetuamente casta e imaculada. <cite>Ulrico Zwinglio, Corpus Reformatorum: Zwingli Opera 2,189</cite>

Portanto, constata-se a legitimidade da veneração prestada à Virgem Maria através das próprias Sagradas Escrituras, as quais constituem grande fonte de certeza e segurança para nossa prática espiritual.