+++
date = "2014-06-26T18:00:00-18:00"
title = "Onde Jesus disse isso nas Escrituras?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/onde-jesus-disse-isso-nas-escrituras.jpg"
+++

No debate com protestantes existe uma insistência muito grande por parte deles contra ensinamentos que não provêm diretamente e explicitamente das Sagradas Escrituras.

<!--more-->

Porém, quanto a isto deve-se notar que existe mais de uma regra de fé:

>Há então oito regras de fé: as Escrituras, a Tradição, a Igreja, os Concílios, os Padres, o Papa, os milagres e a razão natural <cite>São Francisco de Sales</cite>

Diante disto, é evidente que existem várias fontes, além da palavra pessoalmente e sonoramente dita por Jesus, às quais devemos obediência.

Portanto, quando um protestante pergunta: "Onde Jesus disse isso nas Escrituras?" e rejeita as demais regras da fé, ele na verdade está indo contra a própria Sagrada Escritura.

Com efeito, estas pessoas são semelhantes àqueles fariseus que se apegaram à letra da Lei e não conseguiram encontrar um sentido mais profundo nos ensinamentos ali contidos.

Ademais, com um estudo sério e honesto podemos ler na Bíblia que os próprio discípulos não seguiam somente as decisões de Jesus, mas também a dos apóstolos e anciãos:

>Nas cidades pelas quais passavam, ensinavam que observassem as decisões que haviam sido tomadas pelos apóstolos e anciãos em Jerusalém. Assim as igrejas eram confirmadas na fé, e cresciam em número dia a dia. <cite>Atos dos Apóstolos 16, 4s</cite>

Assim, rejeitar as demais regras de fé, expostas acima, seria transgredir e contradizer as próprias Escrituras.

Com efeito, encontramos na boca do Mestre uma sentença importantíssima:

>Quem vos ouve, a mim ouve; e quem vos rejeita, a mim rejeita; e quem me rejeita, rejeita aquele que me enviou. <cite>Evangelho segundo Lucas 16, 10</cite>

Ora, rejeitar o ensinamento de um só discípulo seria rejeitar o próprio Cristo e consequentemente o próprio Deus.

Portanto, amordaçam o Cristo aquele que rejeita os seus discípulos e a sua Igreja.

Se a rejeição de um único discípulo é rejeitar o próprio Cristo em pessoa, imagina como não é grande a rejeição por Cristo feita por aqueles que rejeitam uma multidão de discípulos, inocentes, mártires, patriarcas, profetas, pontífices, confessores, doutores, sacerdotes, monges, eremitas, virgens, viúvas e famílias que nos precederam.

Deve-se notar que a maioria dos protestantes não o fazem com má intenção, mas por ignorância.

Porém, isso não os exime da obrigação de um estudo sério, honesto e completo das Sagradas Escrituras, do que ensinavam seus tradutores e também os discípulos pelos menos até os anos 300 onde ainda não se tinha um consenso mais claro à respeito dos livros canônicos.

Assim, rejeitar estes discípulos católicos é rejeitar as próprias Sagradas Escrituras, pois a mesma foi traduzida por membros de seu Corpo Místico.

Ademais, se somente as Escrituras bastassem por si mesmas, então os próprios protestantes deveriam fechar suas igrejas e demitir todos os seu pastores e missionários, destruir suas comunidades e ficar somente com as Escrituras, pois tudo isso seria supérfluo.

Deve-se reconhecer que o cristianismo verdadeiro, diferente do islamismo, não procede de um livro e reconhecer que somos uma Igreja nascida do peito aberto de Cristo que é levada ao mundo pelo testemunho apostólico de geração em geração.

Como o Sagrado Coração de Jesus e o Imaculado Coração de Maria estariam em festa se os protestantes se detivessem realmente à letra das Escrituras e seguissem o que o próprio Jesus ensinou! Que grande alegria, que grande consolação! À Jesus seria dada maior glória e ele seria mais admirado e conhecido por todos os povos!