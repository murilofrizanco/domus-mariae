+++
date = "2016-04-07T18:00:00-18:00"
title = "Qual o problema com o protestantismo?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/qual-o-problema-com-o-protestantismo.jpg"
+++

A rejeição da devoção aos santos, o desprezo pela Tradição Apostólica, a negação do valor da razão para o conhecimento de Deus, a inexistência de uma instituição vísivel e um preconceito enraizado contra a Igreja Católica. Afinal, qual é o verdadeiro problema por trás do protestantismo?

<!--more-->

A inimizade protestante para com o catolicismo não é meramente fruto de uma interpretação das Escrituras ou de certos livros sagrados, mas sim de uma fraca compreensão do mistério da união hipostática.

Para que se possa resolver a questão, deve-se compreender com clareza a relação existente entre a natureza humana e a natureza divina em Cristo.

Ora, o Verbo, segunda pessoa da Santíssima Trindade, para se encarnar, criou e tomou para si próprio uma natureza humana.

Assim, desde a encarnação, esta pessoa divina passou a ter duas naturezas. O Verbo continuou sendo Deus, mas ao mesmo tempo passou a ser homem.

Por isso, tudo aquilo que se pode afirmar sobre Deus pode, agora, também ser afirmado sobre o homem Jesus; o que se pode afirmar sobre o homem, pode ser afirmado sobre Deus.

Com efeito, as Sagradas Escrituras atribuem realidades humanas à Deus, como quando diz que a pessoa divina de Cristo "Cristo morreu por nós" (Rm 5,8), que aquela pessoa divina "crescia em sabedoria, em estatura e em graça" (Lc 2, 52). Estas coisas não podem ocorrer na natureza divina, que é imortal e onisciente, mas ocorreram em uma pessoa divina na medida em que experimentou tais realidades na humanidade que tomou para si.

Por outro lado, as Sagradas Escrituras também atribuem realidades divinas a um homem, como quando Jesus diz ser "A Verdade" (Jo 14, 6), que "antes que Abraão fosse, eu sou" (Jo 8, 58), que tem poder para dizer "teus pecados estão perdoados" (Mt 9, 2). Estas coisas não se podem dizer dos homens, mas podem ser ditas daquela humanidade que pertence a uma pessoa divina.

Com isto, pode-se compreender que aquela humanidade, isto é, uma criatura, é mediadora da Nova Aliança e sem a qual não se vai ao Pai (cf. Jo 14, 6).

Contudo, deve-se notar que, a Santíssima Trindade poderia salvar diretamente o homem sem precisar da Encarnação, mas aprouve a Deus usar-se da Paixão e Morte de Nosso Senhor Jesus Cristo para que realizasse a rendenção.

Portanto, segue-se que foi determinação divina que uma natureza humana, a humanidade de Cristo Jesus, fosse o instrumento pelo qual se operaria a nossa salvação.

Ora, se a salvação é operada por um instrumento, esta obra será marcada por características próprias do instrumento utilizado.

Com efeito, se derrubamos uma árvore com uma serra, ficará um rastro que permitirá identificar o instrumento utilizado. O mesmo se diga se o fazemos com um machado ou se a atropelamos com um trator.

Assim, se a remissão dos pecados e a justificação são operadas por meio de uma humanidade, então seus efeitos serão marcados por rastros humanos.

Ora, pelos sacramentos somos unidos à Cabeça, que é Cristo, em seu Corpo Místico, que é a Igreja, recebendo Dele a graça "segundo a medida do dom de Cristo" (Ef 4, 7).

Unidos à Cabeça, a congregação de todos os fiéis torna-se uma extensão da santíssima humanidade do Verbo Divino, cuja finalidade é ser instrumento daquela humanidade que subiu aos céus para que possa propagar por todos os tempos e lugares esta obra salvífica até que "cheguemos todos a unidade da fé e do conhecimento do Filho de Deus" (Ef 4,13).

Por isso, Cristo continua a derramar suas graças até o fim dos tempos em todos os lugares sobre todos os povos operando principalmente pela virtude dos sete sacramentos.

É por esta união sobrenatural operada na congregação dos fiéis que cada um pode participar mais ou menos intensamente da santidade do Filho de Deus e progredir na fé e no amor cada vez mais profundamente até que se possa verdadeiramente dizer: "Não sou eu que vivo, senão que é Cristo que vive em mim" (Gl 2, 20).

Ora, é por escandalizar-se com muitos hipócritas, hereges e ignorantes que participam desta Igreja que o protestantismo acabou chegando a um preconceito contra o papel da natureza humana na salvação, negando cada vez mais intensamente os mistérios da fé que procediam como consequência da união das duas naturezas no Messias levando às principais conclusões protestantes.

A consequência disto é cair numa separação entre a Igreja invisível, santa e verdadeira, e as igrejas visíveis, divergentes e humanas, fruto de uma percepção cada vez menos clara das consequências eclesiológicas da união hipostática. 

Se não houvesse sido esquecido que os cristãos são uma extensão da natureza humana de Cristo, jamais se teria condenado as instituições visíveis da Igreja como embaixadores de Cristo aptos a sentenciar se saímos ou não daquilo que foi ensinado pelos Apóstolos.

Se não houvessem desprezado tão intensamente a natureza humana que começa a conhecer as coisas pelos sentidos, não teriam negado a necessidade dos sinais visíveis dos sacramentos, pelo qual a graça do Espírito Santo é infundida nos fiéis.

Se não houvessem desprezado nossa natureza, não haveriam impugnado a reverência para com os santos que, por serem uma extensão da humanidade de Cristo, manifestam na própria carne as virtudes do Divino Mestre e sua vida terrena.

Se não houvessem condenado tudo o que é humano, não negariam a maternidade divina da Santíssima Virgem, nem considerariam como idolatria a adoração prestada diante de representações da humanidade de Cristo.

Se assim não fosse, não teriam negado a necessidade da razão natural para conhecer alguma coisa sobre Deus e inclusive para interpretar as Sagradas Escrituras, descartando-se qualquer analogia entre as criaturas e Deus e qualquer conclusão teológica implícita no conjunto das Sagradas Escrituras.

Finalmente, não teriam rompido com as pessoas que escreveram, traduziram e decretaram a autenticidade dos próprios livros sagrados utilizados por eles.

Portanto, o protestantismo, por debilidade na compreensão do mistério da união hipostática, se fundamentou num falso preconceito contra tudo o que é humano e separou a Igreja fundada por Cristo de qualquer associação humana visível entre os fiéis.

Assim, quanto mais tempo ele corre solto no meio protestante, mais verdades da fé são negadas e corrompidas até que se degenere, como já ocorre atualmente, na própria negação dos milagres de Cristo e de qualquer coisa sobrenatural no Evangelho.