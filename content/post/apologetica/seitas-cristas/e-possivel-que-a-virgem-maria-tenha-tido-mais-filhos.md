+++
date = "2014-10-28T18:00:00-18:00"
title = "É possível que a Virgem Maria tenha tido mais filhos?"
categories = ["Apologetica","Seitas Cristas"]
path = "Seitas Cristãs"
featured_image = "posts/images/e-possivel-que-a-virgem-maria-tenha-tido-mais-filhos.jpg"
+++

É famosa a passagem invocada para alegar que Jesus tinha irmãos de sangue e que a Santíssima Virgem teria tido outros filhos. Com uma afirmação tão explícita, não seria incoerente a fé católica afirmar que Maria não teve mais nenhum outro filho além de Jesus? Qual seria a verdade sobre este fato?

<!--more-->

Com efeito, lemos nas Sagradas Escrituras a alusão aos irmãos de Jesus:

>Não é este o carpinteiro, o filho de Maria, irmão de Tiago, José, Judas e Simão? E as suas irmãs não estão aqui entre nós? <cite>Evangelho segundo São Marcos 6, 3</cite>

>Estando ainda a falar às multidões, sua mãe e seus irmãos estavam fora, procurando falar-lhe. <cite>Evangelho segundo São Mateus 12, 46</cite>

Porém, deve-se notar que uma leitura atenta das Escrituras nos leva a perceber que a palavra "irmão" é usado em quatro sentidos nos textos sagrados.

Primeiro. O sentido mais óbvio e evidente é no qual se fala segundo a natureza, ou seja, se refere aos irmãos de sangue, filhos de uma mesma mãe. Este significado consta, por exemplo, em passagens sobre Esaú e Jacó:

>Quando chegou o tempo de dar à luz, eis que ela trazia gêmeos. Saiu o primeiro: era ruivo e peludo como um manto de pêlos; foi chamado de Esaú. Em seguida saiu seu irmão, e sua mão segurava o calcanhar de Esaú; foi chamado de Jacó. <cite>Gênesis 25, 24-26</cite>

Segundo. Também se usa a palavra irmão segundo a nação, ou seja, entre as pessoas pertencentes a uma mesma nação, como, por exemplo, no caso do povo judaico:

>Quando tiveres entrado na terra que Iahweh teu Deus te dará, tomado posse dela e nela habitares, e disseres: "Quero estabelecer sobre mim um rei, como todas as nações que me rodeiam", deverás estabelecer sobre ti um rei que tenha sido escolhido por Iahweh teu Deus; é um dos teus irmãos que estabelecerás como rei sobre ti. Não poderás nomear um estrangeiro que não seja teu irmão. <cite>Deuteronômio 17, 14s</cite>

Terceiro. Há ainda o uso da mesma palavra segundo o parentesco, ou seja, utilizado entre parentes que são da mesma família, como um tio ou um primo. Este sentido é empregado em passagens que se referem a Abraão e Ló, seu sobrinho:

>Abrão disse a Ló: "Que não haja discórdia entre mim e ti, entre meus pastores e os teus, pois somos irmãos!" <cite>Gênesis 13, 8</cite>

Quarto. Outro uso presente nas Escrituras é aquele segundo o carinho, como, por exemplo, é empregado para referir-se aos discípulos de Cristo por ocasião da ressurreição:

>Jesus lhe diz: "Não me retenhas pois ainda não subi ao Pai. Vai, porém, a meus irmãos e dize-lhes: Subo a meu Pai e vosso Pai; a meu Deus e vosso Deus". Maria Madalena foi anunciar aos discípulos: "Vi o Senhor", e as coisas que ele lhe disse. <cite>Evangelho segundo São João 20, 17s</cite>

Portanto, existem quatro usos completamente diferentes para a palavra "irmão".

Os que defendem que Jesus tinha irmãos de sangue afirmam que Maria era casada e não pecaria se tivesse mais filhos. Embora isto seja verdade, quando analisamos cuidadosamente alguns pontos percebemos que não faz sentido que essa possibilidade se desse de fato por seis razões.

Primeiro. O profeta Isaías atesta claramente:

>Eis que a virgem conceberá e dará à luz um filho <cite>Evangelho segundo Mateus 1, 23</cite>

Ora, a profecia atesta explicitamente que a Virgem dará à luz um filho apenas, sem mencionar outros.

Segundo. O Espírito Santo não destruiu a virgindade de Maria por ocasião do nascimento do Cristo. Ora, certamente São José não faria aquilo que nem Deus atreveu-se a fazer.

Ademais, não é coerente que Deus tenha se dado o trabalho de operar tal prodígio para que depois o santo José destruísse esta obra divina como se nunca tivesse ocorrido milagre algum.

Ademais, se Deus não desejasse a virgindade da Mãe de Deus, poderia ter concebido Cristo pela união carnal dos dois ao invés de operar tão grande maravilha.

Terceiro. Se o Verbo de Deus basta para satisfazer o desejo do Pai Celeste, não seria possível que uma criatura humana, como a Virgem, pudesse desejar algo além deste filho Unigênito.

Com efeito, a capacidade de desejar da Digníssima Maria é menor que a de Deus.

Portanto, se este Filho saciou a Deus, também deve bastar para sua Mãe.

Quarto. Há uma prefiguração disto nos profetas:

>E disse-me o Senhor: Esta porta ficará fechada, não se abrirá, nem entrará por ela homem algum; porque o Senhor Deus de Israel entrou por ela; por isso ficará fechada. <cite>Ezequiel 44, 2</cite>

Ora, Maria é, com razão, chamada de Porta do Céu, pois por ela Cristo entrou no mundo.

Assim, era necessário que Maria permanecesse virgem para que se concretizasse esta profecia.

Quinto. No relato de Jesus aos doze anos não há qualquer referência à irmãos de sangue, de modo que apenas Maria e José estão presentes no acontecimento. Ora, um casal jovem e fértil como os dois teria tido pelo menos seis filhos no período de 12 anos. Porém, nenhum destes é citado em nenhum momento.

Sexto. Na cruz, Cristo entrega sua mãe aos cuidados do discípulo amado. Ora, isto só pode ter sido feito porque não havia irmãos de sangue para cuidar de sua Mãe, a ponto de entregá-la para ser Mãe do discípulo amado.

Diante destes fatos e situações não é possível insistir que existem realmente irmãos de sangue de Jesus. Este erro é muito posterior à pregação apostólica e inclusive tem influência de vários textos apócrifos que se referem a estes "irmãos de sangue". Porém, estes escritos não são fontes confiáveis e não podem ter valor histórico contra o testemunho apostólico e dos primeiros discípulos que conheceram pessoalmente o próprio Cristo e a própria Virgem Maria.

Que Deus possa dignar-se de iluminar a mente dos que caminham no erro e os levar a um grau de interpretação mais profunda das Sagradas Escrituras purificando de toda contaminação da ignorância e das paixões humanas.