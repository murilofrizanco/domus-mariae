+++
date = "2014-08-25T18:00:00-17:00"
title = "Algumas regras práticas para as roupas femininas"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/algumas-regras-praticas-para-as-roupas-femininas.jpg"
+++

Existe muita discussão e dúvida sobre o que seria uma roupa modesta e ideal de uma mulher cristã, imagem da Santíssima Virgem, e uma roupa que não alcance padrões espirituais tão elevados.

<!--more-->

Alguns poderiam objetar que não importa o exterior, mas somente o interior. Porém, deve-se considerar que o corpo manifesta como se encontra a alma.

Com efeito, quando estamos tristes, choramos; quando nervosos, fechamos a cara; quando amedrontados, ficamos pasmos e assim por diante.

Sendo assim, nosso corpo expressa a beleza de nossa alma mais diretamente do que pensamos.

Portanto, separar o corpo e a alma é um erro nefasto que despreza a grande harmonia estabelecida pelo Criador.

Por isso, é virtuosos que as mulheres busquem se vestir de modo que manifestem a nobreza de suas almas. Por isso dizia o grande Doutor Angélico:

>As roupas se ordenam ao culto do corpo <cite>Santo Tomás de Aquino</cite>

Não vamos tratar aqui de como se vestir sem chegar a cometer pecado, mas sim de prudentes diretrizes para manifestar a dignidade de filhas de Deus e irmãs de Maria Santíssima.

Primeiro. Buscar roupas que transpareçam as características femininas.

É evidente que não se deve buscar o uso de roupas que assemelhem as mulheres a homens, pois isto desfigura o corpo feminino:

>Todas mulheres em particular, devem ser advertidas, para que não adulterem de modo nenhum o que Deus fez e plasmou (...) Levantam mãos contra Deus, quando pretendem reformar o que ele formou. <cite>São Cipriano</cite>

Portanto, as roupas devem exaltar a beleza feminina.

Neste sentido pode-se evitar o uso de calças, pois elas foram feitas propositalmente para masculinizar a mulher. Além disso, a saia se assemelha muito mais ao vestuário da Mãe de Deus e não apresenta todos os desconfortos e inconvenientes que as calças apresentam.

Segundo. O comprimento das roupas.

Não basta que sejam utilizadas roupas femininas, como os vestidos, mas devem ser observadas certas normas mínimas para evitar que a exaltação feminina se torne provocação ao pecado:

>Um vestido não pode ser chamado decente se é cortado na largura de mais de dois dedos sob o poço da garganta, que não cubra os braços pelo menos até os cotovelos, e mal chegue até um pouco abaixo dos joelhos. Além disso, os vestidos de materiais transparentes são impróprios. <cite>Cardeal Donato Sbaretti, Festa da Sagrada Família (12 de janeiro de 1930)</cite>

Portanto, decotes e regatas não são aceitáveis para quem quer manifestar mais a sua alma do que seu corpo, assim como também são incompatíveis vestidos acima do joelho e roupas com materiais transparentes.

Ademais, devem ser evitados roupas justas que delineiem demais as partes que se deve esconder, como seios, nádegas ou quadris, pois seriam ocasião de despertar a fraca imaginação masculina.

Terceiro. Resgatar o uso do véu.

Deveríamos resgatar o uso do véu, seja na liturgia, seja até mesmo fora dela, pois trata-se de um acessório belíssimo. Com efeito, "Tudo o que é sagrado a Igreja cobre com um véu" (Pe. Paulo Ricardo). É assim com a Virgem Maria e é assim com o Santíssimo Sacramento.

É preciso parar de considerar ultrapassado aquilo que não compreendemos todo seu significado.

Alguns poderiam dizer que o mundo jamais aceitaria tal padrão. O problema é que não podem aceitar aquilo que não podem ver sua bondade.

Quando as mulheres cristãs se vestirem com perfeição e se portarem com santidade, demonstrando a íntima ligação entre ambas as coisas, sua excelência sobre as demais causará um tal encanto nas pessoas e nos homens que desacreditará as vozes discordantes e as pessoas buscarão a excelência da modéstia.

Devido a íntima relação que estas coisas tem com o estado de nossa alma, diz um grande sábio:

>A roupa não deve ser avaliada de acordo com a estimativa de uma sociedade decadente ou já corrupta, mas de acordo com uma sociedade que preza a dignidade e a seriedade das suas vestimentas em público. [...] Mas seria mais exato e muito mais proveitoso dizer que eles expressam a decisão e a direção moral que uma nação tem a intenção de tomar: ou a de naufragar na libertinagem ou a de se manter no nível ao qual foi erguida pela religião e pela civilização. <cite>Papa Pio XII, Congresso da União Latina de Alta Costura, 8/11/1957</cite>

Que através de um íntimo contato com Cristo as mulheres possam reconhecer sua altíssima dignidade e transparecer no corpo aquela beleza e pureza da alma digna dos anjos. Que nossa querida e honorável Mãe possa ser modelo para nossas mulheres.