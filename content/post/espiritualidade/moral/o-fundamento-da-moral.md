+++
date = "2015-12-08T18:00:00-17:00"
title = "O fundamento da moral"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/o-fundamento-da-moral.jpg"
+++

A cultura moderna parece ter absorvido a tese de Protágoras sobre o conhecimento das coisas, principalmente no que diz respeito a ciência moral.

<!--more-->

Com efeito, a modernidade se ufana de romper com a imposição de padrões de comportamento como se uma de suas máximas fosse aquela postulada pelo antigo sofista grego Protágoras:

>"[Protágoras] disse que: tais como me parecem as coisas, tais são para mim, tais como parecem a ti, tais são para ti." <cite>Protégoras, citado por Platão na sua obra Teeteto</cite>

Assim, a conclusão seria que uma coisa seria moral ou imoral de acordo com o parecer individual de cada um.

Porém, deve-se notar que tal afirmação, tão "na moda", é consequência de uma ignorância completa sobre os fundamentos da verdadeira moral.

A moral tem como matéria os atos humanos e o modo de julgá-los não é semelhante ao que se faz nas ciências especulativas. Quanto a matéria, o Doutor Angélico deixa claro que:

>À ciência moral pertence de modo principal tratar das obras virtuosas, aqui denominadas de obras justas.

E em outra passagem:

>O fim desta ciência são os atos humanos, assim como o é de todas as ciências práticas. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Porém, sendo uma ciência prática, não se ignora que sobre uma mesma obra justa os homens podem ter opiniões diversas conforme cada uma das infinitas circunstâncias:

>Acerca destas obras não existe sentença certa dos homens, existindo, ao contrário, grande diferença naquilo que os homens julgam a este respeito. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Com efeito, um homem que luta contra um assassino parece fazer algo bom do ponto de vista da vítima, mas do ponto de vista do adversário parece agir mal.

A ciência moral é certa e imutável, mas deve-se notar que, segundo o Aquinate no mesmo livro, "na ciência moral não é conveniente chegarmos à certeza perfeita".

Com isto se pretende dizer que na ciência moral não é possível, e nem se pretende, estudar todas as possibilidades de sua matéria cada um de seus mínimos detalhes a ponto de produzir como que um manual sobre a moralidade de cada ato em cada circunstância que poderia ocorrer.

Este tipo de conhecimento tão detalhado somente seria possível se fôssemos capazes de apreender todas as possibilidades que pudessem se dar em cada situação, coisa que somente é possível para Deus.

Ademais, determinar sobre os atos humanos ao ponto de determinar tudo o que ele deveria fazer em cada mínimo detalhe seria o mesmo que proibir seu livre-arbítrio e robotizá-lo.

Portanto, é manifesto que não pertence ao verdadeiro estudo da ciência moral qualquer tentativa de julgar os atos humanos com o mesmo grau de detalhes com que se julga no campo da geometria, da matemática e ciências semelhantes.

Todavia, este fato não quer dizer que a moral seja relativa, isto é, determinada conforme as coisas parecem a cada um:

>Não se deve cair no erro que daqui muitos derivaram, porque por terem visto tantas diferenças de julgamento [acerca dos mesmos atos], que dependem de diferenças de tempo, lugar e pessoas, muitos opinaram que nada é naturalmente justo ou honesto, a justiça ou a honestidade de algum ato se dando apenas em virtude da lei [ou dos costumes de um dado tempo ou lugar]. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Assim, a verdadeira ciência moral não será nem determinista, nem relativista, exigindo um modo próprio de conhecê-la.

Quanto ao modo de conhecê-la, o grande estudioso da Aristóteles nos explica:

>[aqueles que ensinam a ciência moral devam proceder da seguinte forma]: já que, segundo a arte da ciência demonstrativa, os princípios devem ser semelhantes às conclusões, e como na ciência moral as conclusões são tão variáveis, não poderemos proceder como fazemos nas ciências [puramente] especulativas, nas quais partimos dos singulares e compostos e, por modo resolutório, chegamos aos princípios universais e simples.

>[Aqui deveremos fazer o oposto, isto é], partindo dos princípios universais e simples, aplicá-los aos singulares e compostos]. Como a ciência moral tratará dos atos da vontade, e a vontade é motiva não só ao bem, mas ao que parece bem, a verdade na ciência moral deverá ser mostrada figurativamente, isto é, verossimilmente. Como os atos voluntários, de que trata a ciência moral, não são produzidos pela vontade por necessidade, na ciência moral deveremos partir de princípios que sejam conformes a estas conclusões. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Portanto, na ciência moral não testamos cada ato na realidade para chegar a conclusão sobre sua moralidade, mas partimos de um princípio universal certo para julgar a moralidade de cada ato.

Ora, há um princípio certo e universal que se aplica a todos os atos humanos. Este princípio é o fim que se pretende alcançar com aquele ato.

Contudo, deve-se notar que tudo o que desejamos o fazemos tendo em vista algo posterior. Porém, este processo não pode ir ao infinito, caso contrário o homem jamais iniciaria a desejar nada.

Assim, é necessário que o ser humano deseje naturalmente alguma coisa por natureza e que por causa desse desejo natural comece a desejar as demais coisas tendo em vista atingir esse fim. Ora, este fim é o fim ótimo do homem o qual chamamos de felicidade.

>Um fim não somente é bem, mas é ótimo, quando é de uma tal natureza que todas as demais coisas são desejadas por causa desse fim, e este fim é desejado por causa de si mesmo, e não por causa de alguma outra coisa. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Ora, este desejo universal de felicidade é justamente o que irá permitir julgar sobre cada ato moral em particular, pois:

>Importa que toda a vida humana se ordene ao fim ótimo e último fim da vida humana. (...) A razão [que está por trás] desta necessidade consiste em que sempre a razão das coisas que se relacionam com o fim deve ser tomada do [isto é, proveniente do] próprio fim. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Portanto, a verdade ciência moral irá estudar quais são os atos humanos que permitem alcançar a felicidade e quais atos humanos impedem de fazê-lo.

Deste modo, a moral não deve começar estudando os atos por si só, mas deve começar estudando em que consiste a felicidade do homem, que, visto ser um desejo que procede da natureza, deve consistir na mesma coisa em todos os seres humanos.

Por isso, o conhecimento claro deste fim é algo necessário, visto que:

>Nada daquilo que se dirige a outras coisas pode ser diretamente alcançado pelo homem, a não ser que ele conheça aquilo para o qual deve se dirigir. <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Ora, se o homem possui uma natureza racional e a felicidade é a plena realização de sua natureza, disto se segue que a moralidade dos atos exigirá uma adequação destes à sua razão. O que explica a afirmação do Aquinate de que:

>A ciência moral ensina os homens a seguirem a razão e a afastar-se das coisas que inclinam às paixões da alma, tais como a concupiscência, a ira e semelhantes.  <cite>Santo Tomás de Aquino, Comentários à Ética de Aristóteles, Livro I</cite>

Portanto, a verdadeira moral será bem determinada no que diz respeito ao fim ótimo pretendido por todos os atos humanos e relativa quanto aos próprios atos conforme possam ser instrumentos ou impedimentos para a posse da felicidade pelo indivíduo.