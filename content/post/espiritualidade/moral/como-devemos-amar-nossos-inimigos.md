+++
date = "2015-09-30T18:00:00-17:00"
title = "Como devemos amar nosso inimigos?"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/como-devemos-amar-nossos-inimigos.jpg"
+++

Um dos mandamentos mais exigentes e profundos de Nosso Senhor é o amor ao inimigos, tão negligenciado pelos cristãos e tão incompreendido pelos pagãos.

<!--more-->

Com efeito, lemos no Evangelho:

>Ouvistes que foi dito: Amarás o teu próximo e odiarás o teu inimigo. Eu, porém, vos digo: amai os vossos inimigos e orai pelos que vos perseguem;desse modo vos tornareis filhos do vosso Pai que está nos céus, porque ele faz nascer o seu sol igualmente sobre maus e bons e cair a chuva sobre justos e injustos. Com efeito, se amais aos que vos amam, que recompensa tendes? Não fazem também os publicanos a mesma coisa? E se saudais apenas os vossos irmãos, que fazeis de mais? Não fazem também os gentios a mesma coisa? Portanto, deveis ser perfeitos como o vosso Pai celeste é perfeito. <cite>Evangelho segundo São Mateus, Cap. V, v. 43-47</cite>

A razão para este preceito é a que se segue. Cristo nos manda amarmos o próximo. Ora, nossos inimigos são nossos próximos. Portanto, devem ser amados. Assim, o mandamento é claro, mas não se pode dizer o mesmo de sua aplicação.

Uma das coisas que nos obriga este mandamento é a seguinte:

>Qualquer homem, pelo menos o particular, é obrigado a exibir ao próximo, mesmo ao inimigo, sinais comuns de amor e benefício, por preceito. Digo sinais comuns, que são aqueles que são devidos por um cristão a qualquer outro cristão em geral, a um cidadão por outro cidadão, a um parente por outro parente. A razão para isto é que negar estes sinais significa vingar uma injúria, o que não é lícito para nenhum particular. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Portanto, somos proibidos, sob pena de pecado, de negar uma manifestação de amor para com o próximo que tributamos aos demais. Deste modo, devemos tratar nossos inimigos com o mesmo respeito com que tratamos um desconhecido ou uma pessoa indiferente.

Evidentemente que isto significa que todo e qualquer sinal de inimizade, como insultar, fazer chacota, tratar como inferior ou quaisquer outros sinais semelhantes constituem uma transgressão ao amor ao próximo.

De modo geral, o preceito não nos obriga que manifestemos um sinal de amor e bondade mais familiar e especial, a não ser que:

>Se outra origem surja uma razão de obrigação, a qual seria, por exemplo, o temor do escândalo devido à omissão, a esperança da salvação do inimigo, uma necessidade temporal ou espiritual, a deprecação da culpa e a exibição de sinais especiais de amor. A razão é que, nestes casos, a negligência destes sinais especiais seria uma declaração externa de ódio. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Com efeito, a não manifestação destes sinais que em tais circunstâncias costumam são feitos seria uma manifestação de ódio, pois assim o interpretaria o inimigo ou até mesmo outras pessoas.

Colocado estes princípios podemos aplicá-los de modo prático. Ninguém está obrigado, por este preceito, a saudar um inimigo, a procurar conversar com ele, a visitá-lo quando doente, a consolá-lo quando aflito, nem a recolhê-lo em uma habitação ou ter familiaridade com ele, a não ser que isso cause escândalo ou se com pudesse reconciliar o inimigo com Deus.

Outra coisa que nos é proibida é excluir os inimigos das orações comuns, das esmolas comuns. Não é lícito negarmos uma resposta a uma pergunta de nosso inimigo, negarmos uma saudação que oferecemos a todos, negarmos a venda de mercadorias expostas.

Só nos é permitido negar estes sinais ao inimigo por uma razão:

>Não se pode esperar dele outra coisa senão o mal, ou porque, provavelmente teme-se que disto seguir-se-á algo pior, então deve-se abster destes sinais de amizade, desde que interiormente se ame ao inimigo e se repare o escândalo junto aos demais. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Uma outra coisa de grandíssima importância é o fato do preceito do amor aos inimigos obrigar-nos a perdoá-los:

>Obrigados não somente a perdoar interiormente ao inimigo que legitimamente pede perdão, como também a mostrar sinais exteriores de perdão. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Porém, este perdão não nos obriga a perdoar a satisfação pelo dano:

>Não há porém a obrigação de perdoar a satisfação pelo dano, se houve uma lesão por parte do inimigo, nem também de aceitá-la se esta é oferecida. Pode-se também pedir juridicamente uma compensação, desde que em ambos os casos se deponha o ódio. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Sobre a exigência de uma punição pública, deve-se notar que dificilmente isto é solicitado por amor ao bem comum, mas geralmente é feito por modo de vingança pessoal. Quanto isto, deve-se considerar o seguinte:

>Nunca é lícito pedir a punição do inimigo, mesmo que justa e mesmo entendendo-se que será executada por uma legítima autoridade. Corretamente alguém poderá pedir diante de um juiz a satisfação das injúrias se, de outro modo, toda a sua família tenha que passar por infame. Santo Tomás de Aquino também sustenta que a punição pode ser lícita em alguns casos, isto é, quando é tomada "para a emenda do pecador ou pelo menos para a sua coibição e a tranquilidade dos demais". E acrescenta: "e para a conservação da justiça", embora este último, conforme já explicamos, seja verdade mais especulativamente do que praticamente, ou pelo menos rarissimamente. <cite>Santo Afonso de Ligori, Teologia Moral, Livro II, Tratado III, Cap. II</cite>

Portanto, devemos tratar nosso inimigos no mínimo como se não nos tivessem feito mal e perdoá-los como perdoaríamos quaisquer pessoas.

É esta a profundidade do proceito de amar os inimigos. Aplicando isto será possível instaurar na própria alma e até mesmo na sociedade um respeito radical pelo próximo, independente de quaisquer condições em que se encontre.

