+++
date = "2015-10-05T18:00:00-17:00"
title = "Como devemos honrar os pais?"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/como-devemos-honrar-os-pais.jpg"
+++

Entre os mandamentos que dizem respeito ao próximo, o único que preceitua que façamos algo positivamente é o quarto mandamento: honrar pai e mãe.

<!--more-->

Este mandamento infelizmente caiu no esquecimento e não é incomum que os cristãos se gabem de viver a castidade e de levar uma vida piedosa enquanto ao mesmo tempo insistem em transgredir este mandamento.

Por outro lado, outros são os que não compreendem a extensão deste preceito e terminam praticando algo semelhante a uma idolatria aos pais.

Portanto, é necessário ter uma compreensão bem clara à respeito do que nos obriga este preceito.

Em primeiro lugar, deve-se notar que:

>Os filhos são obrigados, por força do quarto mandamento, tanto aos pais como aos superiores, naquelas coisas nas quais cada um e enquanto lhes é submetido, prestar amor, reverência e obediência, de tal modo que se houver um notável defeito destas coisas em matéria grave, gravemente se peca, o que ocorre mais facilmente em relação aos pais do que aos demais. <cite>Santo Afonso de Ligori, Teologia Moral, Livro III, Tratado III, Capítulo II</cite>

Portanto, este mandamento não diz respeito somente aos pais, mas a todos e qualquer superior dentro dos limites de sua autoridade. Isso inclui os professores, as lideranças no ambiente de trabalho e assim por diante.

Deste modo, este preceito nos obriga a prestar amor, reverência e obediência a todas estas pessoas e não apenas aos pais.

A obediência que deve ser prestada evidentemente diz respeito ao cumprimento das ordens dadas pelos superiores naquilo que compete à sua função. Isto significa que não devemos uma obediência em tudo o que estas pessoas ordenam, mas somente naquilo que lhe compete por ocasião de sua função. 

Assim, os filhos devem obedecer os professores quando desempenham sua função na sala de aula, mas não no que diz respeito ao que fazem fora da escola ou na sua vida doméstica.

Quanto ao amor, os filhos não devem apenas guardar um [respeito incondicional](/posts/espiritualidade/moral/como-devemos-amar-nossos-inimigos) pelos pais, mas também devem prestar-lhes certos cuidados especiais quando estes necessitarem. Assim,

>Peca gravemente contra o amor o filho que mostra sinais de ódio aos pais e os trata asperamente. Que quase sempre os olha com olhos turvos e que lhes fala tão asperamente como se lhes tivesse ódio. Que não os auxilia em grave necessidade espiritual ou corporal, de onde que também pecam gravemente os filhos que negligenciam oferecer preces e sacrifícios pelos pais. Que não cumpre o seu testamento e legado, desde porém que seja herdeiro. Que lhes deseja um grave mal, por exemplo, a sua morte.

>Pecam gravemente os filhos que não cuidam para que os pais, constituídos em artigo de morte, recebem o sacramento da Penitência e outros.<cite>Santo Afonso de Ligori, Teologia Moral, Livro III, Tratado III, Capítulo II</cite>

Ora, o que Santo Afonso nos diz pode ser entendido à respeito de todas as necessidades graves de nossos pais. Portanto, não apenas temos a obrigação de procurar que recebam os sacramentos na proximidade da morte, mas também devemos socorrê-los se passarem necessidades materiais, no caso dos pais acolhê-los em casa se não tiverem onde morar e assim por diante.

Quanto à reverência, os filhos devem prestar aos pais a veneração como se estes fossem sagrados.

Por esta razão, diz Santo Afonso:

>Peca gravemente o filho que bate nos pais, ainda que levemente. E se levantar deliberadamente a mão pata batê-los.

>Peca gravemente também o filho que entristece gravemente os pais. E isto mesmo que as palavras não fossem gravemente ofensivas.

>[Alguns sustentam] não ser pecado mortal se a iracúndia dos pais não se origina da própria palavra ou fato, quando estes são apenas levemente injuriosos, pois isto deveria atribuir-se a uma apreensão sinistra por parte dos pais ou por uma indisposição dos mesmos. Neste caso, de fato, se não se lesa gravemente a reverência, pelo menos é gravemente violado o amor para com os pais, entristecendo-os sem justa causa gravemente e deliberadamente.

>Peca gravemente contra a reverência o filho que com ânimo deliberado provoca os pais a uma grave ira por meio de palavras injuriosas ou pelo menos por palavras tais que saiba que os irá ofender gravemente. Daqui [se] sustenta corretamente que não se pode desculpar de pecado mortal aquele que chamar à mãe de "louca, bêbada, besta, bruxa, ladra", e outras semelhantes. Quem, porém, dissesse somente: "velha, atordoada, ignorante", e outras semelhantes, considera que não poderia por si só ser condenado absolutamente de pecado mortal, a não ser que os pais se ofendessem gravemente por estas palavras.

>Peca também gravemente o filho que freqüentemente olha os pais com olhos turvos ou que lhes fala com palavras ásperas, de tal maneira que pareça ter-lhes ódio.

>Peca gravemente também o filho que lança aos pais maldições e gritos. O mesmo deve-se dizer de ridicularizar os pais, deliberadamente, por gestos ou risadas. Note-se que dizemos deliberadamente, porque nestas situações e outras semelhantes, conforme acima mencionado, freqüentemente os filhos são desculpáveis de pecado mortal por causa de indeliberação do ato.
<cite>Santo Afonso de Ligori, Teologia Moral, Livro III, Tratado III, Capítulo II</cite>

Portanto, simplesmente gritar com os pais ou tratá-los de modo que transparece ódio é algo completamente inadmissível. O que não dizer de coisas piores?

Com efeito, todas estas atitudes são impróprios em relação àquilo que se trata como sagrado, como bom e como digno de honra.

Deve-se notar que com tudo o que foi dito, a honra devida aos pais não inclui que eles controlem nossa vida espiritual e moral, ou seja, que eles determinem com quem casaremos, se seguiremos ou não vida religiosa, se exerceremos esta ou aquela profissão, como devemos educar nossos filhos ou se devemos transgredir determinado ensinamento do Evangelho, o que não se inclui na prestação de reverência, amor e obediência citadas anteriormente.

Devemos escutar os conselhos de nosso pais, mas não somos obrigados a tomar decisões em nossa vida pessoal conforme o que eles gostariam ou conforme o que nos aconselharam em nome deste mandamento.

Ademais, no caso de um conflito entre a obediência aos pais e à Deus, evidentemente os pais não são prioridade, pois deve-se obedecer a Deus antes que aos homens.

Este é o grau de respeito adequado que devemos aos nossos pais através do preceito do quarto mandamento. Quão ordenada não seria a sociedade se voltássemos a praticar este preceito em toda sua radicalidade.






