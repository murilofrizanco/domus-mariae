+++
date = "2014-07-02T18:00:00-17:00"
title = "O que Jesus quer dizer ao ordenar que odiemos os parentes?"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/o-que-jesus-quer-dizer-ao-ordenar-que-odiemos-os-parentes.jpg"
+++

É comum lermos a passagem em que Jesus diz que somente pode ser seu discípulo aqueles que odiarem os parentes e a si mesmo. A passagem é controversa, pois o próprio Jesus amou Maria e José com todo seu coração e nos pede que amemos até aos inimigos.

<!--more-->

Com efeito, encontramos nas Sagradas Escrituras:

>Se alguém vem a mim e não odeia seu pai, sua mãe, sua mulher, seus filhos, seus irmãos, suas irmãs e até a sua própria vida, não pode ser meu discípulo. <cite>Evangelho segundo São Lucas 14, 26</cite>

Alguns pensam tratar-se de um erro de tradução. Porém, isto é falso. Pois, na Vulgata Latina consta a palavra "odit", que significa odiar.

Daí que parece haver uma contradição entre o ensino de honrar os pais e ao mesmo tempo odiá-los.

Na realidade, trata-se de uma das passagens mais belas e profundas das Sagradas Escrituras que não percebemos sua riqueza.

Com efeito, nesta passagem Jesus condena o amor desordenado de si e dos demais de modo que sejam mais amados do que Deus ou mais amados do que deveriam:

>Mas o Senhor não ordena ignorar a natureza, nem ser cruel ou desumano, mas não compactuar com ela, de modo que veneremos a seu autor e que não nos separemos de Deus por amor de nossos pais. <cite>Santo Ambrósio</cite>

De fato, quantas não são as pessoas que deixam de seguir sua vocação por causa dos familiares? Quantos não deixam de evitar um pecado ou fazer a vontade de Deus para não desagradar aqueles que são seus parentes?

Isso procede de um amor exagerado pelos parentes, pois ainda que sejam importantes, são meras criaturas.

Por isso, devemos repudiar tudo o que for contrário à Deus que exista neles e não permitir que sejam obstáculos à ação e crescimento da graça.

Ademais, não devemos ordenar nossa vida, nossas decisões e obras segundo o desejo de nossos parentes, mas sim segundo a maior glória de Deus.

Quão grande é a Sabedoria de Deus ao perceber que a desordem nos afetos familiares, fruto de uma atração natural e saudável quando isenta de pecado, é algo difícil de combater. Por isso, diz um grande homem:

>E por que esse ódio, isto é, desapego dos parentes? É porque muitas vezes, no referente ao bem da nossa alma, não temos inimigos maiores do que os nossos parentes: Cada um, diz ainda o Salvador, terá por inimigos os da sua própria casa. <cite>Santo Afonso Maria de Ligório</cite>

Com efeito, as pessoas espirituais percebem o quão grande é a dificuldade de manter a devoção e praticar a virtude no ambiente familiar:

>São Carlos Borromeu dizia que, sempre que ia à casa dos parentes, voltava mais frio de espírito. Quando perguntavam ao Pe. Antônio Mendonza por que não visitava a casa dos parentes, respondeu: ‘Sei por experiência que em nenhum lugar os religiosos perdem tanto a devoção como na casa dos parentes’. <cite>Santo Afonso Maria de Ligório</cite>

Com isso percebemos que é necessário amar os parentes de forma ordenada e odiar o apego à eles que prejudica a alma e atrapalha a santidade.

Que amemos nossos parentes com ternura e respeito dando a cada um o amor devido sem usurpar o amor devido ao próprio Cristo.

Bendito seja Deus pela família e pela sua sabedoria! Maria, Rainha das Famílias, nos guie neste combate para amarmos virtuosamente os parentes e ao seu Filho, Nosso Senhor Jesus Cristo!