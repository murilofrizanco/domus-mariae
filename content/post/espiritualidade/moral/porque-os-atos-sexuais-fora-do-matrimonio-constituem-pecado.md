+++
date = "2015-09-15T18:00:00-17:00"
title = "Porque os atos sexuais fora do matrimônio constituem pecado?"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/porque-os-atos-sexuais-fora-do-matrimonio-constituem-pecado.jpg"
+++

Uma das maiores dificuldades do homem moderno é entender a impossibilidade da prática sexual fora do matrimônio.

<!--more-->

O sexto mandamento condena toda transgressão feita contra a castidade. Porém, o que se pretende dizer com isto é que o que é ilícito é todo pecado contra a dignidade da sexualidade humana.

Deve-se notar que sexualidade e ato sexual são coisas distintas.

A sexualidade é aquela complementariedade entre os sexos da qual procede a preferência de um sexo por outro. É por causa da sexualidade que o homem sente uma atração especial pelo sexo feminino quando, por exemplo, ele prefere ser atendido por uma atendente mulher do que por um homem ou quando, no dia em que se casa, sente uma alegria de estar com aquela pessoa do sexo oposto que não existe quando ele ganha um diploma, quando encontra um amigo muito querido ou quando encontra seus pais. Evidentemente, estas coisas não atentam contra a dignidade da sexualidade e nem constituem pecado contra a castidade.

Por outro lado, os prazeres venéreos são aquelas coisas que dizem respeito à própria relação sexual e ao que excita ou prepara para a consumação desta. Assim, inclui-se no prazer venéreo os pensamentos eróticos e carícias mesmo quando não levam a uma relação sexual, pois iniciam o movimento para terminar nela.

Dada esta distinção, deve-se notar que os prazeres sexuais não são ilícitos, mas os prazeres venéreos sim, isto é, tomar como mais agradável o trato com o sexo oposto não é pecado, mas usar dos prazeres venéreos fora do matrimônio legítimo o é.

As coisas sagradas não podem ser usadas de qualquer jeito, mas exigem, para seu uso, o devido respeito e a devida finalidade.

Ora, o sacerdote tem uma longa preparação para que adquira uma dignidade pessoal compatível com a dignidade daquilo que vai celebrar, isto é, da missa. Assim, a missa não pode ser celebrada em qualquer lugar, de qualquer jeito e por qualquer pessoa, de modo que exige um ministro digno, um lugar sagrado e um modo conveniente. Quando estas condições são desprezadas termina-se profanando a missa.

Portanto, sendo o sexo uma realidade sagrada, o casal que vai praticá-lo não pode fazê-lo de qualquer jeito como se este ato fosse qualquer coisa, mas deve adquirir uma dignidade compatível com o mesmo, realizá-lo em um local sagrado e de um modo conveniente, caso contrário, estariam profanando uma realidade sagrada.

Assim, o pecado contra a castidade é aquele que profana a dignidade da sexualidade humana, que é uma coisa sagrada.

Deve-se notar que o sexto mandamento, junto com outros seis, foram resumidos por Cristo em "amar ao próximo como a ti mesmo". Se isto é assim, devemos demonstrar o motivo pelo qual a profanação da sexualidade humana em qualquer condição transgride o amor ao próximo.

A sexualidade está incluída no amor ao próximo pelo fato de ter como finalidade a constituição de uma família e a geração da vida. Ora, é evidente que o fundamento da uma família e a disposição de gerar um filho é o amor. Assim, a sexualidade, por carregar consigo a capacidade de gerar uma outra pessoa, carrega consigo a dignidade da própria vida humana.

Ademais, deve-se notar que, sem o auxílio da doutrina cristã, a maioria das pessoas estariam privadas de entender o que é o amor e a doação de si mesmo se não se apaixonassem por alguém e constituíssem uma família com ela.

Assim, a única chance que a vasta maioria das pessoas tem para conseguir entender o que é o amor e a doação é quando constituem uma família e ganham um sentido na vida que não são elas mesmas.

Por isso, um ser humano que fosse fruto de uma clonagem e fosse educado pelo Estado, que o trata como número, estaria condenado viver privado do entendimento do que é o amor e a doação.

Isto significa que a sexualidade humana, da qual procede o amor ao cônjuge e o desejo da prática sexual à ele, é sagrada por ser o único caminho pelo qual os cônjuges e as crianças terão para aprender o que é o verdeiro amor e doação orientando sua vida para o outro e não mais para si.

Portanto, a sexualidade está relacionada diretamente com o aprendizado do amor dentro da escola da família. Este é o motivo pelo qual o uso do prazer venéreo sem a orientação para a formação de uma família transgride o amor ao próximo ao privar os cônjuges e possíveis filhos da verdadeira doação e amor sem reservas.

Porém, deve-se notar que tudo o que dissemos diz respeito apenas ao matrimônio conforme a natureza. Ora, duas pessoas que recebem o Sacramento do Matrimônio não realizam apenas esta finalidade natural, mas acrescenta-se a ela uma finalidade sobrenatural.

Com efeito, o matrimônio natural se ordena a três coisas: a educação dos filhos, a felicidade do casal e o uso do prazer venéreo.

Porém, deve-se notar que esta estrutura é um obstáculo completo para a vida espiritual. Com efeito, é por ser casado que a pessoa ganha preocupações em agradar o cônjuge, em sustentar a família, em cuidar de doenças dos filhos. Portanto, do ponto de vista espiritual, o matrimônio é um impedimento.

Isto se deve ao fato da vida espiritual exigir uma vida profunda de oração, um desprendimento das coisas e preocupações do mundo e tempo para as coisas espirituais, coisa que o matrimônio natural impede.

Porém, Cristo veio ao mundo trazer a vida espiritual a todos e sabia que a maioria das pessoas teriam se casar de modo que fossem impedidas dela.

Assim, Jesus resolveu o problema elevando o matrimônio a sacramento. Ora, todo sacramento simboliza aquilo que ele contém. Com efeito, o matrimônio simboliza o amor entre Cristo e da Igreja e realmente produz este efeito ao conceder uma graça especial para que os cônjuges se amam como Cristo ama a Igreja.

Isso significa que o matrimônio sacramental é realizado quando os cônjuges tratam um ao outro como se estas pessoas fossem coisas sagradas, quando delegam um amor ao outro como se este fosse a própria humanidade de Cristo presente na Eucaristia ou a própria Virgem Maria em pessoa.

Assim, ao exercer isto constantemente dentro do matrimônio, a vida conjugal se constitui como um verdadeiro treinamento em que os cônjuges aprender e praticam a própria comunhão com Deus. Portanto, o modo de relacionar-se com a esposa será um exercício para o modo como depois irá ser o relacionamento com Cristo na oração.

Nestas circunstâncias o matrimônio sacramental deixa de ser um impedimento e torna-se um verdadeiro auxílio para a vida espiritual. Portanto, o matrimônio assim constituído torna-se o cume do amor ao próximo.

Assim, a sexualidade e o uso do prazer venéreo estão ordenados a uma vida de amor ao próximo neste nível.

Portanto, o indivíduo que orienta sua sexualidade para outras coisas que não sejam este amor descomunal ao próximo, mas orienta para a satisfação do desejo de prazeres venéreos no namoro, na masturbação, na pornografia, no sexo descompromissado, está adulterando a própria sexualidade de modo que quando casar não vai mais ser capaz desta entrega porque acostumou-se a fazer o oposto tendo destruído a única possibilidade de santificar-se profundamente da sua vida.

Assim, pelo fato da pessoa destruir a única chance que ela tinha de uma vida espiritual fora de um monastério e de entender o que é o amor e doação ao próximo, todo pecado contra a dignidade sagrada da sexualidade humana é um pecado gravíssimo.