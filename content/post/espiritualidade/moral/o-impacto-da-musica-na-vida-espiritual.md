+++
date = "2015-12-03T18:00:00-17:00"
title = "O impacto da música na vida espiritual"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/o-impacto-da-musica-na-vida-espiritual.jpg"
+++

A maioria das pessoas pensa que podemos ouvir qualquer música e que os gostos musicais não apresentam nenhum efeito no desenvolvimento de nossa vida espiritual.

<!--more-->

Para se tratar a questão é necessário dizer que a relação da música com a moral vai muito além da sua letra. É problemática a mentalidade de que sendo a letra da música honesta, isto bastaria para considerar conveniente ouvir qualquer uma.

Deve-se notar que esta opinião parte de um certo desconhecimento dos profundos efeitos que a música causa na alma humana e como o seu uso está diretamente relacionado com o máximo desenvolvimento do intelecto e das virtudes.

Com efeito, deve-se notar que a música causa dois efeitos no homem que transcendem o mero deleite sensível causado pelo som.

Em primeiro lugar, a apreensão da harmonia da música desperta na alma uma operação de abstração que serve de excelente exercício para acostumar o homem a perceber verdades abstratas:

>Deve-se entender que o som da harmonia da música é primeiro apreendido pelo ouvido e a música, ao movê-lo proporcionalmente, induz a deleitação, deleitação à qual podem participar todos. Mas, feito isto, na intenção do som harmônico, a inteligência considera a razão e a causa da proporção como algo inteligível segundo si mesmo e em que há alguma perfeição do intelecto que o Filósofo nesta passagem parece chamar de razão para ocupar-se. Esta é uma causa pela qual o Filósofo diz que a música deve ser imediatamente buscada, isto é, por causa do conhecimento da verdade que há nela. <cite>Santo Tomás de Aquino, Comentários a Ética de Aristóteles, Livro VIII</cite>

Para que este argumento fique claro é preciso considerar que a música não existe simplesmente nos sons em si mesmos, mas nas relações de um som com o outro, o que por si já é algo que já possui algum grau de abstração, pois somente a inteligência percebe estas relações de proporção.

Com isto, o homem vai sendo exercitado no conhecimento de realidades abstratas, de modo semelhante ao que ocorre com o estudo da matemática que também dispõe o homem a isso.

Em segundo lugar, a música causa uma deleitação espiritual que é semelhante àquela que o homem se deleita na prática das virtudes:

>A este conhecimento da verdade das proporções musicais segue-se uma deleitação intelectual, como a toda operação do intelecto. O julgamento, porém, das proporções harmônicas, e a deleitação que se lhes segue, pertencem às coisas que são segundo a reta razão e, semelhantemente, pertence também às coisas que são segundo a virtude. Por causa disto o exercício de julgar sobre as mesmas é um certo exercício para as coisas que são segundo a virtude, conforme o Filósofo o declarará em seguida. É neste sentido que a música é dita ter potência para os costumes. (...)

>Acostumar-se a julgar retamente sobre as harmonias musicais e deleitar-se nelas segundo a razão é acostumar-se a julgar retamente acerca das ações morais e deleitar-se retamente nelas. Portanto, isto é eficacíssimo à retidão das ações morais. <cite>Santo Tomás de Aquino, Comentários a Ética de Aristóteles, Livro VIII</cite>

Este segundo efeito consiste no acostumar-se a sentir prazer nas realidades harmoniosas percebidas pela mente, coisa que transcende a mera consideração da nota musical e acrescenta a consideração da proporção entre as notas, do ritmo, da harmonia, da adequação dos instrumentos e se todos os detalhes são apropriados para o momento, para o indivíduo e para o estado de espírito que é conveniente que o indivídio esteja.

Ora, isto também ocorre na prática das virtudes, visto que as obras das virtudes são boas e agradáveis na medida em que se adequam ao juízo da reta razão que estabelece a devida proporção dos atos humanos.

Com efeito, as proporções dos números entre os sons presentes nas melodias e ritmos também existem semelhantemente na proporção entre o homem e as suas ações, que são virtuosas quando possuem a medida adequada e viciosas quando excedem ou carecem da medida adequada.

Por exemplo, dar esmola é virtuoso quando praticado na medida certa. E a medida certa é dar a esmola para quem convém, quando convém, o quanto convém e se convém.

Isto significa que dar esmola a um homem pobre para comprar comida para seus filhos é virtuoso, mas dar esmola para uma pessoa comprar bebidas alcoólicas ou drogas excede a proporção adequada do ato e se torna vicioso.

Outro exemplo é a tristeza que deve ser sentida quando convém, por um motivo conveniente, o quanto convém e se convém. Entristecer-se por ser privado de comer um chocolate é vicioso, mas entristecer-se por ingerir veneno é algo virtuoso.

Portanto, acostumar-se a deleitar-se na proporção adequada de nossas paixões é muito semelhante a deleitar-se nas músicas, o que se manifesta mais claramente quando vemos que as proporções entre os sons e ritmos induzem nossas paixões:

>Tudo isto é manifesto aos sentidos, porque os que ouvem algumas melodias ou ritmos são transformados segundo a alma, às vezes à ira, às vezes à mansidão, às vezes ao temor, o que não ocorre senão por causa de alguma semelhança destas para com aquelas. (...)

>A mesma coisa pode ser dita dos ritmos. Alguns ritmos possuem a virtude pela qual dispõe a um costume instável. Alguns ritmos possuem movimentos aos que é mais pesado e iliberal, outros ao que é mais deleitável e iliberal.<cite>Santo Tomás de Aquino, Comentários a Ética de Aristóteles, Livro VIII</cite>

Este fenômeno indicado por Santo Tomás de Aquino é atualmente conhecido pela ciência e é utilizado propositalmente por programas de TV, cinemas, apresentações teatrais e coisas do tipo, motivo pelo qual as trilhas sonoras de filmes induzem em nós certas emoções que se adequam com a cena em questão.

Ademais, vemos que a emoção que se pretende induzir varia conforme a melodia com proporção semelhante a daquela emoção, ou seja, certas músicas induzem a coragem, outras a tristeza, outras o medo, conforme se percebe ao assistir filmes.

Considerando estas coisas, podemos compreender o motivo pelo qual certos tipos de música, como a música gregoriana e a música clássica, que foram projetadas considerando estas coisas, dispõe positivamente para a concentração na oração, para o estudo, para o gosto pela contemplação da verdade, para as operações de abstração e para a operação das virtudes.

Um estudo interessante demonstra que a [música de Mozart foi capaz de ativar circuitos neuronais que favoreciam as atividades cerebrais de alto nível](http://ipco.org.br/ipco/noticias/musica-de-mozart-melhora-a-atividade-cerebral), enquanto que a música de Beethoven não causou o mesmo efeito.

Ora, se a música de Beethoven, que é tão superior em composição e harmonia do que a maior parte do que se oferece atualmente, já não eleva a mente como a de Mozart, qual não seria o grande empecilho no desenvolvimento intelectual e na aquisição de bons costumes que causariam músicas que nos incitam à irritabilidade e confusão, como o Rock, ou à sensualidade, como o sertanejos universitário e o funk, amplamente difundidos entre jovens e adultos?

Com efeito, um outro estudo demonstra que [os gatos ficaram mais perturbados com o Heavy Metal](http://ipco.org.br/ipco/noticias/gatos-odeiam-heavy-metal-e-adoram-os-compositores-classicos). Um terceiro caso é o [impacto que músicas mais calmas causaram melhor produção de leite das vacas](http://g1.globo.com/espirito-santo/noticia/2013/09/pecuarista-do-es-faz-vacas-ouvirem-musica-para-melhor-producao-de-leite.html). Deve-se notar que se os animais se perturbam com tais músicas somente pela consideração sensível do som, muito mais o homem que considera também as proporções da obra.

Por isso, devemos avaliar atenta e sabiamente o tipo de obra musical que ouvimos, pois o impacto deste tipo de material na disposição passional e intelectual do indivíduo é vasto e dificilmente reversível.

Assim, depreende-se que a música tem uma relação direta com a possibilidade do homem alcançar a felicidade:

>Se assim for a música, é manifesto que os que se utilizam dela mais se inclinarão à virtude e aos costumes, inclinados à virtude mais operarão segundo a razão e operando segundo a razão facilmente alcançarão a felicidade que consiste na perfeitíssima operação do homem segundo a sua suprema virtude em relação ao seu perfeitíssimo objeto. <cite>Santo Tomás de Aquino, Comentários a Ética de Aristóteles, Livro VIII</cite>