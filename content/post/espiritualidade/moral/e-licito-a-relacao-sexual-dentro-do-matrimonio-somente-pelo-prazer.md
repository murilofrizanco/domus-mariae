+++
date = "2016-02-22T18:00:00-17:00"
title = "É lícito a relação sexual dentro do matrimônio somente pelo prazer?"
categories = ["Espiritualidade","Moral"]
path = "Moral"
featured_image = "posts/images/e-licito-a-relacao-sexual-dentro-do-matrimonio-somente-pelo-prazer.jpg"
+++

Uma das coisas que poucas pessoas se perguntam é se depois de casadas elas podem ter relações sexuais somente pelo prazer. Com efeito, não é incomum que algumas pessoas entendem que os métodos naturais para impedir a procriação são alternativas lícitas e católicas para os anticoncepcionais artificiais.

<!--more-->

Quanto a isto primeiro deve-se considerar que o matrimônio torna relações sexuais legítimas e moralmente lícitas por razões que explicamos em [outro lugar](/posts/espiritualidade/moral/porque-os-atos-sexuais-fora-do-matrimonio-constituem-pecado).

Em segundo lugar, que os bens do matrimônio são três: 

Em terceiro lugar, que o uso de métodos naturais para regular a fertilidade, como por exemplo o Método de Ovulação Billings, é moralmente lícito quando a decisão é tomada:

>Por motivos graves e com respeito pela lei moral, de evitar temporariamente, ou mesmo por tempo indeterminado, um novo nascimento. <cite>Humanae Vitae, 10</cite>

Porém, isto parece causar uma brecha na moral na qual seria possível que um casal pudesse buscar e usufruir do prazer venéreo como um fim sem com isso incorrer em pecado algum. Este entendimento existe em alguns casais que se utilizam dos métodos naturais.

Ademais, muitas pessoas pensam que somente antes do casamento existem pecados sexuais. Porém, a verdade diz exatamente o contrário.

Com efeito, tanto o entendimento filosófico como teológico percebem que:

>Os os cônjuges só podem consumar a conjunção carnal, sem pecado, por duas razões: com o fim de ter filhos e de pagar o débito. Do contrário sempre é pecado, ao menos venial. <cite>Suma Teológica, Suplemento, q. 49, a. 5</cite>

Por "pagamento do débito" se entendem a prática da relação sexual quando esta é pedida razoavelmente pelo cônjuge. Além disso, deve-se notar que

>"Por filhos se entende não só a procriação, mas também a educação deles, para a qual, como para o fim, se ordenam todos os trabalhos partilhados por marido e mulher, enquanto unidos pelo matrimônio" <cite>Suma Teológica, Suplemento, q. 49, a. 2</cite>

Portanto, sempre que o casal desejar o ato sexual sem que neste desejo esteja incluído a formação ou o crescimento de sua família ou a satisfação do desejo do cônjuge, então há alguma pecado.

Embora seja verdade que o prazer obtido no ato conjugal não seja pecado, deve-se notar que, apesar disso, comete um pecado que o busca somente pelo prazer. Com efeito,

>"Se porém [somente] o prazer for procurado dentro dos limites do matrimônio, de modo que não se quisesse tê-lo com outra a não ser com a esposa, então o até é pecado venial." <cite>Suma Teológica, Suplemento, q. 49, a. 6</cite>

Deste modo é evidente que qualquer casal que use os métodos naturais para não ter filhos sem um motivo realmente grave incorre em um pecado venial. Por isso, não é lícito que o casal deseje a relação somente pelo prazer, pois isto vai contra a ordem natural e, por consequência, impede o progresso na vida espiritual, ao qual deve se ordenar todos os nossos atos.

Contudo, deve-se notar que este desejo de procriar e ou de pagar o débito não precisa ser explícito, como se em cada ato sexual o casal precisasse prometer ou pensar explicitamente que é com esta finalidade que vão usar-se do ato conjugal.

Embora seja louvável que se tenha traga a mente esta realidade, para que o ato conjugal seja lícito basta que este desejo seja implícito, isto é, basta que o casal não coloque, por vontade própria e sem necessidade grave, qualquer impedimento para que de sua relação sexual possa gerar-se uma nova vida ou que tenha qualquer aversão a essa possibilidade.

Apesar de ser difícil um casal realmente católico unido em matrimônio cair neste tipo de pecado, a mentalidade moderna tem fomentado em alguns uma mentalidade contrária ao Evangelho e à santidade do matrimônio.