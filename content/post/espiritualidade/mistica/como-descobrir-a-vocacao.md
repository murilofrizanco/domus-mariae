+++
date = "2015-09-28T18:00:00-17:00"
title = "Como descobrir a vocação?"
categories = ["Espiritualidade","Mistica"]
path = "Mística"
featured_image = "posts/images/como-descobrir-a-vocacao.jpg"
+++

A temática da vocação é uma questão importante na vivência cristã. Porém, tem se tornado motivo de perturbação para os fiéis que desejam discernir seu chamado.

<!--more-->

Infelizmente, a vocação é entendida por muitos não como uma chamado divino, mas sim como a escolha pessoal daquilo que mais nos agrada.

Devido a isto, nasce uma profunda perturbação e um certo impulso para discernir a vocação o quanto antes e ao mesmo tempo um medo constante de ter feito a escolha errada.

Deve-se notar que a vocação não é uma escolha autônoma do ser humano, mas uma resposta a um chamado divino para alcançar a perfeição neste ou naquele estado.

Apesar deste consenso, muitos não compreendem o que isto significa. Portanto, pretendemos colocar algumas considerações que podem auxiliar as pessoas a resolver estas perturbações e discernir a vocação.

Primeiro. A vocação particular pressupõe a resposta a vocação universal.

Ora, Cristo faz uma chamado universal a todos os homens para que abraçem a salvação por Ele oferecida.

Assim, não faz sentido pensar ou preocupar-se com uma vocação em particular quando ainda nem respondemos satisfatoriamente ao chamado de salvar a nossa própria alma.

A vocação particular não tem o objetivo principal de nos salvar, mas sim de nos levar até a perfeição na caridade.

Portanto, antes de pensarmos na vocação devemos buscar uma prática constante da oração, desprezar as realidades terrenas e praticar as virtudes, ou seja, que vivamos de modo que tenhamos condições de nos mantermos em estado de graça e de recuperá-lo rapidamente no caso de uma queda.

Somente depois disto é que a vocação individual poderá ser algo a ser seriamente considerado.

Segundo. A vocação não se mistura com a perturbação.

Com efeito, o Espírito Santo age em nós suavemente e sutilmente, de modo que muitas vezes fazemos as coisas impossíveis através de sua graça e nem sequer percebemos.

Assim, aquelas pessoas que vivem se perturbando se a vocação é esta ou aquela, que perdem a paz interior por causa desta temática ou até mesmo deixa de agir por desconhecer atualmente o chamado de Deus podem estar seguras de que essa dúvida não é um verdadeiro chamado divino.

Com efeito, a perturbação da alma procede ou do demônio ou da nossa carne e não pode proceder de Deus.

Quem se encontra nesta situação deve desprezar o assunto e concentrar-se em progredir na vida espiritual seriamente.

Terceiro. A vocação é discernida ao ouvir a voz divina.

Ora, a vocação é verdadeiramente um chamado de Deus para o homem. Se é um chamado, então quer dizer que o homem deve ser capaz de ouví-lo. Porém, somente se aprende a ouvir a voz de Cristo que constantemente fala em nossa alma através da prática constante e correta da oração.

Deve-se notar que dizemos "voz silenciosa" justamente porque ordinariamente não envolve uma visão profética, um arrepio ou uma experiência milagrosa, mas apenas aquela intimidade constante e diária que a alma adquiriu com o Cristo.

Portanto, o que será um excelente critério para discernir a vocação é aquela certeza e firmeza de decisão que procede da experiência de ouvir aquela voz silenciosa que fala dentro de nossa alma na oração.

Este tipo de certeza não deixa a alma em dúvida, nem perturbada e nem se acaba com o tempo, a menos que a alma tente a bondade divina ao recusar responder ao chamado.

Quarto. O matrimônio não se opõe a vida religiosa quando vivido em plenitude.

Deve-se notar que a maioria da humanidade, por disposição divina, não é chamada nem ao sacerdócio e nem à vida religiosa. Portanto, a maioria terá como caminho da perfeição a vivência do sacramento matrimônio.

A consideração da perfeição religiosa leva muitas pessoas chamadas ao matrimônio a vivenciar uma perturbação por não fazer os votos e adentrar em um convento. Porém, isto se deve a uma impressão de que a vida religiosa e a vida matrimonial se opõe ou são inimigas.

Este erro deve ser completamente deixado de lado, pois uma vivência plena do sacramento do matrimônio não apresenta uma diferença tão significativa em relação a vida religiosa.

Com efeito, um casal muito santo vive de modo semelhante a um monge: tem as orações em comum, tem o trabalho diário, não busca entretenimentos fúteis, não se perturba com as coisas materiais e assim por diante.

Portanto, aceitar o matrimônio não é um rebaixamento, mas sim uma busca equivalente pela perfeição cristã.

É verdade que sob condições ideais os religiosos e sacerdotes alcançam uma união mais profunda com Cristo. Porém, na prática nem sempre isso acontece e uma pessoa que insistir na vida consagrada sem ter sido chamada pode condenar-se por aceitar responsabilidades que não é capaz de cumprir.

Ademais, deve-se notar que a aceitação do matrimônio não significa que a pessoa rejeitou a possibilidade de adentrar na vida consagrada, pois muitos foram os santos que tiveram o matrimônio como uma verdadeira preparação para uma vida religiosa após a morte do cônjuge.

Assim, ao escutar na oração a voz de Deus com uma convicção certa, firme e duradoura podemos ter uma certeza daquilo que será agradável a Deus e poderemos empreender todos os esforços humanos para alcançar o estado de vida a que somos destinados, seja no matrimônio, no sacerdócio, na vida religiosa e assim por diante.