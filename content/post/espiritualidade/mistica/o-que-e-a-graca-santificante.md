+++
date = "2015-02-01T18:00:00-17:00"
title = "O que é a graça santificante?"
categories = ["Espiritualidade","Mistica"]
path = "Mística"
featured_image = "posts/images/o-que-e-a-graca-santificante.jpg"
+++

Um dos termos mais utilizados na teologia espiritual como também nas pregações dos santos é "graça santificante".

<!--more-->

Talvez nunca tenhamos sido surpreendidos com a necessidade de ter que dizer em que ela consiste e até incorporamos esta palavra em nosso vocabulário sem conhecer a sua profundidade e pleno significado. Por isso, é oportuno ter este conhecimento.

A graça santificante se define como:

>Uma qualidade sobrenatural inerente à nossa alma que nos dá uma participação, física e formal, embora análoga e acidental, da natureza mesma de Deus sob sua própria razão de divindade. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 86</cite>

Para entender o que esta definição quer significar, vamos explicá-la parte por parte.

A) "É uma qualidade": a qualidade é como determinada coisa está disposta ou ordenada em relação à algo, aqui no caso a graça é um modo de dispôr a nossa natureza humana de maneira que se ordene para Deus.

B) "Sobrenatural": aqui se quer dizer que a graça é algo que excede nossa natureza humana, ou seja, não está em nosso poder alcançá-la por força própria, produzí-la e nem existe em todos os homens naturalmente.

C) "Inerente à nossa alma": aqui se quer dizer que esta qualidade se encontra na nossa alma, ou seja, a graça altera realmente a alma humana de alguma forma e a torna capaz de coisas que não conseguia sem ela. Notemos que quando se fala que inere na alma não se quer dizer que a graça esteja em alguma potência da alma, isto é, alguma capacidade dela, como se estivesse na inteligência ou na vontade. O que se quer dizer é que a graça se encontra na essência mesma da alma, ou seja, não afeta uma ou outra capacidade da alma humana, mas se estende a muitas.

D) "Que nos dá uma participação, física e formal, embora análoga e acidental, da natureza mesma de Deus": por participação se deseja expressar que a alma não se torna o próprio Deus, mas tem alguma coisa de divina. Quando se diz que é física se quer dizer que não é mera aparência, mas é uma alteração real na alma, ou seja, uma alma que a possui e uma sem são realmente distintas. Por formal se quer dizer que a alma não recebe apenas alguma capacidade divina, mas realmente se torna semelhante à Deus. A palavra análoga quer dizer que o é possuído de divino pela alma é diferente ao que Deus mesmo possui, embora tenha alguma semelhança. A palavra acidental quer dizer que a graça não é algo que existe na própria alma por si mesma, mas que é acrescentado à ela e que a alma pode existir sem a graça.

"Sob sua própria razão de divindade": aqui se quer dizer que não se participa da natureza de Deus enquanto ser inteligente, ou seja, enquanto se tem algo de semelhante com a inteligência divina, mas que se assemelha à Deus enquanto divindade, isto é, o sujeito adquire características divinas.

Com esta definição fica mais claro o entendimento de qualquer estudo que envolva este termo tão consagrado na teologia católica e tão amplamente utilizado pela mesma.