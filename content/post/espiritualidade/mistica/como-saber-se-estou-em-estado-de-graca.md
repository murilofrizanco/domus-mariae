+++
date = "2015-03-06T18:00:00-17:00"
title = "Como saber se estou em estado de graça?"
categories = ["Espiritualidade","Mistica"]
path = "Mística"
featured_image = "posts/images/como-saber-se-estou-em-estado-de-graca.jpg"
+++

Muito se fala da conservar e sustentar a graça santificante recebida em nossa alma pelo batismo e que pode vir a ser destruída com um único pecado mortal.

<!--more-->

Muitos são os irmãos que, conscientes disto, colocam todo seu empenho na defesa e proteção deste riquíssimo tesouro que nos permite exclamar: "a graça de um indivíduo vale mais que o bem do universo inteiro"!

Por sua vez, muitas são as quedas no pecado mortal que devastam este grandioso presente e muitas também são as confissões que restauram a mesma. 

Porém, as vezes cometemos pecados que não temos certeza se consentimos completamente ou se realmente era matéria grave, gerando uma dúvida quanto a saber se permenecemos ou não em estado de graça.

Daí a questão: qual seria um meio de saber se tal tesouro está conservado ou se foi saqueado?

Esta questão importantíssima é levantada por Santo Tomás na Suma Teológica, onde se pergunta se é possível ao homem saber que tem a graça.

Para tanto, percebe que é possível termos tal conhecimento, mas de maneiras diferentes e com graus de certeza diferentes através de três meios.

Primeiro. Pela revelação divina:

>Deus o revela, às vezes a certos, por um privilégio especial, para já nesta vida, começarem a gozar a alegria da segurança, e prossigam, mais confiante e fortemente, nas suas obras magníficas, e suportem os males da vida presente. <cite>Suma Teológica, Ia IIae, q. 112, a. 5</cite>

Segundo. Por um conhecimento pleno e total daquilo que sustenta e destrói a graça.

Com efeito, se tivéssemos um conhecimento pleno e total do que produz e sustenta a graça santificante, ou seja, conhecer todos os detalhes da causa da graça sem deixar nenhum de lado, seria possível se chegar a esta certeza.

Porém, deve-se notar que como a causa da graça é o próprio Deus que "por causa da sua excelência, é-nos desconhecido", não é possível saber se estamos em graça ou não, pois o pleno conhecimento da causa excede nossa capacidade.

Terceiro. Através de sinais e efeitos:

>Desta maneira podemos saber que temos a graça, percebendo que pomos em Deus o nosso prazer e desprezamos as coisas mundanas e não tendo consciência de nenhum pecado mortal. <cite>Suma Teológica, Ia IIae, q. 112, a. 5</cite>

Assim, através de nosso conhecimento imperfeito de Deus podemos ter este parâmetro acessível para ter este conhecimento à respeito da presença ou ausência da graça santificante.

Com efeito, não é possível que alguém opere um ato por força dos dons do Espírito Santo sem que esteja em estado de graça, pois:

>Assim como as virtudes morais se ligam umas às outras por meio da prudência, assim os dons do Espírito Santo, pela caridade, de modo tal que, quem a tiver, tem todos os dons do Espírito, e ninguém os pode ter sem a caridade. <cite>Suma Teológica, Ia IIae, q. 68, a. 5</cite>

Por outro lado, ninguém é capaz continuar em posse da graça santificante após praticar um pecado mortal.

Estes sinais vão se tornando cada vez mais manifestos para a alma na medida em que progride na vida espiritual e vai distinguindo as obras do Espírito das obras da carne.

Logo, observamos que, salvo revelação divina e numa vida espiritual avançada, o conhecimento da presença da graça santificante em nossa alma é algo possível de se perceber por certos sinais.

Deve-se notar que isto é conveniente para que não transformemos a nossa vida espiritual num cálculo matemático do quanto é possível alimentar as próprias paixões sem cometer um pecado mortal.

Que evitemos os pecados, pratiquemos as virtudes, busquemos a sabedoria e coloquemos nossas delícias na união com Cristo que se dá pela oração para que possamos assegurar nossa salvação e chegar ao cume da perfeição cristã.