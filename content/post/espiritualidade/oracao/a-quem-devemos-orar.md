+++
date = "2014-07-12T18:00:00-17:00"
title = "A quem devemos orar?"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/a-quem-devemos-orar.jpg"
+++

Se a verdadeira oração é direcionada exclusivamente a Deus, não estaríamos errados ao buscar a intercessão dos santos? A qual das pessoas da Trindade devemos orar?

<!--more-->

Conforme explica um grande sacerdote:

>A oração de súplica, por razão do sujeito sobre o qual recai, pode considerar-se de duas maneiras: 

>a) enquanto que se pede algo a outro diretamente e para que ele mesmo nos dê

>b)indiretamente, para que nos consiga de outra pessoa superior (intercessão simples) <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Do primeiro modo (a) jamais poderíamos fazer pedidos ao santos e somente a Deus poderíamos pedir desta maneira, pois a graça e a glória só podem ser dadas pelo próprio Deus, jamais pelos santos. Portanto, se pedirmos aos santos esperando que deles venham a graça e a glória, estaríamos incorrendo em idolatria. Pois, conforme afirma o salmista: "Deus concede a graça e a glória" (Sl 83, 12).

Vale ressaltar que quando se fala "Deus" inclui-se orar tanto ao Pai, como ao Filho ou ainda ao Espírito Santo, pois na realidade nossa oração é sempre direcionada à Santíssima Trindade, mesmo que a direcionemos somente a uma de suas Pessoas, pois onde está uma das Pessoas, necessariamente estão as outras três, pois são inseparáveis. Disso nasce uma diferença a quem direcionamos a nossa oração e a quem outras religiões monoteístas dirigem suas orações, pois embora monoteístas, dirigem suas orações a um Deus inexistente, embora possam ser ouvidas por pura misericórdia e como meio de levá-los ao pleno conhecimento de Deus.

Do segundo modo (b) podemos e é muito conveniente que o façamos, podendo direcionar aos anjos ou aos santos. O poder da intercessão dos santos em nosso favor é proporcional ao grau de méritos adquiridos nesta vida e ao seu grau de glória correspondente nos céus. Quanto maior o grau de santidade alcançado, maior será o seu poder de intercessão, pois sua oração é mais agradável e aceita por Deus que a dos demais.

Neste sentido a intercessão mais poderosa que existe é a da Santíssima Virgem Maria, pois possui um grau de santidade maior que o de todos os anjos e santos, por causa de sua dignidade de Mãe de Deus. Deste fato vemos aquelas palavras ardentes:

>As orações de Maria junto à Deus tem mais poder junto da Majestade Divina que as preces e intercessões de todos os anjos e santos do Céu e da Terra. <cite>Santo Agostinho de Hipona</cite>

>No céu, Maria dá as ordens aos anjos e aos bem-aventurados. <cite>São Luís Maria de Montfort</cite>

Em segundo lugar vem São José, ao qual devemos tê-lo como o maior dos santos depois da Virgem, conforme vários santos testemunham:

>Quem não achar mestre que lhe ensine a orar, tome este glorioso Santo por mestre, e não errará no caminho <cite>Santa teresa D'ávila</cite>

>Nosso Pai e Senhor São José é Mestre da vida interior. Coloca-te sob o seu patrocínio e sentirás a eficácia do seu poder. <cite>São Josemaria Escrivá</cite>

Porém isto não significa que devemos omitir ou desprezar os demais santos e invocar unicamente a Virgem Maria ou os santos de história mais fantástica.

Santo Tomás coloca cinco razões pelas quais é conveniente a invocação dos santos inferiores:

>1) Talvez nos inspire maior devoção um santo inferior que um superior.

>2) Para que haja certa variedade que evite a monotonia ou aborrecimento.

>3) Há santos especialistas em algumas graças.

>4) Para dar a devida honra a todos.

>5) Porque podem conseguir, entre todos, o que um somente não conseguiu.

Portanto oremos amparados pela oração dos santos, nesta comunhão belíssima e santíssima do Corpo Místico de Cristo, que torna-nos membros uns dos outros e nos une num só Corpo e num só Espírito.