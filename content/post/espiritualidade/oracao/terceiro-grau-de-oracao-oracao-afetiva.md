+++
date = "2014-11-20T18:00:00-17:00"
title = "Terceiro Grau de Oração: Oração Afetiva"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/terceiro-grau-de-oracao-oracao-afetiva.jpg"
+++

Este é o terceiro grau de oração que surge com o aperfeiçoamento da prática da meditação.

<!--more-->

Alguém poderia perguntar: se a meditação só é verdadeira oração quando ela produz o afeto, porque este grau de oração seria diferente do anterior?

Deve-se notar que na meditação prevalece o discurso racional sobre o afeto. Assim, a maior parte do tempo é empregado pensando e meditando na verdade dos mistérios da fé chegando somente ao final de cada meditação a um ato em que se ama aqueles mistérios e seu Autor.

Todavia, na oração afetiva prevalece muito mais o afeto do que o discurso intelectual. Assim, é como uma meditação que vai se tornando mais simples e que com cada vez menos raciocínios se produz afetos mais intensos e prolongados. Deste modo, o esforço da razão vai diminuindo, conforme explica com precisão um grande autor:

>A oração afetiva é aquela na qual predomina os afetos da vontade sobre o discurso do entendimento. É como uma meditação simplificada em que cada vez vai tomando maior preponderância o coração acima do prévio trabalho discursivo. Cremos, por isso mesmo, que não há diefrença específica entre ela e a meditação, como há entre esta e a contemplação. Se trata, repetimos, de uma meditação simplificada e orientada ao coração; nada mais. Isto explica que o trânsito de uma a outra se faça de uma maneira gradual e insensível, embora com maior ou menor rapidez ou facilidade. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 391</cite>

Podemos entender esta diferença de forma mais prática. Quando produzimos um ato de afeto na oração é como se segurássemos um objeto com força. Ora, a mão que segura com firmeza é a nossa vontade que se aplica a amar e o objeto segurado é Deus enquanto autor daqueles mistérios. Na meditação, a mão segura firmemente o objeto, mas vai perdendo a força como se ficasse cansada, exigindo um tempo de descanso para recuperar-se e segurá-lo novamente, que é feito pelo raciocínio; na oração afetiva ocorre o mesmo, com a diferença de que o tempo de descanso é menor e passa-se mais tempo segurando o objeto do que recuperando as forças.

Esta analogia demonstra como foi sábia a comparação do Apóstolo da vida espiritual com o treinamento físico:

>Os atletas se abstêm de tudo; eles, para ganhar uma coroa perecível; nós, porém, para ganhar uma coroa imperecível. <cite>Bíblia de Jerusalém, Primeira Epístola aos Coríntios, 9, 25</cite>

Devemos apenas tomar cuidado para que não ocorra uma oração puramente afetiva sem qualquer impulso inicial da razão, por menor que seja, conforme o mesmo autor adverte:

>O que nunca pode dar-se é uma oração pura e exclusivamente afetiva sem nenhum conhecimento prévio. A vontade é potência cega, e só pode lançar-se a amar o bem que o entendimento a apresenta. Mas, acostumado o entendimento pelas meditações anteriores a encontrar facilmente esse bem, o apresentará cada vez com maior prontidão à vontade, proporcionando a matéria da oração afetiva. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 391</cite>

Aqui vemos que a neste grau de oração prevalece o ato de amor, porém não um amor cego, mas um amor direcionado a um objeto específico que o intelecto propõe, como por exemplo um mistério do rosário, um trecho das Sagradas Escrituras, um escrito de algum santo, um ícone, a presença real de Cristo na oração, etc.

É comum que este seja um dos graus de oração em que os afetos produzam várias emoções na pessoa, como chorar, se emocionar diante de um mistério ou ainda diante do carinho que se sente com tão grande Salvador. Isso não significa que nos graus anteriores isso não ocorra, mas pela facilidade com que se brota o amor diante do sagrado e pela alma já estar muito mais afastada dos pensamentos mundanos, não é incomum que mais facilmente as nossas emoções surjam diante das coisas sagradas.

Este grau de oração prevalece na terceira morada de Santa Teresa em seu livro Castelo Interior e pertence ainda a via purgativa. Estas almas são chamadas de "almas piedosas" e apresentam as seguintes características:

>Pecado mortal: Raríssimo. Arrependimento vivo, confissão imediata, precauções para evitar as recaídas.

>Pecado venial: Sinceramente combatido. Exame particular, mas com pouca constância e fruto escassos.

>Práticas de piedade: Missa e comunhão diárias, mas com certo espírito de rotina. Confissão semanal, mas com escassa emenda dos defeitos. Rosário em família. Visita ao santíssimo. Via sacra semanal, etc.

>Oração: Meditação diária, mas sem grande empenho em fazê-la bem. Muitas distrações. Omissão fácil, sobretudo quando surgem securas e ocupações, que se poderiam ter evitado sem faltar aos deveres do próprio estado. Com frequência, oração afetiva, que tende a simplificar-se cada vez mais. Começa a noite do sentido, como trânsito à via iluminativa. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 155</cite>