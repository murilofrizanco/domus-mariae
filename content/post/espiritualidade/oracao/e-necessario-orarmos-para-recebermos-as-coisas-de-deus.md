+++
date = "2014-07-04T18:00:00-17:00"
title = "É necessário orarmos para recebermos as coisas de Deus?"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/e-necessario-orarmos-para-recebermos-as-coisas-de-deus.jpg"
+++

Esta é uma dúvida frequente, pois Deus, sendo onisciente, sabe tudo o que precisamos e portanto seria absolutamente inútil eu pedir as coisas. Outra questão é que Deus não muda, portanto seria inútil tentar mudar sua vontade com nossos pedidos.

<!--more-->

Sabemos que Deus não muda e que Ele também já sabe tudo o que precisamos. Porque então Cristo deixou o célebre ensinamento de "orar em todo tempo e não desfalecer"?

É preciso entender aqui onde a oração se harmoniza com estas verdades e seu papel no plano da Divina Providência.

Santo Tomás esclarece que a Divina Providência determina todos os efeitos que serão produzidos no mundo, por quais causas secundárias (a primária é sempre o próprio Deus) e em que ordem.

Entre as causas secundárias se encontram, principalmente, os atos humanos, sendo a oração um dos mais importantes. Portanto temos que orar, não para mudar a vontade ou os desígnios de Deus, mas para obter por meio desta o que, desde toda a eternidade, foi determinado ser concedida por ela.

Por isso a oração é causa no sentido em que Deus dispôs as coisas de forma a estarem vinculadas umas as outras e que se produza um efeito quando ocorra suas causas.

Isso é o mesmo que ocorre com nossa alimentação. A Providência Divina já determinou tudo o que precisamos para nos alimentarmos, porém só o conseguimos quando seguimos a harmoniosa ordem estabelecida por Ela: semeando, cultivando, colhendo para somente então termos o alimento. Todos que fizerem estes passo terão acesso ao alimento. O mesmo vale para a oração.

Disto vemos que quando oramos estamos entrando nos planos eternos que Deus estabeleceu. Por isso podemos apenas pedir o que for conforme a vontade Dele. Se fizermos o contrário, iremos não somente desagradá-Lo, mas faremos uma oração completamente inútil e estéril, sem conseguir absolutamente nada. Este é o motivo pelo qual muitas pessoas pedem coisas, normalmente temporais e sem abrir mão delas se não fossem a vontade de Deus, e nunca são atendidas, perdendo tempo e desagradando à Deus.

Portanto é absolutamente necessário que se peça somente o que for para a glória de Deus ou a salvação das almas (própria ou alheia), embora possamos pedir as coisas temporais para servirem de instrumentos a estas.

Um exemplo claríssimo é a graça para resistir ao pecado e progredir na virtude, sem a qual não poderíamos realizar estes atos. Só podemos obtê-la, com certeza, através da oração.

Outro exemplo é em relação às coisas temporais ou ao curso da história, onde por algumas vezes a Providência vincula certos acontecimentos à oração das pessoas. Como é o caso de alguns doentes onde as possibilidades médicas foram ineficazes, mas que quando atendido a necessidade da oração fica curado por um milagre.

Portanto é absolutamente necessário orar para que possamos receber as graças que, desde toda eternidade, Deus vinculou às nossas orações.

Com isso, vemos que Deus não muda e sabe de nossas necessidades, mas nem por isso Ele anula as causas humanas. Assim como Ele estabeleceu que da semeadura vem a colheita (e não pode vir sem ela), a graça é consequência da oração.

Além destes fatos essenciais, existem outras conveniências que vem da oração:

>1) Praticamos um ato excelente de religião.

>2) Damos graças à Deus por seus imensos benefícios.

>3) Exercitamos a humildade, reconhecendo nossa pobreza e pedindo uma esmola.

>4) Exercitamos a confiança em Deus ao pedir-lhe coisas que esperamos obter de vossa Bondade.

>5) Nos leva a uma respeitosa familiaridade com Deus, que é nosso amantíssimo Pai.

>6) Entramos nos desígnios de Deus, que nos concederá as graças que tem desde toda eternidade vinculadas a nossa oração. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Que possamos nos entregar realmente à verdadeira vida espiritual que tira sua eficácia da oração e eleva à participantes dos planos da Divina Providência.