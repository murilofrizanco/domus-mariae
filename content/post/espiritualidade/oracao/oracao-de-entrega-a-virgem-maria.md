+++
date = "2014-07-14T18:00:00-17:00"
title = "Oração de entrega à Virgem Maria"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/oracao-de-entrega-a-virgem-maria.jpg"
+++

Esta oração é baseada na espiritualidade de entrega total à Virgem Santíssima apresentada por São Luís de Montfort no seu Tratado da Verdadeira Devoção.

<!--more-->

>Oh Virgem Santíssima, que tudo o que eu faça seja limpo de todo meu apego e de todas as minhas intenções, por melhores que sejam, para que sejam feitos não por mim, mas pelas tuas mãos virginais. Por isso, como fiel escravo de amor, renuncio à todas as minhas intenções, objetivos, paixões e desejos para que restem apenas as suas santas disposições, ó Mãe querida, sendo eu apenas um instrumento dócil em suas mãos.

>Que não falte a tua presença e conselhos à este seu pequeno servo para que eu mantenha meus olhos fixos em ti, ó Virgem gloriosa, imitando vossas virtudes como modelo perfeito e sublime.

>Que eu realize tudo em seu seio maternal para que eu possa estar protegido do mundo, do pecado e do demônio e seja levado, como a Senhora, aos mais altos graus de contemplação. Que prevaleça sobre mim os sete dons do Espírito Santo para que, na certeza de estar em teu colo maternal, o qual não há lugar mais doce e afável nem no céu e nem na terra, permaneça unido à teu Divino Filho.

>Enfim vos peço que tudo o que eu faça seja para vossa realeza e benefício, ó doce Maria, desejando somente a glória de Deus e pedindo apenas como recompensa que eu pertença à tão amável e admirável Rainha na qualidade de escravo por amor. Doce, suave, santa, belíssima e puríssima Virgem, rogai por nós.