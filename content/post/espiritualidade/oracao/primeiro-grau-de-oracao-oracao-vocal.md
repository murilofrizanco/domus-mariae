+++
date = "2014-07-31T18:00:00-17:00"
title = "Primeiro grau de oração: Oração Vocal"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/primeiro-grau-de-oracao-oracao-vocal.jpg"
+++

Daremos início a analisar o primeiro e mais básico grau de oração, sendo que este é por onde todos devemos começar para ir aprofundando na vida espiritual.

<!--more-->

Esta etapa da vida espiritual é predominantemente ascética, ou seja, é constituída principalmente pelo esforço humano, embora amparado pela graça do Espírito Santo, motivo pelo qual a cooperação divina muitas vezes não é claramente perceptível nem pela própria pessoa.

Este primeiro grau se manifesta com as palavras de nossa linguagem articulada e constitui o único modo de oração pública ou litúrgica.

Embora seja o grau mais inferior, é completamente necessário nas orações públicas, pois não há outro modo pelo qual os fiéis possam intervir nestas orações.

Na oração privada não é sempre necessária, mas é conveniente por três motivos:

>a) Para excitar a devoção interior, pela qual se eleva a alma a Deus; de onde deve-se concluir que devemos usar as palavras exteriores na medida e grau que excitem nossa devoção, e não mais; se nos servem de distração para a devoção interior, deve-se calar, a não ser, naturalmente, que a oração vocal seja obrigatória para o que a utilize, como o é para o sacerdote e religioso de voto solene a oração do breviário;

>b) Para oferecer a Deus a homenagem de nosso corpo além de nossa alma;

>c) Para desabafar ao exterior a veemência do afeto interior. <cite>Pe. Antonio Royo Marin, Teologia de la Perfección Cristiana</cite>

É importante notar que a oração vocal é completamente dependente e subordinada a mental. Só tem sentido esta oração se ela estiver unida a oração mental. Uma oração vocal que tenha se desligado da mental deixa de ser verdadeira oração e passa a ser apenas um exercício mecânico e uma verdadeira perda de tempo.

Desta doutrina extraímos uma aprendizado interessante. Ao contrário do que parece, não devemos permanecer orando vocalmente se o podemos fazer melhor somente de forma mental e devemos renunciar as palavras se a oração mental for mais frutuosa.

Muitas pessoas tem dificuldade de orar nas primeiras etapas justamente por confundir a devoção com as devoções e pensar que se não cumprirem a pronuncia exata e metódica de algumas palavras não fazem oração. Sempre devemos praticar o método que mais excite em nós a caridade, independente de qual seja.

O primeiro grau de oração prevalece na primeira morada de Santa Teresa e pertence a via purgativa. Podemos chamar as pessoas nesta etapa de "almas crentes". As características principais destas almas são as seguintes:

>Pecado mortal: Debilmente combatido, mas com sincero arrependimento e verdadeiras confissões. Com frequência, ocasiões de pecado perigosas buscadas de forma voluntária.

>Pecado Venial: Nenhum esforço para evitá-lo. Concede pouquíssima importância a ele.

>Práticas de Piedade: Somente as preceituadas pela Igreja (missa dominical, confissão anual, ...). Algumas omissões. As vezes, algumas práticas excessivas.

>Oração: Puramente vocal, poucas vezes e com muitas distrações. Pedidos humanos, de interesses temporais (saúde, riqueza, bem-estar, ...), raras vezes de tipo espiritual. <cite>Pe. Antonio Royo Marin, Teologia de la Perfección Cristiana</cite>

Que possamos cada vez mais aprofundar na vida espiritual, passo a passo, para que nos elevemos dos graus mais inferiores para o mais alto cume de santidade e união com Cristo.