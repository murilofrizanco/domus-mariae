+++
date = "2014-07-11T18:00:00-17:00"
title = "Porque me distraio na oração?"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/porque-me-distraio-na-oracao.jpg"
+++

Uma das coisas mais comuns que acontece durante nossos momentos de oração são as distrações, que podem apenas atrapalhar a concentração ou podem chegar a tornar impossível unir-se a Cristo pela oração. É preciso, para combatê-las, entender sua origem e que também combatê-las contribui grandiosamente para nossa santificação.

<!--more-->

As distrações podem ter duas origens: voluntárias ou involuntárias. As voluntárias acontecem totalmente por culpa e desleixo nosso, enquanto que as involuntárias nos advém mesmo contra nossa vontade.

As voluntárias são basicamente duas:

>1) Falta da devida preparação próxima; quanto ao tempo, lugar, postura, passagem excessivamente brusca a oração depois de uma ocupação absorvente...

>2) Falta de preparação remota; pouco recolhimento, dissipação habitual, vida tíbia, curiosidade vã, ânsia de ler tudo... <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Basicamente a preparação próxima se relaciona diretamente com a prática da oração, onde escolhemos um lugar, ficamos numa postura, escolhemos o momento preciso que dedicaremos à mesma, etc. Evidentemente que se tentarmos fazer a oração deitado na cama, escolhermos um local barulhento ou o momento do dia em que estivermos mais cansados não iremos conseguir realizar a oração de forma plena ou adequada e jamais iremos progredir nos graus de oração.

Também é necessário notar que ao sair de ocupações absorventes (a prática de um esporte que demande muito esforço, um trabalho intelectual excessivo, etc) demandam que tenhamos calma e façamos uma preparação e um momento de recolhimento antes de iniciar a oração.

A preparação remota diz respeito ao que fazemos durante o dia normalmente. Se lermos coisas demais, ficarmos atrás de informações sobre a vida dos outros, conversarmos demais (isso inclui mensagens de textos), etc, nunca nos concentrarmos por muito tempo em nada. Deste modo iremos inevitavelmente ter uma grande dificuldade de controlar nossa imaginação na hora da oração. Inúmeros são os jovens que não conseguem se concentrar no momento da oração porque passam tempo demais conversando via mensagens, Facebook, telefone ou outros meios.

Já as causas involuntárias podem são classificadas em quatro origens distintas.

>1) A índole e o temperamento: Imaginação viva e instável; tendência a focar nas coisas exteriores; incapacidade de fixar a atenção ou de prorromper em afetos. Paixões vivas, mal dominadas, que atraem continuamente a atenção até os objetos amados, temidos e odiados...

>2) A pouca saúde e a fatiga mental, que impede fixar a atenção ou abstrair das coisas e circunstância exteriores.

>3) A direção pouco acertada do diretor espiritual, que quer impôr artificialmente suas próprias ideias a alma, sem tem em conta o influxo da graça, a índole, o estado e as necessidades da mesma, empenhando-se em fazer continuar a meditação discursiva quando Deus a move a uma oração mais simples e mais profunda ou apartando-a rapidamente do discurso quando ela necessita...

>4) O demônio, as vezes diretamente, outras muitas, indiretamente, utilizando outras causas e aumentando sua eficácia perturbadora. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Como poder ser visto, as causas involuntárias ocorrem por muitos fatores externos que estão, muitas vezes, além de nossas forças, não sendo possível combatê-los todos, embora se possa aliviá-los. Para tanto precisamos conhecer e estarmos atentos a existência destes, buscando evitar as situações que venham provocá-los. 

Exporemos alguns exemplos mais claros e alguns meios com que poderíamos aliviá-los, não se tratando propriamente de remédios, pois são conselhos que dependem totalmente das circunstâncias, deixando os remédios mais gerais à um outro artigo.

Um temperamento explosivo faz a pessoa se irar facilmente e a irritação é completamente incompatível com a oração e a concentração.

Podemos acabar tendo dificuldades por excesso de atividades pessoais, como muitas obras de apostolado, um trabalho "manhã, tarde e noite", visitas excessivas e desnecessárias às pessoas (principalmente parentes), etc.

Quanto a doença não há muito o que se pode fazer a não ser buscar o tratamento de forma disciplinada e evitar as suas causas. O cansaço nem sempre pode ser evitado, mas uma disciplina no horário de dormir e levantar pode ajudar muito.

A direção espiritual mal feita pode ser um grande problema. Podemos encontrar dificuldades por sermos ensinados a nos prendermos excessivamente ao discurso racional, a afetividade ou a buscarmos o cumprimento do dever de ler determinada oração até o final.

A ação dos demônios é algo que pode ocorrer de diversos meios, seja com a apresentação de ocupações aparentemente mais importantes, a incitação violenta ou mais branda de nossas paixões, as dúvidas sobre a fé no momento da oração, etc.

Com isso vemos que existem muitas coisas que precisam e devem ser remediadas na nossa vida e prática de oração para que possamos adentrar cada vez mais no grandiosos "castelo" que é a nossa alma para encontrar a Santíssima Trindade que ali habita.

Vinde, Ò Paráclito, para impulsionar nossa oração com o sopro dos teus dons e nos fazer aprofundar-nos na verdadeira união com Cristo, nosso Salvador e Senhor.