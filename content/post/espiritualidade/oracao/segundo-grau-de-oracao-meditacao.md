+++
date = "2014-10-16T18:00:00-17:00"
title = "Segundo Grau de Oração: Meditação"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/segundo-grau-de-oracao-meditacao.jpg"
+++

Analisemos agora um grau de oração superior ao primeiro: a meditação.

<!--more-->

Esta etapa ainda é predominantemente ascética, pois ainda prevalece o esforço humano amparado pela graça e muitas vezes ainda não é possível perceber com clareza a cooperação divina. É uma prática de oração muito comum de se encontrar nas pessoas piedosas e se define como:

>A aplicação discursiva da mente a uma verdade sobrenatural para convencermo-nos dela e movermo-nos a amá-la e praticá-la com a ajuda da graça. <cite>Pe. Antonio Royo Marin, Teologia de la Perfección Cristiana</cite>

A aplicação da mente a um objeto é simplesmente atenção, coisa que já existia na oração vocal. Porém a aplicação discursiva é o que tem de essencial neste grau de oração. O discurso mental nada mais é que raciocinar sobre alguma coisa, o que a maioria das pessoas tenta dizer quando pede que se medite sobre algo. Assim, quando o ato de raciocinar desaparece, a oração mental desaparece junto.

Isso não seria o que se entende por estudo e especulação da verdade? Não estaria qualquer pessoa orando ao estudar matemática ou ao especular sobre o futuro da política? Não, pois a diferença entre estudo e oração consiste na sua finalidade. Tanto no estudo como na meditação existe o raciocínio, mas o primeiro tem como finalidade conhecer as coisas, enquanto o segundo tem a finalidade de conhecer e amar.

Um outro ponto importante é que este grau de oração tem como objeto sempre uma verdade sobrenatural. Um cientista aplica a mente a conhecer uma verdade natural, enquanto que na oração a pessoa se aplica a uma verdade sobrenatural. Qual verdade sobrenatural o indivíduo deve aplicar-se pode variar. Poderia ser um texto das Sagradas Escrituras, uma passagem da vida de Cristo ou de um santo, um dogma de fé, uma fórmula da liturgia, etc, porém sempre com a dupla finalidade citada.

A finalidade de "conhecer" é alcançada quando se chega a convicções firmes e enérgicas que resistam à influências contrárias. Por não ter esta prática, muitas pessoas vivem de um sentimentalismo frágil e terminam abandonando a fé por algum fato desagradável na comunidade ou porque é possível sentir emoções mais fortes nas seitas. Mas a convicção não basta, pois ela isoladamente poderia ser buscada pelo estudo. Para ser verdadeiramente oração de meditação é necessário que junto com o conhecimento acompanhe o amor.

Esta segunda finalidade é o elemento mais importante da meditação enquanto oração. A vontade derrama seu amor sobre aquilo que a inteligência conhece através do discurso. Alcança-se este ponto quando a alma, enebriada pela verdade sobrenatural conhecida, se entrega em afetos e atos de amor à Deus. Evidentemente que este amor e entusiasmo afetivo não pode se limitar ao coração e à imaginação, mas tem de se traduzir em resoluções práticas enérgicas, ou seja, em um propósito firme de levar à prática as consequências que decorrem daquela verdade considerada e amada pedindo a Deus ajuda no cumprimento da resolução.

Adverte Santa Teresa Dávila:

>Nunca se insistirá bastante nestes dois últimos elementos da definição: o amor a Deus e o propósito prático, enérgico e decidido. São legiões incontáveis as almas piedosas que se exercitam diariamente na meditação e que, porém, não retiram dela nenhum proveito prático. A explicação deve ser buscada no modo defeituoso de fazê-la. Insistem excessivamente no que não é senão mera preparação para a oração propriamente dita. Se passa o tempo lendo, discorrendo ou em perpétua distração semi-involuntária. O resultado é que quando termina o tempo destinado à oração não permaneceram nela nem um instante. De sua alma não brotou um só ato de amor, uma aspiração a Deus, um propósito prático concreto e enérgico. São almas aleijadas, que, se não vem o próprio Senhor ordenar-lhes que se levantem, como ao que fazia trinta anos que estava na piscina, tem vasta má sorte e estão em grande perigo. <cite>Santa Teresa de Jesus, Primeiras Moradas, I, 8</cite>

Este grau de oração prevalece na segunda morada de Santa Teresa descrita no livro Castelo Interior e pertence ainda a via purgativa. São chamadas de "alma boas" e apresentam as seguintes características gerais:

>Pecado mortal: Sinceramente combatido. As vezes, porém, ocasiões perigosas seguidas de alguma queda. Sincero arrependimento e pronta confissão.

>Pecado Venial: As vezes plenamente deliberado. Luta débil, arrependimento superficial, recaídas constantes na murmuração, etc.

>Práticas de Piedade: Frequência nos sacramentos (por ocasião das primeiras sextas, festas principais, etc.). As vezes missa diária, mas com pouca preparação. Rosário familiar, omitido com facilidade.

>Oração: Em geral orações vocais. As vezes, algum tempo de meditação, mas com pouca fidelidade e muitas distrações voluntárias. <cite>Pe. Antonio Royo Marin, Teologia de la Perfección Cristiana</cite>

Que possamos adentrar os salões de tão nobre castelo e progredirmos até alcançarmos onde se encontra o grande Rei Celestial invocando sempre o auxílio de nosso anjo da guarda e de Maria Santíssima.