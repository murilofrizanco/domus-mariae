+++
date = "2014-07-10T18:00:00-17:00"
title = "Como combater as distrações na oração?"
categories = ["Espiritualidade","Oracao"]
featured_image = "posts/images/como-combater-as-distracoes-na-oracao.jpg"
+++

Vimos anteriormente as distrações que atrapalham a nossa prática da oração e o desenvolvimento espiritual. Conhecendo agora as origens destas, poderemos passar a apontar conselhos adequados e eficazes.

<!--more-->

É importante, antes de mais nada, apresentarmos os conselhos com a consciência de que não é possível, somente por nossas forças, suprimir todas as distrações. A eliminação total das distrações só ocorre por intervenção divina numa vida espiritual avançada, porém podemos fazer muito amparado pela graça do Espírito Santo.

Iniciemos pelas causas que dependem de nossa vontade. Devemos destruí-las com todas as nossas energias e forças, conforme ensina o Pe. Royo Marin:

>Quanto as causas dependem de nossa vontade, as combaterá com energia até destruí-las por completo. Não omitiremos jamais a preparação próxima, recordando sempre que o contrário seria tentar a Deus, como diz a Sagrada Escritura. E cuidemos, ademais, de uma séria preparação remota, que abarca principalmente os pontos seguintes: silêncio, fuga da curiosidade vã, guarda dos sentidos, da imaginação e do coração, e nos acostumarmos a ter o foco no que se está fazendo, sem deixar divagar voluntariamente a imaginação até outra parte. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Portanto aqui vemos que a preparação próxima e remota são completamente fundamentais.

Para fazer uma boa preparação próxima devemos iniciar por determinar o momento do dia e a quantidade de tempo que dedicaremos a oração, sendo muito recomendado não deixar perto do horário de dormir ou que você sinta sono, pois neste caso o cansaço será uma porta aberta para se omitir a oração naquele dia.

Um dos horários mais recomendados é logo quando se acorda, pois estamos descansados e com a mente limpa de qualquer pensamento.

O lugar da oração deve ser silencioso, sendo ideal uma Igreja, oratório, ou um outro espaço que não contenha coisas que chamem a atenção.

A postura deve ser orante, silenciosa e humilde. Bater palmas, fazer gestos eufóricos, rezar deitado ou andando irão contribuir para dificultar a introspecção necessária.

No caso de estar em uma atividade que exija demais do corpo ou da mente, é importante procurar reduzi-la aos poucos, conforme o horário dedicado a oração se aproxima, ou ainda, buscar um horário que não seja afetado pelos efeitos destas ocupações. É sempre importante reservar um tempo, quando necessário, para se recolher e silenciar antes de iniciar a oração.

Para a preparação remota, devemos, durante o dia, evitar falar muito, ouvir muita música (especialmente as músicas de ritmo forte) ou ainda evitar deixar a alma livre para pensar mal dos outros, suspeitar demais das coisas, ficar reclamando, buscar conhecimentos desnecessários, leituras fúteis, excesso de leitura de notícias, etc. Tudo isso é como lixo que vai se acumulando na alma durante o dia e depois torna difícil a concentração, pois atrai a atenção da alma.

Quanto as causas involuntárias, este sábio sacerdote continua:

>Pode diminuir-se o influxo pernicioso das causas independentes da vontade com várias indústrias: lendo, fixando a vista no sacrário ou em uma imagem expressiva, eligindo matérias mais concretas, entregando-se a uma oração mais afetiva, com frequentes colóquios (inclusive vocais, se preciso), etc. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Não temos condições de colocar aqui remédios práticos para todas as mínimas situações, pois elas tendem a uma quantidade inimaginável e de espécies totalmente diversas, embora o Pe. Royo Marin tenha colocado de forma magnífica alguns conselhos mais gerais.

Chamo a atenção para as causas involuntárias que provêm dos demônios: neste caso podemos e devemos utilizar a água benta.

No caso de doenças, não devemos nos angustiar, mas unir o sofrimento da doença à Cristo e já estaremos fazendo uma prática muito frutuosa.

O temperamento deve ser dominado com a prática das virtudes cristãs, enquanto que um diretor espiritual que está atrapalhando pode ser trocado por um outro mais experiente, que compreenda melhor a vida de oração.

Por fim, resta-nos saber como reagir durante a oração quando a distração se apresenta. Deixemos o grande dominicano explicar com suas palavras precisas, as quais, nada tenho a acrescentar:

>Quando, apesar de tudo, nos sintamos distraídos com frequência, não nos impacientemos. Voltemos a trazer suavemente nosso espírito ao recolhimento, mesmo que seja milhares de vezes, se for preciso; humilhemo-nos na presença de Deus, peçamos sua ajuda e não examinemos de imediato as causas que motivaram a distração. Deixemos este exame para o fim da oração, com a finalidade de prevenirmos melhor no futuro. E tenha-se bem presente que toda distração combatida (mesmo que não seja completamente vencida) em nada compromete o fruto da oração nem diminui o mérito da alma. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Que possamos pedir que o Espírito de Fortaleza venha em nosso socorro para nos impulsionar a combater contra estes obstáculos e consumir nosso coração com seu Fogo Divino.