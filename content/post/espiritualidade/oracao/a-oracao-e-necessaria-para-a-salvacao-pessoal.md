+++
date = "2014-07-05T18:00:00-17:00"
title = "A oração é necessária para a salvação pessoal?"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/a-oracao-e-necessaria-para-a-salvacao-pessoal.jpg"
+++

Sabemos que a oração se apresenta como uma das causas secundárias conforme estabelecido e determinado pela Divina Providência, porém será que a salvação está inclusa nisso?

<!--more-->

A resposta é que sim, pois não é possível que alguém encontre um meio eficaz de se salvar sem a oração. 

Evidentemente que isto não pode ser entendido para as crianças, que não tem o pleno uso da razão, porém para os adultos isto é completamente necessário.

É necessário entender que isso não limita a ação divina. Deus pode conceder suas graças à algumas exceções, se assim convier à sua glória e à salvação das almas, como foi com São Paulo Apóstolo, porém não podemos abrir mão do plano seguro e rejeitar o meio que temos certeza pelo que não temos certeza, pois isto seria desprezar a segurança que Deus nos entregou e tentar sua Divina Majestade.

Deus nos deixou um caminho seguro, confiável e certeiro para salvar-nos, e neste caminho se encontra a oração, a qual sempre consegue infalivemente as graças a quem pede com as devidas condições (as quais serão tratadas em outro artigo). Por isso, o que ora com as devidas condições pode ter certeza absoluta que obterá as graças para sua própria salvação.

Fica evidente a necessidade da oração quando notamos que a moral católica é algo tão elevado que qualquer pessoa que tente segui-la a risca irá perceber que é impossível fazê-lo.

Por outro lado, os santos a seguiam sem qualquer fardo ou dificuldade, inclusive se deleitavam na vivência de cada virtude cristã, enquanto a maioria de nós não consegue compreender porque é tão difícil seguir o Evangelho em sua plenitude e eles fazem parecer tão fácil.

Uma vez perguntaram a São Felipe Neri: "porque é tão difícil seguir o Evangelho?", ao qual o santo respondeu: "Porque ele é muito simples". Poderia parecer uma resposta esquiva, mas na verdade é muito profunda.

Realmente é muito simples seguir o Evangelho e alcançar um alto grau de santidade: basta ter uma vida séria de oração. Sem isso não será possível nem sequer viver a castidade, que é uma das virtudes mais fáceis de se obter, e muito menos alcançar a própria salvação.

Lembremos as sábias palavras do grande doutor:

>Deus não manda impossíveis; e ao mandar-nos uma coisa, nos previne que devemos fazer o que pudermos e pedir o que não pudermos e nos ajuda para que possamos. <cite>Santo Agostinho de Hipona</cite>

De tudo isso decorre as conclusões profundas de um dos maiores mestres da oração:

>Coloquemos, portanto, fim a este importante capítulo resumindo tudo que foi dito e deixando bem firme esta afirmação: que o que ora, se salva certamente, e o que não ora, certamente se condena. Se deixarmos de lado as crianças, todos os demais bem-aventurados se salvaram porque oraram, e os condenados se condenaram porque não oraram. E nenhuma outra coisa lhes produzirá no inferno um desespero mais espantoso do que pensar que teria sido coisa muito fácil salvar-se, pois o teriam conseguido pedindo a Deus suas graças, e que agora serão eternamente desgraçados, porque passou o tempo da oração. <cite>Santo Afonso Maria de Ligório</cite>

Com essa certeza peçamos que o Espírito Santo nos impulsione a entregar-nos cada vez mais à vida de oração, seguindo o grande exemplo da Virgem Maria que estava sempre unida a seu Filho pela oração.