+++
date = "2015-08-05T18:00:00-17:00"
title = "Quarto grau de oração: Oração de Simplicidade"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/quarto-grau-de-oracao-oracao-de-simplicidade.jpg"
+++

Analisemos agora um grau de oração superior aos anteriores que é o que se costuma chamar de oração de simplicidade.

<!--more-->

Deve-se notar que neste grau ainda prevalece o nosso esforço sobre a ação de Deus, porém esta diferença vai diminuindo gradativamente até que prevaleça a ação divina.

Este grau de oração foi definido por Bossuet como:

>Uma simples visão, olhar ou atenção amorosa até algum objeto divino, seja Deus em si mesmo ou alguma de suas perfeições, seja Nosso Senhor Jesus Cristo ou algum de seu mistérios, seja outras verdades cristãs. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 397</cite>

Ora, isto nada mais é que um modo muito simplificado das orações anteriores. Com efeito, a alma deixa o discurso ou raciocínio para simplesmente trazer aquilo em seu pensamento e deixa de ter diversos afetos para que se transformem em um único.

Bossuet descreve a prática desta oração deste modo:

>A alma deixa então o discurso, e se vale da doce contemplação, que a mantém no doce sossego e atenção e a torna suscetível às operações e impressões divinas que o Espírito Santo lhe quer comunicar; trabalha pouco e recebe muito; seu trabalho é agradável, e nem por isso deixa de ser frutuoso; e como cada vez mais se chega próximo à fonte de onde emana a luz, a graça e as virtudes, recebe mais e mais dela. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 397</cite>

Com efeito, a oração afetiva é como uma mão que segura um objeto com firmeza e vai perdendo as forças, necessitando descansar para segurá-lo novamente com a mesma firmeza. No terceiro grau de oração a alternância entre o descanço e a ação é grande, por isso fala-se de múltiplo afetos. Por outro lado, na oração de simplicidade a alternância entre a ação e o repouso diminui cada vez mais de modo que a mão dedique um espaço de tempo cada vez maior segurando o objeto do que descansado ou se preparando para isso.

Neste grau não somente a nossa oração vai simplificando, mas também a nossa própria vida, conforme nota Tanquerey:

>Essa simplificação se estende prontamente a todo nosso viver. O exercício deste modo de oração, diz Bossuet, deve começar desde o despertar, fazendo um ato de fé em Deus, que está em todas as partes, e em Jesus Cristo, cujo olhar jamais se apartará de nós mesmo que nos encontremos no centro da terra mais escondido. Continua durante todo o dia. Mesmo ocupados em nossos afazeres ordinários, nos unimos com Deus, o olhamos e o amamos. Nas orações litúrgicas e nas vocais cuidamos mais da presença de Deus que do sentido das palavras e procuramos manifestar-lhe nosso amor. O exame de consciência se simplifica; com um olhar rápido vemos as faltas cometidas e nos doemos delas. O estudo e as obras exteriores de zelo as fazemos com espírito de oração, na presença de Deus e com ardente desejo de dar-lhe glória. Nem sequer as obras mais ordinárias deixam de estar penetradas do espírito de fé e de amor e de converterem-se em hóstias oferecidas continuamente a Deus: 'oferecerdes sacrifícios espirituais aceitáveis a Deus' (1Pd 2,5) <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 397</cite>

Este grau de oração prevalece na quarta morada de Santa Teresa em seu livro Castelo Interior e pertence a via iluminativa. Estas almas são chamadas de "almas fervorosas" e apresentam as seguintes características:

>Pecado mortal: Nunca. Em geral, algumas surpresas violentas e imprevistas. Nestes casos, pecado mortal duvidoso, seguido de um vivíssimo arrependimento, confissão imediata e penitências reparadoras.

>Pecado venial: Séria vigilância para evitá-lo. Raramente deliberado. Exame particular dirigido seriamente a combatê-lo.

>Imperfeições: A alma evita examinar-se muito sobre isto para não ver-se obrigada a combatê-las. Ama a abnegação e a renúncia de si mesmo, mas até certo ponto e sem grandes esforços.

>Práticas de piedade: Missa e comunhão diárias com fervorosa preparação e ação de graças. Confissão semanal diligentemente praticada. Direção espiritual encaminhada a adiantar-se na virtude. Terna devoção a Maria.

>Oração: Fidelidade a ela apesar das aridezes e securas da noite do sentido. Oração de simplicidade, como transição às orações contemplativas. Em momentos de particular intensidade, oração de recolhimento infuso e de quietude. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 155</cite>