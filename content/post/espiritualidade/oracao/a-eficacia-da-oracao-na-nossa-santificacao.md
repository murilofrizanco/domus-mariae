+++
date = "2014-07-13T18:00:00-17:00"
title = "A eficácia da oração na nossa santificação"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/a-eficacia-da-oracao-na-nossa-santificacao.jpg"
+++

É muito comum na vida espiritual os grandes esforços para se exercitar as virtudes cristãs, porém infelizmente este esforço vem muitas vezes acompanhado de uma oração tíbia ou um certo desprezo pela mesma, como se fosse tempo perdido e como se o apostolado fosse mais importante que ela.

<!--more-->

Lemos no Evangelho uma passagem que passa despercebido para a maioria das pessoas:

>Marta, Marta, tu te inquietas e te agitas por muitas coisas; no entanto, pouca coisa é necessária, até mesmo uma só. Maria, com efeito, escolheu a melhor parte, que não lhe será tirada. <cite>Evangelho segundo São Lucas, 10, 41s</cite>

Aqui Jesus demonstra dar mais valor para a vida de oração do que para a ativa e de fato é o que Ele está fazendo. O que vale todos os apostolados da terra, se o maior tesouro, que é a união com Cristo pela presença do Espírito de Amor na alma, deixamos passar?

As pessoas que entendem o cristianismo como uma moral a ser seguida, um clube em que você vai pelos amigos ou uma carreira filantrópica não entendeu ainda do que faz parte e nem o que é o cristianismo. A essência do cristianismo é descrita de forma precisa pelo doutor angélico:

>É a graça do Espírito Santo, dada pela fé em Cristo <cite>Santo Tomás de Aquino, Suma Teológica, Ia IIae, q.106, a.1</cite>

Vemos, de fato, que toda mensagem do Evangelho, tudo o que existe na Igreja e o objetivo do mistério da Encarnação do Verbo, é ordenado para o surgimento e desenvolvimento desta graça derramada sobre os fiéis, ou seja, a presença mesma do Espírito Santo.

Maria escolheu a oração, por isso escolheu a melhor parte. Enquanto isso, Marta fazia coisas muito boas e honestas, mas deixando passar o principal que o Cristo veio trazer: a união com Ele através do Espírito.

Qualquer coisa que façamos, por melhor que seja, ignorando esta vida da graça será em vão e absolutamente inútil e não pode agradar à Deus, por mais que esteja de acordo com os preceitos morais do Evangelho. Portanto todo apostolado, toda virtude e tudo o que fizermos deve ser ordenado para esta vida da graça.

É preciso ter de forma clara que essa vida da graça e a presença do Espírito Santo só se desenvolvem e se tornam eficazes a medida em que a pessoa se entrega na oração e vai permitindo uma ação cada vez maior do Espírito Santo, sem a qual é impossível unir-se a Deus. Quando isso é feito, começamos a nos santificar até alcançar o cume da união com Cristo, que é a santidade.

Por isso, se queremos realmente nos santificar, devemos buscar desenvolver primeiramente o essencial do cristianismo, que é a presença do Espírito e nossa docilidade à Ele, que só pode progredir através das graças obtidas com a oração.

Podemos ver a eficácia da oração em vários belíssimos testemunhos dos santos:

>Porque nela [oração] se recebe a união e a graça do Espírito Santo, a qual ensina todas as coisas. E além disso, se queres subir a altura da contemplação e gozar dos doces abraços do Esposo, exercita-te na oração, porque este é o caminho pelo qual sobe a alma a contemplação e gosto pelas coisas celestiais. <cite>São Boaventura</cite>

>Na oração limpa-se a alma dos pecados, apascenta-se a caridade, certifica-se a fé, fortalece-se a esperança, alegra-se o espírito, derrete-se as entranhas, pacifica-se o coração, descobre-se a verdade, vence-se a tentação, foge a tristeza, renova-se os sentidos, repara-se a virtude enfraquecida, despede-se a tibieza, consome-se a ferrugem dos vícios, e nela saltam centelhas vivas de desejoso do céu, entre os quais arde a chama do Divino Amor. Grandes são as excelências da oração, grandes são seus privilégios. A ela estão abertos os céus, a ela se descobrem os secretos e a ela estão sempre atentos os ouvidos de Deus. <cite>São Pedro de Alcântara</cite>

>Pela oração falamos com Deus e Deus fala conosco, aspiramos a Ele e respiramos Nele, e Ele nos inspira e respira sobre nós. <cite>São Francisco de Sales</cite>

Santa Teresa ainda comenta: "A alma que não faz oração está perdida, jamais chegará a santidade".

A Beata Madre Teresa da Calcutá fazia suas irmãs passarem longos períodos de oração antes de servir aos pobres. Diante das críticas que lhe fizeram por causa disso, ela respondeu: "se as minhas irmãs não passassem tanto tempo em oração, não seriam capazes de servir de fato os pobres e os doentes".

Com isso, fica claro que o principal e mais eficaz meio de progredir na vida espiritual é sempre a oração, pela qual se fortalece essa presença do Divino Amor que esconde nossa vida com Cristo em Deus e nos torna aptos a agir sobrenaturalmente.

Que possamos, à exemplo de Maria, sentarmos aos pés de Nosso Divino Mestre para lançar-lhes contínuos olhares de amor e acender as labaredas da caridade do nosso coração até que nossa alma possa, embriagada com as delícias do Divino Esposo, dizer: O meu Amado é meu e eu sou do meu Amado.