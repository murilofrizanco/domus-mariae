+++
date = "2014-07-07T18:00:00-17:00"
title = "As armadilhas da vida de oração"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/as-armadilhas-da-vida-de-oracao.jpg"
+++

Muitos de nós ao estabelecermos uma vida de oração com uma prática constante corremos o risco de cair em uma série de armadilhas. Estas armadilhas são perigosas, pois impedem o progresso espiritual e passam de forma despercebida. Portanto examinemos quais os tipos que devemos estar sempre atentos para corrigir.

<!--more-->

A lista é enumerada de forma precisa e objetiva pelo dominicano Pe. Antonio Royo Marin, conforme abaixo.

##### A rotina

>A rotina na oração vocal, que a converte em um exercício puramente mecânico, sem valor e sem vida; ou a força do costume na oração mental metódica, que leva a um certo automatismo semiconsciente, que a priva quase totalmente de sua eficácia santificadora. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

É sempre preciso ter o cuidado de não cairmos numa oração automática onde apenas nossa voz profere palavras externas ou nossa mente medita as coisas de forma mecânica, sem colocar realmente a alma. Para evitar isto é necessário que tenhamos sempre em mente uma coisa: que quando cremos e saboreamos uma verdade, nós estamos tocando a humanidade do Cristo Ressuscitado. Isso não é uma figura de linguagem, mas algo completamente perceptível se prestarmos atenção no nosso interior. Nos habituando a isso, não somente se evita a rotina, como nos facilita a perseverança na oração, pois quem não gostaria de estar com o seu Amado?

##### O excesso de atividade natural

>O excesso de atividade natural, que quer conseguir tudo pela força das armas, adiantando-se a ação de Deus na alma; ou a excessiva passividade e inércia, que, sob pretexto de não adiantar-se a ação divina, não faz sequer o que com a graça ordinária poderia e deveria fazer. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Mais um ponto onde muitos ficam impedidos do progresso. Devemos tomar cuidado para não pensarmos que o progresso na vida de oração e na graça é algo que depende somente de nós, pois depende principalmente da graça concedida por Deus. O que podemos, e devemos, fazer é estarmos sempre à disposição desta ação do Espírito Santo. Esta disposição é obtida quando perseveramos diariamente na oração, nos mantemos em estado de graça com a confissão frequente, evitamos os pecados veniais, comungamos com frequência, etc. 

Sem uma alma que se faz acessível à operação da graça, não é possível o progresso espiritual e nem a união com Cristo. Porém, mesmo dispostos à ação da graça, Deus não tem obrigação de operar imediatamente. Na sua infinita Sabedoria, Ele irá operar nos momentos em que souber serem mais convenientes à nosso progresso.

##### O desalento

>O desalento, que se apodera das almas débeis e enfermiças ao não comprovar progressos sensíveis em sua vida longa de oração; ou o excessivo otimismo de outras muitas que se creem mais adiantadas do que na realidade estão. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Muitas são as almas que desanimam da vida de oração e acabam deixando ela em segundo plano quando não constatam de forma nítida que estão progredindo, tendo a impressão de que estão perdendo tempo em que poderiam estar fazendo obras apostólicas. Deve-se evitar isso a todo custo, pois nenhum momento é perdido, pois se Deus está em silêncio, não significa que não está atuando. 

Outros ainda pensam estar muito adiantadas quando na verdade não estão. Isso é prejudicial, pois acabam buscando um modo de oração mais avançado, sendo que não são ainda capazes de tirar proveito dele, e no final acabam não fazendo nem uma coisa direito e nem a outra.

##### O apego aos consolos sensíveis

>O apego aos consolos sensíveis, que gera na alma uma espécie de "gula espiritual" que a move a buscar os consolos de Deus ao invés do Deus dos consolos. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Há muitas almas muito frágeis, principalmente as principiantes, que pensam que somente vale a pena orar quando há uma recompensa sensível, que são os consolos, onde a presença e ação divina se tornam perceptíveis aos nosso sentidos, seja sentindo uma grande paz, uma alegria constante, uma determinada emoção, uma forte dor espiritual pelas nossas ofensas, uma grande facilidade de permanecer concentrado na oração, etc.

Porém devemos ter em mente que Deus concede estes auxílios para nosso bem e não como finalidade da oração e os retira quando nota que somos capazes de nos desapegar deles para avançar nos graus de oração.

##### O apego excessivo a um determinado método

>O apego excessivo a um determinado método, como se fosse o único possível para o exercício da oração; ou a excessiva ligeireza, que nos move a prescindir dele ou abandoná-lo antes do tempo. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Temos que ter em mente que nem todas as devoções e nem todas as técnicas de oração são igualmente conveniente a todas as pessoas ou fases da vida espiritual. Um exemplo são as fórmulas: elas são muito úteis para nos ensinar a rezar, mas quando sua utilização não excita mais a devoção, então está na hora de buscar outro método, mais coerente com a fase da vida espiritual em que a alma se encontra. Evidentemente que deve-se ter a cautela de não ficar mudando toda hora ou por qualquer momento árido na utilização de um determinado método, pois isso apenas atrapalharia o progresso.

Que possamos escapar destas armadilhas perniciosas para que, cada vez mais dóceis ao sopro do Paráclito, possamos nos elevar à íntima união com Cristo na fé e na caridade.