+++
date = "2014-07-03T18:00:00-17:00"
title = "O que é a oração?"
categories = ["Espiritualidade","Oracao"]
path = "Oração"
featured_image = "posts/images/o-que-e-a-oracao.jpg"
+++

Muitos tem diversas ideias dispersas sobre o que seja a oração, porém a maioria não consegue dar uma definição clara do que ela seja. Isso é importante para que possamos compreender realmente o que Jesus quer dizer no Evangelho quando nos ensina a orar.

<!--more-->

Uma excelente definição é recolhida pelo Pe. Antonio Royo Marin, acompanhando a síntese de Santo Tomás, sobre sua definição:

>A oração é a elevação da mente a Deus para louvar-lhe e pedir-lhe coisas convenientes para a eterna salvação. <cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana</cite>

Esta definição pode parecer simplória e pouco significativa, porém vamos desenvolver cada uma de suas partes para que seja evidente a grande profundidade da mesma e o quanto é comum o nosso entendimento por oração não alcançar todos estes detalhes.

**"É a elevação da mente a Deus"**

Não se trata de emoção e nem de atos mecânicos e vocais. A oração é um ato do nosso intelecto, ou seja, um ato da razão. Isso é evidente quando notamos que alguém distraído, que não colocou sua mente em Deus, não faz oração alguma. Dizemos "a Deus" pois somente Dele podemos receber a graça e a glória, as quais devem ordenar-se todas nossas orações.

**"para louvar-lhe"**

Uma finalidade própria da oração, pois não oramos somente para pedir coisas, pois a oração não é somente pautado em um interesse em benefícios, mas inclui também os atos de adoração, louvor, reparação dos pecados e ação de graças.

**"e pedir-lhe"**

De maneira nenhuma é vetado ou condenado se pedir as coisas na oração, muito pelo contrário: pedir é próprio do que ora. E isso decorre do fato de que o orante, diante de Deus, percebe-se débil e indigente e por isso recorre à Deus para que venha em socorro de sua miséria. O pedido que fazemos é o que se costuma chamar de súplica ou petição.

**"coisas convenientes à eterna salvação"**

Na oração não se pode pedir qualquer coisa indiscriminadamente. Evidentemente que não é proibido pedirmos coisas temporais (bens materiais, emprego, saúde, etc), porém devemos ter em conta que estes pedidos só podem ser feitos e atendidos se forem instrumentos para melhor servir à Deus e a nossa felicidade eterna. Em si mesmo, os pedidos próprios da oração são os que se referem à vida sobrenatural, como por exemplo, pedir a graça de resistir à tentação.

Podemos pedir as coisas temporais, única e exclusivamente, com o acréscimo e inteira subordinação aos interesses da glória de Deus e a salvação das almas.

Este último ponto é muito importante e é o motivo pelo qual muitas pessoas não são atendidas: porque não fazem pedidos que tem como finalidade a glória de Deus e a salvação das almas, mas apenas interesses temporais e passageiros. O resultado é que não estão fazendo verdadeira oração.

Que possamos com esta clareza ordenar nossas orações da forma mais conveniente e através dela obter realmente a glória de Deus e a salvação das almas.