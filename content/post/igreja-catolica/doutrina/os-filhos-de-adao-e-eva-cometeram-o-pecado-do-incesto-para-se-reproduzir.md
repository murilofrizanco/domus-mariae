+++
date = "2015-06-16T18:00:00-17:00"
title = "Os filhos de Adão e Eva cometeram o pecado do incesto para se reproduzir?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/os-filhos-de-adao-e-eva-cometeram-o-pecado-do-incesto-para-se-reproduzir.jpg"
+++

Depois de Adão e Eva gerarem muitos filhos e filhas e estes, por sua vez, também geraram outros filhos e filhas. Diante disto, deve-se reconhecer que Caim e Set tiveram filhos com uma mulher, onde haviam somente duas opções: ou se uniram a sua própria mãe Eva ou alguma de suas irmãs, o que parece que foram obrigados a praticar o incesto.

<!--more-->

Quanto à primeira hipótese evidentemente não poderiam ter gerado filhos unindo-se com a sua mãe por dois motivos: incorreriam em adultério, pois Eva já era casada com Adão; em segundo lugar, deve-se notar que unir-se com a própria mãe é sempre pecaminoso, ferindo a honra que o filho deve a esta.

>Na união carnal de pessoas aparentadas há em si mesma, uma certa inconveniência e repugnância à razão natural. Tal o caso da união entre pais e filhos, entre os quais há um parentesco natural direto e imediato; pois, os filhos têm o dever natural de honrar os pais. Por isso, diz o Filósofo, que um certo cavalo, enganado a ponto de copular com a própria mãe, como que tomado de, horror, precipitou–se a si mesmo num precipício, prova de que até certos animais prestam uma especial reverência aos pais. <cite>Santo Tomás, Suma Teológica, IIa IIae, q. 154, a. 9</cite>

Deste modo, fica claro que unir-se a própria mãe ou ao pai para gerar um filho é algo que sempre constitui um pecado mortal. 

Ora, se as coisas são assim, não era coerente que Deus instituísse a natureza de tal forma que os primeiros descendentes de Adão fossem obrigados a unir-se com a própria mãe.

Portanto, fica claro que a hipótese de que Set e Caim tenham gerado se unindo com a própria mãe é absurda.

Resta agora analisar se eles poderiam ter-se unido sem pecado com suas irmãs de sangue e quanto a isto o Aquinate apresenta uma sábia consideração:

>Quanto a outras pessoas, não unidas por laços diretos entre si, mas, só por intermédio dos pais, podem casar sem que isso implique, em si mesmo, qualquer inconveniente. Mas, nesse caso, a conveniência ou não conveniência varia segundo o costume e as leis humanas ou divinas; porque, como dissemos, a prática dos atos venéreos, por se ordenarem ao bem comum, são regulados por lei. Donde o dizer Agostinho: A união sexual entre irmãs e irmãos, praticada antigamente por impulso da necessidade, tornou-se depois condenável por proibição religiosa. <cite>Santo Tomás, Suma Teológica, IIa IIae, q. 154, a. 9</cite>

Portanto, a consanguinidade direta (que há entre os pais e os filhos) torna inviável este tipo de união carnal sempre. Porém, quanto à consanguinidade indireta, como a que ocorre entre irmãos ou primos, não apresenta qualquer mal em si mesmo, sendo portanto determinado por costume e por lei se estas uniões são lícitas ou não, baseado no que convém ao bem comum.

Neste sentido, como na época de Caim e Set não havia tais leis que proibissem a relação entre irmãos, era completamente natural e sem pecado que viessem a tomar suas irmãs como esposas para formar famílias.

Pode-se contra isto objetar que a proximidade genética dos irmãos expõe os filhos a um risco maior de anomalias genéticas. Isto parece ser verdade atualmente, mas deve-se notar que, se considerarmos o fato de que Adão e Eva foram feitos com total perfeição física, podemos inferir que seus descendentes não tinham uma natureza física tão debilitada como a nossa a ponto de ser capaz de transmití-las na geração e que estas levaram muitas gerações até que realmente oferecessem algum risco para a prole.

Sendo assim, do ponto de vista da consanguinidade não haveria qualquer problema, seja moral ou genético, do casamento entre irmãos nos primórdios da humanidade.

Dado este fato, devemos considerar que é perfeitamente lícito e conveniente que a Igreja e as leis civis proíbam atualmente o matrimônio entre irmãos ou primos até certo grau. E de fato o faz, como lemos, por exemplo, no direito canônico:

>Na linha reta de consanguinidade, é nulo o matrimônio entre todos os ascendentes e descendentes, tanto legítimos como naturais. Na linha colateral, é nulo o matrimônio até o quarto grau inclusive. <cite>Código de Direito Canônico, Cân. 1091, §1 e §2.</cite>

Isto foi feito por várias razões, as quais citaremos brevemente sem maiores explicações apenas para se ter uma ideia geral.

Primeiro. Não estamos mais formando a sociedade humana, portanto não há uma necessidade urgente de reprodução ou falta de opção na escolha dos cônjuges.

Segundo. Estamos geneticamente muito distantes de Adão e Eva, que foram criados com perfeição genética, e sabemos que muitos são os problemas genéticos que podem resultar da união de casais consanguíneos muito próximos.

Terceiro. A proibição também provém de uma utilidade espiritual que é evitar que as pessoas que moram sob o mesmo teto possam ter um incentivo à concupiscência, o que impede a vida espiritual.

Quarto. Existe uma utilidade para a sociedade, pois, ao ser obrigado o homem a casar-se com pessoas de outras famílias, a amizade entre as famílias se multiplica, deixando os seus membros de serem completamente estranhos aos da outra família, incentivando que a sociedade se interesse mais pelo bem comum, desensolva o amor ao próximo e evite o mal.