+++
date = "2014-06-07T18:00:00-17:00"
title = "A religião aliena as pessoas para o progresso?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/a-religiao-aliena-as-pessoas-para-o-progresso.jpg"
+++

Um dos filósofos mais aclamados da modernidade diz que "A religião é o ópio do povo". Com efeito, o Cristianismo oferece um caminho que conduz o homem ao desprezo deste mundo em prol da vida eterna. Assim, parece que a verdadeira religião seria oposta ao progresso.

<!--more-->

Esta objeção não é nova e já foi levantada nos tempos da Igreja Primitiva por seus opositores.

Ora, o Doutor Angélico no seu Comentário à Política demonstra claramente que a religião cristã não se opõe à prosperidade:

>A felicidade necessita de alguns bens exteriores como instrumentos que necessitamos para exercermos as obras das virtudes, nas quais a felicidade consiste. Portanto, a felicidade necessita de prosperidade exterior. <cite>Santo Tomás de Aquino, Comentário à Política de Aristóteles, II</cite>

Assim, o grande teólogo reconhece que o homem não pode ser feliz sem a prosperidade exterior.

Deve-se notar que este ensinamento do Aquinate não provém somente de estudos filosóficos, mas procede de uma exigência da relação do cristão com seu Criador. Por isso, os cristãos dos primeiros séculos diziam:

>Nós não somos alheios à vida. Recordamo-nos bem do dever de gratidão para com Deus, Nosso Senhor e Criador; não repudiamos nenhum fruto das suas obras; somente nos moderamos para não usar deles mal ou descomedidamente. E assim não vivemos neste mundo sem foro, sem talhos, sem balneários, sem casas, sem negócios, sem estábulos, sem os vossos mercados e todos os outros tráficos. Nós também convosco navegamos e combatemos, cultivamos os campos e negociamos, e por isso trocamos os trabalhos e pomos à vossa disposição as nossas obras. Verdadeiramente não vejo como podemos parecer inúteis aos vossos negócios com os quais e dos quais vivemos. <cite>Tertuliano, Século II</cite>

Com efeito, a verdadeira religião deixada por Cristo e propagada pelo seu Corpo Místico enxerga bondade, beleza e verdade em todas as realidades materias e busca utilizá-las como instrumentos para a glorificação de seu divino Esposo.

Por isso, não é incomum que um estudo sobre a vida dos santos revele que estes homens profundamente espirituais contribuíram muito mais para o progresso e desenvolvimento da civilização que os ilustres do século:

>De fato, os Santos foram, são e serão sempre os maiores benfeitores da sociedade humana, como também os modelos mais perfeitos em todas as classes e profissões, em todos os estados e condições de vida, desde o camponês simples e rude até ao sábio e letrado, desde o humilde artista até ao general do exército, desde o particular pai de família até ao monarca, chefe de povos e nações, desde as simples donzelas e esposas do lar domestico até às rainhas e imperatrizes. <cite>Papa Pio XI, Encíclica Divini Illius Magistri</cite>

Portanto, uma sociedade de homens espirituais concorre para uma prosperidade muito maior. Motivo pelo qual Santo Agostinho lançava o seguinte desafio:

>Pois bem, aqueles que dizem ser a doutrina de Cristo inimiga do Estado, que nos dêem um exército tal como a doutrina de Cristo ensina que devem ser os soldados; que nos dêem suditos, maridos, esposas, pais, filhos, patrões, criados, reis, juízes, finalmente contribuintes e empregados fiscais, como a doutrina cristã manda que sejam, e atrevam-se depois a dizer que é nocivo ao Estado, ou melhor, não hesitem um instante em proclamá-la a grande salvadora do mesmo Estado em que ela se observa. <cite>Santo Agostinho de Hipona</cite>

A vasta maioria das pessoas se restringiu a uma visão limitada da política de modo que só existam dois modos de ordenação da sociedade. O primeiro seria o capitalismo cuja finalidade seria obter a maior riqueza. O segundo seria o socialismo/comunismo cuja finalidade seria obter a total igualdade segundo a riqueza.

Porém, a doutrina cristã, desprezando os bens materiais, propõe uma dignificação do homem, uma exaltação da busca pela sabedoria, a institucionalização da virtude e a formação de uma sociedade muito diversa tanto do capitalismo como do socialismo.

Ora, não se tem notícia de uma sociedade que tenha implementado completamente o ideal cristão.

Assim, é verdadeira a crítica de um grande filósofo britânico:

>O ideal Cristão não foi tentado e considerado imperfeito; ele foi considerado difícil e não foi tentado. <cite>G. K. Chesterton</cite>