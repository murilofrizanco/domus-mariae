+++
date = "2015-09-24T18:00:00-17:00"
title = "O que são os pecados que bradam aos céus?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/o-que-sao-os-pecados-que-bradam-ao-ceu.jpg"
+++

A tradição cristã nos apresenta quatro pecados que bradam ao céu. Mas, em que consistem estes pecados e porque recebem esta classificação?

<!--more-->

Estes pecados mortais são assim chamado porque comportam uma malícia muito maior que os demais.

Ademais, nos ensina o Catecismo de São Pio X que sua gravidade provoca a ira divina de modo que os puna com os maiores castigos.

Porém, porque estes pecados exigem uma punição maior por parte de Deus e comportam uma malícia superior?

Quanto a isto, deve-se notar que estes pecados possuem em comum a destruição da **comunicação de bens entre os indivíduos**, fundamento da sociedade humana. Portanto, estes pecados atentam diretamente contra a existência da sociedade humana.

Com isto não se pretende dizer que este dano irreversível seja efeito de uma prática particular destes atos, mas sim que se a raça humana se habituasse a cometê-los, iria rapidamente desaparecer.

Ora, o homem, por natureza, precisa da sociedade para alcançar o seu fim último e para sobreviver.

Por esta razão, estes pecados atraem uma vingança severa que não tarda a ser derramada pela divina justiça de modo que o homem rapidamente seja coagido a afastar-se de tais vícios para que este mal seja impedido.

Assim, os pecados que bradam aos céus possem ser definidos como:

"Todo pecado mortal que impossibilita a comunicação de bens entre os indivíduos quando praticado habitualmente por todos".

Quatro são os pecados que pertencem a este grupo.

Primeiro, o **homicídio voluntário**. Isto se deve ao fato de que a comunicação de bens entre indivíduos pressupõe a existência dos mesmos. Portanto, se os indivíduos em ato forem intencionalmente destruídos, a comunicação de bens necessária para o homem se tornará impossível.

Nesta classe de pecados se encontra o aborto, o assassinato, a eutanásia e todas as formas de causar diretamente e intencionalmente a morte de um indivíduo.

Segundo, o **pecado da impureza contra a natureza**. A razão para a gravidade deste pecado é que os pecados da impureza impedem a existência dos homens em potência.

Nesta classe de pecados se encontra a sodomia, a masturbação, o onanismo, a bastialidade e toda forma de prática sexual que atente contra o fim da geração.

Ora, se homens novos não são gerados não será possível operar a comunicação de bens em um futuro próximo.

Ademais, temos o testemunho atual de quanto isto é nocivo quando praticado em um grau muito menor.

Terceiro, a **opressão dos pobres, principalmente das viúvas e orfãos**.

Quanto a isto deve-se notar que a palavra "pobres" não significa apenas os que se encontram na privação de bens temporais suficientes, mas também os que necessitam da salvação:

>Por pobres evangelizados deve entender-se ou os pobres de espírito ou os pobres de riqueza, a fim de que na pregação não haja diferença entre nobres e plebeus, entre ricos e necessitados: isto demonstra o rigor da justiça do Mestre e a verdade do preceptor, posto que todos os que querem salvar-se são iguais diante de seus olhos. <cite>São Jerônimo, Catena Aurea, Mateus, 5, 2-6</cite>

Portanto, este pecado é constituído por toda sujeição tirânica, nas quais se destacam especialmente aquela que procede da destruição da família.

Com efeito, uma mulher viúva e uma criança órfão vêem-se privadas de suas famílias e à mercê de qualquer opressor.

Ademais, os pobres são entendidos por todos aqueles que necessitam de salvação. Portanto, significa todos os homens.

Ora, numa sociedade em que os indivíduos se relacionam por relações de tirania a comunicação de bens com outros homens torna-se detestável e por esta razão impossível, embora os indivíduos existam.

Assim, uma sociedade que se habitua na opressão dos indivíduos tem como consequência sua própria extinção, pois os homens buscarão o isolamento e perecerão na solidão.

Quarto, a **negação do salário ao trabalhador**. Isto se deve ao fato de que este pecado nega a própria comunicação de bens no seu princípio mais elementar.

Quanto a isto deve-se notar que o salário não se entende apenas por uma quantia de dinheiro, mas também pela troca de qualquer riqueza, como terras, comida ou qualquer outro bem que possa ser dado como salário.

Com efeito, é possível que as pessoas tenham certas posses negadas sem que com isso a sociedade chegue a extinção.

Contudo, não é possível que isso ocorra na primeira troca de bens pela qual alguém inicia a aquisição de toda e qualquer posse, isto é, aquilo que se recebe em troca do próprio trabalho.

Portanto, uma sociedade que assim procede termina por negar a primeira comunicação de bens no que diz respeito a suficiência naquilo que é necessário ao homem. Daí que esta sociedade deixará de existir por si mesma.

Por isso, os pecados que bradam ao céu, por constituirem um perigo gravíssimo para toda a família humana quando praticados livremente, deve ser severamente punido por Deus, tanto naturalmente como sobrenaturalmente, para que a espécie seja conservada na existência.