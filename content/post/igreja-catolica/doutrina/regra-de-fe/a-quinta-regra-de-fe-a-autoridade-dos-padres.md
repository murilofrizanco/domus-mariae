+++
date = "2015-07-24T18:00:00-18:00"
title = "A quinta regra de fé: A Autoridade dos Padres"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-quinta-regra-de-fe-a-autoridade-dos-padres.jpg"
+++

Embora a doutrina da Igreja seja assegurada pelo testemunho apostólico escrito e oral, pela prática universal da maioria dos fiéis da Igreja e com os decretos dos mais doutos da Igreja, se faz ainda necessário mais uma regra para assegurar que a Igreja esteja isenta de erro.

<!--more-->

Deve-se notar que tanto a Tradição oral como escrita de origem apostólica está suscetível de várias interpretações. O mesmo pode ser dito do decreto dos doutores em Concílio e também da prática universal do fiéis. Ora, vemos com clareza que a devoção mariana está de acordo com as quatro regras que explicamos até agora, contudo, alguns ainda questionam tal prática por entender que ela afasta de Deus e desvia a vida espiritual.

Esta dúvida pode ocorrer principalmente entre os mais simples que nem sempre são capazes de compreender os argumentos teológicos ou constatar estudar os documentos históricos, muitas vezes em linguas mortas, para comprovar que certa prática sempre foi observada em todos os tempos pela Igreja inteira. Ou ainda, pode-se duvidar dos decretos do Concílio por causa da vida ignomiosa de seus participantes ou de pressões externas que os participantes sofreram.

Portanto, é necessário que toda aquela teoria teológica encontre uma confirmação eficaz na prática para que qualquer dúvida seja dirimada ao ser constatado que aquela doutrina, quando seguida integralmente, produziu um santo.

Sendo assim, é necessário que olhemos para a vida dos santos para encontrar se tal doutrina produz efeitos divinos em quem a segue ou se não passa de invenção humana. Por este motivo, devemos ter em grande conta aqueles que chamamos de "Santos Padres", os quais se distinguiram por sua santidade de vida que atestava a verdade da doutrina que seguiam e viviam:

>Teodósio, o Velho, não achou melhor meio de reprimir as contendas que apareceram em seu tempo sobre a Religião que, seguindo o conselho de Sisinus, fazer chamar aos chefes das seitas e perguntar-lhes se tinham os antigos Padres, que haviam exercido funções na Igreja antes de todas aquelas disputas, por gente de bem, santos e bons católicos e apostólicos; ao que os sectários responderam que sim, e então lhes replicou: Comparemos, pois, vossa doutrina com a deles, e se está conforme a ela, retenhamos ela, e se não está, que a deixem. <cite>São Francisco de Sales, Controvérsias, II, Cap. V, Art. I</cite>

Mais do que recolher relatos ou testemunhos históricos sobre o que viviam os fiéis em geral, devemos olhar em especial para aqueles que demonstraram inequivocamente que a doutrina que lhes foi ensinada produzia uma profunda união com Cristo, o que é um privilégio somente da doutrina verdadeira.

Ademais, os que chamamos de "Santos Padres" ocuparam funções de grande importância na Igreja primitiva e foram testemunhas daquilo que era vivido, ensinado e recebido dos Apóstolos:

>Quem poderá atestar melhor a fé que a Igreja seguia naqueles tempos antigos do que aqueles que viveram com ela e se sentaram em sua mesa? Quem poderá declarar melhor os princípios desta celestial Esposa, na flor de sua idade, do que aqueles que tiveram a honra de exercer nela os principais ofícios? Sob este ponto de vista, os Padres merecem que se lhes preste fé, não pela excelente doutrina de que possuíam, senão pela realidade de suas consciências e a fidelidade com que procederam em seus trabalhos. Para ser testemunha não se requer tanto saber, mas que seja homem de bem e boa fé. Não os queremos aqui como autores de nossa fé, senão unicamente como testemunhas da crença em que vivia a Igreja daquele tempo. <cite>São Francisco de Sales, Controvérsias, II, Cap. V, Art. I</cite>

Com efeito, isto é profundamente necessário, pois sem o testemunho pessoal destes homens de santidade eminente não seria possível conhecer em profundidade e detalhes qual era realmente a fé da Igreja primitiva, a fé de cada diocese, como os fiéis aprenderam a lidar de forma cristã com determinada situação ou quais homens e mulheres foram exemplos de vida para nós. Por isto um deles dizia:

>Levemos em conta que a própria tradição, ensinamento e fé da Igreja Católica desde o principio dada pelo Senhor, foi pregada pelos apóstolos e foi preservada pelos pais, nisto foi fundada a Igreja e se alguém se afasta dela não deve ser chamado de cristão. <cite>Santo Atanásio de Alexandria</cite>

Estas coisas não podem ser promulgadas por um Concílio, pois os Concílios são raros e tratam de matérias que costumam afetar a Igreja inteira; nem podem ser atestadas pelas Escrituras ou Tradição, pois estes fatos ocorreram após a morte dos Apóstolos; nem pelo sentir universal da Igreja, pois sabemos que esta regra trata de coisas mais universais, carecendo daquele caráter mais pessoal. Ademais, a maioria dos fiéis não alcança aquele grau de intimidade espiritual com Cristo como estes homens alcançaram.

Portanto, faz-se necessário que a todas as regras sobreditas seja acrescentado o testemunho e a vivência dos Santos Padres, para que possamos atestar se determinada doutrina, quando observada e ensinada, conduz a santidade. Com grande precisão afirma um santo doutor:

>E se a fé da antiga Igreja deve servir-nos de regra para bem crer, jamais poderíamos achar melhor esta regra do que nos escritos e testemunhos destes santíssimos e importantes avôs. <cite>São Francisco de Sales, Controvérsias, II, Cap. V, Art. I</cite>