+++
date = "2015-07-29T18:00:00-18:00"
title = "A sexta regra de fé: A Autoridade do Papa"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-sexta-regra-de-fe-a-autoridade-do-papa.jpg"
+++

Cinco foram as regras de fé que descrevemos até o momento, mas uma sexta ainda se faz necessária.

<!--more-->

Duas são as fontes da Revelação: as Escrituras e a Tradição. Três classes de pessoas podem manifestar qual é a interpretação correta destas fontes: os Concílios, os santos e o sentir comum dos membros da Igreja.

Num primeiro momento, isto pode parecer suficiente, porém falta ainda uma classe de pessoa em especial que seja aquela capaz de confirmar que os julgamentos feitos pelos sábios, pelos membros da Igreja e pelos santos são realmente verdadeiros. Ademais, alguém precisa ter poderes para impedir ou invalidar possíveis erros dos sábios, dos membros ou até mesmo dos santos.

Este encargo não pode ser confiado a maioria da Igreja, nem a um grupo de sábios ou a determinados santos, pois são justamente estes que devem receber a confirmação de suas práticas e decretos. Ora, este ofício só pode ser confiado a um homem, o qual o chamamos de "Papa".

Alguns entendem que o Papa tomou o lugar de Cristo na Igreja, quando na verdade ele é apenas um de seus humildes servos que serve de instrumento do Mestre para governar a Igreja visivelmente. Não obstante, o Papa é chamado de um dos fundamentos da Igreja, mas sem tomar o lugar de Cristo:

>Nosso Senhor é, pois, fundamento e São Pedro também; ma com uma notável diferença, que em comparação a um, o outro não pode dizer-se que o é. Pois Nosso Senhor é fundamento e fundador, fundamento sem outro fundamento, fundamento da Igreja natural, mosaica e evangélica, fundamento perpétuo e imortal, fundamento da militante e da triunfante, fundamento de si mesmo, fundamento de nossa fé, esperança e caridade e do valor dos sacramentos. São Pedro é fundamento, não fundador de toda a Igreja, fundamento, mas fundado sobre outro fundamento, que é Nosso Senhor; fundamento somente da Igreja evangélica; fundamento sujeito à sucessão, fundamento da militante, não da triunfante; fundamento por participação; fundamento ministerial, não absoluto; enfim, administrador e não senhor e de nenhum modo fundamento de nossa fé, esperança e caridade, nem do valor dos sacramentos. Esta tão grande diferença faz que, em comparação, um não seja chamado fundamento tomado junto com o outro, embora tomado isoladamente possa ser chamado fundamento. <cite>São Francisco de Sales, Controvérsias, II, Cap. VI, Art. II</cite>

Deve-se notar que o modo como se diz que Cristo é fundamento é completamente diferente de como se diz que o Papa é fundamento. Como comparação, o Papa é fundamento da Igreja assim como um casal é fundamento de uma nova família, pois apesar de receberem tudo de Deus, inclusive os filhos, os bens e a graça do matrimônio, eles são ditos fundamentos de sua casa sem que com isso Deus perca sua autoria sobre aquela família e sobre a criação das almas dos filhos gerados.

Com efeito, a autoridade do Papa é restrita a administração da Igreja visível e nada além disso. Ele não pode mudar a doutrina, inventar doutrinas, mudar os sacramentos, nem qualquer outra coisa que Deus não lhe confiou.

Ele apenas administra e dispensa os tesouros confiados a ele por Nosso Senhor Jesus Cristo e nada mais que isso. Isso lhe permite, por exemplo, delegar bispos para dioceses, declarar alguma interpretação da revelação que a Igreja sempre possuiu como verdadeira, aplicar penas, conceder recompensas, determinar as condições para a validade dos sacramentos, enfim, algo semelhante ao que um governante pode fazer com o tesouro dos cidadãos.

É importante notar que o poder do Papa não está ligado a uma pessoa, embora seja confiado a uma só por vez, mas trata-se de uma função específica e necessária na Igreja. Deste modo, tal poder não se restringe ao Apóstolo Pedro, mas é passado adiante aos seus sucessores.

Este ministério é necessário, pois sem ele o fiéis não saberiam se um Concílio foi legítimo, se aquele que alguns declaram como santo realmente o é, se determinados costumes em certos momentos da Igreja eram divinos ou meramente humanos. Ora, é necessário que pelo menos uma pessoa na Igreja esteja totalmente imune ao erro em matéria de fé e moral para que se julgue estas coisas ou ateste a verdade ou falsidade delas.

Isto foi instituído por Cristo justamente para que não aconteça que por alguma circunstância todos caiam em erro e não haja nenhum homem infalível para atestar se aquelas pessoas estão no erro ou na verdade. Por isso, é dito que:

>Quando São Pedro foi colocado como fundamento da Igreja, e a Igreja teve a segurança de que as portas do inferno não prevalecerão contra ela, não foi dizer com bastante clareza que São Pedro, como pedra fundamental do governo e administração eclesiástica, não poderia dobrar-se nem romper-se pela infidelidade ou o erro, que é a porta principal do inferno? <cite>São Francisco de Sales, Controvérsias, II, Cap. VI, Art. XIV</cite>

E este ofício não era somente necessário quando as Escrituras estavam sendo escritas, mas mais ainda nos dias atuais onde a confusão é cada vez maior:

>A Igreja tem sempre a necessidade de um confirmador infalível, ao que possa dirigir-se, de um fundamento que as portas do inferno, e principalmente o erro, não possam derrubar, e de que o Pastor não possa conduzir ao erro a seus filhos: portanto, os sucessores de São Pedro tem todos seus mesmos privilégios, que não são anexos à pessoa, senão à dignidade e cargo público. <cite>São Francisco de Sales, Controvérsias, II, Cap. VI, Art. XIV</cite>

Portanto, se faz necessário que haja uma autoridade imune ao erro doutrinal que tenha poder para punir os desviados, corrigir e apontar seus erros, confirmar os fiéis para que caminhem com consciência tranquila na certeza de que estão na verdade e, finalmente, para que administre visivelmente a Igreja no mundo inteiro. Este encargo, exercido pela pessoa do bispo de Roma, é o sexto fundamento de nossa fé.

Deve-se notar que não confiamos na pessoa encarregada de tal encargo, mas no cargo oficial desta função na Igreja. Por isto se diz:

>Por isto dizemos que deve-se recorrer a ele, não como a um homem douto, embora nisto é ordinariamente muito adiantado, senão como a um Chefe e Pastor geral da Igreja, e como tal honrar-lhe, seguir e abraçar firmemente sua doutrina. <cite>São Francisco de Sales, Controvérsias, II, Cap. VI, Art. XV</cite>

Este ofício não é exercido pelo papa em seus atos pessoais, mas somente em suas ações que dizem respeito a Igreja inteira e que são feitas enquanto Pastor da Igreja Universal. Por esta razão deve-se considerar que:

>Tampouco deve-se pensar que em tudo e por tudo é seu juízo infalível, senão somente quando encerra sentença em matéria de fé e de ações necessárias a toda a Igreja, pois em casos particulares que dependem do ato humano, pode, sem dúvida, errar. <cite>São Francisco de Sales, Controvérsias, II, Cap. VI, Art. XV</cite>