+++
date = "2015-06-06T18:00:00-18:00"
title = "A segunda regra de fé: A Tradição Apostólica"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-segunda-regra-de-fe-a-tradicao-apostolica.jpg"
+++

A segunda regra sobre a qual se funda a fé cristã é a Tradição recebida dos Apóstolos, testemunhas diretas e autênticas do próprio Cristo, de seus ensinamentos e seus feitos.

<!--more-->

Pensa-se que com as Sagradas Escrituras não necessitamos da Tradição Apostólica, porém isto é desprezar um tesouro riquíssimo e necessário, como nota um grande doutor:

>A utilidade da Escritura não torna inútil as santas Tradições, do mesmo modo que o uso de um olho, de uma perna, de uma orelha ou de uma mão não impede o uso da outra <cite>São Francisco de Sales, Controvérsias, II, Cap. II, Art. I</cite>

De fato, lemos no Evangelho que muitas coisas não constam nem no seu Evangelho e nem no conjunto todo:

>Há, porém, muitas outras coisas que Jesus fez e que, se fossem escritas uma por uma, creio que o mundo não poderia conter os livros que se escreveriam. <cite>Bíblia de Jerusalém, Evangelho de São João, 21, 25</cite>

De fato nota o Doutor Angélico: 

>No início da Igreja, muito foi escrito sobre Cristo; mas isto não foi o suficiente; De fato, mesmo que o mundo passasse centenas de milhares de anos, e livros fossem escritos sobre Cristo, suas palavras e feitos não seriam completamente revelados. <cite>Santo Tomás de Aquino, Comentários ao Evangelho de São João, Leitura 6</cite>

Isto que é dito por Sâo João e comentado pelo Aquinate é completamente coerente. Temos como exemplo as coisas que o Cristo Ressuscitado faz até os dias de hoje na vida de cada um de nós. De fato, estas coisas não são narradas nas Escrituras e nem por isso deixam de ser verdadeiras obras e palavras de Cristo.

Outro exemplo foram as pregações dos Apóstolos: constam pouco delas nas Escrituras, mas isso não significa que não ensinaram nada além daquilo ou que não pregaram para lugares que ali não foram citados.

Outro exemplo foi os quarenta dias que Jesus Ressuscitado passou entre os Apóstolos ensinando sobre o Reino dos Céus, dos quais não consta escrito em nenhum lugar tudo o que fez e disse, pois poucos são os relatos das aparições. Estariam perdidas estas palavras e feitos tão preciosos? Certamente que não, caso contrário Jesus teria feito tais aparições em vão.

O sábio bispo nota com perspicácia uma coisa curiosa:

>A Escritura, pois, é o Evangelho, mas não todo o Evangelho, pois as Tradições são a outra parte; quem ensine, no entanto, algo diferente do que ensinaram os Apóstolos, maldito seja; porém, os Apóstolo ensinaram por escrito e por Tradição e tudo é Evangelho. <cite>São Francisco de Sales, Controvérsias, II, Cap. II, Art. I</cite>

Esta Tradição pode ser melhor compreendida nestes termos:

>Nós chamamos, pois, Tradição Apostólica a doutrina, seja de fé ou de costumes, que Nosso Senhor ensinou de sua própria boca ou pela boca dos Apóstolos, e que não estando escrita nos Livros Canônicos, foram conservadas até nós como passando de mão em mão por contínua sucessão da Igreja; mais resumidamente, é a palavra de Deus vivi, impressa, não em papel, senão no coração da Igreja somente. <cite>São Francisco de Sales, Controvérsias, II, Cap. II, Art. I</cite>

Para que Deus não tivesse falado em vão muitas palavras, é necessário que elas estejam todas gravadas no coração da Igreja, seja por escrito, seja por transmissão oral.

Por causa disto é necessário, para conservar a fé pura e íntegra, considerar e tomar com grande apreço aquilo que foi feito ou dito por Cristo e os Apóstolos que não consta nas Escrituras, mas que recebemos daqueles que os ouviram. Por isto dizia um grande doutor dos primeiríssimos séculos da Igreja:

>Levemos em conta que a própria tradição, ensinamento e fé da Igreja Católica desde o principio dada pelo Senhor, foi pregada pelos apóstolos e foi preservada pelos pais, nisto foi fundada a Igreja e se alguém se afasta dela não deve ser chamado de cristão. <cite>Santo Atanásio de Alexandria</cite>