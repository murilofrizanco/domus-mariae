+++
date = "2015-07-23T18:00:00-18:00"
title = "A quarta regra de fé: A autoridade dos Concílios"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-quarta-regra-de-fe-a-autoridade-dos-concilios.jpg"
+++

Em algumas ocasiões se faz necessário que os Pastores se reúnam e deliberem sobre certas práticas ou ideias para discernir se estão de acordo com o Evangelho.

<!--more-->

No decorrer da história da Igreja surgiram muitas ideias ou práticas que causaram dúvidas entre os fiéis. Estas dúvidas podem referir-se tanto a se os princípios capitalistas ou marxistas estão de acordo com o Evangelho, como se seria lícito divorciar-se da mulher em alguma situação ou ainda se determinado modo de orar é coerente.

Ora, é evidente que as Sagradas Escrituras não abrangem todas estas coisas em seus mínimos detalhes, pois seria impossível que um livro contivesse todas as possibilidades em detalhes.

Um exemplo disto é o aborto em caso de risco de vida para a mulher: seria aceitável ou não? Se sim, em que circunstâncias seria pecado e quais não? É fácil perceber que não se pode encontrar uma resposta direta e detalhada para tais situações nas Escrituras, pois na época o aborto nem era discutido.

Para lidar com isto, se faz necessário que diante da dúvida a Igreja reúna os homens mais doutos para deliberar e debruçar-se sobre o tema à luz das Escrituras e da Tradição Apostólica.

É verdade que cada fiel poderia fazer isto por si mesmo. Porém, deve-se notar que é necessário muito tempo para a leitura completa das Escrituras e um tempo maior ainda para a compreensão delas em profundidade.

Deste modo, seria impossível a um pai e uma mãe de família chegar a uma conclusão certa e firme, pois tem que trabalhar e cuidar dos filhos. Ainda deve-se considerar o caso dos iletrados: viveriam o resto de suas vidas na dúvida não conseguissem aprender a ler?

Portanto, é evidente que, para o bem de todos os fiéis, é muito conveniente que nestas situações o assunto seja tratado por pessoas que já detém os estudos necessários evitando que cada fiel tenha que começar do zero por ser surpreendido por uma situação nova.

Deve-se notar que o que a Igreja faz é reunir os melhores e unir aqueles que tem mais chance de acertar. Afinal, se os maiores estudiosos errarem, qual seria a chance de uma pessoa sozinha? Qual seria a chance de acerto de uma pessoa que tem fé, mas nunca estudou as Escrituras? Com efeito, diz o grande paladino da fé:

>Se a Assembleia legítima dos Pastores e chefes da Igreja pudesse alguma vez incorrer em erro, como se realizaria a sentença do Mestre: 'as portas do Inferno nunca prevalecerão contra ela' (Mt 16,18)? Como poderia o erro e a força infernal apoderar-se melhor da Igreja e de suas melhores bandeiras, senão escravizando aos doutores, Pastores e capitães com seu general? E esta palavra, 'Eis que estou convosco até a consumação dos séculos', a que ficaria reduzida? E como seria a Igreja 'coluna e sustentáculo da verdade' (1Tm 3,15), se suas bases e fundamentos se encontrassem no erro e na falsidade? Os doutores e Pastores são os fundamentos visíveis da Igreja e sobre sua administração descansa todo o resto. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. II</cite>

Alguns desprezam a autoridade do Concílio pelo fato de que seus participantes sejam homens dotados de fraquezas. Embora isto seja verdade, são os melhores e mais aptos estudiosos. Deste modo, do ponto de vista do esforço humano seria o melhor a ser feito.

É impossível que reunindo todos os sábios da face da terra não se considere todas as circunstâncias do problema e se investigue todas de modo que mais nada possa ser levantado como objeção, além de contar com a garantia do auxílio divino do Espírito Santo. Por isto, o santo bispo afirma:

>Se se rejeita aos Concílios a soberania de suas decisões e declarações necessárias sobre o entendimento da Santa Palavra, a Santa Palavra será tão profanada como os textos de Aristóteles, e nossos artigos de Religião estarão sujeitos à revisão contínua, e de cristãos resolutos e seguros nos converteremos em miseráveis acadêmicos. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. II</cite>

Uma objeção que poderia ser levantada é se a reunião destes homens sábios possui realmente o auxílio de Deus e se podem apresentar suas conclusões em nome do Espírito Santo.

Quanto a isto deve-se notar que a Revelação Divina já atestou que esta prática é de origem apostólica e que possui a assistência divina:

>Surgindo daí uma agitação e tornando-se veemente a discussão de Paulo e Barnabé com eles [os hereges], decidiu-se que Paulo e Barnabé e alguns outros dos seus subiriam a Jerusalém, aos apóstolos e anciãos, para tratar do problema. (...) Reuniram-se então os apóstolos e os anciãos para examinarem o problema. <cite>Bíblia de Jerusalém, Atos dos Apóstolos, 15, 2.6</cite>

Depois é enviada uma carta com a conclusão do Concílio de Jerusalém que diz "De fato, pareceu bem ao Espírito Santo e a nós não vos impor nenhum outro peso além destas coisas necessárias..." (At 15,28), mostrando que a reunião dos mais doutos da Igreja possui assistência divina.

>A autoridade, pois, dos Concílios deve ser reverenciada como apoiada pela direção do Espírito Santo, pois se contra aquela heresia farisaica o Espírito Santo, doutor e condutor de sua Igreja, assistiu a Assembleia, é necessário crer também que em todas as ocasiões semelhantes assistirá igualmente as Assembleias dos Pastores para ordenar por sua boca nossas ações e crenças. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. II</cite>

Deve-se notar que os Concílios são raros e tratam de temas de grande importância que afligem a Igreja do mundo inteiro. De modo geral, os motivos que levam a Igreja a convocar um concílio são expressos no seguinte escrito:

>Quais são as causas principais pelas quais se reúnem os Concílio Ecumênicos, senão para reprimir ou rechaçar ao herege, ao cismático, ao escandaloso como a lobos do aprisco? Assim se fez nesta primeira Assembléia de Jerusalém para resistir a alguns, impulsionados pela heresia dos fariseus. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. I</cite>

Deve-se considerar que a estrutura do Concílio deve é tal que os juízes estejam em comunhão com os Apóstolos e sucessores, além de ter garantido a exposição livre de ideias para que sejam julgadas pelos homens doutos:

>Falamos aqui, pois, de um Concílio tal como aquele em que se acha a autoridade de São Pedro, tanto no princípio como na conclusão, e a dos demais Apóstolos e Pastores que queiram encontra-se ali, se não todos, pelo menos uma parte notável; onde a discussão seja livre, isto é, que quem queira exponha ali suas razões sobre a dificuldade que nele seja proposta; onde os Pastores tenham voz judicial; tais, enfim, como foram aqueles quatro primeiros. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. I</cite>

O consenso dos doutores deve ser recebido por todos os fiéis como divinamente revelado por Deus, pois estavam de posse da inspiração do Espírito Santo, assim como os autores das Sagradas Escrituras também possuíam. Aos que o negam se diz:

>Se nem mesmo à Igreja der ouvido, trata-o como o gentio ou o publicano. <cite>Bíblia de Jerusalém, Evangelho segundo São Mateus, 18, 17</cite>

Tão fundamental é guardar o ensinamento dos Concílios que desprezá-lo levaria a uma confusão de tal modo que:

>Se alguém entender (...) que lhe é permitido voltar a colocar em dúvida o que já está determinado pelos Concílios Ecumênicos, será preciso que (...) diga que tudo está à mercê de nossas sutis temeridades, e que tudo é incerto e está sujeito à diversidade dos juízos e considerações dos homens. <cite>São Francisco de Sales, Controvérsias, II, Cap. IV, Art. II</cite>

Em outras palavras, se duvidarmos ou negarmos o ensino dos Concílios teremos que admitir que as verdades mudam conforme os tempos e os lugares e que a Igreja pode ir se afastando daquilo que era observado e ensinado anteriormente para se conformar com a sociedade em que está situada.

Portanto, vemos que não é possível que a fé seja incorruptível sem a autoridade dos Concílios, sob pena de cair na confusão de interpretações e em relativizar os textos conforme os costumes de cada povo e da cada época.