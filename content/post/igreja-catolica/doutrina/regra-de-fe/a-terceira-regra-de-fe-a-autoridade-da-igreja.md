+++
date = "2015-07-15T18:00:00-18:00"
title = "A terceira regra de fé: A Autoridade da Igreja"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-terceira-regra-de-fe-a-autoridade-da-igreja.jpg"
+++

A terceira regra sobre a qual se fundamenta a fé cristã é a autoridade da Igreja.

<!--more-->

Alguns pensam que devemos nos basear somente nas Escrituras e na Tradição Apostólica, bastando isto para que não haja erro na interpretação delas.

Porém, o que vemos na prática é muito diferente, como podemos notar quando um católico entende que, no capítulo 16 de Mateus e em outras passagens, Jesus instituiu Pedro como chefe dos Apóstolos; por outro lado, os protestantes entendem outra coisa se fundamentando nas mesmas passagens.

Outro exemplo é quando vemos que a Igreja entende que quando Jesus diz "Eu e o Pai somos um" ela afirma que é de igual natureza com o Pai; por outro lado os testemunhas de Jeová entendem que isto é mera figura de linguagem nesta passagem e que não quer significar isto, de modo a negar a divindade do Cristo.

Ora, estamos diante de duas interpretações completamente fundadas nas Sagradas Escrituras e ao mesmo tempo contraditórias. Evidentemente somente uma delas pode ser a certa, mas a pergunta é: quem é a autoridade legítima para julgar qual interpretação é certa e qual é a errada? Quem poderá resolver, sem perigo de engano, esta dúvida sobre a interpretação de uma passagem?

Este impasse foi percebido pelo grande doutor:

>A isto respondem eles que é preciso julgar as interpretações da Escritura confrontando passagem com passagem e o todo com o Símbolo da fé. Amém, Amém, dizemos nós; mas não estamos perguntando como deve ser interpretada a Escritura, senão quem deve ser o juiz. <cite>São Francisco de Sales, Controvérsias, II, Cap. III, Art. I</cite>

Há casos semelhantes no dia a dia: alguém pode vir a cair de uma escada e morrer porque um outro esbarrou naquela pessoa.

Mas a situação pode ser interpretrada de dois modos. Pode ser que aquele que a derrubou o fez sem intenção, então trata-se de um acidente; por outro lado, pode ser que o fez intencionalmente, e trata-se de um assassinato.

Muitas pessoas podem interpretar o ocorrido de modos diferentes apresentando diversos argumentos válidos, alguns defendendo tratar-se de um acidente e outros defendendo tratar-se de um assassinato; não obstante, ambos os lados se baseiam no mesmo fato.

Neste caso, é evidente que não se pode aceitar que ambas as opiniões são corretas e nem deixar o assunto sem resolução, por isso é necessário um juiz para dizer qual das duas interpretações é a correta para aquela situação para que se proceda com a pena ou a absolvição do culpado.

O juiz legítimo, diante do fato e das evidência, irá pronunciar qual é a interpretação legítima e todos irão aceitá-la.

Do mesmo modo, é necessário um juiz que irá julgar a legitimidade de uma ou outra interpretação das Escrituras diante de uma controvésia. Porém, neste caso, trata-se de algo que excede a capacidade humana. Por este motivo, nenhum homem em particular pode ser um juiz à altura do problema:

>Não é, portanto, razoável que ninguém em particular se atribua este juízo infalível sobre a interpretação ou explicação da Santa Palavra, porque aonde iríamos então parar? Quem gostaria de sofrer o julgo de outro? E porque mais o de um do que de outro? <cite>São Francisco de Sales, Controvérsias, II, Cap. III, Art. I</cite>

De fato, vemos que em nós mesmos que nossa opinião sempre é mais certa que a dos outros e custamos a nos desapegar dela.

Sendo assim, como não é possível existir um homem que possa tomar para si o encargo de juiz infalível da interpretação das Escrituras, necessitamos de uma terceira regra que complemente as duas fontes da Revelação:

>É uma impiedade crer que Deus não nos tenha deixado na terra alguma supremo juiz ao qual possamos dirigir nossas dificuldades, e que seja de tal modo infalível em seus juízos que seguindo seus decretos não se pode errar. Sustento que este juiz não é outro que a Igreja Católica, que de nenhum modo pode errar nas interpretações e consequências que retira da Sagrada Escritura, nem nos juízos que ditam acerca das dificuldades que a ela se apresentam. <cite>São Francisco de Sales, Controvérsias, II, Cap. III, Art. I</cite>

Deve-se notar que a Igreja Católica não se toma no sentido de um grupo de pessoas particulares, mas o que aqui se diz é que o juiz sobre a interpretação das Escrituras não é um ou vários homens em particular, mas a própria instituição chamada Igreja, ou seja, o próprio Corpo Místico de Cristo.

Portanto é necessário que o juiz supremo e infalível seja a instituição da Igreja e não esta ou aquela pessoa. Podemos entender, usando como metáfora, que o juiz das Escrituras não é o que se entende por uma pessoa física, mas sim o que se entende por uma pessoa jurídica.

Por isto afirmava um grande místico:

>Além das verdades reveladas pela Igreja quanto à substância de nossa fé, não há mais o que revelar. Por isso é necessário não só rejeitar qualquer novidade, mas também acautelar-se para não admitir as que aparecem sutilmente misturadas às substâncias dos dogmas. <cite>São João da Cruz</cite>

Deste modo, a terceira regra é aquilo que a instituição da Igreja em todo seu Corpo sempre creu, explícita ou implicitamente, em todos os lugares e desde sua fundação.

Não é possível, portanto, que uma interpretação das Escrituras que contradiga a prática e entendimento perene do cristianismo possa ser verdadeiramente autêntica, uma vez que isso equivale a dizer que a Igreja inteira errou e que devemos fundar uma nova Igreja de Cristo, negando que o inferno jamais prevaleceria sobre ela.

Deste modo, fica evidente que o verdadeiro juiz é a Igreja que não pode errar em seu Corpo inteiro ao mesmo tempo de forma a perverter a doutrina de tal modo que a verdadeira se perca ou seja esquecida.