+++
date = "2015-06-05T18:00:00-18:00"
title = "A primeira regra de fé: As Sagradas Escrituras"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-primeira-regra-de-fe-as-sagradas-escrituras.jpg"
+++

O primeiro e mais seguro fundamento da fé cristã é, apesar de muitos pensarem que não, as próprias Sagradas Escrituras.

<!--more-->

Deve-se notar as Sagradas Escrituras são atestadas como divinamente inspiradas pelo fato de que elas nos fornecem por escrito a Tradição Apostólica:

>A Tradição precedeu a toda Escritura, pois até mesmo uma parte da Escritura não é senão a Tradição posta por escrito com uma infalível assistência do Espírito Santo. <cite>São Francisco de Sales, Controvérsias, II, Cap. I, Art. I</cite>

Com isto, vemos que as Escrituras são uma das fontes primárias da fé cristã e não podem ser desprezadas, pois elas não somente nos transmitem a história de Cristo e seus ensinamentos, além do ensino dos Profetas e Apóstolos, como também constitui uma regra segura e sublime para alcançar a sabedoria e a virtude.

O conjunto dos Livros Inspirados são tão importantes que quem não crê neles integralmente não pode ser verdadeiro cristão:

>A Santa Escritura é de tal modo regra de nossa fé cristã que quem não crê em tudo o que ela contém, ou crê em alguma coisa que seja minimamente contrária a ela, é um infiel. Nosso Senhor encaminhou ela aos judeus para reedificar sua fé (Jo 5,39); os saduceus erravam por ignorar as Escrituras (Mc 12,24); é, pois, um nível muito seguro, é uma "tocha luzindo nas trevas", como diz São Pedro (2Pd 1,19), aquele que, ouvindo por si mesmo a voz do Pai na Transfiguração do Filho, se firma, todavia, mais pelo testemunho seguro dos Profetas do que por sua própria experiência (2Pd 1,17-18). <cite>São Francisco de Sales, Controvérsias, II, Cap. I, Art. I</cite>

Portanto, quem rejeita as Escrituras, algum de seus livros e até mesmo alguns versículos é um infiel e não pode ser chamado de verdadeiro cristão. Por isso dizia sabiamente um grande estudioso e santo:

>Desconhecer a Sagrada Escritura é desconhecer a Cristo. <cite>São Jerônimo</cite>

Ou ainda como dizia um dos homens mais santos que passaram sobre a terra:

>Ler a Sagrada Escritura é pedir conselhos à Cristo. <cite>São Francisco de Assis</cite>

Ou ainda um outro:

>Medite na Palavra de Deus e ela terá o poder de transformar suas inclinações naturais para elevar seu espírito com pensamentos puros e sublimes. <cite>São Pio de Pietrelcina</cite>

Além disso, a Igreja preza por sua integridade para que o sentido de suas palavras jamais sejam falsificados ou adulterados, pois um erro de tradução, a mudança de uma palavra ou um entendimento incorreto pode significar a tomada de um caminho onde se rejeita o tesouro da graça conquistada por Cristo. Por isso dizia o bispo:

>Quem muda, por pouco que seja, a santa Palavra, merece a morte, como quem ousa mesclar o profano com o sagrado (Lv 10,9-10). <cite>São Francisco de Sales, Controvérsias, II, Cap. I, Art. II</cite>

As Escrituras se dividem no Antigo e Novo Testamento, e cada uma destas partes se divide entre os livros que sempre foram aceitos de imediato e os que houve alguma dúvida sobre sua autenticidade, depois sendo atestados como autênticos:

>Os [da primeira categoria], no Antigo Testamento, são cinco de Moisés [Pentateuco], Josué, os Juízes, Rute, quatro dos Reis [Samuel e Reis], dois de Paralipômenos [Crônicas], dois de Esdras e Neemias, Jó, 150 Salmos [151 dividindo um deles em dois], os Provérbios, o Eclesiastes, o Cântico dos Cânticos, os quatro Profetas maiores e os doze menores. (...) A segunda categoria contém os seguintes: Ester, Baruch, uma parte de Daniel, Tobias, Judite, a Sabedoria, o Eclesiástico, os Macabeus, primeiro e segundo. <cite>São Francisco de Sales, Controvérsias, II, Cap. I, Art. II</cite>

>Direi, também, dos livros do Novo Testamento, que os tem em primeira categoria, que sempre foram reconhecidos e recebidos como sagrados e canônicos entre os católicos, tais são: os quatro Evangelhos segundo São Mateus, São Marcos, São Lucas e São João, os Atos dos Apóstolos, todas as epístolas de São Paulo, exceto a dirigida aos Hebreus, uma de São Pedro e uma de São João. Os da segunda categoria são a Epístola aos Hebreus, a de Tiago, a segunda de São Pedro, a segunda e terceira de São João, a de São Judas, o Apocalipse e algumas passagens de São Marcos e São Lucas e do Evangelho e Epístola primeira de São João, e estas últimas não foram de autoridade indubitável no início da Igreja; mas com o tempo foram por fim reconhecidos como obra sagrada do Espírito Santo, e não uma, mas várias vezes. <cite>São Francisco de Sales, Controvérsias, II, Cap. I, Art. II</cite>

Deve-se notar a grande contradição daqueles que negam a inspiração sagrada dos livros deuterocanônicos do Antigo Testamento, que são admitidos pela Igreja, visto que eles aceitam os livros do Novo Testamento em que recaíram dúvida por algum tempo.

Se eles querem rejeitar os livros em que houve alguma dúvida do Antigo Testamento, teriam que igualmente rejeitar muitos do Novo Testamento, incluindo o Apocalipse que é utilizado para chamar a verdadeira Esposa de Cristo de prostituta, porém convenientemente não o rejeitam.

Vemos portanto, que a autoridade das Escrituras não se fundamentam no consenso inicial e imediato de seu cânon, mas sim na autoridade da Igreja, fundada por Nosso Senhor, que os atestou como inspirados pelo Espírito Santo.

Atestou-se isto não por uma certeza histórica sobre o autor, mas por perceber que determinados escritos transmitiam exatamente o que a Igreja sempre ensinou desde os Apóstolos.

Alguns poderiam pensar, por termos este fundamento, que nossa fé, semelhantemente ao Islamismo, vem de um livro, mas isto é falso.

Nossa fé se fundamenta sobre a pessoa do Cristo Ressuscitado que continua a operar a salvação, propagar sua doutrina e oferecer suas graças aos homens através de seu Corpo Místico que é a Igreja, sendo as Escrituras um instrumento que conduz ao Cristo Ressuscitado e aos meios eficazes de obter sua graça.

De fato, a Igreja existiu muitos séculos sem as Escrituras, mas as Escrituras, em sua integridade, nunca existiram sem a Igreja.

Logo, por recebermos os livros sagrados que a Igreja guardou e transmitiu integralmente por seus sucessores são sólidos fundamentos de nossa fé e as tomamos em seu conjunto como regra fundamental da doutrina e espiritualidade cristã, a tal ponto que o Doutor Angélico ensina:

>Não há nada de necessário à fé, contido no sentido espiritual, que ela não explique manifestamente, alhures, no sentido literal. <cite>Santo Tomás de Aquino, Suma Teológica, Ia, q. 1, a. 10</cite>