+++
date = "2015-05-01T18:00:00-18:00"
title = "As regras de fé da Igreja Católica"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/as-regras-de-fe-da-igreja-catolica.jpg"
+++

A tormenta de acusações contra a validade, coerência e realidade da doutrina da Igreja é constante induzindo as almas mais fracas e ignorantes ao erro, destruindo a fé e constituindo falsos doutores que sempre apresentam novidades incoerentes no modo de interpretar o ensinamento de Cristo.

<!--more-->

Diante disso é necessário saber quais são as regras de fé que nos garantem certeza absoluta e infalível de que determinado entendimento ou aplicação da Revelação dada por Cristo é legítimo.

Para tanto, deve-se, em primeiro lugar, enumerar quais são as regras de fé.

Sobre isto há uma síntese genial feita por São Francisco de Sales em sua obra Controvérsias:

>Há então oito regras de fé: as Escrituras, a Tradição, a Igreja, os Concílios, os Padres, o Papa, os milagres e a razão natural. <cite>São Francisco de Sales, As Controvérsias, II, Cap. 1, art. 11</cite>

Esta peça de teologia produzida pelo santo doutor é desconhecida pela maioria das pessoas. Foi escrita no contexto do Calvinismo tratando das disputas teológicas vigentes que tentavam reduziu a enumeração das regras de fé para uma só, isto é, as Escrituras.

Assim, por ocasião desta controvésia, o bispo Francisco viu a necessidade de explicitar e demonstrar a veracidade e necessidade de todas as regras de fé para o reto entendimento e aplicação da doutrina revelada por Cristo.

Alguns poderiam pensar que fosse pretensão ilusória querer julgar qual a interpretação é correta e qual a falsa.

Porém, recordemos que o Espírito Santo nos faz um apelo através da pena do Apóstolo Amado:

>Caríssimos, não acrediteis em qualquer espírito, mas examinai os espíritos para ver se são de Deus, pois muitos falsos profetas vieram ao mundo. <cite>Primeira Epístola de São João, 4, 1</cite>

Sobre este ponto deve-se notar que:

>Aquele que nos disse que provássemos os espíritos, não o faria se não soubesse que teríamos regras infalíveis para distinguir o santo do falso espírito. (...) Importa, portanto, saber exatamente quais são as verdadeiras regras de nossa fé, pois se poderá distinguir facilmente a heresia da verdadeira religião. <cite>São Francisco de Sales</cite>

Assim, o bispo passa a explicar que a fé cristã está fundada na Palavra de Deus e isto é o que lhe confere a certeza da infalibilidade eterna, advertindo que qualquer fé que não se apoie na Palavra de Deus, não pode ser chamada de cristã e, portanto, nem sequer de verdadeira, pois Cristo é a única Verdade.

Contudo, somos advertidos de que para considerar esta regra como válida, deve-se notar que:

>Não basta saber que a Palavra de Deus é a verdadeira e infalível regra de bem crer, se não sei que palavra é de Deus, onde está e quem a deve propôr, aplicar e declarar. E em vão saberei que a Palavra de Deus é infalível. <cite>São Francisco de Sales</cite>

O autor continua apresentando um exemplo claro desta necessicade. Ora, vemos que Cristo é o filho de Deus Vivo. Porém, como entender esta filiação? Deveríamos entendê-la como uma adoção, como propõe os arianos, em que um homem é elevado à condição divina? Deveríamos entender esta filiação no sentido católico que afirma que Cristo sempre foi dotado de natureza divina?

Com efeito, faz-se necessário que hajam regras para discernir qual das duas posições é a correta, pois o adocionismo ariano foi fundamentado nas Escrituras, assim como a filiação natural. Por isso:

>É necessário, pois, além desta primeira e fundamental regra da Palavra de Deus, outra segunda regra, em virtude da qual, a primeira nos seja bem e devidamente proposta, aplicada e declarada, e a fim de que não fiquemos sujeitos à vacilação e à incerteza, é necessário que não somente a primeira regra, a saber, a Palavra de Deus; senão também a segunda, que propõe e aplica esta Palavra, seja completamente infalível, pois de outro modo permaneceríamos na vacilação e na dúvida de estar mal dirigida e apoiada nossa fé e crenças; não por defeito da primeira regra, senão por erro e falta na aplicação dela. <cite>São Francisco de Sales</cite>

Em seguida o santo doutor explica que a infalibilidade do conteúdo da Palavra de Deus e de sua aplicação procedem diretamente de Deus. Ora, quem aplica e propõe a Palavra de Deus é a Igreja. Assim, chegamos a conclusão de que:

>A Palavra de Deus é a regra fundamental e formal; e a Igreja de Deus é a regra de aplicação e explicação. <cite>São Francisco de Sales</cite>

Todavia, para entender melhor em que consiste a Palavra de Deus e quando a Igreja atua de modo legítimo faz-se necessário entender que:

>A Palavra de Deus, regra formal de nossa fé, está nas Escrituras e na Tradição. (...) A Igreja, que é a regra de aplicação, ou se declara em todo seu corpo universal por uma crença geral de todos os cristãos (o Corpo da Igreja), ou (...) por um consentimento de seus pastores e doutores; e nesta última forma, ou em seus pastores reunidos em um lugar e um tempo, como em um Concílio Geral (Concílio Ecumênico), ou em seus pastores divididos em lugares e tempos reunidos na união e correspondência da fé (consentimento dos Padres), ou enfim, esta Igreja se declara e fala por seu chefe ministerial (o Papa). <cite>São Francisco de Sales</cite>

Deve-se notar que além destas garantias ainda acrescentam-se duas regras que Deus provê para suprir a debilidade humana:

>Querendo ajudar a debilidade dos homens não deixa de acrescentar as vezes à estas regras ordinárias uma regra extraordinária muito certa e de grande importância: é o milagre, testemunho extraordinário da verdadeira aplicação da Palavra divina. Enfim, a Razão Natural pode ser chamada uma regra de bem crer (...), pois [o artigo de fé] não pode ir contra a razão natural, (...) pois a razão natural e a fé, como procedentes de uma mesma origem e saídas de um mesmo autor, não podem ser contraditórias. <cite>São Francisco de Sales</cite>

Ademais, tudo o que a Igreja ensina tem que estar em total acordo com todas estas regras ao mesmo tempo. Por isso, Francisco de Sales nos diz que:

>De tal modo de entrelaçam [estas regras] que quem viola uma, viola todas as demais. <cite>São Francisco de Sales</cite>

Portanto, toda a doutrina da Igreja Una, Santa, Católica e Apostólica se fundamenta nestas oito regras e delas retira sua certeza, sendo o verdadeiro critério para reunir todos os cristãos na unidade da fé.