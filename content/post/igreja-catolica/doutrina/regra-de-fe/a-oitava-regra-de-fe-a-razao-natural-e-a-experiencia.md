+++
date = "2015-08-01T18:00:00-18:00"
title = "A oitava regra de fé: A Razão Natural e a Experiência"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-oitava-regra-de-fe-a-razao-natural-e-a-experiencia.jpg"
+++

Além de todas as regras até então apresentadas se faz ainda necessário mais uma regra importantíssima que procede da própria natureza humana.

<!--more-->

As regras anteriores diziam respeito às fontes da revelação, às autoridades que garantiam a aplicação correta das fontes e os eventos extraordinários que ocorrem no Universo e atestam a nossa fé. Porém, deve-se considerar aquilo que foi criado, isto é, as coisas ordinárias. Com efeito, lemos no prólogo escrito pelo Apóstolo São João:

>No princípio era o Verbo e o Verbo estava com Deus e o Verbo era Deus. No princípio, ele estava com Deus. Tudo foi feito por meio dele e sem ele nada foi feito. <cite>Bíblia de Jerusalém, Evangelho segundo São João, 1, 1-3</cite>

Ora, se é verdade que as coisas foram feitas no Verbo e pelo Verbo devemos considerar que elas manifestam Aquele que as criou. Com efeito,

>O que se pode conhecer de Deus é manifesto entre eles, pois Deus lho revelou. Sua realidade invisível — seu eterno poder e sua divindade — tornou-se inteligível, desde a criação do mundo, através das criaturas, de sorte que não têm desculpa. <cite>Bíblia de Jerusalém, Epístola aos Romanos, 1, 19-20</cite>

Por isso um grande teólogo escreveu:

>Toda criatura possui uma causa e uma imagem na razão de Deus e em sua providência eterna; e é por esta causa e sobre o modelo desta imagem que ela foi criada em sua substância <cite>Hugo de São Vitor, De Sacramentis Fidei Christianae, lib. 1, pars 5, cap. 3</cite>

Por isso, é necessário uma oitava regra de fé que se refere a própria Criação, pois esta também é uma revelação de Deus feita para o homem que deve usufruir desta fonte para sua própria vida espiritual:

>O mundo é, de fato, um livro escrito pelo próprio dedo de Deus. Cada criatura é como um sinal, não por convenção humana, mas estabelecido pela vontade divina. O homem ignorante vê um livro aberto, percebe certos sinais, mas não conhece nem as letras nem o pensamento que elas manifestam. Assim também o insensato, o homem animal que não percebe as coisas de Deus, vê a forma exterior das criaturas visíveis, mas não compreende os pensamentos que eles manifestam. Assim como em uma única e mesma obra um homem admira a cor e a forma das letras, enquanto outro louva os pensamentos que elas expressam. É bom, portanto, contemplar assiduamente e admirar as obras de Deus, mas para aquele que souber converter a beleza das coisas corporais em uso espiritual. <cite>Hugo de São Vitor, Didascalicon, livro 7, cap. 4</cite>

Isto demonstra a falsidade daqueles que pretendem tomar a fé sem a razão ou a razão sem a fé. Com efeito, a graça não destróis a natureza, mas a aperfeiçoa. Assim, não é possível que a fé seja verdadeira quando contradiz aquelas coisas certas que o homem podem chegar pela razão ou pela experiência.

>Seja natural ou sobrenatural, a razão é sempre razão, e a verdade, sempre verdade; assim como é o mesmo olho o que vê na escuridão da noite sombria a dois passos dele, e o que vê em pleno dia tudo quanto abarca o círculo do horizonte. As que são distintas são as luzes que a esclarece; do mesmo modo, a verdade, seja natural ou sobrenatural, é sempre a mesma, e somente são distintas as luzes que a mostram ao nosso entendimento; a fé nos mostra de forma sobrenatural, e o entendimento de forma natural; mas a verdade jamais é contrária a si mesma. <cite>São Francisco de Sales, II, Cap. VIII, Art. I</cite>

Deve-se considerar também a importância da nossa experiência sensorial. Ora, é evidente que vemos, ouvimos, cheiramos, tocamos e degustamos as coisas e estas experiências são verdadeiras e criadas por Deus. Ademais, é partindo dos sentidos que chegamos ao pensamento, portanto a razão humana e os sentidos não podem ser separados.

Assim, nem a experiência sensível nem a razão pode contradizer abertamente os verdades da fé, mesmo que se tratasse de coisa de aparente pouca importância. Ou nossa experiência e razão são completamente coerentes com a doutrina que procede da revelação sobrenatural ou esta doutrina sobrenatural é completamente falsa. Isso significa que a fé tem que ter coerência lógica e estar de acordo com todo aquele conhecimento científico experimental certo e verdadeiro que temos ou possamos vir a ter um dia.

O quão grande apreço pela razão tem a verdadeira doutrina é manifestado por um grande escritor narrando uma comparação interessante da sã doutrina e do paganismo:

>Um dos melhores e mais brilhantes [livre-pensadores], o Sr. Bernard Shaw, disse muito recentemente que nunca poderia concordar completamente com a Igreja Católica por esta ser racionalista ao extremo. Neste sentimento ele é ao menos bastante racional; isto é, como diríamos nós, pobres racionalistas, ele está completamente correto. A Igreja é maior que o mundo; e resistiu francamente aos mesquinhos racionalistas que afirmavam que tudo que existe pode ser estudado exatamente da mesma maneira que é usada para algumas coisas particulares no mundo. Mas nunca disse que essas coisas não deveriam ser estudadas, ou que a razão não fosse a forma apropriada de o fazer, ou que alguma pessoa tivesse algum direito de ser irracional ao estudar qualquer coisa. Defende a sabedoria do mundo como o meio de lidar com ele; defende o bom senso e o pensamento consistente e a percepção de que dois e dois são quatro. E nestes dias é a única a defendê-lo. <cite>G. K. Chesterton, Introdução ao livro "Deus e a Inteligência na Filosofia Moderna" de Fulton Sheen</cite>

O próprio Doutor Angélico reconhece que com os infiéis o uso das Escrituras ou da autoridade divina é um argumento inútil, pois rejeitam-nas, e por isto mesmo a razão se torna necessária e útil para demonstrar que a fé cristã não contradiz a criação:

>Entre os que erram, alguns, como os maometanos e os pagãos, não aceitam como nós, a autoridade de algum texto das Escrituras, pelo qual possam ser convencidos. (...) Por esses motivos, deve-se recorrer à razão natural, com a qual todos são obrigados a concordar. Além disso, ao investigarmos uma verdade, juntamente mostraremos os erros por ela excluídos e como a verdade racional concorda com a fé da religião cristã. <cite>Suma Contra os Gentios I, Cap. 2, 3-4</cite>

Porém, deve-se notar que a razão e a experiência não podem provar as verdades da fé, mas podem mostrar que elas são coerentes com o Universo inteiro enquanto outras coisas, supostamente reveladas por Deus, não o são, sendo, portanto, falsas.

Assim, a razão e a experiência são regras importantes para garantir que a Igreja não caia no erro e para que os pagãos possam ver a falsidade de suas doutrinas submetendo-se a Nosso Senhor Jesus Cristo aderindo ao seu Corpo Místico pelo esplendor da verdade e da beleza da Criação.