+++
date = "2015-07-30T18:00:00-18:00"
title = "A sétima regra de fé: A Autoridade dos Milagres"
categories = ["Igreja Catolica","Doutrina","Regra de Fe"]
path = "Doutrina"
featured_image = "posts/images/a-setima-regra-de-fe-a-autoridade-dos-milagres.jpg"
+++

Embora as seis regras anteriores fossem suficientes para que o homem pudesse ter certeza de tudo aquilo que diz respeito à sua salvação, é necessário ainda que haja mais outra, que não pode ser desprezada.

<!--more-->

Uma vez que se recebe as fontes da revelação e as interpretações coerentes com o testemunho dos santos, dos sábios, da prática geral da Igreja e da confirmação do Papa bastaria para que os fiéis trilhassem o caminho da salvação com segurança.

Porém, deve-se notar que tudo isto não tem qualquer valor para os infiéis. Se todo o conjunto da revelação só pudesse ser julgado pelas seis regras anteriores os infiéis estariam excluídos da Igreja.

Ora, é evidente que os infiéis não levam em consideração qualquer especulação teológica e nem qualquer decreto de uma autoridade a qual não estão submetidos. Ademais, como saber que as Sagradas Escrituras e o testemunho Apostólico realmente são divinos quando existem tantos outros, como Maomé, que se apresentam como porta-vozes de Deus?

Assim, é necessário que Deus manifeste com sinais claros que é ali que se revela e não em outro lugar de modo que os homens não possam mais desculpar-se de não crer. Por isso:

>A fim de que Moisés fosse crido Deus lhe deu o poder dos milagres. Nosso Senhor - diz São Marcos - confirmou assim a pregação Apostólica, e se nosso Senhor não tivesse feito tantos milagres, não haveria pecado em não crer - diz o próprio Senhor (Jo 15,24). São Paulo assegura que Deus confirmou a fé por milagres (Hb 11,4); logo o milagre é uma justa prova da fé, e um argumento decisivo para persuadir os homens a crer; pois se assim não fosse, nosso Senhor não teria se servido deles. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Embora as seis regras anteriores bastem aos fiéis, o milagre não é supérfluo para estes. Deve-se notar que, no caso dos fiéis, eles não são necessários, porém servem magnificamente para atestar a cada um, especialmente aos mais rudes, que o ensino da Igreja é coerente:

>Realmente parece que os milagres são testemunhos gerais para os ignorantes e mais rudes, pois nem todos podem sondar a admirável concordância que existe entre os profetas e o Evangelho, a grande sabedoria da Escritura e outras marcas excelentes da Religião Cristã, cujo exame é próprio dos doutos. Mas não é necessário sê-lo para admitir o testemunho de um verdadeiro milagre, pois todo mundo entende esta linguagem entre os cristãos. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Deste modo, quando ocorrem devemos aceitá-los e perceber o que está sendo atestado extraordinariamente pelo ocorrido:

>Não digo que sejam necessários, senão unicamente que ali onde compraz a bondade divina em fazê-los para confirmação de algum artigo, estamos obrigados a crê-lo. Pois ou o milagre é uma justa persuasão e confirmação, ou não o é: se não o fosse, resultaria que nosso Senhor não confirmava justamente sua doutrina; e se são uma justa persuasão resulta disto que no momento em que se façam, somos obrigados a tomá-los por uma razão muito firme, como assim o são. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Alguns podem objetar que o milagre é algo duvidoso, pois com o advento da ciência moderna experimental passamos a ser capazes de explicar muitas coisas que antes nos pareciam fantásticas. Porém, deve-se notar que o que consideramos milagres não se reduz a qualquer evento inexplicável ou a qualquer coisa aparentemente fantástica, mas somente aquilo que não puder ser produzido pelas leis da natureza, pelo homem ou pelos anjos:

>Os milagres que fará o Anticristo, serão todos falsos, tanto porque sua intenção será a de extraviar, como porque uma parte deles não serão senão ilusões e vãs aparências mágicas, e a outra parte não serão milagres por sua natureza, senão na opinião dos homens, isto é, não excederão as forças da natureza, senão que por serem coisas extraordinárias, parecerão milagres aos mais simples. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Quanto aos milagres verdadeiros, podemos dizer que existem dois tipos:

>Entre os verdadeiros milagres, há os que dão uma certeza clara e razão de que o braço de Deus opera neles, o que não acontece com outros, sem a consideração e concurso das circunstâncias. Isto é evidente pelo que se disse, e por exemplo, as maravilhas que fizeram os magos do Egito, eram, quanto a aparência exterior, semelhantes em tudo ao que fazia Moisés; mas quem considere as circunstâncias conhecerá facilmente que um era verdadeiro enquanto os outros eram falsos. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Assim, alguns milagres são evidentes; outros só podem ser percebidos claramente dentro do contexto que os originaram. No primeiro podemos tomar como exemplo a ressurreição de um morto; no segundo, podemos tomar como exemplo quando Jesus revela à Samaritana que sabe que o homem que morava com ela não era seu marido ou quando converte a água em vinho.

>Podia ter-se pensado que neles havia ilusão ou magia; mas estas maravilhas saíam das mesmas mãos que fazia os cegos verem, os mudos falarem, os surdos ouvirem, os mortos ressuscitarem, e sobre isto não cabia nenhuma dúvida. Pois voltar da privação ao uso, do não ser ao ser, e dar os órgãos vitais aos homens, são coisas impossíveis a todas as forças humanas, são atos do Soberano Senhor. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Assim, é claro que o milagre é uma comprovação sobrenatural de que aquilo que é ensinado tem Deus por autor:

>Era razoável que sendo a fé de coisas que excedem a natureza, fosse certificada por obras que excedem a natureza e que mostram que a pregação ou palavra anunciada sai da boca e autoridade do Senhor da natureza, e cujo poder, por não estar limitado por nada, se faz por meio do milagre como testemunho da verdade, e rubrica e coloca seu selo na palavra dita pelo pregador. <cite>São Francisco de Sales, Controvérsias, II, Cap. VII, Art. I</cite>

Portanto, os milagres são necessários, pois sem eles muitas pessoas mais simples não perceberiam a inspiração divina do Magistério da Igreja, sua perfeita coerência e que tem realmente Deus por seu autor.

Entre os acontecimentos milagrosos mais evidentes e comprovados podemos encontrar: a **perna reimplantada de Calandra**, o **manto de guadalupe** e o **Santo Sudário**, embora existam muitos outros.