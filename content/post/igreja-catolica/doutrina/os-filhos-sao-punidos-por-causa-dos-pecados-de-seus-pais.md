+++
date = "2015-03-31T18:00:00-18:00"
title = "Os filhos são punidos por causa dos pecados de seus pais?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/os-filhos-sao-punidos-por-causa-dos-pecados-de-seus-pais.jpg"
+++

Nas Sagradas Escrituras existem passagens que parecem indicar que Deus é injusto, porque castiga os filhos por causa dos pecados de seus pais. Alguns entendem isso como uma confirmação da reencarnação.Porém o que será que realmente acontece e que as Escrituras estão tentando transmitir?

<!--more-->

Quanto a isto, deve-se notar que três são as passagens onde os filhos são castigados por causa dos pais, porém, como todas tem um conteúdo muito semelhante citaremos apenas uma:

>Iahweh é lento para a cólera e cheio de amor, tolera a falta e a transgressão, mas não deixa ninguém impune, ele que castiga a falta dos pais nos filhos até à terceira e quarta geração. <cite>Livro dos Números 14, 18 (Dt 5,9; Ex 20,5)</cite>

Portanto, as Escrituras parecem aludir a um Deus que aplica penas aleatoriamente, tanto aos que merecem como aos que não merecem, como se nisso encontrasse algum prazer sádico.

Porém, isto provém de um entendimento errôneo sobre o motivo pelo qual se aplicam penas e qual seja sua finalidade.

Conforme percebeu o Doutor Angélico, existem duas maneiras de considerar a pena.

Num sentido quanto a pena em si mesmo, isto é, a restauração da justiça. Neste sentido se aplica um mal a quem obteve um bem de maneira indevida, como quando se faz alguém sofrer contra a vontade por ter cedido demais à sua vontade a ponto de buscar o mal. Deste primeiro modo, ninguém pode ser punido senão pela própria culpa.

Num outro sentido a pena pode ser utilizada como remédio que não somente visa reparar o pecado cometido, mas principalmente preservar-nos deles no futuro. Deste segundo modo, isto é, a pena como um remédio que visa preservar a saúde, alguém pode ser punido sem culpa própria.

Deve-se notar que o remédio nunca priva de um bem maior para promover um menor. Com efeito, o gosto ruim de um remédio amargo priva o paciente do sabor agradável para promover um bem superior que é a saúde ou uma cirurgia que causa ferimentos no corpo para que se promova um bem maior que é a saúde de determinado órgão.

Ademais, a pena é aplicada como um remédio quando os pais castigam os filhos de forma moderada, visando que se tornem pessoas honestas e se afastem do mal.

Ora os bens espirituais são bens máximos enquanto os bens materiais são mínimos. Portanto, é perfeitamente coerente que a privação de um bem material possa ser um remédio amargo para promover um bem espiritual, que é superior.

Por isso, é um ato de bondade ser punido sem culpa no que diz respeito aos bens temporais, como ocorre nas provações, se com isto tornamo-nos enriquecidos de bens espirituais e melhor dispostos a eles.

Porém, deve-se notar que este remédio nunca é aplicado em relação aos bens espirituais quando não temos culpa, pois não existem bens superiores a eles que justificassem qualquer pena como remédio.

Assim, somente somos punidos nos bens espirituais por culpa própria, enquanto nos materiais pode ser uma justa vingança ou um remédio.

Portanto, Deus castiga inocentes somente em questões materiais visando preservar os bens espirituais ou melhor dispôr a estes.

Com efeito, existem três razões pelas quais esta espécie de castigo é muito útil:

>Primeiro, quando, na ordem temporal, um homem pertence a outro. E por isso, sofre a pena devida a este; assim, o corpo do filho é de certo modo propriedade do pai, e os escravos, do senhor. <cite>Suma Teológica, IIa IIae, q. 108, a. 4</cite>

Neste sentodo, um filho ou servo inocente pode ser punido para coagir o coração do pai ou senhor a arrepender-se ou desistir do mal. Isto ocorre, por exemplo, quando a divina Providência permite que uma família passe fome por causa da preguiça que o pai tem de trabalhar ou o esbanjamento das economias em coisas fúteis, como jogos de azar.

>Segundo, quando o pecado de um contamina outro. Ou por imitação; assim, os filhos imitam os pecados dos pais e os escravos, os do senhor para pecarem com maior ousadia. Ou como mérito; assim, os pecados dos súditos fazem–lhes merecer um prelado pecador, segundo aquilo da Escritura: ele é o que faz reinar o homem hipócrita por causa dos pecados do povo. E por Davi ter pecado, pelo tato de fazer a resenha do seu povo, todo o povo de Israel foi punido. Ou por qualquer consentimento ou dissimulação; assim, às vezes os bons são punidos juntamente com os maus, por algum castigo temporal, por não terem reprovado os pecados destes, como diz Agostinho. <cite>Suma Teológica, IIa IIae, q. 108, a. 4</cite>

Neste outro sentido, os bons podem ser punidos por não terem se oposto ao mal quando deveriam. Assim, políticos corruptos se elegem por causa da omissão dos bons em impedir sua candidatura.

Outra situação é quando pessoas próximas a nós incorrem em algum imitando nosso mal exemplo. Deus poderia impedir isto, mas o permite na intenção de que o pecador perceba que seu pecado está afundando a vida das pessoas que ama.

>Terceiro, para pôr em evidência a unidade da sociedade humana, da qual um deve velar pelos outros para que não pequem; e para fazer detestar o pecado, enquanto que a pena de um redunda na de todos, como se todos constituíssem um só corpo, como diz Agostinho, do pecado de Acar. <cite>Suma Teológica, IIa IIae, q. 108, a. 4</cite>

Neste terceiro sentido, uma sociedade inteira pode pagar pelo pecado de alguns por causa de sua negligência em persuadir a pessoa a deixar determinado mal.

Com efeito, toda a decadência na sociedade humana nos afeta, seja em maior ou menor grau.

Assim, o pecado do irmão não é algo a que podemos ficar indiferentes como se fosse um problema somente dele, mas devemos tomá-lo como nosso e auxiliá-lo.

Por estas razões, comenta o Doutor Angélico sobre a controversa passagem:

>Quanto ao dito do Senhor: Que vinga a iniquidade dos pais nos filhos até a terceira e a quarta geração, ele antes visa a misericórdia que a severidade; pois, não tira vingança imediata, mas, ao contrário, espera pelos descendentes, para que esses ao menos se emendem. Mas, se a malícia destes aumenta, vê–se como obrigado a castigá–la com a sua vingança. <cite>Suma Teológica, IIa IIae, q. 108, a. 4</cite>

Ademais, acrescenta:

>O juízo divino pune as crianças inocentes, na ordem temporal, juntamente com os pais, quer porque, sendo partes destes, nelas também eles são punidos; quer, porque essa pena redundar em bem delas, pois, se não fossem punidas, viriam a imitar a malícia paterna e assim se tornariam merecedoras de penas mais graves. <cite>Suma Teológica, IIa IIae, q. 108, a. 4</cite>

Assim, vemos que a Sabedoria Divina, que conhece as profundezas dos corações dos homens sempre usa das penas como advertências e remédios para os homens visando que alcancem bens superiores e excelentes.