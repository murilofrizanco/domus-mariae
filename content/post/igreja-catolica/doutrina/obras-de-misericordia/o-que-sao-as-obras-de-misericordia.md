+++
date = "2015-09-23T18:00:00-17:00"
title = "O que são as obras de misericórdia?"
categories = ["Igreja Catolica","Doutrina","Obras de Misericordia"]
path = "Doutrina"
featured_image = "posts/images/o-que-sao-as-obras-de-misericordia.jpg"
+++

Muitas são as exortações para que os cristãos vivam autenticamente sua fé através de uma prática constante e persistente da misericórdia.

<!--more-->

Com efeito, a Igreja exorta os membros do Corpo Místico de Cristo a imitar o divino Mestre no amor ao próximo.

Este amor não pode ser meramente afetivo ao desejar o bem do próximo, mas deve converter-se em um amor efetivo que faz o bem.

É neste sentido que a Igreja convoca os fiéis a que sejam misericordiosos, pois:

>Misericordioso é quem possui coração comiserado, por assim dizer, por contristar-se com a miséria de outrem, como se fora própria e esforçar-se por afastá-la como se esforçaria por afastar a sua própria. <cite>Suma Teológica, Ia, q. 21, art. 3</cite>

Contudo, deve-se notar que não se pode praticar retamente a misericórdia quem ainda permanece na miséria:

>Não praticam a misericórdia ordenadamente os que, esquecendo-se de a praticar para consigo mesmos, antes, fazem-se a si mesmo mal com as suas obras. <cite>Suma Teológica, Supl, q. 99, art. 5</cite>

Ora, é evidente que ninguém pode ajudar alguém se não for capaz de ajudar a si mesmo em primeiro lugar.

Assim, para que possamos levar a prática da misericórdia até seu limite, a Igreja propõe 14 obras que atendem as necessidades mais fundamentais do ser humano que o impedem de bem viver.

Deve-se notar que estas obras não são aleatórias, mas todas possuem uma finalidade comum, que é remover tudo aquilo que impede o homem de afastar-se do vício, praticar a virtude e alcançar a sabedoria.

Elas se dividem em dois grupos: corporais e espirituais.

Assim, entre as **obras de misericórdia corporais** temos que a primeira é "**dar de comer a quem tem fome**", pois sem o alimento o homem fica privado do crescimento.

A segunda é "**dar de beber a quem tem sede**", pois sem a bebida o homem não pode conservar sua saúde.

A terceira é "**vestir os nus**", pois sem a vestimenta o homem não pode proteger seu corpo dos efeitos exteriores.

A quarta é "**dar pousada aos peregrinos**", pois o homem não pode descansar adequadamente sem um abrigo.

A quinta é "**visitar os enfermos**", pois a debilidade do corpo impede o vigor da mente e a autossuficiência.

A sexta é "**visitar os presos**", pois pela prisão somos impedidos de nossa liberdade e de nossa honra.

A sétima é "**enterrar os mortos**", pois na morte nosso corpo fica indefeso e à mercê de violações.

Entre as **obras de misericórdia espirituais** temos que a primeira é "**dar bons conselhos**", pois a inexperiência nos impede de chegar aos nossos objetivos.

A segunda é "**ensinar os ignorantes**", pois pela falta de conhecimento somos impedidos de atingir a verdade.

A terceira é "**corrigir os que erram**", pois pela maldade somos impedidos de operar o bem.

A quarta é "**consolar os tristes**", pois pela tristeza somos impedidos da verdadeira alegria espiritual.

A quinta é "**perdoar as injúrias**", pois pela inimizade somos impedidos do amor ao próximo.

A sexta é "**suportar com paciência as fraquezas do próximo**", pois pela incompreensão somos levados a injutiça no julgar.

A sétima é "**rezar a Deus por vivos e defuntos**", pois pela ausência da graça somos impedidos da eterna bem-aventurança.

Ora, todas estas coisas impedem que o homem possa viver de modo que atinja sua plena realização espiritual.

Assim, todas estas obras são instrumentos para que o homem possa remover os impedimentos que impede o próximo de salvar-se, tornar-se virtuoso e alcancar a sabedoria.

Por isso, dizia o Cristo aos seus discípulos:

>Bem-aventurados os misericordiosos, porque alcançarão misericórdia. <cite>Bíblia de Jerusalém, Evangelho segundo São Mateus, 5, 7</cite>