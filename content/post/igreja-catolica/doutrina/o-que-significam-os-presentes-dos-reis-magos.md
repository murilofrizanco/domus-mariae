+++
date = "2015-01-12T18:00:00-17:00"
title = "O que significam os presentes dos reis magos?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/o-que-significam-os-presentes-dos-reis-magos.jpg"
+++

Todos os anos se recorda a passagem em que os reis magos vem adorar o Menino Jesus e lhe tributam três presentes: ouro, incenso e mirra. Mas o que significam estes presentes para que mereçam tanto destaque no relato da Epifania?

<!--more-->

Esta passagem é suscetível de um duplo significado.

Ora, quando se oferece um presente a alguém costuma-se fazê-lo por alguma condição específica da pessoa presenteada. Por exemplo, como quando entrega-se um aliança de ouro como um presente para uma noiva.

Assim, o presente dos magos manifesta alguma dignidade ou condição da pessoa que os recebe tornando possível conhecer melhor o presenteado.

Um dos mais ilustres estudiosos das Sagradas Escrituras percebeu este detalhe admirável:

>Oferecem-no ouro como a um grande rei, queima-se incenso em sua presença como diante de Deus e oferecem-no mirra como àquele que haveria de morrer pela salvação de todos. <cite>Santo Agostinho, sermões sobre a Epifania</cite>

Quanto ao primeiro, deve-se notar que na história universal da humanidade o ouro sempre foi tido como algo precioso e digno de um rei. De fato, todos os grandes reis sempre apresentaram ornatos e utensílios de ouro por causa da nobreza deste metal. 

Ademais, o ouro também é um presente digno de pessoas ricas, isto é, que possuem muitas riquezas.

Ora, ambas as comparações convêm a Cristo. Com efeito, o menino-Deus possui a dignidade de Rei do Universo e ao mesmo tempo é possuidor de todas as riquezas espirituais e materiais, pois o Universo lhe pertence.

Quanto ao segundo, deve-se notar que o incenso foi comumente utilizado no culto aos deuses.

Com efeito, todos os grandes povos que cultuavam seus ídolos queimavam-lhes incenso. Ademais, os próprios sacerdotes judaicos ofereciam-no ao Deus de Abraão, de Isaac e de Jacó, geração após geração.

Ademais, o incenso era um utensílio digno de um sacerdote, para que então fosse oferecido aos deuses.

Ora, ambas as comparações convêm à Cristo, pois ele é o sumo sacerdote, que oferece o sacrifício agradável a Deus, e também o próprio Deus que o recebe.

Quanto ao terceiro, deve-se notar que a mirra era utilizada para embalsamar os cadáveres e era um presente concedido a um morto.

Ademais, também era utilizado pelos profetas da Antiga Aliança para ungir os reis e sacerdotes. 

Ora, ambas as coisas convém à Cristo, pois simboliza a morte cruel que viria a sofrer na manifestação mais extraordinária de seu Amor e também o poder de consagrar todas as realidades temporais sob o poder dos reis e espirituais sob o poder dos sacerdotes.

Contudo, deve-se notar que o presente não manifesta apenas a dignidade de quem os recebe, mas também a disposição de quem os oferece.

Assim, os magos oferecem um belíssimo aos homens que buscam a Deus e são exemplos para nós que vagamos nas trevas desejando encontrar esta Luz.

Quanto a isto, um outro autor, aprofundando o tema, notou que os três presentes manifestam exatamente o que devemos oferecer a Deus:

>Eles também podem significar outra coisa, entendendo-se por ouro a sabedoria, segundo a frase de Salomão: "Um tesouro apetecível repousará na boca do sábio"; pelo incenso que se queima diante de Deus, a virtude da oração, conforme o versículo de Davi: "Se eleve minha oração como incenso em tua presença", e pela mirra a mortificação da carne. Oferecemos, pois, ouro a este novo Rei, se resplandecemos diante dele com a luz da sabedoria; o incenso, se por meio de nossas orações exalamos em sua presença um odor agradável; e mirra se com a abstinência mortificamos os apetites da sensualidade. <cite>São Gregório Magno, homilia sobre o Evangelho, 10</cite>

Que os três reis magos possam ser exemplo de uma oferta digna do Menino-Deus que deseja, muito mais que exterioridades, a boa disposição do homem aos bens espirituais. Glória a Deus nas alturas e paz na terra aos homens por Ele amados!