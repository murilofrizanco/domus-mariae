+++
date = "2015-08-14T18:00:00-17:00"
title = "A união sexual de anjos com mulheres gerou gigantes?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/a-uniao-sexual-de-anjos-com-mulheres-gerou-gigantes.jpg"
+++

No livro do Gênesis lemos o relato sobre os gigantes ou nefilins, que parece demonstrar que alguns homens foram gerados pela união de mulheres (filhas dos homens) com anjos (filhos de Deus).

<!--more-->

Com efeito, lê-se no Gênesis:

>Quando os homens começaram a ser numerosos sobre a face da terra, e lhes nasceram filhas, os filhos de Deus viram que as filhas dos homens eram belas e tomaram como mulheres todas as que lhes agradaram. Iahweh disse: "Meu espírito não se responsabilizará indefinidamente pelo homem, pois ele é carne; não viverá mais que cento e vinte anos". Ora, naquele tempo (e também depois), quando os filhos de Deus se uniam às filhas dos homens e estas lhes davam filhos, os Nefilim habitavam sobre a terra; estes homens famosos foram os heróis dos tempos antigos. <cite>Bíblia de Jerusalém, Livro do Gênesis, 6, 1-5</cite>

Ora, as Sagradas Escrituras, em algumas ocasiões, tratam os anjos como "filhos de Deus", conforme consta no Livro de Jó:

>No dia em que os Filhos de Deus vieram se apresentar a Iahweh, entre eles veio também Satanás. <cite>Bíblia de Jerusalém, Livro de Jó, 1, 6</cite>

Além disto, as Escrituras testemunham a aparição corporal de diversos anjos aos patriarcas de modo que parece que seria possível que os anjos assumissem um corpo para gerar um filho com uma mulher. Assim, parece foram gerados filhos da união de mulheres com anjos, o que parece algo estranho, pois os anjos são criaturas puramente espirituais.

Para entender esta passagem tão misteriosa é necessário primeiramente estabelecer se os anjos podem ou não unir-se sexualmente a uma mulher para gerar filhos.

É evidente que não poderiam, pois os anjos não poderiam emitir seu sêmen na mulher, pois eles não tem corpo. Porém, visto que podem mover localmente os corpos, poderiam por alguma permissão divina mover o sêmen de um homem distante até ela e assim fecundá-la. Isto não é absurdo, pois as tecnologias modernas de reprodução artificial fazem exatamente isto. Assim, se os homens podem fazer isto, muito mais o poderiam os anjos que são mais poderosos.

Porém, deve-se notar que se um anjo fizesse isso não seria pai daquele ser gerado, pois o pai é aquele que emitiu o sêmen, assim como na reprodução artificial não é o médico que introduz o sêmen que é pai da criança gerada, mas sim aquele que emitiu o sêmen, conforme demonstra Santo Tomás, numa análise que poderia ser considerada ridícula antes da ciência descobrir a fecundação artificial:

>Se, porém, por vezes, alguns nasceram de coito com os demônios, tal não se operou pelo sêmen emitido por eles ou pelos corpos assumidos, mas pelo sêmen de algum homem, obtido para tal fim; e isto por ter o mesmo demônio, súcubo em relação ao homem, se tornado incubo em relação à mulher, assim como assumem as sementes de outros seres para a geração de alguns deles, como diz Agostinho. De modo que o nascido de tal operação não é filho do demônio, mas do homem de quem foi obtido o sêmen. <cite>Santo Tomás de Aquino, Suma Teológica, Ia, q. 51, a. 3</cite>

Deste modo, é impossível que aqui haja uma referência a uma "fecundação artificial" feita por anjos em mulheres, pois os filhos gerados desta união são ditos realmente "filhos" dos "filhos de Deus", coisa que seria falsa dizer daquele que realizou a fecundação artificial.

Portanto, a interpretação que entende que homens foram gerados pela união de anjos com mulheres é absurda como adverte o Abade Sereno:

>De modo algum deve-se crer que as naturezas espirituais tenham podido ter comércio com criaturas humanas. Se isso, em rigor, pôde dar-se alguma vez, porque não poderia repetir-se também hoje? Não esqueçamos que os demônios se comprazem nas paixões vergonhosas. Sem dúvida prefeririam fazer eles mesmos o mal, se fosse possível, do que incitar aos homens e fazê-lo por meio deles. <cite>João Cassiano, Conferências, VIII</cite>

Com efeito, os filhos de Set se mantiveram por gerações separados dos filhos de Caim conforme consta na Escritura que separa suas linhagens. Quanto a isto explica o Abade Sereno:

>Enquanto durou a separação das raças, os filhos de Set, dignos do nobre tronco de que procediam, mereceram por sua santidade o nome de anjos de Deus, ou como consta em alguns exemplares, de filhos de Deus. Os filhos de Caim, inversamente, são chamados filhos dos homens, em razão de sua impiedade e da de seus pais, não menos do que por suas obras terrenas. Porém, houve um imprevisto. Um dia terminou esta feliz separação em que até então haviam vivido. Eis que os filhos de Set, que eram também os filhos de Deus, viram as filhas de Caim. Atraídos por sua formosura, as tomaram por esposas. Estas transmitiram a seus filhos os vícios de seus pais e lhes fizeram cair daquela santidade ingênita de sua raça e da simplicidade de seus antepassados. (...) Da união dos filhos de Set com as filhas de Caim nasceram filhos piores do que seus pais. Foram robustos caçadores, homens violentos e foragidos. Pela corpulência de seus corpos, não menos do que a enormidade de sua malícia, foram chamados de gigantes [nefilim]. Foram estes os primeiros que começaram a roubar os vizinhos e exerceram a pilhagem, preferindo viver do fruto de suas violências do que do suor e trabalho de suas mãos. <cite>João Cassiano, Conferências, VIII</cite>

Deste modo, fica claro que a passagem trata da união da linhagem de Set com a linhagem de Caim e que os gigantes eram homens de maldade enorme e não criaturas mitológicas que procedem da união de anjos com homens.