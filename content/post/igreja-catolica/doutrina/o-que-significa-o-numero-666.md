+++
date = "2015-05-22T18:00:00-17:00"
title = "O que significa o número 666?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/o-que-significa-o-numero-666.jpg"
+++

Existe muita polêmica à respeito do famoso número da besta, o número seiscentos e sessenta e seis. Porém, qual seria o significado deste número e desta passagem tão obscura do Apocalipse?

<!--more-->

Deve-se notar que, se as Sagradas Escrituras são inspiradas por Deus e Ele nunca faz nada supérfluo, então até mesmo o entendimento do porquê tal número foi utilizado em algum versículo é digno de nota, conforme nota um grande doutor das Escrituras:

>Não se deve desprezar os números. Pois em muitas passagens da Sagrada Escritura se manifesta o grande mistério que encerram. Não foi em vão que se escreveu o louvor de Deus no livro da Sabedoria (11,20): 'Dispusestes tudo com medida, número e peso'. <cite>Santo Isidoro de Sevilha, Etimologias, III, 4</cite>

Ora, a polêmica passagem do Apocalipse é a seguinte:

>E fará com que todos, pequenos e grandes, ricos e pobres, livres e servos, tenham uma marca na sua mão direita ou nas suas frontes, e que ninguém possa comprar ou vender senão quem possui a marca, ou o nome da besta, ou o número do seu nome. Aqui está a sabedoria. Quem possui inteligência, calcule o número da besta, pois é número de homem, e seu número é seiscentos e sessenta e seis. <cite>Apocalipse de São João, 13, 16-18</cite>

Uma explicação mais literal é sintetizada por um grande teólogo da Escola de São Vitor que percebeu haver muitas possibilidades de interpretação deste número:

>Para conhecer mais claramente a virtude deste número, devemos discuti-lo segundo a tradição dos santos doutores; dizem eles que o Apocalipse, tendo sido escrito em grego, este número deve ser buscado segundo os gregos, junto aos quais todas as letras significam um número. Ora, o nome do Anticristo em grego é 'Antemos', que significa 'Contrário'. Neste nome 'A' significa um, 'n' cinqüenta, 't' trezentos, 'e' cinco, 'm' quarenta, 'o' setenta, 's' duzentos. A soma de todos constitui seiscentos e sessenta e seis. Porém há outros nomes do Anticristo cuja interpretação resulta no mesmo número. <cite>Ricardo de São Vitor, Comentário ao Apocalipse</cite>

Deve-se notar que, devido a multiplicidade de nomes que cocincidem com este número, a disputa pela interpretação mais adequada não é a única informação que este número nos apresenta. Embora, seja digno de nota que a interpretação dos Santos Padres é superior ao que vemos hoje em dia porque não tenta restringir o Anticristo a um fato ou pessoa, mas sim determinar a natureza deste.

Ademais, este número nos fornece uma realidade espiritual. Com efeito, o número seis possui um sentido alegórico nas Sagradas Escrituras e seu correto entendimento nos fazem entender que o 666 é uma descrição de como será o Anticristo.

Sobre o significa alegórico do número seis nas Escrituras encontramos o seguinte comentário:

>O número seis significa os seis dias nos quais Deus criou as criaturas, como diz o Êxodo (20, 11): "Em seis dias criou Deus o céu e a terra". Significa também as etapas do tempo deste mundo, que comporta seis eras. Daí que Deus, que perfaz todas as suas obras, tenha vindo a este mundo na sexta era, tenha padecido na sexta-feira, no sábado tenha repousado no sepulcro, e no domingo ressuscitado dos mortos. <cite>Rabano Mauro, O Significado Místico dos Números</cite>

Assim, o número seis coincide com a perfeição de obra da Criação que resulta da união e ordem de suas partes partes. Com efeito, Ricardo de São Vitor propõe um sentido semelhante:

>Seis significa a perfeição; pelo senário [6] pode-se entender a mínima perfeição, pelo sexagenário [60] pode-se entender a média perfeição e pelo seiscentenário [600] a máxima e consumada perfeição. <cite>Ricardo de São Vitor, Comentário ao Apocalipse</cite>

Porém, deve-se notar que o número seis não representa a plenitude, isto é, a totalidade das coisas, pois o universo ou a obra da Criação não inclui nela a Santíssima Trindade.

Portanto, o seis pode representar a perfeição da natureza, mas não a sua ordenação para a Santíssima Trindade.

Ademais, vemos que Deus fez as criaturas em seis dias, mas no sétimo repousou.

Com efeito, o repouso deve ser entendido não como um dia ocioso de Deus, mas sim como a contemplação que existe na vida íntima da Santíssima Trindade, como lemos, por exemplo, no Aquinate no que se refere ao preceito do repouso do sábado (sétimo dia):

>Na santificação do sábado, enquanto preceito moral, se preceitua o descanso do coração em Deus. <cite>Santo Tomás de Aquino, Suma Teológica, Ia IIae, q. 100, a. 5</cite>

Ora, descansar o coração em Deus ocorre pela vida espiritual onde o homem se une ao Cristo e de forma mais perfeita na visão beatífica, isto é, a felicidade eterna na presença de Deus.

Tendo isto em mente fica claro que o número seis é conveniente para designar o Anticristo pela exclusão do repouso e pela perfeição de sua maldade:

>O Anticristo é merecidamente descrito por meio de tal número, porque é encontrado perfeitamente maligno em todo grau do mal e perfeitamente contrário a todo grau do bem e, assim, saberá fazer iniciar os males pequenos, progredir os medianos para não deixar de ter em suas mãos os máximos e, inversamente, saberá quebrar os bens insipientes e impedirá os que crescem para que possa diminuir, tanto quanto lhe seja possível, os sumos e perfeitos. Quem é perfeitamente mau nos males pequenos, médios e sumos, e é perfeitamente contrário à perfeição dos bens opostos, tal número lhe convém adequadamente. <cite>Ricardo de São Vitor, Comentário ao Apocalipse</cite>

Com efeito, o Anticristo será aquele que terá a perfeição na maldade e a perfeição na oposição a todos os bens, desde os menores até os maiores.

Ora, o bem máximo é a vida eterna, ou seja, o "repouso do sétimo dia". Portanto, o Anticristo será aquele que, por causa da perfeição de sua maldade e oposição ao bem, irá fazer o possível para impedir e destruir a vida espiritual no homem.

Assim, a simbologia do Apocalipse nos ensina que o Anticristo será um inimigo que terá por objetivo trabalhar no sentido de destruir a vida espiritual,impossibilitar o repouso do coração humano em Deus, rebaixar o homem aos bens passageiros e promover toda e qualquer espécie de maldade.

Não obstante, deve-se notar que nada impede de que os fatos literais ali narrados não possam coincidir com certos eventos históricos e até mesmo atuais em que notamos a marca da besta, isto é, a tendência de colocar esta vida como o centro de tudo e a rejeição da vida eterna.

Que o Espírito da Verdade e da Sabedoria possa descer em nossas almas e que pela graça da fé possamos ser iluminados para enxergar os grandiosos mistérios que estão contidos nos Livros Sagrados.