+++
date = "2014-08-26T18:00:00-17:00"
title = "A narrativa da Criação deve ser entendida ao pé da letra?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/a-narrativa-do-genesis-deve-ser-entendida-ao-pe-da-letra.jpg"
+++

Muitos alegam que a Bíblia é uma farsa por contar uma história fantasiosa no livro do Gênesis que se choca com diversas teorias científicas. Diante disso, seria absurdo crer em Deus, nas Escrituras ou no cristianismo por ser irracional ou contrário à razão natural.

<!--more-->

Porém, deve-se notar que a crítica procede de um total desconhecimento da teologia católica. A Igreja sempre entendeu que a Criação e a Revelação procedem de um mesmo autor e, portanto, não podem apresentar contradições. Por isso, dizia o Doutor Angélico:

>Como a divina Escritura pode ser exposta em muitos sentidos, ninguém deve aderir a uma exposição de maneira tão precisa, a ponto de ser a Escritura objeto de irrisão para os infiéis, aos quais se fecharia, então, a via para a crença, se se estabelecesse, por uma razão certa, como falso aquilo que se presumia ser o sentido escritural. <cite>Santo Tomás de Aquino, Suma Teológica, Ia, q. 68, a. 1</cite>

Deste modo, quando uma interpretação contradiz a filosofia ou a ciência verdadeira e certa, deve-se abandoná-la e reconhecê-la como falsa. Insistir numa falsidade seria causa de desprezo pelas Sagradas Escrituras.

Portanto, o critério de interpretação das Escrituras é a realidade presente em Deus e na Criação. Assim, a Escritura não contém erros, mas os intérpretes podem ser incapazes de compreender o que realmente se pretendia dizer.

Com efeito, o Gênesis trata de verdades de fé, não científicas, embora possa ser encontradas muitas delas.

Um exemplo disto é a narração sobre a serpente no paraíso. Evidentemente que uma serpente não fala e que o demônio não é um animal. Ademais, o pecado original não necessariamente consiste em comer um fruto no paraíso, mas pode aludir a muitas coisas.

Portanto, o sentido literal, que é o sentido que expressa o que realmente o autor pretendia dizer, não comporta uma visão fantasiosa e mitológica descartável.

Com efeito, desde os primórdios do cristianismo podemos ver que o Gênesis sempre foi tomado de forma figurativa em muitos aspectos. Vejamos por exemplo um cristão do século II:

Pois quem tem entendimento vai supor que o primeiro e o segundo e terceiro dia existiram sem o sol e a lua e as estrelas, e que o primeiro dia foi, por assim dizer, também sem um céu?... Eu não suponho que alguém duvide que essas coisas indicam figurativamente certos mistérios, sendo que a história ocorreu em aparência e não literalmente. <cite>Orígenes, As Doutrinas Fundamentais 4:1:16, 225 d.C.</cite>

Ademais, encontramos o mesmo testemunho na pena de um dos maiores estudiosos das Sagradas Escrituras:

>Não raro acontece que algo sobre a terra, sobre o céu, sobre outros elementos deste mundo (...) possa ser conhecido com a maior certeza pelo raciocínio ou pela experiência, até mesmo por quem não é cristão. É muito vergonhoso e desastroso, porém, e muito digno de ser evitado, que ele [o pagão] deva ouvir um cristão falar tão estupidamente sobre estas questões, e como se [estivesse] de acordo com escritos cristãos (...) <cite>Santo Agostinho de Hipona, A Interpretação Literal do Gênesis 1:19-20</cite>

Por esta razão, a Igreja em seu Catecismo exalta o valor das ciências e suas descobertas que muitas vezes auxiliam na admiração sempre mais profunda Criador:

>Muitos estudos científicos... enriqueceram esplendidamente o nosso conhecimento sobre a idade e as dimensões do cosmo, o desenvolvimento de formas de vida, e o aparecimento do homem. Estes estudos nos convidam a uma maior admiração pela grandeza do Criador. <cite>Catecismo da Igreja Católica, §283</cite>

Assim, o modo correto de prescrutar a páginas do livro do Gênesis é com aquele espírito que nos forneceo doutor de Hipona:

>Com as escrituras, é uma questão de tratar sobre a fé. Por essa razão, como já observei várias vezes, se alguém, sem entender o modo de eloquência divina, encontrar alguma coisa sobre estas questões [científicas] em nossos livros, ou ouvir o mesmo desses livros, de tal natureza que pareça estar em desacordo com as percepções de suas próprias faculdades racionais, que ele acredite que essas outras coisas não são de modo algum necessárias às admoestações ou contas ou previsões das escrituras. Em suma, deve-se dizer que os nossos autores sabiam a verdade sobre a natureza dos céus, mas não foi a intenção do Espírito de Deus, que falou através deles, ensinar aos homens tudo o que não seria de utilidade para sua salvação. <cite>Santo Agostinho de Hipona, [408 d.C.], A Interpretação Literal do Gênesis 2:9</cite>

Tendo isto em vista podemos investigar devidamente os Escritos Sagradas para adorar e bendizer ao Senhor Deus do Universo num caminho de admiração, contemplação e sabedoria.