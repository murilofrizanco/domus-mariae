+++
date = "2014-09-09T18:00:00-17:00"
title = "Porque setembro é o mês da Bíblia?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/porque-setembro-e-o-mes-da-biblia.jpg"
+++

Em setembro comemora-se o mês da Bíblia. Porém, isto não se encontra nem no calendário litúrgico, nem em documentos oficiais da Igreja. De onde surgiu este costume?

<!--more-->

Deve-se notar que o mês da Bíblia é uma iniciativa exclusiva das paróquias brasileiras.

Trata-se, portanto, de um costume nascido em nossa nação e propagado com o apoio da CNBB.

A origem histórica deste costume remete a algumas décadas atrás:

>O Mês da Bíblia surgiu em 1971, por ocasião do cinquentenário da Arquidiocese de Belo Horizonte, Minas Gerais. Foi levado adiante com a colaboração efetiva do Serviço de Animação Bíblica – Paulinas (SAB), até posteriormente ser assumido pela Conferência dos Bispos do Brasil (CNBB) e estender-se ao âmbito nacional. <cite>[Site Paulinas](http://www.paulinas.org.br/sab/?system=paginas&action=read&id=1946)</cite>

Quanto a data, não parece haver uma correspondência proposital entre a iniciativa e o calendário litúrgico. Porém, oportunamente no dia 30 de Setembro comemora-se a memória litúrgica de São Jerônimo, santo e doutor da Igreja.

Uma das contribuições de Jerônimo para a Igreja foi traduzir as Escrituras do Grego e Hebraico para o Latim, resultando na famosa Vulgata Latina, tradução utilizada nos textos litúrgicos até a promulgação da Nova Vulgata promulgada pelo Papa João Paulo II, a qual contém apenas pequenas correções.

Assim, a iniciativa é muito boa e tem a intenção de aproximar os católicos do estudo e valorização das Sagradas Escrituras, uma vez que culturalmente muitos de nós temos as Escrituras como um "enfeite" que fica com sua riqueza guardada no fundo de um baú.

Portanto, para os que não tem contato assíduo com as Escrituras é oportuno aproveitar este momento para aprofundar a vida espiritual através das Escrituras.

Deixamos algumas recomendações que podem ser muito úteis para aprofundar a espiritualidade e fomentar nossa veneração pelos textos sagrados:

1. Evitar o termo "Bíblia" e passar a chamar de "Sagradas Escrituras". Com efeito, uma coisa é quando alguém fala "conjunto de livros" e outras quando diz "livros sagrados". É um pequeno exercício que pode nos ajudar a tomar consciência da riqueza que temos em mãos.
2. Procurar adquirir uma Bíblia em tradução católica caso tenhamos apenas livretos ou uma tradução protestante que carece de sete livros.
3. Ler as Sagradas Escrituras com o acompanhamento dos comentários dos santos à respeito dos textos. Um excelente comentário é a ["Cantena Aurea de Santo Tomás de Aquino"](http://hjg.com.ar/catena/c0.html) que compila os comentários dos Santos Padres para cada versículo dos Evangelhos.
4. Rezar o Ofício Divino, ou seja, a [Liturgia das Horas](http://www.liturgiadashoras.org/), as quais é um excelente guia para orarmos com as Sagradas Escrituras.
5. Promover nas paróquias a oração das Vésperas, das Laudes e do Ofício das Leituras.
6. Ler um livro completo da Bíblia, seja um Evangelho, seja uma carta paulina, seja um dos livros sapienciais.

Que neste mês possa conduzir a um contato mais íntimo com aquela belíssima e sapientíssima carta de amor escrito por Deus para os homens, sempre mantendo em vista aquela grande máxima que nos impede de cairmos em uma série de erros: 

>A Sagrada Escritura está escrita no coração da Igreja, mais do que em instrumentos materiais. <cite>Santo Hilário de Poitiers</cite>