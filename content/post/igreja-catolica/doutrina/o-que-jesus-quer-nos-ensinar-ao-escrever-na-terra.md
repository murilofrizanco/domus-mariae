+++
date = "2016-03-14T18:00:00-17:00"
title = "O que Jesus quer nos ensinar ao escrever na terra?"
categories = ["Igreja Catolica","Doutrina"]
path = "Doutrina"
featured_image = "posts/images/o-que-jesus-quer-nos-ensinar-ao-escrever-na-terra.jpg"
+++

É famosa a passagem da mulher adúltera na qual Jesus pede que atire a primeira pedra quem não tiver qualquer pecado. Porém, consta na passagem que, em resposta à acusação dos fariseus, o Cristo se abaixa e começa a escrever na terra.

<!--more-->

Com efeito, lemos no início da capítulo 8 do Evangelho segundo São João:

>Os escribas e fariseus lhe levaram uma mulher surpreendida em adultério, a colocaram no mei e disseram: "Mestre, esta mulher foi surpreendida em flagrante adultério. Moisés manda na lei apedrejar estas mulheres. O que tú dizes?". Isto o diziam para tentar-lhe, para ter do que acusá-lo. Porém, Jesus, inclinando-se, se colocou a escrever com o dedo na terra. Mas, como insistiam em perguntar-lhe, se levantou e lhes disse: "Aquele entre vós que não tenha pecado, que atire a primeira pedra". E inclinando-se de novo, escrevia na terra. <cite>Bíblia de Jerusalém, Evangelho segundo São João, 8, 3-8</cite>

A primeira coisa que podemos nos perguntar é o que será que Cristo escreveu na terra. O Evangelho não apresenta esta informação, mas homens de profunda sabedoria consideram duas possibilidades:

>Alguns dizem que ele escreveu as palavras de Jeremias: "Terra! Terra! Terra! escutai (...) inscrevei este homem: um sem filhos, um fracasso na vida". Outros dizem, e esta opinião parece mais conveniente, que Jesus escreveu as próprias palavras que disse: "Aquele entre vós que não tenha pecado, que atire a primeira pedra". <cite>Santo Tomás de Aquino, Comentário ao Evangelho de São João, Capítulo 8, Leitura 1</cite>

As duas opiniões são aceitáveis e não temos como ter certeza do que foi escrito. Porém, podem nos auxiliar em frutuosas meditações.

A segunda coisa que devemos considerar é por qual motivo ele escreveu na terra ao invés de responder diretamente em palavras. Quanto a isto, há pelo menos três razões:

>Primeiro, conforme Agostinho, para demonstrar que aqueles que o estavam provocando seriam inscritos na terra: "Os que se apartam de tu, serão inscritos na terra" (Jr 17, 13). Porém, aqueles que são justos e discípulos que o seguem serão inscritos no céu: "Alegrai-vos, porque vossos nomes estão inscritos no céu" (Lc 10, 20).

>Segundo, ele escreveu na terra para demonstrar que ele operaria sinais na terra, pois quem escreve faz um sinal. Assim, escrever na terra é operar um sinal. Por isso, ele diz que Jesus inclinou-se, pelo mistério da Encarnação, por meio do qual ele opera milagres na carne que ele assumiu.

>Terceiro, ele escreveu na terra porque a Lei Antiga foi escrita em tábuas de pedra (Ex 31; 2Co 3), que significa sua dureza: "Se alguém viola a Lei de Moisés é condenado sem misericórdia" (Hb 10, 28). Porém, a terra é macia. Assim, Jesus escreveu na terra para demonstrar a dolura e suavidade da Lei Nova que ele nos deu. <cite>Santo Tomás de Aquino, Comentário ao Evangelho de São João, Capítulo 8, Leitura 1</cite>

Portanto, ao escrevar na terra Jesus estava manifestando a condenação de seus adversário e exaltação dos eleitos; demonstrando que é por sua divina humanidade é que ele opera e realiza a salvação; e, por fim, ensinando que a Lei Nova se fundamenta num coração que ama o Cristo e não na dureza que só obedece por medo.

Também Jesus nos ensina como é que Deus é justo e misericordioso ao mesmo tempo:

>Primeiro, deve haver beniginidade na condescendência com aqueles que serão punidos; e assim ele diz que Jesus se inclinou: "Terá um juízo sem misericórdia o que não teve misericórdia" (Tg 2, 13); "Quando alguém incorrer em alguma falta, vós, os espirituais, corrigi com espírito de mansidão" (Gl 6, 1). Segundo, deveria haver discrição no discernimento. E assim ele diz que Jesus escreveu com seus dedos, cuja flexibilidade significa o discernimento: "Apareceram os dedos de uma mão humana que se puseram a escrever" (Dn 5, 5). Terceiro, deveria haver certeza sobre a sentença dada. Assim, ele diz que Jesus escreveu. <cite>Santo Tomás de Aquino, Comentário ao Evangelho de São João, Capítulo 8, Leitura 1</cite>

Uma terceira consideração que deve ser feita é por qual motivo Jesus voltou a escrever depois de ter respondido aos fariseus, o qual Santo Tomás observa que:

>Ele fez isto primeiro para mostrar a firmeza de sua sentença: "Deus não é um homem para mentir, nem filho de homem para voltar atrás" (Nm 23, 19). Segundo, ele o fez para demonstrar que ele não eram dignos de olhá-lo. Por eles se perturbarem com seu zelo pela justiça, ele não pensou que era conveniente que continuassem olhando para ele, então afastou-se de sua visão. Terceiro, ele o fez em consideração ao seu constrangimento, para dar-lhes completa liberdade de se retirarem. <cite>Santo Tomás de Aquino, Comentário ao Evangelho de São João, Capítulo 8, Leitura 1</cite>

Que observando este gesto possamos nos converter mais intensamente para que não nos supreendamos com Cristo nos dando às costas e inscrevendo nossos nomes na terra. 