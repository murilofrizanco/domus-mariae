+++
date = "2014-06-26T06:36:10-03:00"
title = "É permitido bater palmas na missa?"
categories = ["Igreja Catolica","Santa Missa"]
path = "Santa Missa"
featured_image = "posts/images/e-permitido-bater-palmas-na-missa.jpg"
+++

A questão das palmas na missa é um assunto que divide opiniões. Ora, muitas opiniões existem onde faltam certezas. Assim, é preciso entender melhor em que consiste a Santa Missa para que se possa proceder a uma conclusão sobre o assunto.

<!--more-->

Para tanto, convém apresentar o que ocorre na Missa segundo o documento do Concílio Vaticano II que trata sobre a Liturgia:

>O nosso Salvador instituiu na última Ceia, na noite em que foi entregue, o Sacrifício eucarístico do seu Corpo e do seu Sangue para perpetuar pelo decorrer dos séculos, até Ele voltar, o Sacrifício da cruz, confiando à Igreja... <cite>Sacrosanctum Concilium, 47</cite>

Deste modo, é manifesto que a Igreja reconhece, desde a época dos Apóstolos, a Missa como o sacrifício da cruz ocorrido no próprio Calvário e não um evento social, como nos ensina o Papa Francisco:

>É triste, mas a missa muitas vezes se transforma num evento social e não estamos próximos da memória da Igreja, que é a presença do Senhor diante de nós. <cite>Homilia Matinal em Santa Marta, 03/10/2013</cite>

Ademais, os santos nos oferecem as mesmas considerações. Com efeito, quando perguntaram ao São Padre Pio o modo de assistir à Missa obteve-se a seguinte resposta:

>Como assistiram a Santíssima Virgem e as piedosas mulheres. Como assistiu São João Evangelista ao Sacrifício Eucarístico e ao Sacrifício cruento da Cruz. <cite>São Pio de Pietrelcina</cite>

Portanto, a Igreja entende unanimemente que a Missa é o próprio sacrifício da Cruz e com este espírito deve ser acompanhada.

Se as coisas são assim, então as palmas são uma espécie de gesto completamente inconveniente para a Missa. Com efeito, São João XXIII testemunha que a sacralidade da Missa não admite as palmas:

>Estou muito satisfeito por estar aqui. Mas eu devo exprimir-lhes um desejo: que na igreja não gritem e não batam palmas, nem mesmo para saudar o Papa, porque Templum Dei Templum Dei (o templo de Deus é o templo de Deus). <cite>São João XXIII, IV Domingo da Quaresma, 1963</cite>

O mesmo ensinamento, com maior detalhe, foi apresentado por Dom Mauro em uma entrevista ao blog "Salvem a Liturgia":

>Nas normas litúrgicas atuais não são previstas as palmas, seja o aplauso de aprovação ou rítmico. É possível admitir em momentos de oração extra-litúrgicos, se verdadeiramente é necessário, porem não na liturgia.<cite>Dom Mauro Gagliardi, consultor da Sagrada Congregação para o Culto Divino e Disciplina dos Sacramentos</cite>

Com efeito, quem é que estaria batendo palmas diante de Jesus sendo erguido na cruz e sangrando? Uma pessoa assim certamente seria considerada blasfêma e impiedosa.

Assim, fica claro que as palmas não são adequadas para a Liturgia da Santa Missa e faz-se necessário ensinar os fiéis a respeito do tema para que seu comportamento adeque-se ao mistério que estão celebrando.