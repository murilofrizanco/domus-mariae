+++
date = "2014-08-13T06:36:10-03:00"
title = "Qual o significado do sinal da paz?"
categories = ["Igreja Catolica","Santa Missa"]
path = "Santa Missa"
featured_image = "posts/images/qual-o-significado-do-sinal-da-paz.jpg"
+++

Muitos se perguntam qual seria o significado deste pequeno sinal. Seria um momento de confraternização com os que estão próximos? Teria alguma utilidade?

<!--more-->

O sinal da paz ocorre logo após o a recitação ou canto do Cordeiro de Deus (Agnus Dei) e é precedido pela seguinte oração na forma extraordinária:

>Senhor Jesus Cristo, que dissestes aos vossos apóstolos: "Eu vos deixo a paz, eu vos dou a minha paz": não olheis os meus pecados, mas para a fé da vossa Igreja; dai-lhes, a paz e a unidade, segundo a vossa misericórdia. Vós que sendo Deus, viveis e reinais, em união com o Espírito Santo, por todos os séculos dos séculos. Amém. <cite>Rito Romano na Forma Extraordinária</cite>

A oração que precede o sinal da paz no rito ordinário é muito semelhante. Porém, pela maior clareza que apresenta o rito extraordinário optamos por citá-lo aqui.

Após esta oração, o sacerdote dá a paz aos ministros dizendo: "A Paz esteja convosco" sinalizando o dom da paz, um dos efeitos da Sagrada Comunhão.

Não é necessário que todos os fiéis realizem o sinal da paz para atingir seu significado, porém é permitido que os fiéis façam o sinal entre si quando o sacerdote considerar conveniente.

Deve-se notar que não existe nenhum "canto da paz" e deve ser feito em ambiente silencioso por motivos muito profundos.

A verdadeira paz é concedida aos Apóstolos quando o Senhor aparece glorioso entre os discípulos e lhes mostra as mãos e o lado para atestar-lhes a realidade da Ressurreição. Com efeito, as Sagradas Escrituras afirmam:

>Disse-lhes ele: A paz esteja convosco! Dito isso, mostrou-lhes as mãos e o lado. Os discípulos alegraram-se ao ver o Senhor. <cite>Evangelho de São João, 20, 19s</cite>

Assim, do contato direto com a humanidade ressucitada de Cristo nasce uma grande alegria, um prazer espiritual, e dela brota a verdadeira paz que o mundo não pode dar. Com efeito, a paz é a perfeição da alegria:

>A perfeição da alegria é a paz, de dois modos. Primeiro, quanto à tranquila libertação das perturbações exteriores; (...) E por isso o coração perfeitamente pacificado num gozo, por nada pode ser molestado, pois considera tudo o mais como quase não existente; (...) Segundo, quanto à satisfação do desejo volúvel, pois não gozamos suficientemente quando não nos satisfaz no objeto do nosso gozo. Ora, ambos esses casos implicam a paz, de modo que não sejamos perturbados pelas causas externas e descansemos os nossos desejos num só objeto.<cite>Santo Tomás de Aquino, Suma Teológica, Ia IIae, q. 70, a. 3</cite>

Portanto, a Liturgia é muito sábia ao simbolizar a realidade sublime e maravilhosa da paz que brota no coração daqueles que tocam pessoalmente a humanidade do Cristo glorioso e ressuscitado. Ora, a Eucaristia nada mais é que um sacramento que nos induz a um contato real com a humanidade ressucitada do Cristo. Portanto, nada mais conveniente do que sinalizar este efeito neste gesto litúrgico e induzir os fiéis a receber a Sagrada Comunhão neste espírito.

Com efeito, do contato íntimo com nosso Senhor surge um recolhimento profundo em que as coisas exteriores passam a ser desprezadas e o desejo aquieta-se somente no Amado favorecendo uma união intensa com Nosso Senhor. 

Assim, o dom da Paz é fruto da presença de Deus e não provém do mero esforço humano, sendo consequência da atuação do próprio Espírito Santo em nossas almas.

Quanto ao modo como pode ser dado o "ósculo da paz" entre os fiéis não há uma norma fixa. Porém, se a paz é fruto de um recolhimento espiritual, qualquer confusão ou atitude que nos derrame ao exterior é inadequado. Por isso, erram aqueles que tornam este momento barulhento, confuso ou passional ao confundir a paz com a confraternização.

Com efeito, os Santos Padres ensinam que este ósculo não pode ser como aqueles que são dados em praça pública sem recato, silêncio e piedade:

>«Acolhei-vos mutuamente e dai-vos o ósculo da paz». Não suponhas que este ósculo seja como os que os amigos íntimos se dão na praça pública. Este ósculo não é assim. Mas este ósculo une as almas entre si e é para elas penhor de esquecimento de todos os ressentimentos. <cite>São Cirilo de Jerusalém, Catequeses Mistagógicas, V, 3</cite>

Portanto, se nosso modo de conceder a paz é semelhante ao que vemos nas ruas, nos ambientes de trabalho ou locais públicos quando dois pagãos se encontram, isto é sinal de que não estamos sinalizando a verdadeira paz cristã que é recolhida, ordenada e une das almas dis fiéis com laços sobrenaturais.

Para esclarecimentos e recomendações da Santa Sé sobre o tema, basta que façamos a leitura de um documento oficial que dá orientações claras para que se evitem abusos: [Carta Circular: O significado ritual do dom da paz na Missa](http://www.catolicostradicionais.com.br/2014/08/novo-documento-da-congregacao-para-o.html)

