+++
date = "2016-02-11T18:00:00-03:00"
title = "Qual o significado das cinzas?"
categories = ["Igreja Catolica","Santa Missa"]
path = "Santa Missa"
featured_image = "posts/images/qual-o-significado-das-cinzas.jpg"
+++

Na quarta-feira que marca o início da quaresma a Igreja propõe em sua liturgia que os fiéis cubram sua cabeça com cinzas. Qual seria o significado deste gesto? Qual a origem deste rito?

<!--more-->

A origem deste rito remonta a muitos séculos. Com efeito, encontramos diversas vezes no Antigo Testamento o uso das cinzas como sinal de dor e humilhação:

>Conhecia-te só de ouvido, mas agora viram-te meus olhos: por isso, retrato-me e faço penitência no pó e na cinza. <cite>Bíblia de Jerusalém, Livro de Jó, 42, 5-6</cite>

Muitos séculos depois a prática permaneceu dentro do cristianismo e os penitentes públicos, que por algum pecado considerado gravíssimo tinham recebido penas que deveriam cumprir diante de seus irmãos na fé, pediam perdão revestidos de sacos e com a cabeça coberta de cinzas, como sinal de sua contrição.

Porém, como todos os homens são pecadores essa cerimônia foi estendida a todos os fiéis para recordar o preceito da penitência, pois

>Todos têm necessidade de se renovar a cada dia para evitarmos a ferrugem inerente à nossa condição mortal, e não há ninguém que não deva se esforçar para progredir no caminho da perfeição; por isso, todos sem exceção, devemos empenhar-nos para que, no dia da redenção, pessoa alguma seja ainda encontrada nos vícios do passado. <cite>São Gregório Magno, Sexto Sermão da Quadragésima</cite>

Deste modo, a meditação de dois grandes baluartes de nossa vida espiritual são propostos através das cinzas: a humilhação e a dor.

Ora, a humilhação é necessária para que cresça em nós a virtude da humildade, sem a qual torna-se impossível até mesmo iniciar uma vida espiritual e receber a graça. Com efeito, ela:

>expulsa a soberba, a que Deus resiste, e nos torna submissos e sempre dispostos a receber o auxilio da graça, por eliminar a inflação da soberba. Por isso, diz a Escritura: Deus resiste aos soberbos e dá a sua graça aos humildes. E neste sentido a humildade é considerada o fundamento do edifício espiritual. <cite>Santo Tomás de Aquino, Suma Teológica, IIa IIae, 161, a. 5</cite>

Assim, sem humildade não é possível que Deus infuda em nós sua graça e a faça crescer, pois a nossa soberba impede a graça de ser nutrida e desenvolvida pelo Sol da Justiça. Por isso, já reconhecia São Gregório Magno que a soberba é a "mãe e a rainha de todos os vícios" que levou à condenação o primeiro homem e o maior de todos os Anjos.

Portanto, é necessário que pratiquemos constantemente atos de humildade a fim de fomentar esta virtude e reconhecer nossa pequenez, o que fazemos quando:

>considerando os nossos defeitos, colocamo–nos, conforme a nossa condição, em situação ínfima; assim, Abraão disse ao Senhor: Falarei ao Senhor, ainda que eu seja cinza e pó. <cite>Santo Tomás de Aquino, Suma Teológica, IIa IIae, 161, a. 1</cite>

Um segundo ponto que as cinzas nos recordam é a dor. A dor requer duas condições: a perda de um bem e a percepção desta perda. Portanto, sentimos dor quando algum bem nos é retirado e percebemos esta perda.

Ademais, a dor pode ser tanto externa quanto interna. Externa quando nosso corpo sofre um mal, como um ferimento; interna quando nossa alma se entristece ou sofre.

Ora, as cinzas são o produto da destruição de alguma coisa pelo fogo. Assim, servem para nos recordar o processo de morte e destruição que devemos nos submeter para que possamos seguir ao Cristo e alcançar a Ressurreição.

É um processo doloroso, pois não poucas vezes sofremos para fazer oposição as nossas paixões, aos nossos egoísmos, aos nossos caprichos e para praticar o bem obtendo muitas vezes como recompensa perseguições, insultos, incompreensões, ódio e inimizades.

Há ainda um terceiro significado das cinzas: a fragilidade da criatura humana que torna-se manifesta com a morte.

Com efeito, ao receber as cinzas ouvimos "lembra-te que és pó, e ao pó hás de tornar" ou "convertei-vos e crede no Evangelho". A primeira admoestação nos recorda que existe um inimigo que todos teremos de enfrentar e que inevitavelmente seremos derrotados; a segunda, nos recorda o que devemos fazer para que esta derrota seja instrumento para uma nova vida de união com Deus.

Quando formos pó ficaremos sob os pés de todos, até mesmo dos animais. Quando formos pó perderemos tudo aquilo que conquistamos, seja fama, glória, poder, riquezas ou prazeres. Quando formos pó perceberemos que deveríamos fizemos muitas coisas vãs e deixamos de fazer muitas necessárias.

Por isso, as cinzas recordam a fragilidade humana e nos propõe que vivamos sempre diante do momento de nossa morte e que julguemos cada um de nossos atos presentes como se estivéssemos à porta do julgamento.

Com efeito, quem ousaria enfrentar seu juíz neste momento derradeiro? Quem teria coragem de transgredir e ofender Nosso Senhor Jesus Cristo? Quem recusaria arrepender-se e confessar seus pecados naquele momento? Quem viveria relaxado como se sua vida não pudesse ser perdida dali a poucos instante? Quem presumiria que poderia salvar-se por seus próprios caminhos e ilusões nestes momentos?

Por isso Dom Bosco propunha a seus queridos jovens esta saudável reflexão:

>Considera portanto, meu filho, que a tua alma deverá separar-se do corpo; mas não sabes se a morte te assalta na tua cama, ou durante o trabalho, ou na rua, ou em outra parte. A ruptura de uma veia, um catarro, uma hemorragia, uma febre, uma chaga, uma queda, um terremoto, um raio, bastam para te tirar a vida. Isso pode acontecer daqui a um ano, daqui a um mês, a uma semana, a uma hora e talvez ao terminar a leitura desta consideração. Quantos se deitaram à noite cheios de saúde e de manhã foram encontrados mortos! Quantos acometidos de algum ataque morreram de repente! E depois para onde foram? (...) Dizei-me, filho, se tivesses que morrer neste instante, o que seria de tua alma? Ai de ti, se não te manténs sempre preparado! Quem não está hoje preparado para bem morrer, corre grande perigo de morrer mal. (...) Agora o demônio,para induzir-te a pecar, procura arrancar-te deste pensamento e levar-te a escusar a tua culpa, dizendo-te não ser enfim tão grande mal aquele prazer, aquela desobediência, aquela omissão da missa nos domingos; mas na hora da morte descobrir-te-á a gravidade destes e de outros teus pecados, pondo-os diante de ti. E que haverás de fazer tu então, no ponto de te encaminhares para tua eternidade? <cite>São João Bosco, Meditação sobre a morte</cite>

Portanto, que possamos humilhar-nos reconhecendo nossa fragilidade de modo que nossa vida seja uma constante preparação para voltarmos ao pó de onde viemos. Que a dor da contrição esteja sempre presente em nossa alma e a dor das mortificações estejam sempre em nosso corpo. Só assim poderemos aproveitar-nos do mistério das cinzas para cresce na graça, na virtude e na sabedoria.