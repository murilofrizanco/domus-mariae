+++
date = "2014-11-05T06:36:10-03:00"
title = "Como obter uma indulgência plenária?"
categories = ["Igreja Catolica","Sacramentos","Penitencia"]
path = "Sacramento da Penitência"
featured_image = "posts/images/como-obter-uma-indulgencia-plenaria.jpg"
+++

Deparando-se com várias dúvidas sobre os requisitos para a obtenção das indulgências é conveniente reunir em um só lugar como é possível obter uma indulgência plenária.

<!--more-->

Quanto a isto devemos primeiramente tomar conhecimento de quais pessoas estão aptas a receberem uma indulgência plenária. Com efeito, diz o [Manual de Indulgências](http://www.presbiteros.com.br/site/wp-content/uploads/2011/12/Manual-de-Indulg%C3%AAncias.pdf):

>Para que alguém seja capaz de lucrar indulgências, deve ser batizado, não estar excomungado e encontrar-se em estado de graça, pelo menos no fim das obras prescritas. O fiel deve também ter intenção, ao menos geral, de ganhar a indulgência e cumprir as ações prescritas, no tempo determinado e no modo devido, segundo o teor da concessão.<cite>Manual de Indulgências, Normas sobre as indulgências, 20, § 1-2</cite>

Portanto, somente o católico batizado que não foi espiritualmente amputado da Igreja pela excomunhão e que esteja em estado de graça pode tomar parte neste tesouro espiritual.

Ademais, é necessário que busca uma indulgência plenária deve realmente ter a intenção de obtê-la. Isto pode parecer supérfluo, porém, não é incomum que muitas pessoas façam as obras indulgenciadas por costume sem intenção de recebê-la ou sequer conhecer que trata-se de uma obra enriquecida com indulgência.

Assim, o mesmo manual nos oferece os requisitos para obter a indulgências nos seguintes termos:

>Para lucrar a indulgência plenária, além da repulsa de todo o afeto a qualquer pecado até venial, requerem-se a execução da obra enriquecida da indulgência e o cumprimento das três condições seguintes: confissão sacramental, comunhão eucarística e oração nas intenções do Sumo Pontífice.<cite>Manual de Indulgências, Normas sobre as indulgências, 23, § 1</cite>

Como o texto do manual é técnico e objetivo, é conveniente detalhar cada um dos pontos. Assim, o fiel pode receber as indulgência cumprindo os seguintes requisitos.

Primeiro. Requer-se que o fiel rejeite todo afeto, isto é, toda simpatia ou apego a qualquer pecado, até mesmo os veniais. Esta é a parte mais difícel, pois é necessário colocar-se diante de Deus disposto a abrir mão de toda disposição, gosto ou desejo desordenado por causa do amor ao Cristo. Esta disposição não é fácil de se alcançar. Por isso, Santa Catarina de Gênova dizia:

>Não confies, pois, dizendo: me confessarei e conseguirei depois a indulgência plenária e naquele momento me verei purificado de todos meus pecados. Pensa que esta confissão e contrição, que é preciso para receber a indulgência plenária, é coisa tão difícil de conseguir que, se soubesses, tremerias com grande temor e estarias mais certo de não tê-la do que de podê-la conseguir. <cite>Santa Catarina de Gênova, Tratado do Purgatório, Parte III</cite>

Isto não significa que esta dificuldades deva nos fazer desistir das indulgências plenárias, pois a falta desta disposição torna a indulgência parcial, o que ainda assim é muito benéfico para a vida espiritual e para as almas do purgatório.

Segundo. O fiel deve executar a obra indulgenciada, isto é, enriquecida com a indulgência pela autoridade da Igreja. As principais obras que atualmente constam no Manual podem ser consultadas [neste link](https://docs.google.com/spreadsheets/d/1WjKMBcFM0G9Rm3jUDF1kfGz_WdNb6yvW5WrSgCcOWG8/pubhtml?gid=0&single=true).

Terceiro. Requer-se que o fiel receba a Confissão Sacramental, isto é, que se confesse. Ela não precisa ser feita no mesmo dia em que se cumpre a obra indulgenciada, mas pode ser feita dentro de aproximadamente 20 dias (cf. documento ["O dom da indulgência"](http://www.vatican.va/roman_curia/tribunals/apost_penit/documents/rc_trib_appen_pro_20000129_indulgence_po.html)), tanto antes como depois. Deve-se notar que com uma única confissão pode-se obter diversas indulgências.

Quarto. Requer-se que o fiel receba a Comunhão Eucarística. Embora, como a confissão, possa ser cumprida dentro de 20 dias, antes ou depois da obra, é recomendado que seja recebida no mesmo dia em que se executa a obra. Cada indulgência plenária exige a recepção da Comunhão.

Quinto. Requer-se a oração nas intenções do Papa. Embora, como a confissão, possa ser cumprida dentro de 20 dias, antes ou depois da obra, é recomendado que seja feita no mesmo dia em que se executa a obra. Cada indulgência plenária exige que se repita este ato individualmente. Esta condição se cumpre com a recitação, nas intenções do Papa, de um Pai-Nosso e uma Ave-Maria, mas podem ser acrescentadas livremente quaisquer outras orações pessoais.

Cumprido estes requisitos, obtem-se a indulgência plenária que pode ser aplicada tanto a si mesmo como aos defunto.

Porém, quando falta algum deles a indulgência será apenas parcial.