+++
date = "2016-02-25T18:00:00-17:00"
title = "Exame de consciência dos pecados veniais"
categories = ["Igreja Catolica","Sacramentos","Penitencia"]
path = "Sacramento da Penitência"
featured_image = "posts/images/exame-de-consciencia-dos-pecados-veniais.jpg"
+++

Na medida que progredimos na vida espiritual e chegamos ao ponto de evitar com facilidade os pecados mortais, devemos passar a um combate sério contra os pecados veniais. Embora os pecados veniais sejam impossíveis de se listar, podemos contar com um exame muito apropriado feito por Santo Antônio Maria Claret:

<!--more-->

A alma deve evitar todos os pecados veniais, especialmente os que abrem caminho ao pecado grave.

Ó minha alma, não chega desejar firmemente antes sofrer a morte do que cometer um pecado grave? É necessário tem uma resolução semelhante em relação ao pecado venial.

Quem não encontrar em si esta vontade, não pode sentir-se seguro. Não há nada que nos possa dar uma tal certeza de salvação eterna do que uma preocupação constante em evitar o pecado venial, por insignificante que seja, e um zelo definido e geral, que alcance todas as práticas da vida espiritual.

Zelo na oração e nas relações com Deus; zelo na mortificação e na negação dos apetites; zelo em obedecer e em renunciar à vontade própria; zelo no amor de Deus e do próximo. Para alcançar este zelo e conservá-lo, devemos querer firmemente evitar sempre os pecados veniais, especialmente os seguintes:
 
1. pecado de dar entrada no coração de qualquer suspeita não razoável ou de opinião injusta a respeito do próximo.
2. pecado de iniciar uma conversa sobre os defeitos de outrem, ou de faltar à caridade de qualquer outra maneira, mesmo levemente.
3. pecado de omitir, por preguiça, as nossas práticas espirituais, ou de as cumprir com negligência voluntária.
4. pecado de manter um afeto desregrado por alguém.
5. pecado de ter demasiada estima por si próprio, ou de mostrar satisfação vã por coisas que nos dizem respeito.
6. pecado de receber os Santos Sacramentos de forma descuidada, com distrações e outras irreverências, e sem preparação séria.
7. Impaciência, ressentimento, recusa em aceitar desapontamentos como vindo da Mão de Deus; porque isto coloca obstáculos no caminho dos decretos e disposições da Divina Providência quanto a nós.
8. pecado de nos proporcionarmos uma ocasião que possa, mesmo remotamente, manchar uma situação imaculada de santa pureza.
9. pecado de esconder propositadamente as nossas más inclinações, fraquezas e mortificações de quem devia saber delas [isto é, de quem poderia nos ajudar], querendo seguir o caminho da virtude de acordo com os caprichos individuais e não segundo a direção da obediência.