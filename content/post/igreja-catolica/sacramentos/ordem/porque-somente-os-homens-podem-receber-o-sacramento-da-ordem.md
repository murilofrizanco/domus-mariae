+++
date = "2014-07-11T07:00:00-18:00"
title = "Porque somente os homens podem receber o Sacramento da Ordem?"
categories = ["Igreja Catolica","Sacramentos","Ordem"]
path = "Sacramento da Ordem"
featured_image = "posts/images/porque-somente-os-homens-podem-receber-o-sacramento-da-ordem.jpg"
+++

A restrição do Sacramento da Ordem somente aos homens parece provir de uma cultura machista da antiguidade e parece absurdo que a Igreja ainda sustente tal restrição numa sociedade que busca a igualdade dos sexos.

<!--more-->

Com efeito, sabemos que, segundo o Magistério da Igreja, as mulheres não podem, nem nunca poderão, receber o sacramento da Ordem e isso está definido e declarado pela Igreja:

>Portanto, para que seja excluída qualquer dúvida em assunto da máxima importância, que pertence à própria constituição divina da Igreja, em virtude do meu ministério de confirmar os irmãos (cf Lc 22,32), declaro que a Igreja não tem absolutamente a faculdade de conferir a ordenação sacerdotal às mulheres, e que esta sentença deve ser considerada como definitiva por todos os fiéis da Igreja. <cite>São João Paulo II, Carta Apostólica Ordinatio Sacerdotalis</cite>

Alguns afirmam que tal afirmação procede de uma certa discriminação das mulheres. Porém, deve-se notar que existem razões profundas inscritas no Sacramento e no homem que fazem as coisas serem assim.

Primeiro. O sacramento realiza aquilo que ele significa. Ora, há uma semelhança natural entre os sinais e a realidade produzida pelos efeitos dos sacramentos. Com efeito, como a água é símbolo do batismo, o homem é símbolo do sacramento da ordem. Ora, o ordenado torna-se apto a agir na pessoa de Cristo (in persona Christi). Se o sacerdote age como se fosse a pessoa de Cristo, então ele mesmo deve ser sinal desta realidade operada pelo sacramento. Portanto, sendo encarnando Cristo no sexo masculino, somente os homens podem sinalizar mais perfeitamente a realidade significada.

Segundo. Recebemos as graças através da humanidade masculina de Cristo. Quanto a isto deve-se notar que mesmo após a Ressurreição o Corpo de Cristo não perdeu sua característica sexual. Ora, é através deste Corpo Ressuscitado que nos vem todas as graças divinas. Ademais, o sacerdote, ao ensinar e conferir os sacramentos, faz-se instrumento desta humanidade. Assim, somente o homem é capaz de significar em plenitude a intermediação das graças feitas pela humanidade de Cristo.

Terceiro. A escolha dos apóstolos. Com efeito, Cristo curiosamente escolheu apenas homens para compôr o ofício de Apóstolo ao mesmo tempo que aceitava mulheres como discípulas. Ora, Cristo nunca operou sua obra salvífica submetido à mentalidade dos que estavam a sua volta de modo tudo o que fez foi feito com toda liberdade e soberania de modo que o teria feito em circunstâncias diversas. Com efeito, muitas vezes escandalizou aos judeus com suas obras e ensinamentos. Portanto, a restrição da ordem ao sexo masculino procede diretamente de uma escolha livre de Cristo.

Quarto. Não conferiu o Sacramentio da Ordem à sua Mãe. É manifesto que a dignidade da Virgem Santíssima é maior do que a de todos os Apóstolos e fiéis e até mesmo maior do que a de todos os anjos por ser Mãe do Verbo. Ora, Cristo não conferiu este sacramento à sua Mãe, julgando que uma abundância maior da graça e uma participação maior na glória era muito maior que o Sacramento da Ordem. Com efeito, os maiores no céu não são os que receberam tal sacramento, mas os que possuem maior grau na glória. Portanto, restringir este sacramento não priva a pessoa do verdadeiro e sumo bem que deve ser preferido a todos os demais.

Quinto. A estrutura masculina sinaliza o amor a Deus. Com efeito, pela apreensão da estrutura masculina alcançamos a noção do amor a Deus, enquanto pela feminina alcançamos a do amor ao próximo. Isto se demonstra na disposição do homem ao sacrifício, à direção, à voltar-se para o abstrato, o combate, enquanto a mulher é melhor disposta à comunicação, à execução, ao voltar-se para as coisas temporais e a geração de um novo ser humano. Por isso, somente um homem pode receber a Ordem, enquanto somente com uma mulher pode-se receber o sacramento do Matrimônio.

Assim, a Igreja não definiu esta questão por uma discriminação sexual ou mantelidade ultrapassada, mas por imitação à Nosso Senhor Jesus Cristo, que assim o fez por razões muito profundas que revelam intimamente a natureza masculina. Com efeito, assim como não é possível um homem gerar um filho em seu ventre, não é possível que a mulher signifique tudo o que está contido no Sacramento da Ordem.