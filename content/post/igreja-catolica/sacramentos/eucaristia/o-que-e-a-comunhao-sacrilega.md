+++
date = "2014-08-25T07:00:00-18:00"
title = "O que é uma comunhão sacrílega?"
categories = ["Igreja Catolica","Sacramentos","Eucaristia"]
path = "Sacramento da Eucaristia"
featured_image = "posts/images/o-que-e-a-comunhao-sacrilega.jpg"
+++

A comunhão sacrílega é uma das ofensas mais graves à Nosso Senhor Jesus Cristo. Porém, muitos a cometem sem perceber os grandes prejuízos que ela causa na alma.

<!--more-->

A comunhão sacrílega ocorre quando a pessoa recebe a Santíssima Eucaristia em estado de pecado mortal, isto é, com pecados mortais ainda não perdoados através da Confissão. Este pecado corrói vorazmente o Sagrado Coração de Jesus e o Imaculado Coração de Maria e faz pesar a mão Deus sobre os homens. Com efeito, os Santos Padres admoestam frequentemente contra este grande mal:

>Não há praticamente nenhum crime que mais ofende a Deus que a comunhão sacrílega.<cite>Santo Antônio Maria Claret</cite>

>O comungante em pecado mortal comete um crime maior que Herodes.<cite>Santo Agostinho de Hipona</cite>

>[O comungante comete um crime] mais assustador do que Judas.<cite>São João Crisóstomo</cite>

>Os Judeus crucificaram ao Senhor da glória enquanto era terrestre e mortal, e os sacrílegos crucificam-No agora que reina no céu; aqueles só uma vez se atreveram a crucificá-Lo, estes renovam o deicídio freqüentes vezes; aqueles se tinham declarado inimigos figadais de Cristo, estes traem-No ao mesmo tempo que, pelo menos exteriormente, O reconhecem por seu Deus, simulando reverência e devoção, e imitando a Judas, abusam do sinal de paz: "Com um beijo entregas o Filho do homem"<cite>Santo Afonso de Ligori</cite>

>Quem faz comunhão sacrílega, recebe em seu coração a Satanás e a Jesus Cristo; a satanás, para fazê-lo reinar, e a Jesus Cristo para oferecê-lo em sacrifício a Satanás.<cite>São Cirilo de Alexandria</cite>

Ademais, os grandes místicos testemunham o mesmo: 

>Não existe na terra um suplício que seja suficiente para punir quem comunga em estado de pecado mortal.<cite>Jesus à Santa Brígida</cite>

Portanto, trata-se de um crime nefasto que deve ser evitado a todo custo. Por isso, aconselhavam os grandes doutores:

>Confessai-vos com humildade e devoção. Se for possível, todas as vezes que fostes comungar, mesmo que na consciência não estejais sentindo nenhum remorso de pecado mortal.<cite>São Francisco de Sales</cite>

>Também eu levanto a voz e vos suplico, peço e esconjuro para não vos abeirardes desta Mesa sagrada com uma consciência manchada e corrompida. De fato, uma tal aproximação nunca poderá chamar-se comunhão, ainda que toquemos mil vezes o corpo do Senhor, mas condenação, tormento e redobrados castigos.<cite>São João Crisóstomo</cite>

Alguns poderiam pensar que tais exortações são apenas expressões exageradas. Porém, o testemunho histórico atesta tais sentenças:

>São Cipriano refere que alguns de seu tempo, não sendo dignos de receber a Sagrada Comunhão, depararam-se com uma dor intolerável nas entranhas e às portas da morte. São João Crisóstomo conhecia muitos possuídos por demônios por causa deste crime. (...) Lemos na vida de um monge de São Bernardo que este se atreveu a comungar em pecado mortal. Algo terrível! Logo que o Santo lhe deu a Sagrada Hóstia, rebentou como Judas e como ele foi condenado eternamente.<cite>Santo Antônio Maria Claret</cite>

Deve-se notar que estes casos drásticos não costumam ocorrer frequentemente. Porém, embora a punição não ocorra ordinariamente de modo visível, todavia, ocorre sempre de modo invisível. Sobre as penas de tal ofensa podemos ler:

>Se a estes últimos pecadores [comungantes] não os pune de forma visível, já o está a fazer invisivelmente: com a cegueira de entendimento, dureza de coração, do seu abandono neste mundo, e em seguida, no outro, com o castigo eterno do Inferno.<cite>Santo Antônio Maria Claret</cite>

Portanto, quem comunga sacrilegamente recebe quatro efeito em sua alma. É conveniente enumerar em que consistem tais penas para que nos seja manifesto.

Primeiro. A cegueira de entendimento que torna as pessoas incapazes de distinguir entre o bem e o mal. Com efeito, vemos que são destas pessoas que diz o Profeta: "Ai dos que ao mal chamam bem e ao bem mal, dos que transformam as trevas em luz e a luz em trevas, dos que mudam o amargo em doce e o doce em amargo!" (Is 5,20), pois vão perdendo a aversão ao mal e a afinidade com o bem. Assim, pouco a pouco vão afastando-se dos bens espirituais e se fechando à vida eterna.

Segundo. A dureza do coração em que a pessoa se fecha sobre si mesma tornando-se incapaz de se abrir as necessidades alheias, tanto corporais ou aos bens espirituais. Ora, sem o amor ao próximo não é possível amar a Deus e abri-se a sua graça. Portanto, a se fecha a ação da graça e a possibilidade de deixar seus pecados e vícios.

Terceiro. O abandono pelo qual Deus, sabendo que a alma não corresponderá aos seus apelos, entrega o sacrílego para que se perca nas prosperidades e adversidades do mundo e ao combate dos demônios numa escravidão em que muito trabalha e nada aproveita. A estas pessoas diz o Eclesiástes: "Ao homem do seu agrado ele dá sabedoria, conhecimento e alegria; mas ao pecador impõe como tarefa ajuntar e acumular para dar a quem agrada a Deus" (Ec 2,26).

Quarto. Estas almas merecem o inferno e são entregues ao demônios para receber suplícios eternos e mais cruéis do que os piores sofrimentos desta vida. Com efeito, muitas são as almas que passam a eternidade odiando-se por ter podido evitar o inferno tão facilmente e não o fizeram. É a estes que Cristo adverte: "Lá haverá choro e ranger de dentes, quando virdes Abraão, Isaac, Jacó e
todos os profetas no Reino de Deus, e vós, porém, lançados fora." (Lc 13,28).

Assim, esta grande ofensa deve ser evitada com todas as forças para que não venhamos a ser surpreendidos pela justiça divina que nos castiga por tão grande ofensa.

Livrai-nos Virgem Maria de tão grave ofensa. Protegei-nos e dai-nos sabedoria e discernimento para não ocorrer de ofendermos vosso Diviníssimo Filho. Com seu amor terno, adentrai o coração dos sacrílegos e convertei-os para que alcancem a santidade e a felicidade Eterna.