+++
date = "2014-08-23T07:00:00-18:00"
title = "Como se preparar para receber a Sagrada Comunhão?"
categories = ["Igreja Catolica","Sacramentos","Eucaristia"]
path = "Sacramento da Eucaristia"
featured_image = "posts/images/como-se-preparar-para-receber-a-sagrada-comunhao.jpg"
+++

A recepção inadequada da Eucaristia pode resultar em um efeito praticamente nulo ou até mesmo constituir uma verdadeira ofensa a Nosso Senhor, isto é, um pecado mortal. Diante da presença sublime e misteriosa de Nosso Senhor na Eucaristia, como se preparar adequadamente para recebê-lo?

<!--more-->

Para que a comunhão não resulte em prejuízo por tornar-se um ato sacrílego, é necessário que quem deseja recebê-la faça um exame de consciência para verificar se possui algum pecado mortal que ainda não foi perdoado no Sacramento da Confissão. Este exame deve ser feito de acordo com o ensinamento da Igreja e não conforme a pessoa se sinta emocionalmente desconfortável ou não com determinado fato.

Se a pessoa encontrar algum pecado mortal não confessado, não pode comungar e deve primeiramente confessar-se. Com efeito, diz o Catecismo:

>Se alguém tem consciência de ter pecado mortalmente, não deve comungar a Eucaristia sem ter recebido previamente a absolvição no sacramento da penitência [confissão].<cite>Catecismo da Igreja Católica, n. 1415</cite>

Se a pessoa já confessou todos os pecados mortais de que tem consciência, então a pessoa pode comungar sem risco de cometer sacrilégio.

Porém, isto não impede que a comunhão tenha um efeito praticamente nulo na pessoa. Para obter o máximo de frutos espirituais levando à um verdadeiro crescimento na santidade, a pessoa deve preparar-se com fervor e devoção. Para tanto, São Luís de Montfort nos ensina quatro atos que nos permitem uma preparação adequada para receber a Eucaristia com frutos.

Primeiro. Deve-se fazer um ato de humildade:

>Humilhar-te-ás profundamente diante de Deus<cite>São Luís de Montfort, Tratado da Verdadeira Devoção, 266</cite>

Para isso, é preciso reconhecer a nossa maldade e perceber o quão indignos, cruéis, inúteis, impuros e traiçoeiros somos para receber tão grande tesouro. Com efeito, por nós mesmos merecemos apenas o castigo e a condenação. Por outro lado, devemos reconhecer a grandeza e majestade da Santíssima Trindade que, apesar de insistirmos em ofendê-la, faz-se paciente, bondosa e misericordiosa para conosco e tenta incansavelmente nos resgatar de nossa miséria. Apesar da crucificação foi a obra prima de nossa maldade, Cristo insiste em oferecer-se inteiramente a nós na Eucaristia. Deve-se contemplar o grande abismo que existe entre a bondade de Deus e a nossa maldade.

Segundo. Deve-se fazer um ato de renúncia a si mesmo:

>Renunciarás ao teu fundo todo corrompido e às tuas disposições, embora o teu amor próprio as faça parecer boas.<cite>São Luís de Montfort, Tratado da Verdadeira Devoção, 266</cite>

Para tanto, é preciso reconhecer que nossos melhores desejos, atos e intenções estão manchados pela nossa imperfeição e, mesmo os melhores, se fundamentam em um amor egoísta, interesseiro e estéril. Com efeito, muitas das nossas boas obras são contaminadas por um apego a si mesmo ou uma soberba que confia em nossas disposições corrimpidas. Muitas são as almas bem intencionadas que estão neste momento no inferno lamentando-se do seu amor próprio. Assim, deve desfazer-se desta natureza humana decaída e não esperar nenhuma boa disposição que proceda dela.

Terceiro. Deve-se fazer um ato de entrega à Virgem Santíssima:

>Renovarás a tua consagração dizendo: "Todo Vosso sou, ó querida Mãe, e tudo o que tenho é Vosso!"<cite>São Luís de Montfort, Tratado da Verdadeira Devoção, 266</cite>

Para isso, deve-se fazer uma nova entrega à Virgem, mãe misericordiosa, sublime, pura e santa, que apenas aguarda nosso consentimento para nos auxiliar a alcançar a plena comunhão com seu Filho. Com a consagração somos apartados da posse de Satanás e deste mundo para tornar-se propriedade desta Digníssima Senhora e entregar-mo-nos a seu cuidado e proteção para que não sejamos saqueados pelo combate dos demônios. Com efeito, reforçamos a promessa de pertencer somente ela e, por consequência, à seu Filho Unigênito oferecendo todo nosso amor.

Quarto. Deve-se pedir a Santíssima Virgem que empreste seu coração:

>Suplicarás a esta boa Mãe que te empreste o seu Coração, para n'Ele receberes seu Filho com as disposições d'Ela.<cite>São Luís de Montfort, Tratado da Verdadeira Devoção, 266</cite>

Para tanto, devemos pedir a Virgem Maria que venha receber seu Filho em nós. Assim, não seremos nós, receptáculo indigno, que o receberemos, mas sim Maria, Arca da Nova Aliança, que o acolherá em nosso coração. Com efeito, Maria recebe seu Filho em seu coração ardente e comunica a nós as graças abundantes que jorram da Fonte de Água Viva de modo muito mais eficaz, pois sabe como aproximar-se de seu Cristo.

Este é o modo como devemos nos preparar para a recepção da Sagrada Eucaristia com grandes frutos.

Que possamos, ò Virgem Santíssima, receber teu Filho todo inteiro através de vosso Imaculado Coração, manancial de graças para todos aqueles que se confiam à Vós.