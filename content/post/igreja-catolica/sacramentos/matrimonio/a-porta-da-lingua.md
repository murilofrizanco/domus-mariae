+++
date = "2014-11-07T06:36:10-03:00"
title = "A porta da língua"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/a-porta-da-lingua.jpg"
+++

Guardar a língua dos filhos é de máxima importância. Com efeito, por ela se sabe o nível espiritual das pessoas. Por isso, Cristo nos ensina: "A boca fala daquilo de que o coração está cheio." (Mt 12, 34b)

<!--more-->

Na guarda dos sentidos o grande doutor que dedicou escritos profundos sobre a educação das crianças nos escreve:

>Eis, pois, vamos primeiramente à porta da língua, pois esta é a que mais se movimenta, e a devemos construir antes de qualquer outra, porta e fechadura, não de madeira nem de ferro, mas de ouro.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Com efeito, uma pessoa que não é capaz de controlar a sua língua, certamente não será capaz de controlar os demais sentidos e muito menos de progredir na vida espiritual. Portanto, guardá-la é essencial para o crescimento na virtude.

Para tanto, o santo nos oferece três conselhos principais.

Primeiro. Faz-se necessário eliminar toda palavra inconveniente:

>Deveremos praticar uma rigorosa expulsão de estrangeiros, afim de que não se misturem com estes cidadãos, gente em abundância e corruptoras; ou seja, havemos de excluir as palavras insolentes e maledicentes, as insensatas, as torpes, as seculares e mundanas. Importa banir todas essas palavras. Ninguém, exceto o rei [Cristo], deverá entrar por estas portas. Só a Ele e a quantos com Ele vão, há de estar aberta a porta.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Ora, as palavras insolentes são as desrespeitosas; as maledicentes são as difamatórias; as insensatas são as imprudentes; as torpes são as indecentes; as seculares são as profanas; as mundanas são as que se referem aos prazeres deste mundo.

Todas estas palavras devem ser eliminadas da língua dos filhos com todo empenho e rigor.

O modo de se conseguir isto é o que se segue:

>Impõe a ele [ao filho], imediatamente, a lei de que não insulte a ninguém nem fale mal de ninguém, nem jure, nem procure desavenças.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Com a transgressão desta lei deve-se castigar o filho e voltar as próprias palavras dele contra ele:

>Se percebes que insulta seu acompanhante, não consinta, mas castiga-o livremente. (...) Costura sua boca para toda a maldade. Se notas que calunia alguém, fecha-lhe a boca e força-o a voltar a língua para seus próprios pecados.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Alguns poderiam pensar que isto seria um trabalho de uma vida inteira. Porém, com pouco esforço é possível educar a língua do filho e obter resultados eficazes:

>E não pensa, te suplico, que a isto requeira muito tempo. Se desde o início o atacas e ameaças e colocas tantos guardiões, dois meses bastam e tudo se consegue e endireita, e a coisa vem tomar a firmeza do natural.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Segundo. Faz-se necessário encher a língua do filho com boas palavras:

>Ensinemo-lhes a levar sempre nos seus lábios as palavras divinas, inclusive nos seus passeios, não casualmente e superfluamente, nem poucas vezes, mas continuamente. (...) Ensinando as crianças a falar palavras nobres e piedosas.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Com efeito, se a criança apenas eliminar de sua boca as más palavras vai ficar em um silêncio insuportável que a inclinará a voltar a sua linguagem inconveniente.

Por isso, deve-se ensinar aos filhos palavras boas, úteis, honestas e espirituais para que ocupe o lugar das anteriores. Para tanto, Crisóstomo nos oferece alguns exemplos:

>As palavras sejam de ação de graças, cânticos sagrados, todas as suas conversas sejam sobre Deus e sobre a filosofia do céu.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Ora, nos inclinamos a praticar aquilo que dizemos. Assim, uma alma que só fala palavras espirituais certamente irá se inclinar as coisas nobres e espirituais.

Terceiro. Deve-se oferecer ao filhos cânticos e histórias:

>Aprenda, portanto, a criança a cantar a Deus, para que não se entregue às canções medíocres e às histórias inconvenientes.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Este ensinamento é grande, pois a falta de boa cultura deixa o filho aberto a sentir-se interessado por qualquer coisa torpe que lhe seja apresentado.

Porém, aqui não se fala apenas de quaisquer histórias, mas daquelas que tornam manifesta a beleza, a bondade e a verdade da virtude. Com efeito, geralmente encontra-se isto nas histórias dos santos e de homens virtuosos.

Os cantos também devem ter critério. Deve-se apresentar músicas e cânticos que tenham ritmos harmonisos e ordenados. Nada de barulhos que ressaltam mais a batida do que a harmonia. O exemplo, por excelência, de música que pode ser apresentada é o canto gregoriano e a música clássica de um Mozart, Vivaldi e outros autores.

Que com o auxílio da Virgem Maria possamos erradicar toda palavra inconveniente da boca dos filhos e conduzí-la ao louvor divino.