+++
date = "2014-06-26T20:36:10-03:00"
title = "Destruir a vanglória desde o berço"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/destruir-a-vangloria-desde-o-berco.jpg"
+++

Foi dito que o problema da educação mundana é justamente [fundamentar-se na vanglória](/posts/igreja-catolica/sacramentos/matrimonio/filhos/o-principio-da-educacao-mundana). Porém, lamentavelmente muitos pais não percebem este problema a tempo de corrigí-lo.

<!--more-->

Não é incomum aquela pergunta: "Onde foi que eu errei?". A resposta não poderia ser outra: "errou desde o berço!" ao educar a criança sem destruir a vanglória desde o berço.

Muitos pais sempre levaram na Igreja, levaram os filhos na catequese e nunca os deixaram ter contato com coisas ignóbeis. Porém, apesar disto, muitos acabam educando os filhos para a vanglória desde o berço sem perceber.

Com efeito, as palavras do grande Crisóstomo acusam a consciência de muitos pais:

>Apenas nascido o menino, o pai busca todos os meios imagináveis, não para educá-lo, mas para adorná-lo e vesti-lo com roupas de ouro. (...) Para que fim colocas-lhe um enfeite no pescoço? O que a criança precisa é de um pajem escrupuloso para educá-lo, e não de adornos de ouro. (...) amoleces o lado rígido da sua natureza, infundindo-lhe desde o nascimento o amor supérfluo das riquezas e persuadindo-o a que se iluda com coisas inúteis. (...) Por que fazes com que se interesse pelo corporal? (...) A menina que aprende já no quarto materno a encantar-se com as roupas femininas, quando sair da casa paterna, será ingovernável e um fardo para seu marido. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

De fato, não é assim que muitas crianças iniciam suas vidas? Os parentes, amigos e até mesmo os pais não se desdobram para que o recém-chegado tenha roupas chamativas e desconfortáveis, comidas caras e requintadas, brincadeiras e diversões mundanas e a ser o centro das atenções?

E isto fatalmente leva os pais a fazerem grande esforço para que seus filhos sejam doutos segundo as máximas do mundo, que adquiram fama e sejam aplaudidos, que sejam os melhores entre os demais e que tomem a prosperidade como fim de suas vidas.

>O certo é que todas as pessoas se esforçam para que seus filhos se instruam nas artes, nas letras e na eloquência; mas a ninguém ocorre pensar em como exercitar sua alma. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Assim como a aurora anuncia o nascer do Sol, esta educação anuncia, salvo uma ação sobrenatural de Deus, a miséria e a reprovação.

Portanto, é preciso educar desde o berço na sabedoria, na virtude, na oração e no amor à Cristo. As crianças precisam ser ensinadas no desprezo completo das coisas que procedem da vanglória.

Com efeito, é isto que corresponde a um bom cristão:

>O que nos corresponde é a modéstia, o desprezo pelas riquezas, o desprezo pela glória, zombarmos da honra vinda do povo, transcender a natureza através da virtude da vida. Esta é a decência, esta é a glória, esta é a honra. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Por muitos adultos não terem recebido sabedoria suficiente do ensino paterno, pensa-se que afastar a mundanidade de seus filhos é tolher sua liberdade e conduzí-los à revolta e à frustração. Porém, isto é falso. Com efeito, Crisóstomo prova que quando a virtude que se adquire na infâcia não pode ser perdida:

>Se os bons ensinamentos se imprimem na alma quando esta ainda é terna, logo, quando se tiverem endurecido como uma imagem, ninguém será capaz de os arrancar. É o que acontece com a cera. Agora o tens em tuas mãos quando ainda teme, treme e se espanta com teu olhar, com uma palavra, com qualquer gesto teu. Usa o teu poder para o que convém. Se tens um filho bom, tu serás o primeiro a gozar desse bem; em breve Deus. Para ti trabalhas. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Assim, os filhos revoltados não são os que foram afastados da vanglória mundana, mas aqueles que foram educados em costumes que não fazem sentido, sofreram repreensões contraditórias e não encontraram sabedoria em seus pais, isto é, foram educados na vanglória de agradar seus pais e não na verdadeira virtude.

Portanto, o momento de iniciar este tipo de destruir a vanglória é no berço encaminhando os filhos para a eternidade, transmitindo o desprezo do mundo e da glória humana para a honra e glória de Nosso Senhor Jesus Cristo.

>Educa um atleta para Cristo, e mesmo permanecendo no mundo, ensina-o a ser piedoso desde os primeiros anos. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>