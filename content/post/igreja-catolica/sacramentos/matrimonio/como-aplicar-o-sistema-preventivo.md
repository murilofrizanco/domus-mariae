+++
date = "2014-06-26T07:36:10-03:00"
title = "Como aplicar o sistema preventivo?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/como-aplicar-o-sistema-preventivo.jpg"
+++

Tratamos à respeito do [modo de corrigir os filhos](/posts/igreja-catolica/sacramentos/matrimonio/filhos/como-corrigir-seus-filhos) baseado nos escritos de São João Bosco. Para tanto, demonstrou-se a importância da aplicação do Sistema Preventivo. Assim, é conveniente expôr como aplicar tal método.

<!--more-->

Deve-se notar que o fundamento primeiro deste método é:

>A caridade é benigna e paciente; tudo sofre, mas espera tudo e suporta qualquer incômodo. Por isso, somente o cristão pode aplicar com êxito o Sistema Preventivo. Razão e Religião são os instrumentos de que o educador se deve servir; deve inculcá-los, praticá-los ele mesmo, se quiser ser obedecido e alcançar os resultados que deseja.<cite>São João Bosco</cite>

Portanto, é evidente que as práticas deste método visam educar através do fomento das virtudes naturais e infusas.

O método foi originalmente escrito para os educadores, porém é completamente aplicável aos filhos.

Assim, o grande educador dos jovens apresenta seis práticas para aplicar este método com os filhos.

Primeiro. Faz-se necessário consagrar o tempo aos filhos:

>Permaneça sempre com seus alunos, todas as vezes que não estiverem regularmente ocupados, salvo estejam por outros devidamente assistidos.<cite>São João Bosco</cite>

É necessário que os pais disponham de todo o tempo possível ao filhos. Ora, isto significa que acabou os entretenimentos com terceiros, as viagens do casal, a dedicação excessiva ao trabalho e todas as demais ocupações que excluam os filhos ou de algum modo atrapalhe a constante presença paterna e materna.

Deve-se notar que até mesmo os apostolados devem ser moderados de modo que não constituam um impecílio para o relacionamento entre pais e filhos, salvo uma grande e extrema necessidade.

Em alguns momentos torna-se impossível permanecer com o filho, como quando hpá necessidade de ambos os pais trabalharem fora ou quando a mulher se encontra internada. Quando isto ocorrer, deve-se entregar os filhos aos cuidados de algum cristão virtuoso, sempre preferindo estas pessoas do que os próprios familiares, amigos ou vizinhos.

Segundo. Faz-se necessário que os pais sejam exemplos:

>A moralidade dos professores, mestres de oficina, assistentes, deve ser notória.<cite>São João Bosco</cite>

Não é possível combater os defeitos dos filhos sem combater os próprios. Não há nada mais prejudicial para a educação de uma criança do que ser ensinado a fazer o certo e ver os pais fazendo o oposto. Expôr a criança a esta constante de contradição é um dos maiores perigos para sua alma e fonte de indignação para os filhos.

Com efeito, se um dos pais for imoral ou infiel à Cristo, a salvação santidade da criança estará seriamente comprometida. Deve-se notar que o que se entende por "imoral" não é apenas o que escandaliza a sociedade, mas também os pecados cotidianos que muitas vezes são considerados inofensivos, como o falar mal dos outros, ficar reclamando, discussões desnecessárias, mentiras, a falta de perdão e atitudes semelhantes.

Terceiro. Deve-se dar permissão total para o filho fazer o que quiser, exceto o pecado:

>Dê-se ampla liberdade de correr, pular e gritar, à vontade. Os exercícios ginásticos e desportivos, a música, a declamação, o teatro, os passeios, são meios eficacíssimos para se alcançar a disciplina, favorecer a moralidade e conservar a saúde. Mas haja cuidado em que a matéria das diversões, as pessoas que tomam parte, as falas, não sejam repreensíveis.<cite>São João Bosco</cite>

Com efeito, muitos pais impedem as crianças de fazer muitas coisas inofensivas como correr dentro de casa, brincar com os as panelas da mãe, fingir que está dirigindo o carro do pai, espalhar os brinquedos pelo chão e outras atitudes semelhantes.

Não há nada mais cruel para uma criança do que impedí-la de explorar as coisas que estão ao seu redor.

Porém, isto deve ser tolerado desde tais atitudes não representem riscos para as crianças.

Além disso, é necessário que os pais ofereçam bons entretenimentos como ensinar brincadeiras, ensinar a apreciar a natureza, contar histórias, oferecer música, fazer passeios ao ar livre, fazer caminhadas e atividades do tipo. Contudo, deve-se lembrar sempre daquela máxima de São Felipe Néri: "Fazei quanto quiserdes, a mim me basta não cometais pecados".

Todavia, os pais não devem fornecer qualquer entretenimento, mas somente aqueles que são isentos de comportamentos incompatíveis com as virtudes e costumes cristãos e que não exponham a criança a más companhias. Caso isto aconteça, não demore para mudar aquela atividade em outra.

Quarto. Deve-se incentivar a frequência dos filhos nos sacramentos:

>A confissão freqüente, a comunhão freqüente e a missa cotidiana são as colunas que devem sustentar um edifício educativo, do qual se queira eliminar a ameaça e a vara. Nunca se obriguem os jovens a frequentar os santos sacramentos: basta encorajá-los e dar-lhes comodidade de se aproveitarem deles.<cite>São João Bosco</cite>

Sem os sacramentos, não é possível fundamentar esta prática educativa solidamente e estaria seriamente comprometido.

Deve-se notar que não se deve obrigar os filhos a frequência aos sacramentos, mas atraí-los a fazer livremente. Isto pode ser obtido ensinando os efeitos dos sacramentos aos filhos, a alegria de recebê-los e criar todas as condições possíveis para que seja mais fácil e cômodo o filho ir a uma missa do que ir a um passeio.

Muitos pais se queixam que os filhos não vão a missa, mas não se oferecem para levá-los à Igreja.

Ademais, pode-se incentivar a participação em obras de piedade como novenas, tríduos e outras atividades.

Assim, o filho deve sentir-se atraído pela beleza teológica, utilidade na vida, comodidade de frequentá-los e encorajamento dos pais.

Quinto. Deve-se erradicar todas as más influências do lar.

>Use-se a máxima vigilância para impedir que entrem no instituto companheiros, livros ou pessoas que tenham más conversas. A escolha de um bom porteiro é um tesouro para uma casa de educação.<cite>São João Bosco</cite>

Deve-se eliminar do lar todo e qualquer livro, filme, seriado, costume, visita ou atividade que exponham a criança a maus costumes ou a coisas torpes.

Quanto as visitas, onde inclui-se parentes, devem ser aceitas desde que respeitem os bons costumes do lar. Isto não significa não aceitar visitas, mas aceitá-las sob a condição de que respeitem as regras de não ter más conversações, não entrar trazer até a casa material contrário à fé cristã e outras regras estabelecidas pelos pais.

No caso de haver alguma visita ou parente que não respeite as regras da casa, deve-se impedir tanto quanto for possível sua presença na casa. Não é necessário causar uma discussão, basta simplesmente não convidar nunca mais e ocupar-se de modo que tais pessoas não consigam mais frequentar a casa. Este é o jeito mais gentil de dizer não sem criar inimizades.

Sexto. Deve-se transmitir a sabedoria aos filhos:

>Todas as noites, após as orações de costume e antes que os alunos se recolham, o diretor, ou quem faz as vezes por ele, dirija em público algumas afetuosas palavras, dando algum aviso ou conselho sobre o que convém fazer ou evitar.<cite>São João Bosco</cite>

Os pais devem nutrir constantemente a inteligência dos filhos com ensinamento constantes e sábios, mesmo que sejam breves. A transmissão da sabedoria é o que criará admiração dos filhos pelos pais e fará com que a obediência a eles seja agradável e proveitosa.

Este ensinamentos não deve ser repreensões individuais que os envergonhe, mas exortações caridosas e claras que os incentive à virtude.

Ademais, deve-se manifestar todos os dias carinho para com os filhos de modo que percebam o quanto são amados.

Assim, com estas práticas será possível educar o filho sem a necessidade de castigos constantes.