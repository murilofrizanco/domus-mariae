+++
date = "2014-06-23T06:36:10-03:00"
title = "A educação dos filhos e a salvação dos pais"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/educacao-filhos-salvacao-pais.jpg"
+++

É comum vermos casais que recebem o matrimônio e não querem ter filhos ou querem o mínimo de filhos possível, afinal, educar dá trabalho. É necessário ressaltar novamente a missão primordial de um casal: educar com todo empenho e amor a Deus os seus filhos e, através disso, salvar suas almas.

<!--more-->

Esta visão do Matrimônio dissociada da educação dos filhos é completamente distorcida e lamentavelmente muito comum nos dias de hoje. O Catecismo observa:

>A fecundidade do amor conjugal estende-se aos frutos da vida moral, espiritual e sobrenatural que os pais transmitem aos filhos pela educação.
<cite>Catecismo da Igreja Católica, 1653</cite>

Ainda, segundo São Jerônimo de Strídon, pais que não educam nem sequer amam seus filhos:

>Não vos lisonjeies pelo fato de amar vossos filhos, se acaso não sabeis repreendê-los nem corrigi-los.

Com isso, vemos que pais que educam mal os filhos ou não se empenham nisso estão falhando com a missão do matrimônio e possuem um amor infrutífero. São como sacerdotes infecundos que se recusam a celebrar a missa.

Santo Afonso de Ligório já prevenia:

>Se desejarem casar, aprendam as obrigações que se adquirem na observância da formação de vossos filhos, e aprendam também que, se vós não observardes integralmente essas obrigações, terão sobre vós e sobre vossos filhos a condenação.

Os santos não cessam de lembrar que é tão sério a missão de educar os filhos que se estes forem condenados ao inferno por causa da negligência ou falta de empenho dos pais, o próprio casal também é condenado:

>Ainda que os pais levem uma vida de piedade, contínua oração e comunhão diária, estes virão a se condenar se, por negligência, descuidarem da educação de seus filhos. <cite>Santo Afonso de Ligório</cite>

>Aqueles que não se ocupassem bastante de seus filhos, embora fossem piedosos e regrados pessoalmente, sujeitar-se-iam, por esta falta, à mais formidável condenação. <cite>São João Crisóstomo</cite>

Portanto vemos que a salvação dos pais é intimamente ligada à dos filhos. Como lembra um sábio sacerdote:

>Faz horror pensar que no inferno muitos filhos amaldiçoam aos pais e muitos pais aos filhos, porque foram uns para outros causadores de sua condenação. E que dor seria um pai se condenar por educar mal seus filhos e passar a eternidade sendo lembrado no inferno: "Estou aqui por culpa sua!"<cite>Pe. Guilherme Vaessen</cite>

Que possamos guardar a grande missão dada pelo próprio Deus aos casais de educar nossos filhos. Uma missão grande, de tão alto valor, dignidade e seriedade que é confiada através de um sacramento! Eduquemos os nosso filhos para que no fim possamos gozar das maravilhas e delícias do paraíso, com toda nossa família, contemplando a face de Deus numa alegria sublime e incessante!