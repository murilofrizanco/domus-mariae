+++
date = "2014-11-11T06:36:10-03:00"
title = "A porta do olfato"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/a-porta-do-olfato.jpg"
+++

A guarda do olfato dos filhos não pode ser negligenciada. Com efeito, através deste sentido muitos são induzidos à contemplação ou à perdição.

<!--more-->

Os efeitos na alma da entrega aos prazeres do olfato são descritos pelo grande místico o Carmelo:

>De gozar-se nos odores suaves, nasce o nojo dos pobres, que é contra a doutrina de Cristo; inimizade à servidão, pouca submissão de coração nas coisas humildes e insensibilidade espiritual, pelo menos segundo a proporção de seu apetite.<cite>São João da Cruz, Subida ao Monte Carmelo III, 25, 4</cite>

Ora, este tipo de pessoas tende a ter repulsa de trabalhos servis, como por exemplo o de lixeiro, e imputar tais trabalhos a gente inferior.

Ademais, os que buscam odores exóticos costumam ser vaidosos e desprezas as coisas mais simples.

A insensibilidade espiritual nada mais é que uma incapacidade de fruir dos gozos espirituais decorrente da alma ter-se apegado excessivamente aos gozos sensíveis.

Finalmente, um dos efeitos mais nefastos é o desprezo e repulsa por um filho de Deus porque ele cheira mal ou está sujo.

Portanto, embora possa ser o sentido menos perigoso, não deixa de ter efeitos nefastos se não for educado. Por isso, adverte-nos o grande mestre:

>Também essa porta pode provocar muitos danos, se não a fechamos bem. Tal, os aromas e perfumes. Nada destrói tanto o vigor da alma, nada a relaxa tanto, como o deleitar-se com os bons odores. (...) Ninguém tem que gastar perfumes, pois recebendo-os imediatamente todo o cérebro se relaxa. Daí se estimula também prazeres e isso é muito perigoso. Portanto, importa fechar esta porta, pois a função do olfato é respirar o ar, não receber os bons perfumes. <cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos, 53</cite>

Com isto não se pretende dizer que o uso de bons odores seja um pecado. Porém, deve-se tomar cuidado para que não haja afeição por odres que incitem a sensualidade ou a uma alma efeminada que se aflige na ausência de deleites corporais e os coloca como principal.

Com efeito, existem aromas que excitam propositalmente a nossa concupiscência. Por exemplo, perfumes masculinos e femininos que tem a intenção de excitar sexualmente a pessoa do sexo oposto.

Ademais, os que correm atrás dos prazeres do olfato assemelham-se aos cães que passam a vida inteira farejando incapazes de elevar seu olhar para o céu.

Embora isto seja válidos para os odores artificiais, não se pode dizer o mesmo dos odores naturais, pois, diferente dos anteriores, foram criados por Deus para o conhecimento do homem, se algo está ou não podre, e, quando utilizados adequadamente, para a contemplação da beleza do universo. Por isso, diz um grande teólogo dominicano:

>Quanto ao perfume das flores, é em si mesmo mais espiritual que os produtos artificiais e poderia ser utilizado se elevar nosso pensamento a Deus, autor de tais delicadezas e maravilhas, mas cuidando muito de retificar a intenção e não permitindo ao sentido que se entregue ao seu próprio gosto sem referência alguma a Deus. Houve santos, que por mortificar seu olfato, não queriam sentir nunca o perfume suave das flores<cite>Pe. Antonio Royo Marin, Teologia de La Perfección Cristiana, 240</cite>

Portanto é fundamental que guardemos o olfato das crianças dos odores artificiais e do hábito de utilizar perfumes para que não se acostumem a este tipo de sensualidade e sejam inclinados excessivamente ao prazer corporal.