+++
date = "2015-10-23T18:00:00-17:00"
title = "O que fazer com meu filho quando ele faz birra?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
tags = ["Igreja Catolica","Sacramentos","Matrimonio","Filhos"]
featured_image = "posts/images/o-que-fazer-com-meu-filho-quando-ele-faz-birra.jpg"
+++

Na idade de 1 a 3 anos começa uma fase na vida da criança na qual ela entende com bastante clareza o que falamos, mas não é capaz de controlar suas emoções. Neste período é perfeitamente natural que os pais tenham que lidar com as birrras de seus filhos.

<!--more-->

Nesta fase a criança quando se sente irritada, seja porque foi proibida de fazer algo ou por outra causa, algumas vezes reage com um ataque de gritos e com movimentos corporais violentos, seja se debatendo, esperneando ou comportamentos semelhantes.

A primeira resposta para este tipo de atitude é que a criança possivelmente estaria fazendo "manha".

Porém, deve-se notar que este tipo de surto não é desagradável somente para os pais, mas também para as crianças. Com efeito, é manifesto que quando nos irritamos muito o movimento violento de nossas paixões nos causam um grande desconforto, por isso ninguém gosta de ficar irritado. No caso da criança a sensação é a mesma e possivelmente até mais intensa do que nos adultos.

Portanto, os filhos dificilmente fariam isso como uma provocação aos pais ou um ato de rebeldia diante de algum atrito.

Quanto a isto deve-se notar que a criança nesta idade tem bastante capacidade de apreensão intelectual dos conceitos e até mesmo consegue compreender, embora com total perfeição, comandos, histórias e conversas.

Por outro lado, o intelecto ainda é fraco na criança e ela não consegue ainda coordenar seu corpo perfeitamente com a razão.

Devido a isto, quando a criança se depara com alguma situação que causa um forte movimento das paixões (emoções) ela tende a ceder completamente e ser dominada por elas saindo completamente fora de si.

Num primeiro momento poderíamos pensar que esta explicação, baseada na psicologia de Santo Tomás de Aquino, seria uma suposição sem prova nehnuma.

Porém, o Doutor Angélico, ao discutir os efeitos do prazer percebe que:

>A atenção aplicada intensamente a um objeto se enfraquece em relação aos demais, ou totalmente deles se desvia. E então, se o prazer corpóreo for grande, impedirá totalmente o uso da razão, atraindo para si toda a atenção do espírito, ou a impedirá em grande parte <cite>Suma Teológica, Ia IIae, q. 33, art. 3</cite>

E sobre a ira:

>A mente ou razão, embora não dependa, pra o seu ato próprio, de um órgão corpóreo, contudo, como depende, para o mesmo, de certas potências sensitivas, cujos atos ficam impedidos pela perturbação do corpo, necessariamente as perturbações corpóreas hão-de impedir também o juízo da razão, como mui claramente o manifesta a embriaguez e o sono. Pois, como já dissemos, a ira produz perturbação corpórea sobretudo no coração e de modo tal que esta deriva até para os membros exteriores. Por onde, dentre as demais paixões, ela é a que mais manifestamente nos priva do uso da razão. <cite>Suma Teológica, Ia IIae, q. 48, art. 3</cite>

Ora, não é difícil percebermos que os adultos são incapazes de raciocinar e até mesmo se comportam como animais sem inteligência quando estão muitos irados ou quando experimentam um prazer corporal muito intenso, como no caso do ato conjugal.

Portanto, se um adulto perde o controle diante de paixões fortes, muito mais uma criança o fará diante de tais paixões, visto que sua razão é bem mais débil do que a nossa.

Assim, é preciso reconhecer que nesta fase não estamos lidando com uma rebeldia de caso pensando, mas com um verdadeiro surto emocional da criança.

Evidentemente que é possível perceber que em alguns momento as emoções são mais brandas, o que permite que a criança consiga dar ouvido aos pais. Porém, quando a coisa passa dos limites, o que pode até mesmo se prolongar, não adianta bater ou gritar com a criança durante o momento do surto.

Como a criança não dispõe da razão para contêr a ira, o melhor que podemos fazer é retirar ela de um local agitado, caso ela esteja.

Além disso, devemos fazer o possível para não fomentar mais a ira. Para tanto, é recomendável permanecer ao lado da criança enquanto ela tem o surto, para que ela não tenha mais motivo para chorar, e ir falando calmamente com ela e dizendo que ela deve se acalmar, que ela consegue, que deve pedir ajuda para seu anjo da guarda e coisas do tipo.

Ademais, não ser capaz de fazer o outro compreender o motivo da ira só piora a situação. Este é justamente o caso desta fase, pois a criança não consegue se expressar direito em palavras para dizer o que a irrita tanto. Mostre que você compreender perguntando pra ela se determinada coisa é o motivo de ficar tão nervosa e se ela responder você pode aproveitar e tentar dizer-lhe o porque aquilo não pode agora.

Outra coisa que pode ser feita para acalmar a ira é oferecer alguma distração ou coisa para fazer se a criança tiver um grau de consciência suficiente para tal.

O que se deve evitar a todo custo é irar-se junto com a criança e começar a gritar, bater ou coisas do tipo, pois isso vai intensificar mais ainda a ira dela e vai apenas dar para ela o exemplo de que um adulto não sabe fazer aquilo que está exigindo dela.