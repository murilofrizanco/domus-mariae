+++
date = "2014-06-26T06:36:10-03:00"
title = "Como corrigir seus filhos?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/como-corrigir-seus-filhos.jpg"
+++

São João Bosco, grande educador dos jovens, percebeu que haviam basicamente dois modos para corrigir aqueles que estavam sob seus cuidados.

<!--more-->

O primeiro era o que ele chamava de Sistema Repressivo. O segundo, o Sistema Preventivo.

O Sistema Repressivo consiste em fazer com que os filhos conheçam a lei e vigiar para saber se vão transgredir a lei. Quando houver uma infração contra lei paterna, então aplica-se um castigo merecido.

Por exemplo, o pai diz para o filho não colocar o dedo na tomada. Então, passa a vigiá-lo constantemente para atestar se a criança fez ou não aquilo que estava proibido. Se o filho transgredir esta norma, então o pai aplica-lhe algum castigo, como uma palmada ou alguma outra punição.

Vemos com bastante clareza que a maioria dos pais aplica este primeiro sistema hoje em dia. Com efeito, o próprio Dom Bosco reconhece que "Esse sistema é fácil, menos trabalhoso".

Porém, corrigir deste modo causa consequências no filho:

>O Sistema Repressivo pode impedir uma desordem, mas dificilmente melhorará os culpados. Diz a experiência que os jovens não esquecem os castigos recebidos, e geralmente conservam ressentimento acompanhado do desejo de sacudir o jugo e até de tirar vingança. Podem, às vezes, parecer indiferentes; mas quem lhes segue os passos sabe quão terríveis são as reminiscências da juventude.<cite>São João Bosco</cite>

Ademais, esta correção tende a frustrar os pais, pois é desanimador quando faz-se algo sem eficácia. Com efeito, muitos pais terminam desistindo de corrigir os filhos pelo fato de se esgotarem aplicando punições. Assim, acabam abandonando, pouco a pouco, a educação dos filhos por causa da falta de esperança em educá-lo e terminam passivos em relação ao filho com medo de afastá-lo, contrariá-lo e até mesmo chega a defender o filho contra alguém que proponha alguma correção. Com efeito, pensando nisto é que adverte o Apóstolo:

>Pais, não exaspereis vossos filhos. Pelo contrário, criai-os na educação e doutrina do Senhor.<cite>Bíblia de Jerusalém, Epístola aos Efésios, 6, 4</cite>

Contra este pais que desanimaram são dirigidas as seguintes palavras:

>Os pais estão obrigados a instruir seus filhos na prática da virtude, não somente por meio de palavras, mas também com o exemplo.<cite>Santo Afonso de Ligório</cite>

Portanto, sendo o método repressivo ordinariamente um fracasso, faz-se necessário buscar um outro meio que seja eficaz para corrigir os filhos.

Resta, portanto, o outro método, a saber, o Sistema Preventivo, que consiste em:

>Tornar conhecidas as prescrições e as regras de uma instituição, e depois vigiar de modo que os alunos estejam sempre sob os olhares atentos do diretor ou dos assistentes. Estes, como pais carinhosos, falem, sirvam de gula em todas as circunstâncias, dêem conselhos e corrijam com bondade. Consiste, pois, em colocar os alunos na impossibilidade de cometerem faltas. <cite>São João Bosco</cite>

Embora o texto seja uma exortação para professores, pode muito bem ser aplicado para o ofício paterno. Assim, este método consiste em impossibilitar que o filho cometa alguma falta ou transgressão.

Deve-se notar que, para que isto seja possível, faz-se necessário que os pais mantenham uma vigilância constante, uma proximidade permanente e se disponha a oferecer coisas melhores para atrair os filhos para fora do pecado. Ora, um jovem que esteja sendo acompanhado deste modo termina sem oportundade para ocupar-se de coisas torpes e formando laços de proximidade e carinho muito mais sólidos com seus pais.

É evidente que trata-se de um método de lidar com as transgressões. Portanto, faz-se necessário que os pais ensinem sobre as virtudes e os vícios.

Certamente trata-se de um sistema muito mais árduo e trabalhoso. Porém, a correção gera frutos mais eficazes e duradouros mesmo quando o filho for exposto às máximas do mundo.

Que os pais possam educar seus filhos no amor, na caridade, na firmeza de doutrina e no caminho da salvação, pois grande é o prêmio e brilhante é a coroa da glória para os bons pais que formarem santos para a Igreja de Cristo.