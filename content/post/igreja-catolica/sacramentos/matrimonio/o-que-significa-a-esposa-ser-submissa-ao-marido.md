+++
date = "2014-08-13T06:36:10-03:00"
title = "O que significa a esposa ser submissa ao marido?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/o-que-significa-a-esposa-ser-submissa-ao-marido.jpg"
+++

A submissão da esposa ao marido tornou-se símbolo de opressão machista e argumento para o desprezo das Sagradas Escrituras.

<!--more-->

Deve-se notar que, apesar das acusações, foi a própria doutrina cristã que retirou a mulher da condição de objeto e submissão ao homem e lhe abriu as portas para uma autonomia na vida religiosa ou de virgem consagrada que não a obrigava fatalmente a casar-se. Ademais, o paganismo sempre tratou a mulher como objeto, enquanto o cristianismo lhe concedia a dignidade de ser humano.

É verdade que nos Textos Sagrados encontramos passagens que parecem apresentar a ideia de que a mulher é inferior ao homem:

>As mulheres sejam submissas a seus maridos, como ao Senhor, pois o marido é o chefe da mulher, como Cristo é o chefe da Igreja, seu corpo, da qual ele é o Salvador. Ora, assim como a Igreja é submissa a Cristo, assim também o sejam em tudo as mulheres a seus maridos. Maridos, amai as vossas mulheres, como Cristo amou a Igreja e se entregou por ela.<cite>Epístola aos Efésios 5, 22-25</cite>

>Mulheres, sede submissas a vossos maridos, porque assim convém, no Senhor. Maridos, amai as vossas mulheres e não as trateis com aspereza.<cite>Epístola aos Colossenses 3, 18s</cite>

>...a serem prudentes, castas, cuidadosas da casa, bondosas, submissas a seus maridos, para que a palavra de Deus não seja desacreditada. Exorta igualmente os moços a serem morigerados...<cite>Epístola a Tito 2, 5s</cite>

>Vós, também, ó mulheres, sede submissas aos vossos maridos. (...) Do mesmo modo vós, ó maridos, comportai-vos sabiamente no vosso convívio com as vossas mulheres, pois são de um sexo mais fraco. Porquanto elas são herdeiras, com o mesmo direito que vós outros, da graça que dá a vida.<cite>Primeira Epístola de São Pedro 3, 1.7</cite>

Quanto a isto deve-se dizer que o papa São João Paulo II já resolveu a questão negando a possibilidade de interpretar estas passagens como inferioridade da mulher:

>O Autor não pretende dizer que o marido é patrão da mulher e que o pacto interpessoal próprio do matrimônio é um pacto de domínio do marido sobre a mulher. <cite>São João Paulo II, Audiência Geral, 11 de Agosto de 1982</cite>

Alguns dizem que, apesar da jsutificativa do papa, é impossível aceitar que a mulher seja submissa ao marido em uma sociedade que se empenha em promover os direitos da mulher. Contra estes a Igreja já se pronunciou:

>Esta igualdade de direitos, porém, que tanto se exagera e se enaltece, deve reconhecer-se em tudo o que é próprio da pessoa e dignidade humana, e que resulta do pacto nupcial e está na essência do matrimônio; nestas coisas certamente ambos os cônjuges gozam inteiramente do mesmo direito e estão ligados pelo mesmo dever; quanto ao resto, deve existir certa desigualdade e moderação, que o próprio interesse da família e a necessária unidade e firmeza da ordem e da sociedade doméstica requerem. <cite>Papa Pio XI, Casti Connubii, 77</cite>

Assim, a mulher não é um ser inferior ao homem segundo a natureza e a Igreja tem o compromisso de reconhecer e promover seus direitos.

Porém, deve-se reconhecer a necessidade de uma justa e saudável desigualdade dentro do matrimônio. Isto se deve ao fato de que as coisas iguais não se complementam, mas apenas se multiplicam. Ora, se a mulher é completamente igual dentro do matrimônio, então poderíamos desfazermo-nos dela, pois o homem poderia fazer tudo o que ela faz. Por isso, se a mulher não tiver alguma diferença em relação ao homem, será inútil sua presença no matrimônio.

Portanto, trata-se de uma complementariedade que é um grande bem para a família e os filhos, necessária para a ordem doméstica da qual se se segue a própria ordem da sociedade.

Com efeito, seria inútil que na sociedade tivéssemos dois presidentes, dois governadores ou duas autoridades absolutamente iguais em poder. Isto certamente seria fonte de divisões onde provavelmente uma autoridade desfaria o que a outra fez num ciclo vicioso. Ora, se as coisas são assim na sociedade, porque seria diferente na família?

Ademais, deve-se notar que a submissão da mulher aplica-se somente no que diz respeito a relação de marido e esposa. Assim, a mulher casada goza de todos os direitos, permissões e benefícios de que gozam os maridos diante da sociedade civil.

Assim, é ilegítimo que o marido use sua autoridade para impedir que a esposa socorra seus pais nas adversidades, que cumpra seus deveres religiosos, que trabalhe, que tenha participação política, que saia de casa ou qualquer outra atividade, desde que estas não comprometam seus deveres de esposa, os quais aceitou livremente.

Ademais, ao exigir a submissão da mulher também exige que o método pelo qual a mulher deve ser submetida é pelo amor:

>Queres que a mulher te obedeça, conforme a Igreja a Cristo? Tem solicitude por ela bem como Cristo pela Igreja. Mesmo se for preciso dar a vida, até mesmo ser mil vezes ferido, suportar e sofrer seja o que for, não recuses. Nada fizeste ainda à imitação de Cristo. <cite>São João Crisóstomo, Comentário à Epístola aos Efésios, Homilia XX</cite>

Assim, a autoridade do marido é exercida legitimamente quando ele a utiliza para servir a esposa. Portanto, o marido que governa a mulher para o próprio bem, isto é, como uma escrava transgride o mandamento do amor que se deve a esposa:

>É possível conter um servo pelo medo, ou melhor, talvez nem mesmo ele; pode escapar e ir embora. Quanto à partícipe de tua vida, contudo, mãe de teus filhos, causa de toda alegria, não deves contê-la por medo e ameaças, mas por amor e afeição. Qual a união se a mulher tem horror do marido? Que prazer terá o marido, se conviver com a mulher à semelhança de uma escrava, não de uma livre? E se tiveres de sofrer algo por causa dela, não a ultrajes. Cristo não procedeu desta maneira. <cite>São João Crisóstomo, Comentário à Epístola aos Efésios, Homilia XX</cite>

Entendido neste sentido, o marido investido com autoridade é justamente aquele que ficou em último lugar e se faz servo de todos à semelhança de Cristo.

Alguns, observando pagãos e maus cristãos, poderiam perguntar como é possível aplicar isto na prática sem que com isso a mulher perca a liberdade. A estes deve-se dizer:

>Tal sujeição não nega nem tira à mulher a liberdade a que tem pleno direito, quer pela nobreza da personalidade humana, quer pela missão nobilíssima de esposa, mãe e companheira, nem a obriga a condescender com todos os caprichos do homem, quando não conformes à própria razão ou à dignidade da esposa, nem exige enfim que a mulher se equipare às pessoas que se chamam em direito 'menores' [de idade], às quais, por falta de maior madureza de juízo ou por inexperiência das coisas humanas, não se costuma conceder o livre exercício dos seus direitos; mas proíbe essa licença exagerada que despreza o bem da família, proíbe que no corpo desta família se separe o coração da cabeça, com grande detrimento de todo o corpo e perigo próximo de ruína. Se efetivamente o homem é a cabeça, a mulher é o coração; e, se ele tem o primado do governo, também a ela pode e deve atribuir-se como coisa sua o primado do amor. <cite>Papa Pio XI, Casti Connubii, 27</cite>

Assim, ao marido, dotado do "primado do governo", cabe preceituar e ordenar a casa para a virtude e sabedoria, estabelecer regras e prover o necessário, enquanto à esposa, dotada do "primado no amor", cabe aquele poder de unir os membros, tornar amável e doce a convivência familiar e agradável o caminho da virtude. Por isso, dizia Pio XII:

>Se ao vosso marido e ao seu trabalho cabe procurar e estabelecer a vida do lar, a vós e ao vosso cuidado cabe ajustar o conveniente bem-estar e providenciar a pacífica serenidade comum de vossas duas vidas. (...) de vós procedam no coração do marido e dos filhos tantos luminosos raios de sol, que confortem, fomentem e fecundem, também nas horas da separação exterior, a espiritual união do lar. <cite>Papa Pio XII aos recém-casados, 11 de Março de 1942</cite>

Um outro autor manifesta um exemplo prático de como o casal, mantendo o primado de governo do marido, deve tomar as decisões:

>O marido e a mulher constituem uma como que assembléia deliberativa, na qual tem, cada um, voto na matéria a resolver-se. A boa vontade mútua, a preocupação do bem comum, o desinteresse pessoal levam cada um dos esposos a apreciar melhor o valor [das propostas] e opiniões do outro. Entrados em acordo, ao marido compete [supervisionar] a boa execução das decisões. <cite>Abade Jean Viollet, Moral Familiar, pgs 16-17</cite>

Portanto, a autoridade do marido não é opressão, mas acima de tudo serviço e aniquilamento de si mesmo.

Que a Virgem Santíssima ensine aos casais os caminhos da santa submissão das esposas e que São José seja modelo e professor dos maridos para que sejam um outro "Cristo" que se entrega por sua "Igreja" conciliando assim a perfeita ordem da sociedade doméstica.