+++
date = "2014-08-13T06:36:10-03:00"
title = "Os pais são como reis de uma cidade"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/os-pais-sao-como-reis-de-uma-cidade.jpg"
+++

A cultura atual tende a rebaixar a importância dos pais na educação dos filhos colocando a especialistas como principais educadores. Assim, alguns pais chegam a pensar que mais vale o nome de "amigo" do que de "pai" ou "mãe".

<!--more-->

Estas coisas procedem da opinião de que os pais não tem competência para dirigir e educar os filhos. Com efeito, é o diagnóstico oferecido por um grande pensador:

>A educação moderna é baseada no princípio de que o pai ou a mãe têm mais chance de serem cruéis que qualquer outra pessoa. Ora, qualquer um pode ser cruel, mas as maiores chances de crueldade estão nas multidões indiferentes e sem cor dos completos estranhos e dos mercenários mecanicistas, que agora é moda chamar de agentes de melhoria: policiais, médicos, detetives, inspetores, instrutores etc.<cite>G. K. Chesterton, A história da família</cite>

É preciso ter uma concepção clara do que é ser pai e mãe. Ora, a paternidade e maternidade é a consagração de um rei e de uma rainha em uma jovem cidade:

>Imagina seres um rei e que uma cidade esteja submetida a ti, que é a alma da criança; pois a alma é, efetivamente, uma cidade.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Com efeito, ser pai ou mãe é como:

>Ser a Rainha Elizabete dentro de uma área definida, decidindo salários, banquetes, tarefas e feriados; ser Whiteley dentro de certa área, suprindo brinquedos, sapatos, bolos e livros; ser um Aristóteles dentro de certa área, ensinando moral, boas maneiras, teologia e higiene.<cite>G. K. Chesterton, A emancipação da domesticidade, Livro "O que está errado com o mundo", Capítulo III</cite>

Ora, como toda cidade recém fundada deve ser ordenada para a perfeição e será tão mais eficaz este trabalho quanto mais cedo começar:

>Cidade, pois, é a alma do menino, cidade recém fundada, habitada por cidadãos forasteiros que ainda não têm experiência alguma. E esses são justamente os que mais facilmente se educam.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Esta pequena cidade é fundada com a presença de muitos habitantes conflitantes entre si. Com efeito, é conveniente a analogia da cidade, pois as paixões da alma assemelham-se a habitantes que, ora estão em conflitos, ora estão em harmonia:

>Do mesmo modo que em uma cidade uns roubam e outros agem justamente; uns trabalham e outros fazem tudo pelo prazer, assim acontece exatamente na alma com a inteligência e os pensamentos; uns lutam contra os que cometem iniquidade, como fazem na cidade os soldados; outros provêem ao conjunto, ao corpo e à casa, como os que administram a cidade; outros ordenam e mandam, como fazem os governantes; e há também os que narram coisas dissolutas, como fazem os intemperantes; outros, coisas graves, como os castos ou temperantes; há também os afeminados, como mulheres entre nós; outros falam com pouca razão, como fazem os meninos; e uns são mandados como os criados, como se faz com os escravos; outros são nobres, como o são os cidadãos livres.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Por isso, para que a cidade seja levada a sua perfeição deve-se instituir leis que ordenem seu bom funcionamento e a convivência adequada entre todos estes cidadãos:

>Necessitamos, pois, de leis, a fim de desterrar os maus e admitir os bons e não consentir que os maus se revoltem contra os bons. Porque do mesmo modo que se em uma cidade se fizessem leis concedendo total impunidade aos ladrões, se transtornaria tudo; e se os soldados não usassem devidamente da ira, se faria um dano universal, e se cada um deixasse seu posto, se intrometesse no dos outros, com tal intromissão se chegaria a perder toda a disciplina; assim acontece exatamente na alma.<cite>São João Crisóstomo, Da Vanglória e da Educação dos Filhos</cite>

Ora, a instituição das leis de uma cidade cabe somente ao rei, que nesta analogia são os próprios pais. Por isso, somente numa família o ser humano pode ser levado até a virtude e a sabedoria.

Com efeito, os pais são os únicos que terão poder absoluto sobre a cidade para ordená-la por inteiro, enquanto que as demais instituições irão apenas ordenar uma parte sem coerência com o todo. Por isso, dizia o Doutor Angélico à respeito da importância da educação paterna:

>A natureza não visa só a geração dos filhos, mas, a criação deles e a sua educação até o estado de homem perfeito, como tal, que é o estado de homem virtuoso.<cite>Suma Teológica, Supl., q. 41, art. 1</cite>

Que desçam graças abundantes sobre os pais e mães através das mãos maternais da Virgem para torná-los dignos de sua vocação.