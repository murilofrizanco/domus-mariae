+++
date = "2015-09-04T06:36:10-03:00"
title = "Jesus diz que o matrimônio se rompe com a fornicação?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/jesus-diz-que-o-matrimonio-se-rompe-com-a-fornicacao.jpg"
+++

Nosso Senhor Jesus Cristo parece ensinar que é permitido divorciar-se e depois casar-se com outra pessoa no caso de fornicação.

<!--more-->

Com efeito, lemos no Evangelho de Mateus:

>Foi dito: Aquele que repudiar a sua mulher, dê-lhe uma carta de divórcio. Eu, porém, vos digo: todo aquele que repudia sua mulher, a não ser por motivo de 'fornicação', faz com que ela adultere; e aquele que se casa com a repudiada comete adultério. <cite>Bíblia de Jerusalém, Evangelho segundo São Mateus 5, 31-32</cite>

>E eu vos digo que todo aquele que repudiar a sua mulher — exceto por motivo de fornicação — e desposar uma outra, comete adultério. <cite>Bíblia de Jerusalém, Evangelho segundo São Mateus 19, 9</cite>

Com estas palavras, Cristo parece afirmar que a fornicação seria uma "brecha" que permitiria ao cônjuge que não cometeu tal pecado separar-se do outro e casar-se novamente sem pecado de adultério. Deste modo, aquilo que se chama atualmente de "segunda união" estaria de acordo com o Evangelho em determinados casos.

Que casar-se com uma outra pessoa depois de divorciar-se constitui adultério não apenas é claro no próprio texto citado, como também nos demais Evangelhos onde esta questão é tratada por Cristo:

>E, em casa, os discípulos voltaram a interrogá-lo sobre esse ponto. E ele disse: Todo aquele que repudiar a sua mulher e desposar outra, comete adultério contra a primeira; e se essa repudiar o seu marido e desposar outro, comete adultério. <cite>Bíblia de Jerusalém, Evangelho segundo São Marcos 10, 10-12</cite>

>Todo aquele que repudiar sua mulher e desposar outra comete adultério, e quem desposar uma repudiada por seu marido comete adultério. <cite>Bíblia de Jerusalém, Evangelho segundo São Lucas 16, 18</cite>

Se as coisas são assim, o que significa esta exceção acrescentada por Cristo sobre a fornicação?

O grande Doutor Angélico observa uma importante questão sobre o matrimônio:

>Nada de sobreveniente ao matrimônio pode ser causa de sua dissolução. Logo, o adultério não pode anular um casamento verdadeiramente existente. Pois, como diz Agostinho, o vinculo conjugal subsiste entre ambos por toda a vida, nem pode ser rompido pela separação ou pela união com outra pessoa. Portanto, enquanto vive um, não pode o outro passar a segundas núpcias. <cite>Suma Teológica, Supl, q. 62, art. 5</cite>

Assim, nada do que aconteça após um matrimônio válido, mesmo um adultério, pode dissolver o vínculo entre os dois cônjuges. 

Quanto ao texto de Mateus, o mesmo autor deixa bem claro que:

>Essa exceção, fundada nas palavras do Senhor, se refere ao repúdio da esposa. Por onde, a objeção procede de um mau entendimento do texto. <cite>Suma Teológica, Supl, q. 62, art. 5</cite>

Com efeito, ao ler com atenção o primeiro texto (Mt 5) citado vemos que Jesus afirma que o marido que repudia sua mulher, exceto por causa de fornicação, a faz cometer adultério.

Ora, quem se separa de sua mulher ou marido, seja por qualquer razão, expõe o cônjuge a que busque segundas núpcias e deste modo o induz ao pecado do adultério.

Assim, a fornicação não é uma exceção que permite um novo casamento, mas que isenta quem repudia da culpa de induzir o repudiado a este pecado por deixá-lo sozinho, pois neste caso não fará diferença já que o cônjuge já está em pecado de adultério antes mesmo do repúdio.

O segundo texto (Mt 19) parece mais problemático, pois causa a impressão de afirmar categoricamente que o marido pode casar-se com outra quando houver fornicação. Porém, isto procede, novamente, de um entendimento errôneo do texto.

Com efeito, o texto afirma que as duas classes de pessoas - tanto quem repudia o cônjuge, exceto em caso de fornicação, como quem desposa um outro - cometem adultério. No caso da fornicação, o texto está afirmando que o que repudiar não será réu do pecado de induzir o outro ao adultério. Ora, repudiar é diferente de desposar outra pessoa:

>O marido pode repudiar a mulher de dois modos. ─ Primeiro, só quanto ao leito conjugal. E então pode fazê-lo, por juízo próprio, desde que lhe souber da infidelidade. Nem fica obrigado a cumprir para com ela o dever conjugal, quando dele o exigir, salvo se a Igreja lho impuser. E neste último caso, o cumprimento desse dever não lhe causa nenhum dano. ─ De outro modo, quando ao leito conjugal e à coabitação. E então não na pode repudiar senão por juízo da Igreja. E se proceder diferentemente, deve ser compelido à coabitação, salvo se lhe puder provar imediatamente o fato da infidelidade. E esse repúdio se chama divórcio. Donde devemos concluir que este não pode ser decretado senão por juízo da Igreja. <cite>Suma Teológica, Supl, q. 62, art. 3</cite>

Assim, o marido pode repudiar a esposa se recusando ao ato sexual ou então se recusando a coabitar com ela.

Por isso, repudiar não significa dissolver um matrimônio, mas apenas negar o direito do cônjuge às relações sexuais ou a coabitação. O primeiro pode ser feito pelo marido, o segundo está descrito no Código de Direito Canônico.

Portanto, sempre será adúltero aquele que, depois de casado, desposar outra pessoa, tenha sido vítima de adultério ou não. Sobre este ponto no adverte Santo Tomás:

>Embora a mulher, depois do divórcio, não esteja obrigada a cumprir o dever conjugal para com o marido adúltero e a coabitar com ele contudo ainda subsiste o vínculo matrimonial que a obrigava. Portanto não pode contrair outro casamento, durante a vida do marido. <cite>Suma Teológica, Supl, q. 62, art. 5</cite>

Com efeito, Cristo permite o repúdio, mas o repúdio não é o fim do vínculo conjugal que torna a união do casal realmente participante do própria união de Cristo com a Igreja, efeito próprio do Sacramento do Matrimônio.

Por isso, Cristo diz: "O que Deus uniu, o homem não separe". Ora, a união sexual e a coabitação procedem da vontade humana, mas o vínculo sacramental procede diretamente de Deus. Assim, o homem pode romper com a relações sexuais e a coabitação, mas não pode fazê-lo com relação ao vínculo sacramental, pois isto procede de Deus.

Com efeito, nunca encontramos em qualquer lugar das Escrituras que o vínculo do Matrimônio deixa de existir pelo fato dos esposos não mais terem relações sexuais ou coabitarem ou que quem desposa uma outra mulher em união sacramental não comete adultério. Ora, assim como o descumprimento das promessas bastimais não desfaz o sacramento permitindo um segundo batismo, o descumprimento das realidades materiais que procedem da vontade humana não desfaz o sacramento permitindo uma segunda núpcia enquanto o cônjuge viver.

Assim, fica evidente que Cristo jamais afirmou que o repúdio ou divórcio rompem o vínculo do Sacramento abrinda as portas para novas núpcias.