+++
date = "2014-06-30T06:36:10-03:00"
title = "É tão pesado o fardo dos divorciados que não podem constituir nova união?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/e-pesado-o-fardo-dos-divorciados-que-nao-podem-constituir-nova-uniao.jpg"
+++

Com a cultura do divórcio cresce a quantidade de lares desfeito, laços rompidos e esposos entregues cada um a sua sorte. Porém, um problema maior diz respeito aos divorciados católicos: a impossibilidade de contrair nova união conjugal sem incorrer em adultério.

<!--more-->

Com efeito, para os que recebem o Sacramento do Matrimônio não é mais possível contrair um segundo Matrimônio enquanto o conjuge viver, sob pena de cometer adultério.

Alguns afirmam que isto é um fardo cruel imposto aos fiéis pela autoridade da Igreja. Porém, isto é falso, pois esta situação decorre das promessas que o conjuge fez por livre e espontânea vontade diante de Deus. Assim, mesmo que os conjuges se esqueçam das promessas matrimoniais, Deus jamais se esquecerá.

Alguns poderiam argumentar que estas palavras são fáceis de serem ditas, mas impossíveis de serem aplicadas e procedem de uma imcompreensão da tristeza de quem passou pelo divórcio. Contra estes lembra um santo sacerdote:

>Diria a essas mulheres, compreendendo o seu sofrimento, que também podem ver nessa situação a Vontade de Deus, que nunca é cruel, porque Deus é Pai amoroso.<cite>São Josemaria Escrivá</cite>

Ora, quando Cristo viu-se amedrontado pelo preço que teria que pagar para cumprir a promessa que havia feito de oferecer a salvação aos homens a ponto de suar sangue, Ele não desistiu da promessa que havia feito. Assim, percebendo que a Cruz é um caminho de salvação e para deixar o exemplo a ser seguido pelos homens, perseverou até o fim.

Ora, o problema dos divorciados que pensam ser impossível manter sua fidelidade ao conjuge com a separação não é com o Sacramento do Matrimônio, mas sim com a decisão se querem realmente seguir e imitar seu Cristo ou se querem apostatar.

Com efeito, muitos foram os mártires que preferiram a morte do que romper com sua promessa de ser fiel ao Cristo. De fato, João Batista preferiu morrer do que fingir que Herodes não estava ofendendo ao Senhor com sua união ilícita. Se o primo de Cristo agiu assim, porque propõe-se ao conjuges que fujam da imitação de Cristo Crucificado?

A indissolubilidade é parte da perfeição do matrimônio, não um fardo insuportável. Com efeito, o Matrimônio é indissolúvel porque sua finalidade é levar os conjuges a amarem-se mutuamente como amariam a própria humanidade de Cristo. Ora, ninguém pode tributar o amor pessoal devido à humanidade de Cristo a outro sem cair em idolatria, nem os conjuges seu amor para terceiros sem incorrer em adultério.

Por isso, Josemaria lembrava aos casais grande bem que é a indissolubilidade matrimonial:

>A indissolubilidade do matrimônio não é um capricho da Igreja e nem sequer uma mera lei positiva eclesiástica. É de lei natural, de direito divino, e corresponde perfeitamente à nossa natureza e à ordem sobrenatural da graça. Por isso, na imensa maioria dos casos, é condição indispensável de felicidade dos cônjuges, e de segurança, mesmo espiritual, para os filhos.<cite>São Josemaria Escrivá</cite>

Quanto aos que se divorciaram o santo complementa:

>Hão de entender generosamente que essa indissolubilidade, que para elas implica sacrifício, é para a maior parte das famílias uma defesa da sua integridade, algo que enobrece o amor dos esposos e impede o desamparo dos filhos.<cite>São Josemaria Escrivá</cite>

Com isto não se pretende dizer que o estado de divórcio é agradável ou fácil de suportar, mas sim que, já que as coisas são a sim, devemos para de tentar inventar um novo sacramento, deixar a preguiça de progredir na vida espiritual e recorrer ao Cristo que torna o fardo leve e o julgo suave. Com efeito, reconhecemos que:

>É possível que por algum tempo a situação seja especialmente difícil, mas, se recorrerem ao Senhor e à sua Santa Mãe, não lhes faltará a ajuda da graça.<cite>São Josemaria Escrivá</cite>

Ademais, deve-se notar que, entre tantas adversidades, a solidão dos conjuges é insignificante perto muitos problemas que existe:

>A vida de uma mulher nessas condições será realmente mais dura que a de outra mulher maltratada, ou que a vida de quem padece algum dos outros grandes sofrimentos físicos ou morais que a existência traz consigo?<cite>São Josemaria Escrivá</cite>

Ora, muitos são os que suportam e superam o sofrimento que provém do assassinato de um ente querido, de uma doença mortal, de perseguições violentas, da privação do marido por causa da guerra, da extrema pobreza ao unirem-se ao Cristo sofredor. Será mesmo que a solidão do divórcio, fruto da falta de virtude do casal, é um fardo maior do que estas coisas?

Certamente não é e mesmo assim vemos estas pessoas em pior situação acolherem a adversidade e viverem em  santa alegria.

Ora, a infelicidade naõ provém do impedimento da pessoa de conseguir um conjuge, mas:

>O que verdadeiramente torna uma pessoa infeliz – e até uma sociedade inteira – é essa busca ansiosa de bem-estar, o cuidado de eliminar, seja como for, tudo o que nos contrariar. (...) [Cada situação árdua] é uma chamada original de Deus, uma ocasião inédita de trabalhar, de dar o testemunho divino da caridade. A quem sentir a angústia de uma situação difícil, eu aconselharia que procurasse também esquecer-se um pouco dos seus próprios problemas para se preocupar com os problemas dos outros. Fazendo isto, terá mais paz e, sobretudo, santificar-se-á.<cite>São Josemaria Escrivá</cite>

Que possamos nos lembrar que Cristo nos amou tanto que este Amor nos basta e estamos, com a graça de Deus, acima das necessidades dos mundanos. O Amor não reside neste mundo que passa, mas nas delícias e alegrias eternas que estão reservadas aos que se sacrificam na vida presente. Assim, mesmo que aquele que nos prometeu fidelidade nos abandone, jamais devemos abandonar o amor ao Cristo!