+++
date = "2014-06-26T06:36:10-03:00"
title = "O princípio da educação mundana"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/o-principio-da-educacao-mundana.jpg"
+++

Há muitos casais cristãos que são católicos praticantes mas vêem-se entristecidos quando seus filhos terminam seguindo uma vida totalmente mundana sem serem capazes de identificar a causa desta situação.

<!--more-->

Com efeito, sempre ensinaram a virtude e a santidade guiando nos mandamentos do Senhor, porém quado cresceram isto parece ter sido ofuscado pelas máximas do mundo.

Este resultado que surpreende a muitos é, na verdade, exatamente o que se espera quando a educação cristã vai acompanhada lado a lado com o princípio da mentalidade mundana sem que os pais percebam.

Esse princípio destruidor é amplamente tratado por São João Crisóstomo em um de seus escritos sobre a educação das crianças onde aponta que a origem da mundanidade que origina todos os seus males e destrói a vida espiritual é a vanglória.

Porém, antes de prosseguirmos ao ensinamento de Crisóstomo, deve-se ter uma noção inicial do que seria exatamente a vanglória. Para tanto, encontramos esta exposição nos escritos do Doutor Angélico:

>O fim da vanglória é a manifestação da nossa própria excelência <cite>Santo Tomás de Aquino, Suma Teológica, IIa IIae, q. 132, art. 5</cite>

Em suma, a vanglória leva o homem a buscar conduzir sua vida buscando que as pessoas vejam suas excelências. Não é difícil perceber que disto nasce o desejo da prosperidade e a repugnância pela adversidade nesta vida. Assim, a pessoa em que não se extinguiu a vanglória irá colocar seus desejos e esforços nesta vida e irá destruir todo o edifício espiritual construído quando perceber que terá de contrariar estas coisas se quiser unir-se ao Cristo.

Com efeito, esta doença espiritual dilacera a Igreja com uma violência terrível. Seu efeito: a Igreja vai se assemelhando ao mundo.

>Essa vanglória que danifica todo o corpo, que o divide, não obstante sua unidade, em mil pedaços e afasta a caridade! Como uma fera que se lança sobre um corpo nobre e terno e incapaz de defender-se, assim a vanglória cravou seus dentes execráveis e inoculou seu veneno e encheu tudo com seu mau cheiro. Umas partes, depois de despedaçá-las, arrojou ao chão; outras, dilacerou; outras, espremeu entre seus dentes. Se nos fosse dado contemplar com os olhos a vanglória e a Igreja, veríamos um espetáculo lastimável e muito mais espantoso que o dos estádios: o corpo estendido no solo e ela, a vanglória, presidindo desde o alto, dirigindo seu olhar a toda parte, agarrando os que caem, não cedendo um ponto e perdoando jamais. <cite>São João Crisóstomo, Sobre a Vanglória e a Educação dos Filhos</cite>

Deve-se notar que não se trata de um exagero, pois é justamente desta doença que a Igreja que muitos fiéis padecem e se vêem impedido da vida espiritual.

Surge então a pergunta: qual a relação da vanglória e da educação dos filhos?

>A causa de todos estes males é que os meninos são educados desde o princípio na vanglória. <cite>São João Crisóstomo, Sobre a Vanglória e a Educação dos Filhos</cite>

Portanto, toda a corrupção que se segue na educação dos filhos está justamente na exaltação deste princípio que os ensina desde o berço a roubar a glória de Deus. É este princípio que o santo condena, o princípio destruidor da educação.

Que possamos buscar a sabedoria e prudência para reconhecer a vanglória nos atos, pensamentos e palavras pessoais, nas de nossos filhos e parentes, buscando eliminar tudo quanto proceder deste vício. Somente assim educaremos as crianças conforme para contemplar a face de Cristo.