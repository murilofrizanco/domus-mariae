+++
date = "2014-06-27T06:36:10-03:00"
title = "A alma dos filhos revela a dos pais"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/a-alma-dos-filhos-revela-a-dos-pais.jpg"
+++

Um antigo axioma diz que os fiéis estão sempre um degrau abaixo do fervor do seu pároco. Assim, se o pároco for santo, seus fiéis serão piedosos; se o pároco for piedoso, seus fiéis serão modelares [que serve de modelo]; se o pároco for modelar, seus fiéis serão corretos; se o pároco for correto, seus fiéis serão tíbios; se o padre for tíbio, os seus fiéis serão ímpios.

<!--more-->

Ora, a casa é uma verdadeira Igreja doméstica onde os párocos são os pais. A verdadeira paróquia constitui-se como uma família espiritual, assim como a casa. Assim, não seria inconveniente dizer que o fervor dos filhos está sempre um degrau abaixo do fervor de seus pais.

Com efeito, o Doutor Angélico demonstra que:

>É da essência da paternidade e da filiação que o filho, pela geração, tenha a perfeição da natureza existente no pai, como também o pai. <cite>Santo Tomás de Aquino, Suma Teológica, I, q. 42, art. 4</cite>

Numa leitura superficial somos levados a pensar que ilustre doutor está tratando apenas da paternidade em seu sentido biológico. Porém, recolhendo seus escritos podemos entender o que ele exprime nesta breve sentença:

>Não apenas quem transmite a potência à vida é dito pai de quem a recebe, mas também o que comunica um ato vital pode ser chamado de pai. Portanto, quem induz um ato vital, seja uma boa operação, seja um ato do entendimento, da vontade ou do amor, pode ser chamado de pai. (...) Nos atos hierárquicos pelos quais um anjo ilumina, aperfeiçoa e purifica outro, é evidente que aquele anjo é pai deste, assim como o mestre é pai do discípulo. <cite>Santo Tomás de Aquino, Comentários a Epístola aos Efésios, Capítulo 3, Leitura 4</cite>

Assim, fica evidente que o pai não é apenas aquele que induz as características biológicas do filho, mas principalmente aquele que induz o filho no caminho do conhecimento, da virtude e do amor.

Ora, se isto é verdade, a condição espiritual do filho, seus costumes e seus vícios devem ser semelhantes ao de seus pais.

Portanto, de modo geral, será possível avaliar a santidade dos pais através da santidade dos filhos.

Assim, um filho é como uma luz que permite penetrar no mais profundo que existe na alma de seus pais, desvelando seus vícios e virtudes. Com efeito, é como uma janela pela qual os pais podem contemplar a própria alma.

Isto é assim porque as crianças em tenra idade são muito suscetíveis a qualquer influência:

>Disseste que as pérolas, no momento de se colher, são apenas água. Pois bem, se aquele que a colhe é esperto, põe em sua mão aquela gota de água e, fazendo-a girar sobre o vazio da mesma, a torneia de modo perfeito e a torna completamente redonda. E uma vez que tenha tomado uma forma, já não é possível dar-lhe outra. E é assim que, em geral, o tenro, como ainda não recebeu sua própria forma, presta-se a adotar qualquer uma que se lhe dê. Daí a facilidade com que se faz dele o que se quer. <cite>São João Crisóstomo, Sobre a Vanglória e a Educação dos Filhos</cite>

Por isso, a forma que a criança vai recebendo é aquela que os pais vão lhe imprimindo através do ensino e do exemplo à semelhança de um escultor:

>Cada um, pois, de nós, pais e mães, pela maneira que vemos como os pintores e escultores trabalham tão esmeradamente seus quadros e estátuas, assim devemos cuidar destas maravilhosas esculturas, que são os filhos. <cite>São João Crisóstomo, Sobre a Vanglória e a Educação dos Filhos</cite>

Com efeito, tanto quanto os pais buscarem a santidade, o filho o fará; tanto quanto se entregarem aos vícios, o filho o fará. Por isso, os defeitos dos filhos muitas vezes são sinais daquilo que os pais devem emendar ou persistir.

Assim, a alma dos filhos é um desvelar da alma paterna e materna segundo o qual podem santificar-se cada vez mais com uma conhecimento profundo sobre si mesmos.

Deve-se notar que os que tem a graça de possuir muitos filhos costumam ter uma visão mais clara sobre si mesmos mais janelas estão abertas; já os que, com culpa, renegam uma quantidade maior de filhos não tem esta clareza de visão, poucas são as janelas e pouca é a claridade.

O sorriso do filho revela a alegria dos pais; a obediência revela a sabedoria; a admiração revela a excelência; a perseverança revela a coragem; a modéstia revela a pureza e o recato; enfim, a devoção revela a santidade.

Assim, era de se esperar que os pais de uma doutora da Igreja recebessem um elogio que manifestasse a sua excelência:

>O bom Deus me deu um pai e uma mãe mais dignos do céu do que da terra. <cite>Santa Teresinha do Menino Jesus</cite>

Se nossos filhos tem almas envolta em trevas, é porque a nossa alma possui uma luz débil; se a alma é iluminada, é porque uma chama viva arde na alma dos pais e aquece os coraçõezinhos dos filhos.

Ó castíssimo esposo, São José, guiai, junto com sua puríssima esposa, os pais para que possam abrasar seus filhos com um amor sobrenatural que se alastra na família sem poder ser apagado.