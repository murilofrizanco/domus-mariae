+++
date = "2015-10-21T18:00:00-17:00"
title = "Qual a idade certa para casar-se?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/qual-a-idade-certa-para-casar-se.jpg"
+++

A cultura moderna criou uma séria de artifícios e tabus quanto a idade na qual seria conveniente contrair o matrimônio. Atualmente existe um verdadeiro preconceito para com casais que desejam casar-se cedo. Qual seria a posição da Igreja sobre isso?

<!--more-->

Com efeito, vemos na cultura moderna adiar cada vez mais os casamentos sob diversos pretextos. Um estudo, por exemplo, sugere que [as pessoas deveriam casar entre os 28 e 32 anos de idade](http://oglobo.globo.com/sociedade/estudo-sugere-idade-perfeita-para-se-casar-16884567).

Por outro lado, filósofos da antiguidade, como um Aristóteles, postulam idades bem diferentes:

>"O Filósofo determinou o tempo da perfeição da mulher aos dezoito anos ou aproximadamente, e do homem aos trinta e sete anos ou aproximadamente. (...) deve-se considerar que, segundo a intenção do Filósofo, se considerarmos a boa disposição dos que geram e a boa disposição da prole a ser gerada e, por conseqüência, a utilidade comum da cidade ou da região per se será melhor que esta união se realize quando os corpos de ambos estiverem perfeitos, que é o tempo determinado pelo Filósofo ou aproximadamente, na maioria dos casos." <cite>Santo Tomás de Aquino, Comentário sobre a Política, IV, 12</cite>

Embora a idade masculina seja semelhante, vemos um contraste onde o mundo moderno recomenda o casamento para mulheres mais velhas, enquanto que a antiguidade recomenda o casamento para as jovens.

Este contraste não ocorre por acaso. É preciso considerar que houve um grande movimento para, por uma engenharia social, mudar a cultura mundial para que os casamentos ocorresem cada vez mais tardiamente, de modo que assim fosse possível diminuir a população mundial.

É possível constatar isto em um artigo digno de nota de um dos maiores sociólogos da cultura da morte que descreve o modo como as fundações internacionais deveriam fazer isso:

>Outorgar maiores recompensas para os cargos não familiares do que para os familiares ajudaria como meio de se encorajar a limitação da reprodução dentro do matrimônio e também o adiamento do mesmo.

>Um modo muito simples de conseguir isto seria permitir que se atribuíssem vantagens econômicas às pessoas solteiras em contraposição às casadas, e às famílias pequenas em contraposição às grandes.

>O governo poderia pagar, por exemplo, às pessoas que se submetessem à esterilização, poderia pagar também todos os custos do aborto, poderia cobrar uma quantia voluptuosa para uma licença matrimonial, exigir que os casos de gravidez ilícitos fossem abortados.

>Poderiam, por exemplo, deixar de conceder isenções fiscais especiais para os pais de família, reduzir as pensões familiares, revogar as legislações que proíbem o aborto e a esterilização.

>Um método estreitamente relacionado para se retirar a ênfase dada à família seria a modificação da complementaridade dos papéis do homem e da mulher. 

>Atualmente os homens podem participar no mundo mais amplo ao mesmo tempo em que se regozijam com a satisfação de ter diversos filhos porque o cuidado com a casa e com a educação das crianças pesa principalmente sobre as suas esposas. (...) Para modificar esta situação, poderia ser exigido que as mulheres trabalhassem fora de casa ou fazer com que fossem compelidas a isto pelas circunstâncias. <cite>Kingsley Davis, Política Populacional: os programas
atuais terão sucesso?</cite>

Portanto, nota-se que a mentalidade atual quanto a idade do casamento não procede da consideração de uma melhor compleição física da prole visando uma melhor disposição à virtude, como visava Aristóteles, mas sim de um projeto proposital de se enfraquecer a insituição da família para reduzir o número de filhos.

A Igreja, muito justa e prudente neste assunto, considera, mais do que a natureza, a vida espiritual e a maior glória de Deus, sem com isso ingerir indevidamente na liberdade dos casais.

Assim, ela impõe aos uma idade mínima baseado na capacidade de gerar. Assim, tão logo o casal esteja apto à geração, tão logo podem casar-se. Por isso, consta no Direito Canônico:

>O homem antes de dezesseis anos completos de idade e a mulher antes de catorze anos também completos não podem contrair matrimónio válido. <cite>Código Direito Canônico, Cân. 1083, § 1</cite>

Com isto a Igreja está explicitamente dizendo que todos podem casar-se a partir dos 16 anos de idade.

Alguns entendem que os jovens não teriam condições e maturidade suficiente para aceitar o compromisso de um matrimônio ou escolher prudentemente um cônjuge. Porém, contra estes adverte Santo Tomás:

>Em matéria a que a natureza inclina, não é necessário um tão grande desenvolvimento da razão para deliberar, como o é em casos diferentes. Por isso pode, antes da idade legal, suficientemente deliberar e consentir no matrimônio, quem, em matéria de outros contratos, não poderia dirigir bem os seus negócios, sem a assistência de um tutor. <cite>Suma Teológica, Supl., q.58, art. 5</cite>

Com efeito, Santo Tomás, bebendo do conhecimento de grandes autores, demonstra a suficiência do discernimento na idade mínima estipulada pela Igreja:

>Ao cabo do primeiro setênio [7 anos] já começa a ser apto a se comprometer para o futuro, em matéria a que sobretudo a razão natural inclina. Como porém ainda não lhe é firme a vontade, não pode obrigar-se a vínculo perpétuo. Por isso, nessa idade pode contrair esponsais [noivado]. Mas no fim do segundo setênio [14 anos] já pode assumir as obrigações pessoais, de entrar em religião ou contrair matrimônio. E no fim do terceiro setênio pode assumir também outras obrigações. Por isso as leis lhe dão o poder de dispor dos seus bens depois dos vinte e cinco anos. <cite>Suma Teológica, Supl., q.43, art. 2</cite>

Que a Igreja não comete nenhuma irresponsabilidade ao declarar como moralmente lícito o casamento a partir dos 16 anos se demonstra pela experiência de séculos em que as pessoas sempre casaram cedo, como era o caso da Idade Média e como foi até antes do ativismo da Cultura da Morte em mudar estes padrões.

Inclusive, casar nesta idade não é imperfeito do ponto de vista espiritual e nem deveria ser alvo de preconceito, pois temos o exemplo sublime da Santíssima Virgem Maria e do Castíssimo São José que casaram muitíssimo jovens.

Com efeito, na cultura judaica, a menina casava tão logo adquirisse aptidão para gerar. Por isso, estima-se que a Virgem Maria tenha casado entre 12 ou 14 anos. A isto se soma o fato da Tradição nos ensinar que inclusive ela engravidou de Cristo na sua primeiríssima ovulação. São José, por sua vez, estima-se que na ocasião tinha entre 16 e 18 anos.

Portanto, a idade certa para casar-se é, de modo geral, a partir dos 16 anos uma vez que se detenha os meios necessário para para assumir uma família, o qual certamente inclui uma renda própria, mas que não exige um rendimento supérfluo exigido pelo mundo ou sequer o sustento de muitas coisas súpérfluas; exige uma moradia, mas nada impede que um casal more de aluguel ou até mesmo na casa dos pais.

Tudo isto é muito coerente com a missão do matrimônio enquanto sacramento, segundo o qual busca-se gerar o maior número de santos possível. Assim, quanto mais cedo se casa, mais filhos pode-se gerar e educar para a virtude e a sabedoria rendendo maior glória ao Pai Onipotente e a Nosso Senhor Jesus Cristo pelos séculos dos séculos, amém.