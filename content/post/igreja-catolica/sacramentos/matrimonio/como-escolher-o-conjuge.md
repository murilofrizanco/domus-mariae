+++
date = "2015-10-23T18:00:00-17:00"
title = "Como escolher o cônjuge?"
categories = ["Igreja Catolica","Sacramentos","Matrimonio"]
path = "Sacramento do Matrimônio"
featured_image = "posts/images/como-escolher-o-conjuge.jpg"
+++

Alguns pensam nas riquezas, outros nas honras, outros na boa fama e outros na aparência. Mas o que realmente importa na escolha do cônjuge?

<!--more-->

Para se resolver esta questão deve-se notar que o fato do Matrimônio ser um sacramento significa que ele tem como finalidade a maior glória de Deus e a nossa santificação.

Que a finalidade principal do matrimônio seja a santificação dos cônjuges, e por consequência a dos filhos, é algo evidente pelo que foi dito. Por dizia um homem muito sábio:

>Os casados devem ter bem presente que Deus os chamou para esse estado do Matrimônio, para que se auxiliem mutuamente, continuadamente, com orações, exortações e bons exemplos na santificação um do outro. <cite>Santo Antônio Maria Claret, Conselhos Muito Úteis para os Pais de Família</cite>

Ora, este amor sobrenatural é operado quando um dos cônjuges ama o outro como se aquela humanidade com quem casou fosse a própria humanidade de Cristo.

Considerando isto, podemos analisar algumas qualidades que determinam se alguém seria um cônjuge recomendável.

##### A escolha exige a cooperação de nossa razão e vontade

Alguns tem a tendência a delegar para Deus a escolha do cônjuge. Embora Deus certamente seja o principal nessa escolha, Ele não a faz sem a intervenção da nossa vontade.

Isto significa que não dianta passarmos anos em oração pedindo o noivo ou a noiva se não movemos um único dedo para procurar conhecer ou conversar com uma pessoa que seria um bom candidato ao matrimônio.

Devemos seguir a vontade de Deus, mas também devemos lembrar que faz parte desta vontade que façamos uma escolha baseada na razão. Com efeito, diz um grande defensor do Matrimônio:

>Quando te decidires por uma eposa, não corre atrás de ajuda humana. Volta-te a Deus, pois Ele não se envergonhará de ser vosso casamenteiro. Foi Ele mesmo quem prometeu: Buscai primeiro o Reino de Deus, e todas estas coisas vos serão acrescentadas (Mateus 6:33). Não te perguntes: “Como posso ver a Deus? Afinal, Ele não falará nem conversará comigo de maneira explícita, e portanto não conseguirei Lhe fazer perguntas”. Estas são palavras de uma alma de pouca fé. Deus pode facilmente organizar tudo da maneira que Ele quiser, sem o uso da voz.

Alguns tendem a dizer que não se escolhe a quem se ama como futuro esposo afirmando que esta escolhe acontece por acaso por via da emoção, da "química" e afins.

Nada mais falso! Escolher com quem casaremos é algo que vai trazer consequências para o resto de nossas vidas, para nossa vida de oração e santidade e também para a santificação dos futuros filhos. Por isso, devemos proceder racionalmente nesta escolha, como o fazemos com coisas muito menos importante como o dinheiro:

>Não é mesmo uma tolice? Quando estamos sob ameaça de perder dinheiro, tomamos todos os cuidados possíveis, mas quando nossa alma está sob risco de ser eternamente punida, nem ao menos prestamos atenção.

>Tu sabes que tem duas escolhas. Se tu escolheres uma má esposa, terás de enfrentar aborrecimentos. Se não aceitares enfrentá-los, serás culpado de adultério por divorciar-te dela. Se tivesses investigado as leis do Senhor e as conhecesse bem antes de te casares, terias tomado muito cuidado e escolhido uma esposa decente e compatível com teu caráter desde o início. Se tivesses te casado com uma esposa assim, terias ganhado não apenas o benefício de não te divorciares dela como o benefício de amá-la intensamente, conforme Paulo ordenou. <cite>São João Crisóstomo, Homília "Como escolher uma esposa"</cite>

##### A beleza do cônjuge é a virtude

Não devemos ter como principal a beleza do corpo, mas antes desprezá-la para que não sejamos levados a um juízo cego sobre as qualidades da pessoa. Com efeito, a beleza do corpo sema beleza da alma se torna insuportável:

>A beleza do corpo, se não estiver aliada à virtuda da alma, será capaz de atrair o marido somente por uns vinte ou trinta dias, mas não conseguirá ir além disto antes que a perversidade da esposa destrua toda sua atratividade. <cite>São João Crisóstomo, Homília "Como escolher uma esposa"</cite>

De tudo quanto foi dito sobre a finalidade do matrimônio se depreende claramente que a principal qualidade, que é indispensável e que tornará o matrimônio conveniente é justamente a virtude, que é a beleza da alma:

>Eis o que tu deves buscar em uma esposa: virtude de alma e nobreza de caráter, para que desfrutes de tranquilidade, para que luxuries em harmonia e amor duradouro.

>Quanto àquelas que irradiam beleza de alma, quanto mais o tempo passsa e sua nobreza se evidencia, tanto mais aquecido será o amor do marido e tanto mais ele sentirá afeição por ela. <cite>São João Crisóstomo, Homília "Como escolher uma esposa"</cite>

##### As condições financeiras são desprezíveis

Outra coisa que deve ser expressada é uma condenação à mentalidade atual de pensar que o casamento só é conveniente quando o casal possui determinados padrões financeiros. A virtude basta.

Alguns poderia dizer isso seria loucura. Porém, estes se esquecem que um casal virtuoso gasta muito menos com coisas supérfluas e desnecessárias e que um casal virtuoso vai conseguir meios de subsistência muito mais facilmente, com mais frutos e alcançar melhores condições facilmente pelo fato de que a sabedoria lhe tornará possível ordenar suas atividades melhor que os demais.

Ademais, os homens sábios, percebendo as perturbações que se instalam no coração dos homens por causa das riquezas, sempre aconselharam:

>Prefira ser quais as mais pobres do que ela própria, considere quantas jovens nobres e de estirpe nobre, não somente nada receberam do marido, mas também lhes doaram bens, que foram todos esbanjados. Pense nos perigos que daí se originam e abrace a vida isenta destas questões.

>Ensine-lhe não ser a pobreza mal algum; ensine não só através de palavras, mas também de obras; ensine a desprezar a glória.

>Podia escolher esposa rica e opulenta, mas não cogitei disso. Por quê? Não foi em vão, inutilmente. Sabia com razão que as riquezas não constituem posses, mas são desprezíveis. Possuem-nas os ladrões, as meretrizes e os violadores de sepulcros. 

>Se procuramos os bens incorruptíveis, os corruptíveis igualmente afluirão. <cite>São João Crisóstomo, Comentários à Epístola aos Efésios, Homilia XX</cite>

Por isso,

>Assim sendo, deixemos de lado as riquezas da esposa, mas examinenos seu caráter e sua piedade e recato. A esposa recatada, gentil e moderada, mesmo que seja pobre, irá transformar a pobreza em algo muito melhor do que a riqueza.

>É por meio do recato que o marido conseguirá atrair à sua família a boa vontade e a proteção de Deus. É assim que os homens de bem dos velhos tempos se casavam: buscando nobreza de alma em vez de riqueza monetária. <cite>São João Crisóstomo, Homília "Como escolher uma esposa"</cite>

##### É preferível a esposa que se dedique ao lar e aos filhos

Boa parte das mulheres foram coagidas por promessas fúteis e ilusórias a priorizarem a carreira ou a dinheiro de modo que busquem um casamento no qual o cuidado da casa e dos filhos seja secundário.

A quantidade de males que resultam desta conduta é numerosa. Entre as piores vemos a falta de desejos de gerar mais filhos para Deus, a busca pelo aborto, o desprezo e desrespeito pelo marido e a usurpação da educação paterna pelo Estado.

Evidentemente que a Igreja não proíbe que uma esposa trabalhe, mas sempre lembra que seria imoral deixar em segundo plano sua família.

Evidemente que os filhos nascidos em uma família que possuem uma esposa dedicada exclusivamente à família fornecem um ambiente muito mais propício para a santidade e para uma educação cristã. É certamente impossível aplicar o sistema educacional preventivo de Dom Bosco sem uma esposa em casa.

Por isso, o grande paladino do matrimônio nos ensinava muito acertadamente:

>De maneira geral, a vida é composta de duas esferas de atividade: a pública e a privada. Quando Deus a diviviu assim, Ele designou a administração da vida doméstica à mulher, mas ao homem designou todas as tarefas relativas à cidade, às questões comerciais, judiciais, políticas, militares e assim por diante. [...] De fato, o que quer que o marido pense sobre questões domésticas, a esposa o saberá melhor que ele. Ela é incapaz de administrar as questões públicas competentemente, mas ela é capaz de cuidar bem dos filhos, que é o maior dos tesouros. [...] Se Deus tivesse dotado o homem para administrar ambas as esferas de atividade, teria sido fácil aos homens dispensar o gênero feminino. [...] Por isso Deus não concedeu ambas as esferas a um sexo, para que nenhum deles pareça supérfluo. Mas Deus não designou ambas as esferas igualmente a cada sexo, para que a igualdade de honra não engendre rixas e conflitos. Deus preservou a paz reservando a cada um sua esfera adequada. <cite>São João Crisóstomo, Homília "Como escolher uma esposa"</cite>

Estes são os principais pontos que qualificam alguém como um cônjuge ideal, adequado e conveniente para constituir uma família espiritual através do sacramento do matrimônio para que Deus seja glorificado e o casal santificado.





